using System;
using System.Xml;
using System.Configuration;
using System.Web;
using System.IO;
using Pedidos_Mobile.App_Code.Tools;

namespace Nextel.Utils
{
    public class BaseLN
    {
        public static void registrarLog(String xml)
        {
            string LOG_ACTIVO = ConfigurationManager.AppSettings["LOG_ACTIVO"];

            if (LOG_ACTIVO == "1")
            {
                string ruta = ConfigurationManager.AppSettings["rutaLog"];
                //Log.Write(HttpContext.Current.Server.MapPath("~") + ruta, xml);
                new LogHelper().Debug("BaseLN -> registrarLog", xml);
            }
        }

        public static String recuperarVersioSuite()
        {
            string salida = String.Empty;
            string ubicacion_padre = "";
            string nombre = "";

            try
            {
                string sFilename = HttpContext.Current.Server.MapPath("~");
                ubicacion_padre = ConfigurationManager.AppSettings["ubicacion_version"];
                string[] niveles = ubicacion_padre.Split('/');
                if (niveles.Length == 0)
                {
                    throw new Exception("");
                }
                else
                {
                    nombre = niveles[niveles.Length - 1];
                    for (int i = 0; i < niveles.Length - 1; i++)
                    {
                        if (niveles[i].Equals(".."))
                        {
                            sFilename = Path.GetDirectoryName(sFilename);
                        }
                    }
                }
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(sFilename + "/" + nombre);
                XmlElement rootElem = xmlDoc.DocumentElement; //Gets the root element, in your xml its "Root"
                if (rootElem != null)
                {
                    return rootElem.Attributes["suite"].Value.ToString();
                }
                else
                {
                    throw new Exception("no tiene contenido xml v�lido");
                }
            }
            catch (Exception e)
            {
                return "Problemas al cargar versiones, ubicacion_padre='" + ubicacion_padre + "': " + e.Message;
            }
        }
    }
}
