using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Collections.Generic;
using NextlibServer.Utils;

namespace Java.Configuracion
{
    public class IdiomaCultura
    {
        #region Etiquetas idioma
        public const string TIPO_JAVA = "J";
        public const string TIPO_WEB = "W";

        public static String CULTURE = "IdiomaCultura_CULTURA";
        public static String WEB_TITULO = "WEB_TITULO";
        public static String WEB_BUSCAR_LUPA = "WEB_BUSCAR_LUPA";
        public static String WEB_TODOS = "WEB_TODOS";
        public static String WEB_BUSCAR = "WEB_BUSCAR";
        public static String WEB_GRABAR = "WEB_GRABAR";
        public static String WEB_CANCELAR = "WEB_CANCELAR";
        public static String WEB_VOLVER = "WEB_VOLVER";
        public static String WEB_FECHA = "WEB_FECHA";

        public static String WEB_FECHA_VENCIMIENTO_CANJE = "WEB_FECHA_VENCIMIENTO_CANJE";

        public static String WEB_FECHAINICIO = "WEB_FECHAINICIO";
        public static String WEB_FECHAFIN = "WEB_FECHAFIN";
        public static String WEB_SI = "WEB_SI";
        public static String WEB_NO = "WEB_NO";
        public static String WEB_ELIMINAR_TODO = "WEB_ELIMINAR_TODO";
        public static String WEB_ELIMINAR_TODO_DATOS = "WEB_ELIMINAR_TODO_DATOS";
        public static String WEB_RESTAURAR_TODO_DATOS = "WEB_RESTAURAR_TODO_DATOS";
        public static String WEB_RESTAURAR_TODO = "WEB_RESTAURAR_TODO";
        public static String WEB_CREAR = "WEB_CREAR";
        public static String WEB_PROGRAMACION = "WEB_PROGRAMACION";
        public static String WEB_ORDEN = "WEB_ORDEN";
        public static String WEB_DURACION = "WEB_DURACION";
        public static String WEB_VER = "WEB_VER";
        public static String WEB_ERROR_CARACTERES_ESPECIALES = "WEB_ERROR_CARACTERES_ESPECIALES";
        public static String WEB_SATELITE = "WEB_SATELITE";
        public static String WEB_NO_INFORMACION = "WEB_NO_INFORMACION";
        public static String WEB_NO_UBICO = "WEB_NO_UBICO";
        public static String WEB_FACTURA = "WEB_FACTURA";
        public static String WEB_BOLETA = "WEB_BOLETA";
        public static String WEB_MONITOREO = "WEB_MONITOREO";
        public static String WEB_DETALLE_COBRANZA = "WEB_DETALLE_COBRANZA";
        public static String WEB_DETALLE_GENERAL = "WEB_DETALLE_GENERAL";
        public static String WEB_SIN_DATOS = "WEB_SIN_DATOS";

        #region equipo
        public static String WEB_EQUIPO = "WEB_EQUIPO";
        public static String WEB_INFORMACION = "WEB_INFORMACION";
        public static String WEB_BATERIA = "WEB_BATERIA";
        public static String WEB_SENAL = "WEB_SENAL";
        public static String WEB_MODELO = "WEB_MODELO";
        public static String WEB_LATITUD = "WEB_LATITUD";
        public static String WEB_LONGITUD = "WEB_LONGITUD";
        public static String WEB_TELEFONO = "WEB_TELEFONO";
        public static String WEB_GPS = "WEB_GPS";
        public static String WEB_REPORTE_EQUIPO = "WEB_REPORTE_EQUIPO";
        public static String WEB_ULT_FEC_REGISTRO = "WEB_ULT_FEC_REGISTRO";
        public static String WEB_ULT_POS_EQUIPO = "WEB_ULT_POS_EQUIPO";
        public static String WEB_NEXTEL = "WEB_NEXTEL";
        public static String WEB_PAGINACION = "WEB_PAGINACION";
        public static String WEB_SELE_ICONO_G = "WEB_SELE_ICONO_G";
        public static String WEB_DETALLEICONO = "WEB_DETALLEICONO";
        #endregion

        #region LOGIN
        public static string WEB_LOGOEMPRESA = "WEB_LOGOEMPRESA";
        public static string WEB_BIENVENIDO_H2 = "WEB_BIENVENIDO_H2";
        public static string WEB_BIENVENIDO_P = "WEB_BIENVENIDO_P";

        public static string WEB_LOGIN_ERROR = "WEB_LOGIN_ERROR"; // nuevos
        public static string WEB_DISTRIBUIDORA = "WEB_DISTRIBUIDORA"; // nuevos

        public static string WEB_USUARIO_ALERTA = "WEB_USUARIO_ALERTA";
        public static string WEB_CLAVE = "WEB_CLAVE";
        public static string WEB_CLAVE_ALERTA = "WEB_CLAVE_ALERTA";
        public static string WEB_ERROR = "WEB_ERROR";
        public static string WEB_PIE_P = "WEB_PIE_P";
        public static string WEB_PIE_BR = "WEB_PIE_BR";
        public static string WEB_INGRESAR = "WEB_INGRESAR";
        #endregion;

        #region MASTERPAGE
        // /*- nuevos
        public static string WEB_NAV_MANTENIMIENTO = "WEB_NAV_MANTENIMIENTO";
        public static string WEB_NAV_CLIENTES = "WEB_NAV_CLIENTES";
        public static string WEB_NAV_RUTAS = "WEB_NAV_RUTAS";
        public static string WEB_NAV_USUARIOS = "WEB_NAV_USUARIOS";
        public static string WEB_NAV_PRODUCTOS = "WEB_NAV_PRODUCTOS";
        public static string WEB_NAV_LISTAPRECIOS = "WEB_NAV_LISTAPRECIOS";
        public static string WEB_NAV_CONFIGURACION = "WEB_NAV_CONFIGURACION";
        public static string WEB_NAV_REPORTES = "WEB_NAV_REPORTES";
        public static string WEB_NAV_PEDIDO = "WEB_NAV_PEDIDO";
        public static string WEB_NAV_NOPEDIDO = "WEB_NAV_NOPEDIDO";
        public static string WEB_NAV_DEVOLUCION = "WEB_NAV_DEVOLUCION";
        public static string WEB_NAV_CANJE = "WEB_NAV_CANJE";
        public static string WEB_NAV_COBRANZA = "WEB_NAV_COBRANZA";
        public static string WEB_NAV_PEDIDOTOTAL = "WEB_NAV_PEDIDOTOTAL";
        public static string WEB_NAV_GPSRUTA = "WEB_NAV_GPSRUTA";
        public static string WEB_NAV_CARGADESCARGA = "WEB_NAV_CARGADESCARGA";
        public static string WEB_NAV_CARGA = "WEB_NAV_CARGA";
        public static string WEB_NAV_DESCARGA = "WEB_NAV_DESCARGA";
        public static string WEB_NAV_CARGA_TAB = "WEB_NAV_CARGA_TAB";
        public static string WEB_NAV_GENERAL = "WEB_NAV_GENERAL";

        // -*/ nuevos

        public static string WEB_INICIO = "WEB_INICIO";
        public static string WEB_AYUDA = "WEB_AYUDA";
        public static string WEB_SALIR = "WEB_SALIR";
        public static string WEB_USUARIO = "WEB_USUARIO";
        public static string WEB_PERFIL = "WEB_PERFIL";
        public static string WEB_CONFIGURAR = "WEB_CONFIGURAR";
        public static string WEB_CARGA = "WEB_CARGA";

        public static string WEB_ROL = "WEB_ROL";
        public static string WEB_MANTENIMIENTOROL = "WEB_MANTENIMIENTOROL";
        public static string WEB_REALIZADO = "WEB_REALIZADO";
        public static string WEB_ULTIMO_REGISTRO = "WEB_ULTIMO_REGISTRO";
        public static string WEB_TIEMPO_INACTIVO = "WEB_TIEMPO_INACTIVO";


        public static string WEB_NAV_GEOANALISIS = "WEB_NAV_GEOANALISIS";
        public static string WEB_GEOANALISIS_DETGEOANALISIS = "WEB_GEOANALISIS_DETGEOANALISIS";


        public static string WEB_MAPA_GEOANALISIS = "WEB_MAPA_GEOANALISIS";

        public static string WEB_MAPA_GEOANALISIS_DATO = "WEB_MAPA_GEOANALISIS_DATO";


        public static string WEB_MAPA_GRAFICOS = "WEB_MAPA_GRAFICOS";
        public static string WEB_NAV_GRAFICOS = "WEB_NAV_GRAFICOS";
        public static string WEB_GRAFICOS_DETGRAFICOS = "WEB_GRAFICOS_DETGRAFICOS";
        #endregion;

        #region HOME
        public static string WEB_HOME_ASPX_BIENVENIDO = "WEB_HOME_ASPX_BIENVENIDO";
        public static string WEB_HOME_ASPX_MENSAJE = "WEB_HOME_ASPX_MENSAJE";
        public static string WEB_HOME_ASPX_IR = "WEB_HOME_ASPX_IR";
        //aMendiola 02/11/2010 -->
        public static string WEB_MANUAL_IT = "WEB_MANUAL_IT";
        public static string WEB_EJEMPLO_XLS = "WEB_EJEMPLO_XLS";
        public static string WEB_RUTA_MANUAL = "WEB_RUTA_MANUAL";
        public static string WEB_RUTA_MANUAL_IT = "WEB_RUTA_MANUAL_IT";
        public static string WEB_GEN_ARCHIVOS = "WEB_GEN_ARCHIVOS";
        public static string WEB_SERVICIO_WINDOWS = "WEB_SERVICIO_WINDOWS";
        #endregion

        #region mantenimiento

        public static string WEB_CLI_DESHAB = "WEB_CLI_DESHAB";
        public static string WEB_ABRIRBUSCADOR = "WEB_ABRIRBUSCADOR";
        public static string WEB_TIPO = "WEB_TIPO";
        public static string WEB_TIPO_CANAL = "WEB_TIPO_CANAL";
        public static string WEB_SOLO_BANCO = "WEB_SOLO_BANCO";

        public static string WEB_GIRO = "WEB_GIRO";
        public static string WEB_EMPRESA = "WEB_EMPRESA";
        public static string WEB_CONDICION = "WEB_CONDICION";
        public static string WEB_MONTO = "WEB_MONTO";
        public static string WEB_DETALLE = "WEB_DETALLE";
        public static string WEB_MOTIVO = "WEB_MOTIVO";
        public static string WEB_MOTIVO_VALOR = "WEB_MOTIVO_VALOR";
        public static string WEB_CANTIDAD = "WEB_CANTIDAD";
        public static string WEB_DESCPORCENTAJE = "WEB_DESCPORCENTAJE";
        public static string WEB_GEORUTA = "WEB_GEORUTA";
        public static string WEB_BUSQUEDAGEORUTA = "WEB_BUSQUEDAGEORUTA";
        public static string WEB_BUSCADOR = "WEB_BUSCADOR";
        public static string WEB_VENDEDOR = "WEB_VENDEDOR";
        public static string WEB_FECHAVISITA = "WEB_FECHAVISITA";
        public static string WEB_NUEVOCLIENTE = "WEB_NUEVOCLIENTE";
        public static string WEB_NUEVARUTA = "WEB_NUEVARUTA";
        public static string WEB_NUEVOPRODUCTO = "WEB_NUEVOPRODUCTO";
        public static string WEB_NUEVOSUARIO = "WEB_NUEVOSUARIO";
        public static string WEB_NUEVOGENERAL = "WEB_NUEVOGENERAL";
        public static string WEB_LOGIN = "WEB_LOGIN";

        public static string WEB_REPORTEPEDIDOS = "WEB_REPORTEPEDIDOS";
        public static string WEB_REPORTENOPEDIDOS = "WEB_REPORTENOPEDIDOS";
        public static string WEB_REPORTEDEVOLUCION = "WEB_REPORTEDEVOLUCION";
        public static string WEB_REPORTECANJE = "WEB_REPORTECANJE";
        public static string WEB_REPORTEPEDIDOTOTAL = "WEB_REPORTEPEDIDOTOTAL";
        public static string WEB_MODULOCARGADESCARGA = "WEB_MODULOCARGADESCARGA";
        public static string WEB_MENSAJE_GUIA = "WEB_MENSAJE_GUIA";
        public static string WEB_CARGADETALLE = "WEB_CARGADETALLE";
        public static string WEB_DESCARGADETALLE = "WEB_DESCARGADETALLE";
        public static string WEB_GENERARREPORTE = "WEB_GENERARREPORTE";
        //public static string WEB_EXCEL = "WEB_EXCEL";
        public static string WEB_GRAFICO = "WEB_GRAFICO";
        public static String WEB_OBTENER = "WEB_OBTENER";
        public static String WEB_DESCARGAR_INFORMACION = "WEB_DESCARGAR_INFORMACION";


        public static string WEB_SELECCIONE_ARCHIVO = "WEB_SELECCIONE_ARCHIVO";
        public static string WEB_UBIQUE_ARCHIVO = "WEB_UBIQUE_ARCHIVO";
        public static string WEB_CARGAR_ARCHIVOS = "WEB_CARGAR_ARCHIVOS";
        public static string WEB_MANTENIMIENTORUTAS = "WEB_MANTENIMIENTORUTAS";
        public static string WEB_MANTENIMIENTOPRODUCTOS = "WEB_MANTENIMIENTOPRODUCTOS";
        public static string WEB_MANTENIMIENTOUSUARIO = "WEB_MANTENIMIENTOUSUARIO";
        public static string WEB_MANTENIMIENTOCLIENTES = "WEB_MANTENIMIENTOCLIENTES";
        public static string WEB_MANTENIMIENTO_CONFIGURACION = "WEB_MANTENIMIENTO_CONFIGURACION";
        public static string WEB_MENSAJEOBLIGATORIO = "WEB_MENSAJEOBLIGATORIO";
        public static string WEB_REGISTROS = "WEB_REGISTROS";
        public static string WEB_HASTA = "WEB_HASTA";
        public static string WEB_HABILITADO = "WEB_HABILITADO";
        public static string WEB_NUEVO = "WEB_NUEVO";
        public static string WEB_ELIMINAR = "WEB_ELIMINAR";
        public static string WEB_ELIMINAR_REGISTRO = "WEB_ELIMINAR_REGISTRO";
        public static string WEB_RESTAURAR = "WEB_RESTAURAR";
        public static string WEB_EJECUTAR_ARCHIVOS = "WEB_EJECUTAR_ARCHIVOS";
        public static string WEB_EJECUTAR = "WEB_EJECUTAR";
        //public static string WEB_GPS = "WEB_GPS";
        public static string WEB_EDITAR = "WEB_EDITAR";
        public static string WEB_DIRECCION = "WEB_DIRECCION";
        public static string WEB_DESDE = "WEB_DESDE";
        public static string WEB_NOMBRE = "WEB_NOMBRE";
        public static string WEB_NOMBRE_CORTO = "WEB_NOMBRE_CORTO";
        public static string WEB_CODIGO = "WEB_CODIGO";
        public static string WEB_OBSERVACION = "WEB_OBSERVACION";
        public static string WEB_CLIENTE = "WEB_CLIENTE";

        public static string WEB_INGRESECONCEPTO = "WEB_INGRESECONCEPTO";
        public static string WEB_INGRESEVALOR = "WEB_INGRESEVALOR";
        public static string WEB_ELIJAVALOR = "WEB_ELIJAVALOR";

        public static string WEB_EDITARRUTA = "WEB_EDITARRUTA";
        public static string WEB_EDITARCLIENTE = "WEB_EDITARCLIENTE";
        public static string WEB_EDITARPRODUCTO = "WEB_EDITARPRODUCTO";
        public static string WEB_EDITARLISTAPRECIO = "WEB_EDITARLISTAPRECIO";
        public static string WEB_EDITARUSUARIO = "WEB_EDITARUSUARIO";
        public static string WEB_MENSAJEDATOSGRABADOS = "WEB_MENSAJEDATOSGRABADOS";
        public static string WEB_MENSAJECODIGOYAREGISTRADO = "WEB_MENSAJECODIGOYAREGISTRADO";
        public static string WEB_MENSAJEDATOSINCORRECTOS = "WEB_MENSAJEDATOSINCORRECTOS";

        public static string WEB_MENSAJEFECHASINCORRECTOS = "WEB_MENSAJEFECHASINCORRECTOS";
        public static string WEB_MENSAJEDATOYAREGISTRADO = "WEB_MENSAJEDATOYAREGISTRADO";
        public static string WEB_MENSAJELOGINYAREGISTRADO = "WEB_MENSAJELOGINYAREGISTRADO";
        public static string WEB_MENSAJEDATOOBLIGATORIOSEL = "WEB_MENSAJEDATOOBLIGATORIOSEL";
        public static string WEB_MENSAJEDATOOBLIGATORIO = "WEB_MENSAJEDATOOBLIGATORIO";

        public static string WEB_MENSAJENOEXISTENDATOS = "WEB_MENSAJENOEXISTENDATOS";



        public static string WEB_NUEVOCLIENTEGUARDADO = "WEB_NUEVOCLIENTEGUARDADO";
        public static string WEB_NUEVARUTAGUARDADO = "WEB_NUEVARUTAGUARDADO";
        public static string WEB_NUEVOPRODUCTOGUARDADO = "WEB_NUEVOPRODUCTOGUARDADO";
        public static string WEB_NUEVOLISTAPRECIOGUARDADO = "WEB_NUEVOLISTAPRECIOGUARDADO";
        public static string WEB_NUEVOUSUARIOGUARDADO = "WEB_NUEVOUSUARIOGUARDADO";
        public static string WEB_NUEVOGENERALGUARDADO = "WEB_NUEVOGENERALGUARDADO";
        public static string WEB_CERRAR = "WEB_CERRAR";
        //public static string WEB_DESCARGAR_INFORMACION = "WEB_DESCARGAR_INFORMACION";

        public static string WEB_MOSTRAR = "WEB_MOSTRAR";
        public static string WEB_SELECCIONE = "WEB_SELECCIONE";
        public static string WEB_MENSAJEERRORBD = "WEB_MENSAJEERRORBD";
        public static string WEB_MENSAJEREPITEASIG = "WEB_MENSAJEREPITEASIG";

        public static string WEB_RUTA = "WEB_RUTA";

        public static string WEB_USUARIOS_DESHA = "WEB_USUARIOS_DESHA";

        public static string WEB_PRODUCTO = "WEB_PRODUCTO";
        public static string WEB_CANAL = "WEB_CANAL";
        public static string WEB_LISTAPRECIO = "WEB_LISTAPRECIO";
        public static string WEB_STOCK = "WEB_STOCK";

        public static string WEB_PRECIOBASE = "WEB_PRECIOBASE";
        public static string WEB_UNIDADDEFECTO = "WEB_UNIDADDEFECTO";
        public static string WEB_UNIDADMEDIDA = "WEB_UNIDADMEDIDA";

        public static string WEB_PRODUCTOS_DESHA = "WEB_PRODUCTOS_DESHA";
        //public static string WEB_PRODUCTOS_DESHA = "WEB_LISTAPRECIO_DESHA";
        public static string WEB_PRECIO = "WEB_PRECIO";
        public static string WEB_PRECIO_POR = "WEB_PRECIO_POR";
        public static string WEB_PRECIO_EDITABLE = "WEB_PRECIO_EDITABLE";
        public static string WEB_TABLA = "WEB_TABLA";
        public static string WEB_ERRORGRABAR = "WEB_ERRORGRABAR";
        public static string WEB_CONFIGURACIONGUARDADO = "WEB_CONFIGURACIONGUARDADO";

        public static string WEB_SELECCIONECONFIGURACION = "WEB_SELECCIONECONFIGURACION";
        public static string WEB_CONFIG_GENERAL = "WEB_CONFIG_GENERAL";
        public static string WEB_CONFIG_MOVIL = "WEB_CONFIG_MOVIL";
        public static string WEB_CONFIG_SIMBOLO = "WEB_CONFIG_SIMBOLO";
        public static string WEB_CONFIG_GPSRUTA = "WEB_CONFIG_GPSRUTA";
        public static string WEB_CONFIG_EFICIENCIA = "WEB_CONFIG_EFICIENCIA";
        public static string WEB_CONFIG_DESCUENTO = "WEB_CONFIG_DESCUENTO";
        public static string WEB_CONFIG_CABECERA_DESCARGA = "WEB_CONFIG_CABECERA_DESCARGA";

        public static string WEB_CONFIG_PUNTO = "WEB_CONFIG_PUNTO";
        public static string WEB_CONFIG_COMA = "WEB_CONFIG_COMA";
        public static string WEB_CONFIG_MONEDAIDIOMA = "WEB_CONFIG_MONEDAIDIOMA";
        public static string WEB_CONFIG_MONEDA = "WEB_CONFIG_MONEDA";
        public static string WEB_CONFIG_IDIOMA = "WEB_CONFIG_IDIOMA";
        public static string WEB_CONFIG_CANTIDAD_DECIMALES_VISTA = "WEB_CONFIG_CANTIDAD_DECIMALES_VISTA";
        public static string WEB_CONFIG_CANTIDAD_DECIMALES_CARGA = "WEB_CONFIG_CANTIDAD_DECIMALES_CARGA";
        public static string WEB_CONFIG_DECIMAL = "WEB_CONFIG_DECIMAL";

        public static string WEB_FLAG_GENERADOR = "WEB_FLAG_GENERADOR";



        public static string WEB_DETALLECLIENTE = "WEB_DETALLECLIENTE";
        public static string WEB_DETALLERUTA = "WEB_DETALLERUTA";
        public static string WEB_DETALLEUSUARIO = "WEB_DETALLEUSUARIO";
        public static string WEB_DETALLEPRODUCTO = "WEB_DETALLEPRODUCTO";
        public static string WEB_DETALLELISTAPRECIO = "WEB_DETALLELISTAPRECIO";
        public static string WEB_DETALLECONFIGURACION = "WEB_DETALLECONFIGURACION";
        //public static string WEB_CONFIGURACION = "WEB_CONFIGURACION";
        public static string WEB_PEDIDOSHOY = "WEB_PEDIDOSHOY";
        public static string WEB_COBRANZASHOY = "WEB_COBRANZASHOY";
        public static string WEB_REPORTECOBRANZA = "WEB_REPORTECOBRANZA";

        public static string WEB_PEDIDO_DETPEDIDO = "WEB_PEDIDO_DETPEDIDO";
        public static string WEB_NOPEDIDO_DETNOPEDIDO = "WEB_NOPEDIDO_DETNOPEDIDO";
        public static string WEB_DEVOLUCI_DETDEVOLUCI = "WEB_DEVOLUCI_DETDEVOLUCI";
        public static string WEB_CANJE_DETCANJE = "WEB_CANJE_DETCANJE";
        public static string WEB_COBRANZA_DETCOBRANZA = "WEB_COBRANZA_DETCOBRANZA";
        public static string WEB_PEDTOTAL_DETPEDTOTAL = "WEB_PEDTOTAL_DETPEDTOTAL";

        public static string WEB_RUTA_DETRUTA = "WEB_RUTA_DETRUTA";
        public static string WEB_CAMPOREQUERIDO = "WEB_CAMPOREQUERIDO";

        public static string WEB_FIN = "WEB_FIN";
        public static string WEB_DETALLEPEDIDO = "WEB_DETALLEPEDIDO";
        public static string WEB_TOTAL = "WEB_TOTAL";

        public static string WEB_TOTAL_VENTA = "WEB_TOTAL_VENTA";

        public static string WEB_PAGADO = "WEB_PAGADO";
        public static string WEB_SALDO = "WEB_SALDO";
        public static string WEB_DETALLECOBRANZA = "WEB_DETALLECOBRANZA";
        public static string WEB_REPORTEDEPEDIDOS = "WEB_REPORTEDEPEDIDOS";
        public static string WEB_REPORTEDECANJE = "WEB_REPORTEDECANJE";
        public static string WEB_REPORTEDECOBRANZA = "WEB_REPORTEDECOBRANZA";
        public static string WEB_REPORTEDEDEVOLUCION = "WEB_REPORTEDEDEVOLUCION";
        public static string WEB_REPORTEDENOPEDIDOS = "WEB_REPORTEDENOPEDIDOS";
        public static string WEB_REPORTEDEPEDIDOTOTAL = "WEB_REPORTEDEPEDIDOTOTAL";
        public static string WEB_RUTAVENDEDOR = "WEB_RUTAVENDEDOR";
        public static string WEB_NOTIENEGPS = "WEB_NOTIENEGPS";
        public static string WEB_SESSIONFINALIZADO = "WEB_SESSIONFINALIZADO";
        public static string WEB_NOPOSICIONESFILTRO = "WEB_NOPOSICIONESFILTRO";
        public static string WEB_NOLATLONREGISTRADA = "WEB_NOLATLONREGISTRADA";

        public static string WEB_NUMPEDIDOUPPERCASE = "WEB_NUMPEDIDOUPPERCASE";

        public static string WEB_CONDVENTAUPPERCASE = "WEB_CONDVENTAUPPERCASE";

        public static string WEB_NUMCANJEUPPERCASE = "WEB_NUMCANJEUPPERCASE";
        public static string WEB_NUMNOPEDIDOUPPERCASE = "WEB_NUMNOPEDIDOUPPERCASE";
        public static string WEB_NUMDEVOLUCIONUPPERCASE = "WEB_NUMDEVOLUCIONUPPERCASE";

        public static string WEB_PAGOSDECOBRANZA = "WEB_PAGOSDECOBRANZA";
        public static string WEB_VOUCHER = "WEB_VOUCHER";
        public static string WEB_BANCO = "WEB_BANCO";
        public static string WEB_TRACKINGCOBRANZA = "WEB_TRACKINGCOBRANZA";
        public static string WEB_TRACKINGPEDIDO = "WEB_TRACKINGPEDIDO";
        public static string WEB_TRACKINGCANJE = "WEB_TRACKINGCANJE";
        public static string WEB_TRACKINGNOPEDIDO = "WEB_TRACKINGNOPEDIDO";
        public static string WEB_TRACKINGDEVOLUCION = "WEB_TRACKINGDEVOLUCION";
        public static string WEB_MOTIVODEVOLUCION = "WEB_MOTIVODEVOLUCION";
        public static string WEB_MOTIVO_CANJE = "WEB_MOTIVO_CANJE";
        public static string WEB_MOTIVO_NOPEDIDO = "WEB_MOTIVO_NOPEDIDO";
        public static string WEB_IZQUIERDA = "WEB_IZQUIERDA";
        public static string WEB_MSJ_YAEXISTEZONADESCRIPCION = "WEB_MSJ_YAEXISTEZONADESCRIPCION";


        public static string WEB_GRAFICOPEDIDO = "WEB_GRAFICOPEDIDO";
        public static string WEB_PEDIDOSDELDIAMES = "WEB_PEDIDOSDELDIAMES";
        public static string WEB_TOPPRODUCTOSMES = "WEB_TOPPRODUCTOSMES";


        public static string WEB_VENTASULT30D = "WEB_VENTASULT30D";
        public static string WEB_CLIENTESMASVIS30D = "WEB_CLIENTESMASVIS30D";
        public static string WEB_GRAFICONOPEDIDO = "WEB_GRAFICONOPEDIDO";
        public static string WEB_NOPEDIDOSULT5D = "WEB_NOPEDIDOSULT5D";
        public static string WEB_CLIENTESMASNOPED30D = "WEB_CLIENTESMASNOPED30D";
        public static string WEB_GRAFICOCANJE = "WEB_GRAFICOCANJE";
        public static string WEB_CANJESULT5D = "WEB_CANJESULT5D";
        public static string WEB_CLIENTESMASCANJES30D = "WEB_CLIENTESMASCANJES30D";
        public static string WEB_GRAFICODEVOLUCION = "WEB_GRAFICODEVOLUCION";
        public static string WEB_PAGOSULT5D = "WEB_PAGOSULT5D";
        public static string WEB_CLIENTESMASPAGOS30D = "WEB_CLIENTESMASPAGOS30D";

        public static string WEB_GRAFICOCOBRANZA = "WEB_GRAFICOCOBRANZA";
        public static string WEB_DEVOLUCIONESULT5D = "WEB_DEVOLUCIONESULT5D";
        public static string WEB_CLIENTESMASDEV30D = "WEB_CLIENTESMASDEV30D";
        public static string WEB_EJECUCIONEXITO = "WEB_EJECUCIONEXITO";
        public static string WEB_AGREGAR = "WEB_AGREGAR";
        public static string WEB_MODULODE = "WEB_MODULODE";
        public static string WEB_VERGRAFICO = "WEB_VERGRAFICO";
        public static string WEB_NOPOSICIONESREGISTRADAS = "WEB_NOPOSICIONESREGISTRADAS";


        public static string WEB_MENSAJE_SELECCION = "WEB_MENSAJE_SELECCION";
        public static string WEB_MENSAJE_ACCIONREGISTROS = "WEB_MENSAJE_ACCIONREGISTROS";
        public static string WEB_MENSAJE_ACCIONREGISTRO = "WEB_MENSAJE_ACCIONREGISTRO";
        public static string WEB_MENSAJE_ACCION = "WEB_MENSAJE_ACCION";
        public static string WEB_ARCHIVO_NOEJECUTADO = "WEB_ARCHIVO_NOEJECUTADO";
        public static string WEB_CARGA_EXITO = "WEB_CARGA_EXITO";
        public static string WEB_ERROR_CARGANDO_ARCHIVO = "WEB_ERROR_CARGANDO_ARCHIVO";
        public static string WEB_ERROR_NOSELECCIONADO = "WEB_ERROR_NOSELECCIONADO";

        public static string WEB_AUMENTO_ARCHIVO = "WEB_AUMENTO_ARCHIVO";
        public static string WEB_REMOVIO_ARCHIVO = "WEB_REMOVIO_ARCHIVO";
        public static string WEB_ERROR_EJECUTAR_DTS = "WEB_ERROR_EJECUTAR_DTS";
        public static string WEB_NOEXISTE = "WEB_NOEXISTE";
        public static string WEB_ERRORDTS_VARIABLESGLOBALES = "WEB_ERRORDTS_VARIABLESGLOBALES";
        public static string WEB_ERROR_ARCHIVO = "WEB_ERROR_ARCHIVO";

        public static string WEB_ERROR_EN_ARCHIVO = "WEB_ERROR_EN_ARCHIVO";
        public static string WEB_ERROR_EN_DATOS = "WEB_ERROR_EN_DATOS";

        public static string WEB_OK_ARCHIVO = "WEB_OK_ARCHIVO";
        public static string WEB_REGISTROS_INSERTADOS = "WEB_REGISTROS_INSERTADOS";
        public static string WEB_REGISTROS_ACTUALIZADOS = "WEB_REGISTROS_ACTUALIZADOS";

        public static string WEB_NOCOBRANZASHOY = "WEB_NOCOBRANZASHOY";
        public static string WEB_NOCLIENTESVISITA = "WEB_NOCLIENTESVISITA";
        public static string WEB_CLIENTESCONPEDIDO = "WEB_CLIENTESCONPEDIDO";

        public static string WEB_ABRIRBARRA = "WEB_ABRIRBARRA";

        public static string WEB_DESCARGAEXITOSA = "WEB_DESCARGAEXITOSA";
        public static string WEB_CARGA_DETCARGA = "WEB_CARGA_DETCARGA";
        public static string WEB_DESCARGA_DETDESCARGA = "WEB_DESCARGA_DETDESCARGA";

        public static string WEB_DIASEMANA_LU = "WEB_DIASEMANA_LU";
        public static string WEB_DIASEMANA_MA = "WEB_DIASEMANA_MA";
        public static string WEB_DIASEMANA_MI = "WEB_DIASEMANA_MI";
        public static string WEB_DIASEMANA_JU = "WEB_DIASEMANA_JU";
        public static string WEB_DIASEMANA_VI = "WEB_DIASEMANA_VI";
        public static string WEB_DIASEMANA_SA = "WEB_DIASEMANA_SA";
        public static string WEB_DIASEMANA_DO = "WEB_DIASEMANA_DO";

        public static string WEB_NODATOS_DESCARGAR = "WEB_NODATOS_DESCARGAR";

        public static string WEB_CARGAR = "WEB_CARGAR";

        public static string WEB_DESCARGAR = "WEB_DESCARGAR";

        public static string WEB_GEOCERCA_NO_ENCONTRO_DIR = "WEB_GEOCERCA_NO_ENCONTRO_DIR";
        public static string WEB_SECUENCIA = "WEB_SECUENCIA";

        public static string WEB_SUPERVISOR = "WEB_SUPERVISOR";
        public static string WEB_ADMINISTRADOR = "WEB_ADMINISTRADOR";
        public static string WEB_PUNTOS = "WEB_PUNTOS";

        public static string WEB_SELECCIONAR_RUTA = "WEB_SELECCIONAR_RUTA";
        public static string WEB_CLAVE_NUMERICA = "WEB_CLAVE_NUMERICA";

        public static string WEB_MESES = "WEB_MESES";
        public static string WEB_DIAS = "WEB_DIAS";
        public static string WEB_MONT_PRODUCTO = "WEB_MONT_PRODUCTO";
        public static string WEB_MONT_VENDEDOR = "WEB_MONT_VENDEDOR";
        public static string WEB_MONT_CLIENTE = "WEB_MONT_CLIENTE";
        public static string WEB_VEND_MAS_VENTAS = "WEB_VEND_MAS_VENTAS";
        public static string WEB_CLI_PEDIDOS = "WEB_CLI_PEDIDOS";
        public static string WEB_SKU_MAS_ROTACION = "WEB_SKU_MAS_ROTACION";
        public static string WEB_PRO_MAS_VENDIDOS = "WEB_PRO_MAS_VENDIDOS";
        public static string WEB_VENTAS_TOTALES = "WEB_VENTAS_TOTALES";
        public static string WEB_COBRANZAS_TOTALES = "WEB_COBRANZAS_TOTALES";

        public static string WEB_GRAFICO_LINEA = "WEB_GRAFICO_LINEA";
        public static string WEB_GRAFICO_PIE = "WEB_GRAFICO_PIE";
        public static string WEB_GRAFICO_BARRAS = "WEB_GRAFICO_BARRAS";
        public static string WEB_SKU_DEMANDADOS = "WEB_SKU_DEMANDADOS";
        public static string WEB_SKU_MAS_VENDIDOS = "WEB_SKU_MAS_VENDIDOS";
        public static string WEB_CLI_PRO_MAS_VENDIDOS = "WEB_CLI_PRO_MAS_VENDIDOS";
        public static string WEB_CLIENTES_MAS_VENTAS = "WEB_CLIENTES_MAS_VENTAS";

        public static string WEB_MANUAL_PROCEDURES = "WEB_MANUAL_PROCEDURES";
        public static string WEB_MIDDLEWARE = "WEB_MIDDLEWARE";
        public static string WEB_RUTA_MANUAL_PROCEDIMIENTOS = "WEB_RUTA_MANUAL_PROCEDIMIENTOS";


        public static string WEB_GRAFICO_DONA = "WEB_GRAFICO_DONA";
        public static string WEB_CLIENTES_MAS_DEVOLUCIONES = "WEB_CLIENTES_MAS_DEVOLUCIONES";
        public static string WEB_CLIENTES_MAS_CANJES = "WEB_CLIENTES_MAS_CANJES";
        public static string WEB_INDICADOR_CIRCULAR = "WEB_INDICADOR_CIRCULAR";
        public static string WEB_INDICADOR_SEMICIRCULAR = "WEB_INDICADOR_SEMICIRCULAR";
        public static string WEB_INDICADOR_TERMOMETRO = "WEB_INDICADOR_TERMOMETRO";
        public static string WEB_LIMITE_INFERIOR = "WEB_LIMITE_INFERIOR";
        public static string WEB_LIMITE_SUPERIOR = "WEB_LIMITE_SUPERIOR";
        public static string WEB_META = "WEB_META";
        public static string WEB_OPERACION = "WEB_OPERACION";
        public static string WEB_UNIDADES_VENDIDAS = "WEB_UNIDADES_VENDIDAS";
        public static string WEB_SELECCIONAR = "WEB_SELECCIONAR";

        public static string WEB_VENTAS_TOTALES_PRODUCTO = "WEB_VENTAS_TOTALES_PRODUCTO";
        public static string WEB_UNIDADES = "WEB_UNIDADES";


        public static String WEB_RUTA_MANUAL_USUARIO = "WEB_RUTA_MANUAL_USUARIO";
        public static String WEB_RUTA_MANUAL_TECNICO = "WEB_RUTA_MANUAL_TECNICO";
        public static String WEB_RUTA_EXCEL_EJEMPLO = "WEB_RUTA_EXCEL_EJEMPLO";
        public static String WEB_RUTA_MANUAL_PROCEDURE = "WEB_RUTA_MANUAL_PROCEDURE";
        public static String WEB_RUTA_GENERADOR = "WEB_RUTA_GENERADOR";
        public static String WEB_RUTA_WIN_SERVICE = "WEB_RUTA_WIN_SERVICE";

        public static String WEB_RUTA_TERMINOS_Y_CONDICIONES = "WEB_RUTA_TERMINOS_Y_CONDICIONES";

        public static String WEB_TERMINOS_Y_CONDICIONES = "WEB_TERMINOS_Y_CONDICIONES";

        public static String WEB_FLAG_MOSTRARLOGOTOP = "WEB_FLAG_MOSTRARLOGOTOP";


        //***news***/// 
        public static String WEB_NINGUN_ARCHIVO = "WEB_NINGUN_ARCHIVO";
        public static String WEB_NIN_ARCHIVO_EJECUTADO = "WEB_NIN_ARCHIVO_EJECUTADO";
        public static String WEB_ARCHIVOS_EJECUTADO = "WEB_ARCHIVOS_EJECUTADO";
        public static String WEB_ARCHIVOS_EJECUTADO_CORRECT = "WEB_ARCHIVOS_EJECUTADO_CORRECT";
        public static String WEB_ERROR_CARGA = "WEB_ERROR_CARGA";
        public static String WEB_ERROR_NUM_COLUMNAS = "WEB_ERROR_NUM_COLUMNAS";
        public static String WEB_ERROR_ENCRIPTACION = "WEB_ERROR_ENCRIPTACION";
        public static String WEB_AGREGAR_ARCHIVO_MIN = "WEB_AGREGAR_ARCHIVO_MIN";
        public static String WEB_ERROR_SUBIENDO_ARCHI = "WEB_ERROR_SUBIENDO_ARCHI";
        public static String WEB_NUEVA_CARGA = "WEB_NUEVA_CARGA";
        public static String WEB_FORMATO_INVAL_ARCHIV = "WEB_FORMATO_INVAL_ARCHIV";
        public static String WEB_PORFAVOR_REVISARLO = "WEB_PORFAVOR_REVISARLO";
        public static String WEB_DETALLE_ERROR = "WEB_DETALLE_ERROR";
        public static String WEB_DATOS_INVALIDOS = "WEB_DATOS_INVALIDOS";
        public static String WEB_ARCHIVOS = "WEB_ARCHIVOS";
        public static String WEB_ESTADO = "WEB_ESTADO";
        public static String WEB_SUBIDO = "WEB_SUBIDO";
        public static String WEB_INSERTADOS = "WEB_INSERTADOS";
        public static String WEB_ACTUALIZADOS = "WEB_ACTUALIZADOS";
        public static String WEB_TOTAL_CARGADOS = "WEB_TOTAL_CARGADOS";
        public static String WEB_TOTAL_NO_CARGADOS = "WEB_TOTAL_NO_CARGADOS";

        public static String WEB_ERR_DATA = "WEB_ERR_DATA";
        public static String WEB_EJECUCION_EXITOSA = "WEB_EJECUCION_EXITOSA";

        public static String WEB_RUTA_ICONO_LOGO = "WEB_RUTA_ICONO_LOGO";
        public static String WEB_RUTA_ICONO_LOGO_SOLNEXTEL = "WEB_RUTA_ICONO_LOGO_SOLNEXTEL";

        public static String RUTA_ICONO_LOGO_DEFAULT = "nextel-logo-footer.jpg";
        public static String RUTA_ICONO_LOGONO_DEFAULT = "logono.jpg";
        public static String WEB_PIE2_BR = "WEB_PIE2_BR";

        public static String WEB_GEO_GRAFNPANT = "WEB_GEO_GRAFNPANT";

        public static String WEB_FILENAME_VENDEDOR = "WEB_FILENAME_VENDEDOR";
        public static String WEB_FILENAME_PRODUCTO = "WEB_FILENAME_PRODUCTO";
        public static String WEB_FILENAME_RUTA = "WEB_FILENAME_RUTA";
        public static String WEB_FILENAME_CLIENTE = "WEB_FILENAME_CLIENTE";
        public static String WEB_FILENAME_COBRANZA = "WEB_FILENAME_COBRANZA";
        public static String WEB_FILENAME_GENERAL = "WEB_FILENAME_GENERAL";
        public static String WEB_FILENAME_LISTAPRECIOS = "WEB_FILENAME_LISTAPRECIOS";


        #endregion

        #region new carga

        //public static String WEB_ARCHIVO = "WEB_ARCHIVO";
        public static String WEB_SUBIDOS = "WEB_SUBIDOS";
        public static String WEB_ERROR_ESTRUCTURA = "WEB_ERROR_ESTRUCTURA";
        public static String WEB_TIENE = "WEB_TIENE";
        public static String WEB_DEBE_TENER = "WEB_DEBE_TENER";
        public static String WEB_ERROR_INSERTANDO = "WEB_ERROR_INSERTANDO";
        //public static String WEB_ERROR_EN_ARCHIVO = "WEB_ERROR_EN_ARCHIVO";
        public static String WEB_NOMBRE_CLIENTE = "WEB_NOMBRE_CLIENTE";
        public static String WEB_CODIGO_GRUPO = "WEB_CODIGO_GRUPO";
        public static String WEB_FILA = "WEB_FILA";
        public static String WEB_ERR = "WEB_ERR";
        public static String WEB_ERR_NO_DATA = "WEB_ERR_NO_DATA";
        public static String WEB_FORMATO = "WEB_FORMATO";

        public static String WEB_NUMERO = "WEB_NUMERO";
        public static String WEB_TIPO_CLIENTE = "WEB_TIPO_CLIENTE";
        public static String WEB_CODIGO_CLIENTE = "WEB_CODIGO_CLIENTE";
        public static String WEB_CODIGO_VENDEDOR = "WEB_CODIGO_VENDEDOR";
        public static String WEB_TIPO_DOCUMENTO = "WEB_TIPO_DOCUMENTO";
        public static String WEB_MONTO_PAGADO = "WEB_MONTO_PAGADO";
        public static String WEB_MONTO_TOTAL = "WEB_MONTO_TOTAL";
        public static String WEB_FECHA_VENCIMIENTO = "WEB_FECHA_VENCIMIENTO";
        public static String WEB_SERIE = "WEB_SERIE";
        public static String WEB_FECHA_REGISTRO = "WEB_FECHA_REGISTRO";
        public static String WEB_TIPO_CARGA = "WEB_TIPO_CARGA";
        public static String WEB_DESCRIPCION = "WEB_DESCRIPCION";
        public static String WEB_DESCRIPCION_PRODUCTO = "WEB_DESCRIPCION_PRODUCTO";
        public static String WEB_DISTANCIA = "WEB_DISTANCIA";
        public static String WEB_ABREVIATURA = "WEB_ABREVIATURA";
        public static String WEB_CODIGO_CANAL = "WEB_CODIGO_CANAL";
        public static String WEB_CODIGO_PRODUCTO = "WEB_CODIGO_PRODUCTO";





        #endregion

        #region descarga nombre archivos
        public static string WEB_DESCARGA_PEDIDO = "WEB_DESCARGA_PEDIDO";
        public static string WEB_DESCARGA_DETALLE_PEDIDO = "WEB_DESCARGA_DETALLE_PEDIDO";
        public static string WEB_DESCARGA_NOVENTA = "WEB_DESCARGA_NOVENTA";
        public static string WEB_DESCARGA_CANJE = "WEB_DESCARGA_CANJE";
        public static string WEB_DESCARGA_DETALLE_CANJE = "WEB_DESCARGA_DETALLE_CANJE";
        public static string WEB_DESCARGA_DEVOLUCION = "WEB_DESCARGA_DEVOLUCION";
        public static string WEB_DESCARGA_DETALLE_DEVOLUCION = "WEB_DESCARGA_DETALLE_DEVOLUCION";
        public static string WEB_DESCARGA_PAGO = "WEB_DESCARGA_PAGO";
        #endregion

        #region descarga cabecera archivos
        public static string WEB_DESCARGA_PEDIDO_COL01 = "WEB_DESCARGA_PEDIDO_COL01";
        public static string WEB_DESCARGA_PEDIDO_COL02 = "WEB_DESCARGA_PEDIDO_COL02";
        public static string WEB_DESCARGA_PEDIDO_COL03 = "WEB_DESCARGA_PEDIDO_COL03";
        public static string WEB_DESCARGA_PEDIDO_COL04 = "WEB_DESCARGA_PEDIDO_COL04";
        public static string WEB_DESCARGA_PEDIDO_COL05 = "WEB_DESCARGA_PEDIDO_COL05";
        public static string WEB_DESCARGA_PEDIDO_COL06 = "WEB_DESCARGA_PEDIDO_COL06";
        public static string WEB_DESCARGA_PEDIDO_COL07 = "WEB_DESCARGA_PEDIDO_COL07";
        public static string WEB_DESCARGA_PEDIDO_COL08 = "WEB_DESCARGA_PEDIDO_COL08";//OBSERVACION
        public static string WEB_DESCARGA_PEDIDO_COL09 = "WEB_DESCARGA_PEDIDO_COL09";//LATITUD
        public static string WEB_DESCARGA_PEDIDO_COL10 = "WEB_DESCARGA_PEDIDO_COL10";//LONGITUD

        public static string WEB_DESCARGA_DETALLE_PEDIDO_COL01 = "WEB_DESCARGA_DETALLE_PEDIDO_COL01";
        public static string WEB_DESCARGA_DETALLE_PEDIDO_COL02 = "WEB_DESCARGA_DETALLE_PEDIDO_COL02";
        public static string WEB_DESCARGA_DETALLE_PEDIDO_COL03 = "WEB_DESCARGA_DETALLE_PEDIDO_COL03";
        public static string WEB_DESCARGA_DETALLE_PEDIDO_COL04 = "WEB_DESCARGA_DETALLE_PEDIDO_COL04";
        public static string WEB_DESCARGA_DETALLE_PEDIDO_COL05 = "WEB_DESCARGA_DETALLE_PEDIDO_COL05";
        public static string WEB_DESCARGA_DETALLE_PEDIDO_COL06 = "WEB_DESCARGA_DETALLE_PEDIDO_COL06";
        public static string WEB_DESCARGA_DETALLE_PEDIDO_COL07 = "WEB_DESCARGA_DETALLE_PEDIDO_COL07";
        public static string WEB_DESCARGA_DETALLE_PEDIDO_COL08 = "WEB_DESCARGA_DETALLE_PEDIDO_COL08";
        public static string WEB_DESCARGA_DETALLE_PEDIDO_COL09 = "WEB_DESCARGA_DETALLE_PEDIDO_COL09";
        public static string WEB_DESCARGA_DETALLE_PEDIDO_COL10 = "WEB_DESCARGA_DETALLE_PEDIDO_COL10";
        public static string WEB_DESCARGA_DETALLE_PEDIDO_COL11 = "WEB_DESCARGA_DETALLE_PEDIDO_COL11";

        public static string WEB_DESCARGA_DEVOLUCION_COL01 = "WEB_DESCARGA_DEVOLUCION_COL01";
        public static string WEB_DESCARGA_DEVOLUCION_COL02 = "WEB_DESCARGA_DEVOLUCION_COL02";
        public static string WEB_DESCARGA_DEVOLUCION_COL03 = "WEB_DESCARGA_DEVOLUCION_COL03";
        public static string WEB_DESCARGA_DEVOLUCION_COL04 = "WEB_DESCARGA_DEVOLUCION_COL04";
        public static string WEB_DESCARGA_DEVOLUCION_COL05 = "WEB_DESCARGA_DEVOLUCION_COL05";

        public static string WEB_DESCARGA_DETALLE_DEVOLUCION_COL01 = "WEB_DESCARGA_DETALLE_DEVOLUCION_COL01";
        public static string WEB_DESCARGA_DETALLE_DEVOLUCION_COL02 = "WEB_DESCARGA_DETALLE_DEVOLUCION_COL02";
        public static string WEB_DESCARGA_DETALLE_DEVOLUCION_COL03 = "WEB_DESCARGA_DETALLE_DEVOLUCION_COL03";
        public static string WEB_DESCARGA_DETALLE_DEVOLUCION_COL04 = "WEB_DESCARGA_DETALLE_DEVOLUCION_COL04";
        public static string WEB_DESCARGA_DETALLE_DEVOLUCION_COL05 = "WEB_DESCARGA_DETALLE_DEVOLUCION_COL05";
        public static string WEB_DESCARGA_DETALLE_DEVOLUCION_COL06 = "WEB_DESCARGA_DETALLE_DEVOLUCION_COL06";
        public static string WEB_DESCARGA_DETALLE_DEVOLUCION_COL07 = "WEB_DESCARGA_DETALLE_DEVOLUCION_COL07";
        public static string WEB_DESCARGA_DETALLE_DEVOLUCION_COL08 = "WEB_DESCARGA_DETALLE_DEVOLUCION_COL08";
        public static string WEB_DESCARGA_DETALLE_DEVOLUCION_COL09 = "WEB_DESCARGA_DETALLE_DEVOLUCION_COL09";
        public static string WEB_DESCARGA_DETALLE_DEVOLUCION_COL10 = "WEB_DESCARGA_DETALLE_DEVOLUCION_COL10";

        public static string WEB_DESCARGA_CANJE_COL01 = "WEB_DESCARGA_CANJE_COL01";
        public static string WEB_DESCARGA_CANJE_COL02 = "WEB_DESCARGA_CANJE_COL02";
        public static string WEB_DESCARGA_CANJE_COL03 = "WEB_DESCARGA_CANJE_COL03";
        public static string WEB_DESCARGA_CANJE_COL04 = "WEB_DESCARGA_CANJE_COL04";
        public static string WEB_DESCARGA_CANJE_COL05 = "WEB_DESCARGA_CANJE_COL05";

        public static string WEB_DESCARGA_DETALLE_CANJE_COL01 = "WEB_DESCARGA_DETALLE_CANJE_COL01";
        public static string WEB_DESCARGA_DETALLE_CANJE_COL02 = "WEB_DESCARGA_DETALLE_CANJE_COL02";
        public static string WEB_DESCARGA_DETALLE_CANJE_COL03 = "WEB_DESCARGA_DETALLE_CANJE_COL03";
        public static string WEB_DESCARGA_DETALLE_CANJE_COL04 = "WEB_DESCARGA_DETALLE_CANJE_COL04";
        public static string WEB_DESCARGA_DETALLE_CANJE_COL05 = "WEB_DESCARGA_DETALLE_CANJE_COL05";
        public static string WEB_DESCARGA_DETALLE_CANJE_COL06 = "WEB_DESCARGA_DETALLE_CANJE_COL06";
        public static string WEB_DESCARGA_DETALLE_CANJE_COL07 = "WEB_DESCARGA_DETALLE_CANJE_COL07";
        public static string WEB_DESCARGA_DETALLE_CANJE_COL08 = "WEB_DESCARGA_DETALLE_CANJE_COL08";
        public static string WEB_DESCARGA_DETALLE_CANJE_COL09 = "WEB_DESCARGA_DETALLE_CANJE_COL09";
        public static string WEB_DESCARGA_DETALLE_CANJE_COL10 = "WEB_DESCARGA_DETALLE_CANJE_COL10";

        public static string WEB_DESCARGA_NOVENTA_COL01 = "WEB_DESCARGA_NOVENTA_COL01";
        public static string WEB_DESCARGA_NOVENTA_COL02 = "WEB_DESCARGA_NOVENTA_COL02";
        public static string WEB_DESCARGA_NOVENTA_COL03 = "WEB_DESCARGA_NOVENTA_COL03";
        public static string WEB_DESCARGA_NOVENTA_COL04 = "WEB_DESCARGA_NOVENTA_COL04";
        public static string WEB_DESCARGA_NOVENTA_COL05 = "WEB_DESCARGA_NOVENTA_COL05";
        public static string WEB_DESCARGA_NOVENTA_COL06 = "WEB_DESCARGA_NOVENTA_COL06";

        public static string WEB_DESCARGA_PAGO_COL01 = "WEB_DESCARGA_PAGO_COL01";
        public static string WEB_DESCARGA_PAGO_COL02 = "WEB_DESCARGA_PAGO_COL02";
        public static string WEB_DESCARGA_PAGO_COL03 = "WEB_DESCARGA_PAGO_COL03";
        public static string WEB_DESCARGA_PAGO_COL04 = "WEB_DESCARGA_PAGO_COL04";
        public static string WEB_DESCARGA_PAGO_COL05 = "WEB_DESCARGA_PAGO_COL05";
        public static string WEB_DESCARGA_PAGO_COL06 = "WEB_DESCARGA_PAGO_COL06";
        public static string WEB_DESCARGA_PAGO_COL07 = "WEB_DESCARGA_PAGO_COL07";
        public static string WEB_DESCARGA_PAGO_COL08 = "WEB_DESCARGA_PAGO_COL08";

        #endregion

        #region graficos
        public static String WEB_GRP_TOP_CLIENTES = "WEB_GRP_TOP_CLIENTES";
        public static String WEB_GRP_TOP_PRODUCTOS = "WEB_GRP_TOP_PRODUCTOS";
        public static String WEB_GRP_TOP_VENDEDORES = "WEB_GRP_TOP_VENDEDORES";

        public static String WEB_GRP_AVANCE = "WEB_GRP_AVANCE";
        public static String WEB_GRP_INDICADOR = "WEB_GRP_INDICADOR";
        public static String WEB_GRP_INDICADORPRODUCTO = "WEB_GRP_INDICADORPRODUCTO";

        public static String WEB_GRP_FILTROOPERACION = "WEB_GRP_FILTROOPERACION";
        public static String WEB_GRP_FILTROTIPO = "WEB_GRP_FILTROTIPO";

        public static String WEB_GRP_OPERACIONCANTIDAD = "WEB_GRP_OPERACIONCANTIDAD";
        public static String WEB_GRP_OPERACIONMONTO = "WEB_GRP_OPERACIONMONTO";

        public static String WEB_GRCLIENTE_PEDIDO_VS_CANTIDAD = "WEB_GRCLIENTE_PEDIDO_VS_CANTIDAD";
        public static String WEB_GRCLIENTE_PEDIDO_VS_MONTO = "WEB_GRCLIENTE_PEDIDO_VS_MONTO";
        public static String WEB_GRCLIENTE_COBRANZA_VS_MONTO = "WEB_GRCLIENTE_COBRANZA_VS_MONTO";
        public static String WEB_GRCLIENTE_CANJE_VS_CANTIDAD = "WEB_GRCLIENTE_CANJE_VS_CANTIDAD";
        public static String WEB_GRCLIENTE_DEVOLUCION_VS_CANTIDAD = "WEB_GRCLIENTE_DEVOLUCION_VS_CANTIDAD";
        public static String WEB_GRPRODUCTO_PEDIDO_VS_CANTIDAD = "WEB_GRPRODUCTO_PEDIDO_VS_CANTIDAD";
        public static String WEB_GRPRODUCTO_PEDIDO_VS_MONTO = "WEB_GRPRODUCTO_PEDIDO_VS_MONTO";
        public static String WEB_GRPRODUCTO_CANJE_VS_CANTIDAD = "WEB_GRPRODUCTO_CANJE_VS_CANTIDAD";
        public static String WEB_GRPRODUCTO_DEVOLUCION_VS_CANTIDAD = "WEB_GRPRODUCTO_DEVOLUCION_VS_CANTIDAD";
        public static String WEB_GRVENDEDOR_PEDIDO_VS_CANTIDAD = "WEB_GRVENDEDOR_PEDIDO_VS_CANTIDAD";
        public static String WEB_GRVENDEDOR_PEDIDO_VS_MONTO = "WEB_GRVENDEDOR_PEDIDO_VS_MONTO";
        public static String WEB_GRVENDEDOR_COBRANZA_VS_MONTO = "WEB_GRVENDEDOR_COBRANZA_VS_MONTO";
        public static String WEB_GRVENDEDOR_CANJE_VS_CANTIDAD = "WEB_GRVENDEDOR_CANJE_VS_CANTIDAD";
        public static String WEB_GRVENDEDOR_DEVOLUCION_VS_CANTIDAD = "WEB_GRVENDEDOR_DEVOLUCION_VS_CANTIDAD";
        public static String WEB_GRMAPA_CLIENTE_VS_CANTIDAD = "WEB_GRMAPA_CLIENTE_VS_CANTIDAD";
        public static String WEB_GRMAPA_CLIENTE_VS_MONTO = "WEB_GRMAPA_CLIENTE_VS_MONTO";
        public static String WEB_GRMAPA_PRODUCTO_VS_CANTIDAD = "WEB_GRMAPA_PRODUCTO_VS_CANTIDAD";
        public static String WEB_GRMAPA_PRODUCTO_VS_MONTO = "WEB_GRMAPA_PRODUCTO_VS_MONTO";








        #endregion

        #region icono
        public static String WEB_MANTENIMIENTO_ICONO = "WEB_MANTENIMIENTO_ICONO";
        public static String WEB_ERROR_EXTENSION_ICONO = "WEB_ERROR_EXTENSION_ICONO";
        public static String WEB_ERROR_TAMANIO_ICONO = "WEB_ERROR_TAMANIO_ICONO";
        public static String WEB_ICONO_ACTUALIZADO_EXITOSAMENTE = "WEB_ICONO_ACTUALIZADO_EXITOSAMENTE";
        public static String WEB_CONF_ICONO_GUARDADA = "WEB_CONF_ICONO_GUARDADA";
        public static String WEB_ICONO = "WEB_ICONO";
        public static String WEB_MOSTRAR_ICONO = "WEB_MOSTRAR_ICONO";
        public static String WEB_SHOW = "WEB_SHOW";
        public static String WEB_SUBIR_ICONO = "WEB_SUBIR_ICONO";

        #endregion
        #endregion

        public static Dictionary<string, string> data = new Dictionary<string, string>();

        public static String getMessage(String key)
        {
            if (data.ContainsKey(key))
            {
                return data[key];
            }
            return "";
        }

        public static String getUpperMessage(String key)
        {
            if (data.ContainsKey(key))
            {
                return data[key].ToUpper();
            }
            return "";
        }

        public static String getMensajeEncodeHTML(String key)
        {
            if (data.ContainsKey(key))
            {
                return HttpUtility.HtmlEncode(data[key]);
            }
            return "";
        }


        //aMendiola 07/10/2010
        public static String getMensajeEncodeHTML_Upper(String key)
        {
            if (data.ContainsKey(key))
            {
                return HttpUtility.HtmlEncode(data[key].ToUpper());
            }
            return "";
        }
        public static String getMensajeEncodeHTML_FistLetterCapital(String key)
        {
            if (data.ContainsKey(key))
            {
                return StringUtils.changeFirstLetterToUppercase(HttpUtility.HtmlEncode(data[key]));
            }
            return "";
        }
        public static String getMensajeEncodeJS(String key)
        {
            if (data.ContainsKey(key))
            {
                return StringUtils.JSEncode(data[key]);
            }
            return "";
        }

        public static String getMensajeEncodeJS_Upper(String key)
        {
            if (data.ContainsKey(key))
            {
                return StringUtils.JSEncode(data[key].ToUpper());
            }
            return "";
        }

        public static String getMensaje(String key, String code)
        {
            return getMessage(key).Replace("{0}", code);
        }


        public static String getCodeCountry()
        {
            return getMessage(CULTURE);
        }

        public static String getRutaIconoLogo()
        {
            String logoPath = getMensajeEncodeHTML(IdiomaCultura.WEB_RUTA_ICONO_LOGO).Trim();
            if (logoPath == String.Empty)
            {
                logoPath = RUTA_ICONO_LOGO_DEFAULT;
            }

            return logoPath;
        }

        public static String getRutaIconoLogoSolNextel()
        {
            String rutaLogo;
            try { rutaLogo = getMensajeEncodeHTML(IdiomaCultura.WEB_RUTA_ICONO_LOGO_SOLNEXTEL).Trim(); }
            catch { rutaLogo = RUTA_ICONO_LOGONO_DEFAULT; }

            return rutaLogo;
        }


        public static String getTituloGrafico(String strgrafico, String strOperacion, String strTipo)
        {

            String title = "";

            String data = strgrafico.Trim() + "||" + strOperacion.Trim() + "||" + strTipo.Trim();

            switch (data)
            {
                case "GR_TOP_CLIENTES||P||C":
                    title = getMensajeEncodeHTML(IdiomaCultura.WEB_GRCLIENTE_PEDIDO_VS_CANTIDAD);
                    break;

                case "GR_TOP_CLIENTES||P||M":
                    title = getMensajeEncodeHTML(IdiomaCultura.WEB_GRCLIENTE_PEDIDO_VS_MONTO);
                    break;

                case "GR_TOP_CLIENTES||CO||M":
                    title = getMensajeEncodeHTML(IdiomaCultura.WEB_GRCLIENTE_COBRANZA_VS_MONTO);
                    break;

                case "GR_TOP_CLIENTES||C||C":
                    title = getMensajeEncodeHTML(IdiomaCultura.WEB_GRCLIENTE_CANJE_VS_CANTIDAD);
                    break;

                case "GR_TOP_CLIENTES||D||C":
                    title = getMensajeEncodeHTML(IdiomaCultura.WEB_GRCLIENTE_DEVOLUCION_VS_CANTIDAD);
                    break;

                case "GR_TOP_PRODUCTOS||P||C":
                    title = getMensajeEncodeHTML(IdiomaCultura.WEB_GRPRODUCTO_PEDIDO_VS_CANTIDAD);
                    break;

                case "GR_TOP_PRODUCTOS||P||M":
                    title = getMensajeEncodeHTML(IdiomaCultura.WEB_GRPRODUCTO_PEDIDO_VS_MONTO);
                    break;

                case "GR_TOP_PRODUCTOS||C||C":
                    title = getMensajeEncodeHTML(IdiomaCultura.WEB_GRPRODUCTO_CANJE_VS_CANTIDAD);
                    break;

                case "GR_TOP_PRODUCTOS||D||C":
                    title = getMensajeEncodeHTML(IdiomaCultura.WEB_GRPRODUCTO_DEVOLUCION_VS_CANTIDAD);
                    break;

                case "GR_TOP_VENDEDORES||P||C":
                    title = getMensajeEncodeHTML(IdiomaCultura.WEB_GRVENDEDOR_PEDIDO_VS_CANTIDAD);
                    break;

                case "GR_TOP_VENDEDORES||P||M":
                    title = getMensajeEncodeHTML(IdiomaCultura.WEB_GRVENDEDOR_PEDIDO_VS_MONTO);
                    break;

                case "GR_TOP_VENDEDORES||CO||M":
                    title = getMensajeEncodeHTML(IdiomaCultura.WEB_GRVENDEDOR_COBRANZA_VS_MONTO);
                    break;

                case "GR_TOP_VENDEDORES||C||C":
                    title = getMensajeEncodeHTML(IdiomaCultura.WEB_GRVENDEDOR_CANJE_VS_CANTIDAD);
                    break;

                case "GR_TOP_VENDEDORES||D||C":
                    title = getMensajeEncodeHTML(IdiomaCultura.WEB_GRVENDEDOR_DEVOLUCION_VS_CANTIDAD);
                    break;
            }

            return title;
        }

        public static String getmensajeGuia(String value)
        {
            String mensajeGuia = IdiomaCultura.getMessage(IdiomaCultura.WEB_MENSAJE_GUIA);
            mensajeGuia = mensajeGuia.Replace("{0}", value);
            return mensajeGuia;
        }
    }
}