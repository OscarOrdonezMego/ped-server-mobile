using System;
using System.Xml;
using System.Configuration;
using Pedidos_Mobile.App_Code.Tools;

namespace Nextel.BL
{
    public class BLBase
    {
        /// <summary>
        /// Ruta fisica donde se guardara el archivo log
        /// </summary>
        public static string logRuta
        {
            get
            {
                return ConfigurationManager.AppSettings["rutaLog"].ToString();
            }
        }

        public static void subRegistrarLog(String xml)
        {
            if (ConfigurationManager.AppSettings["LOGACTIVO"] == "1")
            {
                //Log.Write(System.Web.HttpContext.Current.Server.MapPath("~") + "\\Server\\LOG\\", xml);
                new LogHelper().Debug("BLBase -> subRegistrarLog", xml);
            }
        }
        public static void fnInicioXML(XmlDocument salidaXML)
        {

            salidaXML.AppendChild(salidaXML.CreateProcessingInstruction("xml", "version='1.0' encoding='ISO-8859-1'"));
        }

        /// <summary>
        /// Funcion que Devuelve un XML de Error
        /// </summary>
        /// <param name="psMensaje">Mensaje de Error a Mostrar</param>
        /// <returns></returns>
        public static string fnXmlError(String psMensaje)
        {
            return "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?> <r e=\"" + psMensaje + "\" /> ";
        }
        public static String fnCambiarCaracteres(String psCadena)
        {

            psCadena = psCadena.Replace("?", "&aacute;");
            psCadena = psCadena.Replace("?", "&eacute;");
            psCadena = psCadena.Replace("?", "&iacute;");
            psCadena = psCadena.Replace("?", "&oacute;");
            psCadena = psCadena.Replace("?", "&uacute;");
            psCadena = psCadena.Replace("?", "&#241;");
            psCadena = psCadena.Replace("?", "&Aacute;");
            psCadena = psCadena.Replace("?", "&Eacute;");
            psCadena = psCadena.Replace("?", "&Iacute;");
            psCadena = psCadena.Replace("?", "&Oacute;");
            psCadena = psCadena.Replace("?", "&Uacute;");
            psCadena = psCadena.Replace("?", "&#209;");
            return psCadena;
        }

        public static String fnConvertirBinaryToString(Byte[] binary)
        {
            string s = "";
            for (Int64 i = 0; i < binary.Length; i++)
            {
                s += (char)binary[i];
            }
            return s;
        }

        public static string SimpleBinaryToString(Byte[] binary)
        {
            char[] chars = new char[binary.Length / sizeof(char)];
            System.Buffer.BlockCopy(binary, 0, chars, 0, binary.Length);
            return new string(chars);
        }
    }
}
