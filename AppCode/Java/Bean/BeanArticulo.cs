using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Collections.Generic;
using System.Collections;
using NextlibServer.RMS;
using NextlibServer.Utils;

namespace Java.Bean
{
    public class BeanArticulo : Persistent
    {
        #region Atributos
        private String _codigo;
        private String _nombre;
        private String _stock;
        //private String _unidadDefecto;
        private String _precioBase;
        private List<Persistent> _lstPrecios;
        private int _codAlmacen;
        private String _tipoBusqueda;
        #endregion

        #region Propiedades
        public String codigo
        {
            get { return _codigo; }
            set { _codigo = value; }
        }

        public String nombre
        {
            get { return _nombre; }
            set { _nombre = value; }
        }

        public String stock
        {
            get { return _stock; }
            set { _stock = value; }
        }

        public String tipoBusqueda
        {
            get { return _tipoBusqueda; }
            set { _tipoBusqueda = value; }
        }

        public String precioBase
        {
            get { return _precioBase; }
            set { _precioBase = value; }
        }

        public List<Persistent> lstPrecios
        {
            get { return _lstPrecios; }
            set { _lstPrecios = value; }
        }
        public int codAlmacen
        {
            get { return _codAlmacen; }
            set { _codAlmacen = value; }
        }
        #endregion

        #region Constructor
        public BeanArticulo()
        {
            lstPrecios = new List<Persistent>();
        }
        #endregion

        #region Implementación métodos "Persistente"
        public override void persist(DataOutputStream dataOutput, List<Persistent> list)
        {
            BeanArticulo loArticulo = null;

            dataOutput.writeInt(list.Count);

            for (int i = 0; i < list.Count; i++)
            {
                loArticulo = (BeanArticulo)list[i];
                loArticulo.persistirItem(dataOutput);
            }
        }

        public void persistirItem(DataOutputStream dataOutput)
        {
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_codigo));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_nombre));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_stock));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_tipoBusqueda));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_precioBase));

            BeanListaPrecios loListaPrecios = null;

            int liCantidadLstPercios = _lstPrecios.Count;

            dataOutput.writeInt(liCantidadLstPercios);

            if (liCantidadLstPercios > 0)
            {
                loListaPrecios = new BeanListaPrecios();
                loListaPrecios.persist(dataOutput, _lstPrecios);
                loListaPrecios = null;
            }
        }
        #endregion

       
    }
}
