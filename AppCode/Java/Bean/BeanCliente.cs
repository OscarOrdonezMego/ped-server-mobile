using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Collections.Generic;
using System.Collections;
using NextlibServer.RMS;
using NextlibServer.Utils;

namespace Java.Bean
{
    public class BeanCliente : Persistent , IComparable<BeanCliente>
    {
        #region Atributos
        public long _idCliente;
        public String _codigoEmpresa;
        public String _codigo;
        private String _nombre;
        private String _codigoTipoCliente;
        private String _nombreTipoCliente;
        private String _giro;
        private String _secuencia;
        private String _direccion;
        private String _deuda;
        private String _estado;
        private String _fecUltPedido;
        private String _nombUsrUltPedido;
            
        private int _orden;

        private String _campoAdicional1;
        private String _campoAdicional2;
        private String _campoAdicional3;
        private String _campoAdicional4;
        private String _campoAdicional5;
        private String _saldoCredito;
        private String _codUsuario;
        
        #endregion

        #region Propiedades
        public long idCliente
        {
            get { return _idCliente; }
            set { _idCliente = value; }
        }
        public String codUsuario
        {
            get { return _codUsuario; }
            set { _codUsuario = value; }
        }
        public String saldoCredito
        {
            get { return _saldoCredito; }
            set { _saldoCredito = value; }
        }
        public String codigoEmpresa
        {
            get { return _codigoEmpresa; }
            set { _codigoEmpresa = value; }
        }

        public String codigo
        {
            get { return _codigo; }
            set { _codigo = value; }
        }

        public String nombre
        {
            get { return _nombre; }
            set { _nombre = value; }
        }

        public String codigoTipoCliente
        {
            get { return _codigoTipoCliente; }
            set { _codigoTipoCliente = value; }
        }

        public String nombreTipoCliente
        {
            get { return _nombreTipoCliente; }
            set { _nombreTipoCliente = value; }
        }

        public String giro
        {
            get { return _giro; }
            set { _giro = value; }
        }

        public String secuencia
        {
            get { return _secuencia; }
            set { _secuencia = value; }
        }

        public String direccion
        {
            get { return _direccion; }
            set { _direccion = value; }
        }

        public String deuda
        {
            get { return _deuda; }
            set { _deuda = value; }
        }

        public String estado
        {
            get { return _estado; }
            set { _estado = value; }
        }

        public int orden 
        {
            get { return _orden; }
            set { _orden = value; }
        }

        public String fecUltPedido
        {
            get { return _fecUltPedido; }
            set { _fecUltPedido = value; }
        }

        public String nombUsrUltPedido
        {
            get { return _nombUsrUltPedido; }
            set { _nombUsrUltPedido = value; }
        }

        public String campoAdicional1
        {
            get { return _campoAdicional1; }
            set { _campoAdicional1 = value; }
        }

        public String campoAdicional2
        {
            get { return _campoAdicional2; }
            set { _campoAdicional2 = value; }
        }

        public String campoAdicional3
        {
            get { return _campoAdicional3; }
            set { _campoAdicional3 = value; }
        }

        public String campoAdicional4
        {
            get { return _campoAdicional4; }
            set { _campoAdicional4 = value; }
        }

        public String campoAdicional5
        {
            get { return _campoAdicional5; }
            set { _campoAdicional5 = value; }
        }

        #endregion

        #region Constructor
        public BeanCliente(){}
        #endregion

        #region Implementación métodos "Persistente"
        public override void persist(DataOutputStream dataOutput, List<Persistent> list)
        {
            BeanCliente loCliente;

            dataOutput.writeInt(list.Count);

            for (int i = 0; i < list.Count; i++)
            {
                loCliente = (BeanCliente)list[i];

                loCliente.persistirItem(dataOutput);
            }
        }

        public void persistirItem(DataOutputStream dataOutput)
        {
            dataOutput.writeShort(this.getIdPosition());
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_codigo));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_nombre));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_codigoTipoCliente));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_nombreTipoCliente));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_giro));

            dataOutput.writeString(StringUtils.convertNullStringToBlank(_secuencia));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_direccion));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_codigoEmpresa));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_deuda));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_estado));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_fecUltPedido));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_nombUsrUltPedido));
            
            dataOutput.writeLong(_idCliente);
        }
        #endregion

        #region Otros métodos
        public int CompareTo(BeanCliente bean)
        {
            try
            {
                int x = this.codigo.Length;
                int y = bean.codigo.Length;

                if (x == y)
                {
                    return codigo.CompareTo(bean.codigo);
                }
                else if (x > y)
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }

            catch (Exception ex)
            {
                return -1;
            }
        }
        #endregion
    }
}
