using System;
using System.Collections.Generic;
using System.Text;
using NextlibServer.RMS;
using Java.Bean;
using System.Configuration;
using NextlibServer.Utils;

namespace Java.Bean
{
    public class BeanPago : Persistent
    {
        #region Atributos
        private String _cuenta;
        private String _idDocCobranza;
        private String _idUsuario;
        private String _idCliente;
        private String _pago;
        private String _pagosoles;
        private String _pagodolares;
        private String _idFormaPago;
        private String _nroDocumento;
        private String _idBanco;
        private String _fechaDocumento;
        private String _latitud;
        private String _longitud;
        private String _fechaMovil;
        private String _celda;
        private int _errorConexion;
        private int _errorPosicion;

        private Int32 _recordIdCobranza;
        private short _posicionIdCobranza;

        private String _flgEnCobertura;

        private String _fechaDiferida;

        private string _idtipocambiosoles;
        private string _idtipocambiodolares;
        private string _tipomonedasoles;
        private string _tipomonedadolares;


        #endregion

        #region Propiedades
        public String celda
        {
            get { return _celda; }
            set { _celda = value; }
        }

        public String fechaDiferida
        {
            get { return _fechaDiferida; }
            set { _fechaDiferida = value; }
        }

        public int errorConexion
        {
            get { return _errorConexion; }
            set { _errorConexion = value; }
        }

        public int errorPosicion
        {
            get { return _errorPosicion; }
            set { _errorPosicion = value; }
        }

        public Int32 recordIdCobranza
        {
            get { return _recordIdCobranza; }
            set { _recordIdCobranza = value; }
        }

        public short posicionIdCobranza
        {
            get { return _posicionIdCobranza; }
            set { _posicionIdCobranza = value; }
        }

        public String cuenta
        {
            get { return _cuenta; }
            set { _cuenta = value; }
        }

        public String idDocCobranza
        {
            get { return _idDocCobranza; }
            set { _idDocCobranza = value; }
        }

        public String idUsuario
        {
            get { return _idUsuario; }
            set { _idUsuario = value; }
        }

        public String idCliente
        {
            get { return _idCliente; }
            set { _idCliente = value; }
        }

        public String pago
        {
            get { return _pago; }
            set { _pago = value; }
        }

        public String pagosoles
        {
            get { return _pagosoles; }
            set { _pagosoles = value; }
        }

        public String pagodolares
        {
            get { return _pagodolares; }
            set { _pagodolares = value; }
        }

        public String idFormaPago
        {
            get { return _idFormaPago; }
            set { _idFormaPago = value; }
        }

        public String nroDocumento
        {
            get { return _nroDocumento; }
            set { _nroDocumento = value; }
        }

        public String idBanco
        {
            get { return _idBanco; }
            set { _idBanco = value; }
        }

        public String fechaDocumento
        {
            get { return _fechaDocumento; }
            set { _fechaDocumento = value; }
        }
        public String latitud
        {
            get { return _latitud; }
            set { _latitud = value; }
        }
        public String longitud
        {
            get { return _longitud; }
            set { _longitud = value; }
        }
        public String fechaMovil
        {
            get { return _fechaMovil; }
            set { _fechaMovil = value; }
        }
        public String FlgEnCobertura
        {
            get { return _flgEnCobertura; }
            set { _flgEnCobertura = value; }
        }

        public String tipomonedasoles
        {
            get { return _tipomonedasoles; }
            set { _tipomonedasoles = value; }
        }

        public String tipomonedadolares
        {
            get { return _tipomonedadolares; }
            set { _tipomonedadolares = value; }
        }

        public String idtipocambiosoles
        {
            get { return _idtipocambiosoles; }
            set { _idtipocambiosoles = value; }
        }

        public String idtipocambiodolares
        {
            get { return _idtipocambiodolares; }
            set { _idtipocambiodolares = value; }
        }
        #endregion

        #region Constructor
        public BeanPago(){ }
        #endregion

        #region Implementación métoos "Persistenet"
        public List<BeanPago> recuperar(DataInputStream dataInput)
        {
            List<BeanPago> loLstPago = new List<BeanPago>();
            BeanPago loPago = null;
            
            int loCantidadLstPagos = dataInput.readInt();
            
            for (int i = 0; i < loCantidadLstPagos; i++)
            {
                loPago = new BeanPago();

                loPago.recordIdCobranza = dataInput.readInt();
                loPago.posicionIdCobranza = dataInput.readShort();

                loPago.idUsuario = dataInput.readString();
                loPago.idCliente = dataInput.readString();
                loPago.idDocCobranza = dataInput.readString();
                loPago.fechaDocumento = dataInput.readString();
                loPago.pago = dataInput.readString();
                loPago.idFormaPago = dataInput.readString();
                loPago.idBanco = dataInput.readString();
                loPago.nroDocumento = dataInput.readString();
                loPago.cuenta = dataInput.readString();

                loPago.celda = dataInput.readString();
                loPago.errorConexion = dataInput.readInt();
                loPago.errorPosicion = dataInput.readInt();

                loPago.pagosoles = dataInput.readString();
                loPago.pagodolares = dataInput.readString();

                loPago.tipomonedasoles = dataInput.readString();
                loPago.tipomonedadolares = dataInput.readString();
                loPago.idtipocambiosoles = dataInput.readString();
                loPago.idtipocambiodolares = dataInput.readString();

                loLstPago.Add(loPago);
            }
            return loLstPago;
        }

        public override void persist(DataOutputStream dataOutput, List<Persistent> list)
        {
            BeanPago loPago = null;

            dataOutput.writeInt(list.Count);

            for (int i = 0; i < list.Count; i++)
            {
                loPago = (BeanPago)list[i];
                loPago.persistirItem(dataOutput);
            }
        }

        public void persistirItem(DataOutputStream dataOutput)
        {
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_cuenta));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_idDocCobranza));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_idUsuario));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_idCliente));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_pago));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_idFormaPago));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_nroDocumento));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_idBanco));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_fechaDocumento));

            dataOutput.writeString(StringUtils.convertNullStringToBlank(_celda));
            dataOutput.writeInt(_errorConexion);
            dataOutput.writeInt(_errorPosicion);
        }
        #endregion

        /*
        public static string logRuta
        {
            get
            {
                return ConfigurationManager.AppSettings["rutaLog"].ToString();
            }
        }
        */ 
    }
}
