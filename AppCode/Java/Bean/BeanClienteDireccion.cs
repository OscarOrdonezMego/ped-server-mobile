using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Collections.Generic;
using System.Collections;
using NextlibServer.RMS;
using NextlibServer.Utils;

namespace Java.Bean
{
    public class BeanClienteDireccion : Persistent
    {
        #region Atributos
        private String _pk;
        private String _codCliente;
        private String _nombre;
        private String _latitud;
        private String _longitud;
        private String _tipo;
        private String _fechaCreacion;
        private int _error;
        private String _mensaje;
        private String _codDireccion;

        #endregion

        #region Propiedades
        public String pk
        {
            get { return _pk; }
            set { _pk = value; }
        }
        public String codCliente
        {
            get { return _codCliente; }
            set { _codCliente = value; }
        }
        public String nombre
        {
            get { return _nombre; }
            set { _nombre = value; }
        }
        public String latitud
        {
            get { return _latitud; }
            set { _latitud = value; }
        }
        public String longitud
        {
            get { return _longitud; }
            set { _longitud = value; }
        }
        public String tipo
        {
            get { return _tipo; }
            set { _tipo = value; }
        }
        public String fechaCreacion
        {
            get { return _fechaCreacion; }
            set { _fechaCreacion = value; }
        }
        public int error
        {
            get { return _error; }
            set { _error = value; }
        }
        public String mensaje
        {
            get { return _mensaje; }
            set { _mensaje = value; }
        }
        public String codDireccion
        {
            get { return _codDireccion; }
            set { _codDireccion = value; }
        }
        #endregion

        #region Constructor
        public BeanClienteDireccion() { }
        #endregion

        #region Implementación métoos "Persistenet"
        public List<BeanClienteDireccion> recuperar(DataInputStream dataInput)
        {
            List<BeanClienteDireccion> loLstClienteDireccion = new List<BeanClienteDireccion>();
            BeanClienteDireccion loClienteDireccion = null;

            int loCantidadLstClienteDirecciones = dataInput.readInt();

            for (int i = 0; i < loCantidadLstClienteDirecciones; i++)
            {
                loClienteDireccion = new BeanClienteDireccion();

                loClienteDireccion.pk = dataInput.readString();
                loClienteDireccion.codCliente = dataInput.readString();
                loClienteDireccion.latitud = dataInput.readString();
                loClienteDireccion.longitud = dataInput.readString();
                loClienteDireccion.nombre = dataInput.readString();
                loClienteDireccion.tipo = dataInput.readString();
                loClienteDireccion.fechaCreacion = dataInput.readString();
                loClienteDireccion.error = dataInput.readInt();
                loClienteDireccion.mensaje = dataInput.readString();
                loClienteDireccion.codDireccion = dataInput.readString();

                loLstClienteDireccion.Add(loClienteDireccion);
            }
            return loLstClienteDireccion;
        }

        public override void persist(DataOutputStream dataOutput, List<Persistent> list)
        {
            BeanClienteDireccion loClienteDireccion = null;

            dataOutput.writeInt(list.Count);

            for (int i = 0; i < list.Count; i++)
            {
                loClienteDireccion = (BeanClienteDireccion)list[i];
                loClienteDireccion.persistirItem(dataOutput);
            }
        }

        public void persistirItem(DataOutputStream dataOutput)
        {
            dataOutput.writeString(StringUtils.convertNullStringToBlank(pk));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_codCliente));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_latitud));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_longitud));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_nombre));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_tipo));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_fechaCreacion));
            dataOutput.writeInt(_error);
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_mensaje));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(codDireccion));
        }
        #endregion

    }
}
