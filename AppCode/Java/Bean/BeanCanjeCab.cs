using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using NextlibServer.RMS;
using NextlibServer.Utils;

namespace Java.Bean
{
    public class BeanCanjeCab : Persistent
    {
        #region Atributos
        private String _codigoCanje;
        private String _idCuenta;
        private String _idCliente;
        private String _idUsuario;
        private String _numeroDocumento;
        private String _fechaRegistro;

        private List<BeanCanjeDet> _lstCanjeDet;
        private String _latitud;
        private String _longitud;
        private String _fechaMovil;

        private String _celda;
        private int _errorConexion;
        private int _errorPosicion;
        private String _flgEnCobertura;
        private String _tipoArticulo;

       
        #endregion

        #region Propiedades
        public int errorPosicion
        {
            get { return _errorPosicion; }
            set { _errorPosicion = value; }
        }

        public int errorConexion
        {
            get { return _errorConexion; }
            set { _errorConexion = value; }
        }

        public String celda
        {
            get { return _celda; }
            set { _celda = value; }
        }

        public String fechaRegistro
        {
            get { return _fechaRegistro; }
            set { _fechaRegistro = value; }
        }

        public String numeroDocumento
        {
            get { return _numeroDocumento; }
            set { _numeroDocumento = value; }
        }

        public String codigoCanje
        {
            get { return _codigoCanje; }
            set { _codigoCanje = value; }
        }

        public String idCuenta
        {
            get { return _idCuenta; }
            set { _idCuenta = value; }
        }

        public String idCliente
        {
            get { return _idCliente; }
            set { _idCliente = value; }
        }

        public String idUsuario
        {
            get { return _idUsuario; }
            set { _idUsuario = value; }
        }

        public List<BeanCanjeDet> lstCanjeDet
        {
            get { return _lstCanjeDet; }
            set { _lstCanjeDet = value; }
        }
        public String latitud
        {
            get { return _latitud; }
            set { _latitud = value; }
        }
        public String longitud
        {
            get { return _longitud; }
            set { _longitud = value; }
        }
        public String fechaMovil
        {
            get { return _fechaMovil; }
            set { _fechaMovil = value; }
        }
        public String FlgEnCobertura
        {
            get { return _flgEnCobertura; }
            set { _flgEnCobertura = value; }
        }
        public String tipoArticulo
        {
            get { return _tipoArticulo; }
            set { _tipoArticulo = value; }
        }
        #endregion

        #region Constructor
        public BeanCanjeCab()
        {
            _lstCanjeDet = new List<BeanCanjeDet>();
        }
        #endregion

        #region Implementación métodos "Persistente"
        public List<BeanCanjeCab> recuperar(DataInputStream dataInput)
        {
            List<BeanCanjeCab> loLstCanjeCab = new List<BeanCanjeCab>();
            BeanCanjeCab loCanjeCab = null;
            BeanCanjeDet loCanjeDet = null;

            int liCantidadLstCanjeCab = dataInput.readInt();
            int liCantidadCajeDet = 0;

            for (int i = 0; i < liCantidadLstCanjeCab; i++)
            {
                loCanjeCab = new BeanCanjeCab();
                loCanjeCab.idCuenta = dataInput.readString();
                loCanjeCab.idCliente = dataInput.readString();
                loCanjeCab.idUsuario = dataInput.readString();
                loCanjeCab.numeroDocumento = dataInput.readString();
                loCanjeCab.fechaRegistro = dataInput.readString();
                loCanjeCab.tipoArticulo = dataInput.readString();

                loCanjeCab.celda = dataInput.readString();
                loCanjeCab.errorConexion = dataInput.readInt();
                loCanjeCab.errorPosicion = dataInput.readInt();

                liCantidadCajeDet = dataInput.readInt();

                if (liCantidadCajeDet > 0)
                {
                    loCanjeDet = new BeanCanjeDet();
                    loCanjeCab.lstCanjeDet = loCanjeDet.recuperar(dataInput);
                }
                loLstCanjeCab.Add(loCanjeCab);
            }
            return loLstCanjeCab;
        }

        public override void persist(DataOutputStream dataOutput, List<Persistent> list)
        {
            BeanCanjeCab loCanjeCab;

            dataOutput.writeInt(list.Count);

            for (int i = 0; i < list.Count; i++)
            {
                loCanjeCab = (BeanCanjeCab)list[i];
                loCanjeCab.persistirItem(dataOutput);
            }
        }

        public void persistirItem(DataOutputStream dataOutput)
        {
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_idCuenta));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_idCliente));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_idUsuario));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_numeroDocumento));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_fechaRegistro));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_tipoArticulo));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_celda));
            dataOutput.writeInt(_errorConexion);
            dataOutput.writeInt(_errorPosicion);

            dataOutput.writeInt(1);

            BeanCanjeDet loCanjeDet = new BeanCanjeDet();
            loCanjeDet.persistirTemp(dataOutput, lstCanjeDet);
        }
        #endregion
    }
}
