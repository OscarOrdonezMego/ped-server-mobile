using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using NextlibServer.RMS;
using NextlibServer.Utils;

namespace Java.Bean
{
    public class BeanDevolucionCab :  Persistent
    {
        #region Atributos
        private String _codigoDevolucion;
        private String _idCuenta;
        private String _idCliente;
        private String _idUsuario;
        private String _latitud;
        private String _longitud;
        private String _fechaMovil;
        private List<BeanDevolucionDet> _lstDevolucionDet;

        private String _celda;
        private int _errorConexion;
        private int _errorPosicion;
        private String _flgEnCobertura;
        private String _tipoArticulo;

        
        #endregion

        #region Propiedades
        public int errorPosicion
        {
            get { return _errorPosicion; }
            set{ _errorPosicion = value; }
        }

        public int errorConexion
        {
            get { return _errorConexion; }
            set{ _errorConexion = value; }
        }

        public String celda
        {
            get { return _celda; }
            set { _celda = value; }
        }

        public String codigoDevolucion
        {
            get { return _codigoDevolucion; }
            set { _codigoDevolucion = value; }
        }

        public String idCuenta
        {
            get { return _idCuenta; }
            set { _idCuenta = value; }
        }

        public String idCliente
        {
            get { return _idCliente; }
            set { _idCliente = value; }
        }

        public String idUsuario
        {
            get { return _idUsuario; }
            set { _idUsuario = value; }
        }

        public String latitud
        {
            get { return _latitud; }
            set { _latitud = value; }
        }
        public String longitud
        {
            get { return _longitud; }
            set { _longitud = value; }
        }
        public String fechaMovil
        {
            get { return _fechaMovil; }
            set { _fechaMovil = value; }
        }
        public List<BeanDevolucionDet> lstDevolucionDet
        {
            get { return _lstDevolucionDet; }
            set { _lstDevolucionDet = value; }
        }
        public String FlgEnCobertura
        {
            get { return _flgEnCobertura; }
            set { _flgEnCobertura = value; }
        }

        public String tipoArticulo
        {
            get { return _tipoArticulo; }
            set { _tipoArticulo = value; }
        }

        #endregion

        #region Constructor
        public BeanDevolucionCab()
        {
            _lstDevolucionDet = new List<BeanDevolucionDet>();
        }
        #endregion

        #region Implementación métodos "Persistente"
        public List<BeanDevolucionCab> recuperar(DataInputStream dataInput)
        {
            List<BeanDevolucionCab> loLstDevolucionesCab = new List<BeanDevolucionCab>();
            BeanDevolucionCab loDevolucionCab = null;
            BeanDevolucionDet loDevolucionDet = null;

            int liCantidadDevolucionesCab = dataInput.readInt();
            int liCantidadDevolucionDets = 0;

            for (int i = 0; i < liCantidadDevolucionesCab; i++)
            {
                loDevolucionCab = new BeanDevolucionCab();
                loDevolucionCab.codigoDevolucion = dataInput.readString();
                loDevolucionCab.idCuenta = dataInput.readString();
                loDevolucionCab.idCliente = dataInput.readString();
                loDevolucionCab.idUsuario = dataInput.readString();

                loDevolucionCab.celda = dataInput.readString();
                loDevolucionCab.errorConexion = dataInput.readInt();
                loDevolucionCab.errorPosicion = dataInput.readInt();
                loDevolucionCab.tipoArticulo = dataInput.readString();

                liCantidadDevolucionDets = dataInput.readInt();

                if (liCantidadDevolucionDets > 0)
                {
                    loDevolucionDet = new BeanDevolucionDet();
                    loDevolucionCab.lstDevolucionDet = loDevolucionDet.recuperar(dataInput);
                }
                loLstDevolucionesCab.Add(loDevolucionCab);
            }
            return loLstDevolucionesCab;
        }

        public override void persist(DataOutputStream dataOutput, List<Persistent> list)
        {   
            BeanDevolucionCab loDevolucionCab = null;

            dataOutput.writeInt(list.Count);

            for (int i = 0; i < list.Count; i++)
            {
                loDevolucionCab = (BeanDevolucionCab)list[i];
                loDevolucionCab.persistirItem(dataOutput);
            }
        }

        public void persistirItem(DataOutputStream dataOutput)
        {
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_codigoDevolucion));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_idCuenta));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_idCliente));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_idUsuario));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_tipoArticulo));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_celda));
            dataOutput.writeInt(_errorConexion);
            dataOutput.writeInt(_errorPosicion);

            dataOutput.writeInt(1);
            BeanDevolucionDet loDevolucionDet = new BeanDevolucionDet();
            loDevolucionDet.persistirTemp(dataOutput, _lstDevolucionDet);
        }
        #endregion
    }
}
