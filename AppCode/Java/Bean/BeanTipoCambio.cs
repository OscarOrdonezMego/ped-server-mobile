﻿using NextlibServer.RMS;
using NextlibServer.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pedidos_Mobile.AppCode.Java.Bean
{
    public class BeanTipoCambio : Persistent
    {
        private int _id;
        private String _codigo;
        private String _valor;
        private DateTime _fecha;
        private String _flag;

        public int id
        {
            get { return _id; }
            set { _id = value; }
        }

        public String codigo
        {
            get { return _codigo; }
            set { _codigo = value; }
        }

        public String valor
        {
            get { return _valor; }
            set { _valor = value; }
        }

        public DateTime fecha
        {
            get { return _fecha; }
            set { _fecha = value; }
        }

        public String flag
        {
            get { return _flag; }
            set { _flag = value; }
        }

        #region Implementación métodos "Persistente"
        public override void persist(DataOutputStream dataOutput, List<Persistent> list)
        {
            BeanTipoCambio loIdioma = null;
            dataOutput.writeInt(list.Count);

            for (int i = 0; i < list.Count; i++)
            {
                loIdioma = (BeanTipoCambio)list[i];
                loIdioma.persistirItem(dataOutput);
            }
        }

        public void persistirItem(DataOutputStream dataOutput)
        {

            dataOutput.writeInt(_id);
            dataOutput.writeStringShort(StringUtils.convertNullStringToBlank(_codigo));
            dataOutput.writeStringShort(StringUtils.convertNullStringToBlank(_valor));
            dataOutput.writeString(_fecha.ToString());
            dataOutput.writeStringShort(StringUtils.convertNullStringToBlank(_flag));
            
        }
        #endregion
    }
}