using NextlibServer.RMS;
using NextlibServer.Utils;
using System;
using System.Collections.Generic;

namespace Java.Bean
{
    public class BeanPedidoDet : Persistent
    {
        #region Atributos
        private const String ConstTipDocumento = "B"; //Para todos segun BD
        private const String constCodTransaccion = "000";
        private const String constPreEmpaque = "0.00";

        private String _empresa;
        private String _codigoArticulo;
        private String _cantidad;
        private String _bonificacion;
        private String _monto;
        private String _montoSoles; //@JBELVY
        private String _montoDolar; //@JBELVY
        private String _precioBase;
        private String _precioBaseSoles; //@JBELVY
        private String _precioBaseDolares; //@JBELVY
        private String _stock;
        private String _descuento;
        private String _codigoPedido;
        private String _nombreArticulo;
        private String _descripcion;
        private String _tabla;
        private String _tipoDescuento;

        private String _atendible;
        private String _codListaArticulo;
        private int _codAlmacen;
        private String _flete = "";
        private String _cantidadPre = "";
        private String _cantidadFrac = "";
        private String _precioFrac = "";
        private long _codBonificacion;
        private String _montoSinDescuento = "0";
        private String _bonificacionFrac = "0";
        private string _tipoMoneda = "0"; //@JBELVY


        #endregion

        #region Propiedades

        public long codBonificacion
        {
            get { return _codBonificacion; }
            set { _codBonificacion = value; }
        }
        
        public String montoSinDescuento
        {
            get { return _montoSinDescuento; }
            set { _montoSinDescuento = value; }
        }
        
        public String empresa
        {
            get { return _empresa; }
            set { _empresa = value; }
        }

        public String codigoArticulo
        {
            get { return _codigoArticulo; }
            set { _codigoArticulo = value; }
        }

        public String cantidad
        {
            get { return _cantidad; }
            set { _cantidad = value; }
        }

        public String monto
        {
            get { return _monto; }
            set { _monto = value; }
        }
        public String montoSoles
        {
            get { return _montoSoles; }
            set { _montoSoles = value; }
        }
        public String montoDolar
        {
            get { return _montoDolar; }
            set { _montoDolar = value; }
        }

        public String precioBase
        {
            get { return _precioBase; }
            set { _precioBase = value; }
        }
        public String precioBaseSoles
        {
            get { return _precioBaseSoles; }
            set { _precioBaseSoles = value; }
        }
        public String precioBaseDolares
        {
            get { return _precioBaseDolares; }
            set { _precioBaseDolares = value; }
        }

        public String stock
        {
            get { return _stock; }
            set { _stock = value; }
        }

        public String descuento
        {
            get { return _descuento; }
            set { _descuento = value; }
        }

        public String codigoPedido
        {
            get { return _codigoPedido; }
            set { _codigoPedido = value; }
        }

        public String nombreArticulo
        {
            get { return _nombreArticulo; }
            set { _nombreArticulo = value; }
        }

        public String descripcion
        {
            get { return _descripcion; }
            set { _descripcion = value; }
        }

        public String tabla
        {
            get { return _tabla; }
            set { _tabla = value; }
        }

        public String tipoDescuento 
        {
            get { return _tipoDescuento; }
            set { _tipoDescuento = value; }    
        }
        public String atendible
        {
            get { return _atendible; }
            set { _atendible = value; }
        }

        public String bonificacion
        {
            get { return _bonificacion; }
            set { _bonificacion = value; }
        }

        public String codListaArticulo
        {
            get { return _codListaArticulo; }
            set { _codListaArticulo = value; }
        }
        public int codAlmacen
        {
            get { return _codAlmacen; }
            set { _codAlmacen = value; }
        }
        public String flete
        {
            get { return _flete; }
            set { _flete = value; }
        }

        public String cantidadPre
        {
            get { return _cantidadPre; }
            set { _cantidadPre = value; }
        }
        public String cantidadFrac
        {
            get { return _cantidadFrac; }
            set { _cantidadFrac = value; }
        }
        public String precioFrac
        {
            get { return _precioFrac; }
            set { _precioFrac = value; }
        }
        public String bonificacionFrac
        {
            get { return _bonificacionFrac; }
            set { _bonificacionFrac = value; }
        }

        public String tipoMoneda
        {
            get { return _tipoMoneda; }
            set { _tipoMoneda = value; }
        }

        
        #endregion

        #region Constructor
        public BeanPedidoDet() { }
        #endregion

        #region Implementación métodos "Persistente"
        public List<BeanPedidoDet> recuperar(DataInputStream dataInput)
        {
            List<BeanPedidoDet> loLstPedidoDets = new List<BeanPedidoDet>();
            BeanPedidoDet loPedidoDet = null;
            int liCantidadPedidoDets = dataInput.readInt();

            for (int i = 0; i < liCantidadPedidoDets; i++)
            {
                loPedidoDet = new BeanPedidoDet();
                loPedidoDet.codBonificacion = dataInput.readLong();
                loPedidoDet.empresa = dataInput.readString();
                loPedidoDet.codigoArticulo = dataInput.readString();
                loPedidoDet.nombreArticulo = dataInput.readString();
                loPedidoDet.cantidad = dataInput.readString();
                loPedidoDet.precioBase = dataInput.readString();
                loPedidoDet.precioBaseSoles = dataInput.readString(); //@JBELVY
                loPedidoDet.precioBaseDolares = dataInput.readString(); //@JBELVY

                loPedidoDet.monto = dataInput.readString();
                loPedidoDet.montoSoles = dataInput.readString(); //@JBELVY
                loPedidoDet.montoDolar = dataInput.readString(); //@JBELVY
                loPedidoDet.stock = dataInput.readString();
                loPedidoDet.descuento = dataInput.readString();
                loPedidoDet.codAlmacen = dataInput.readInt();
                loPedidoDet.flete = dataInput.readString();
                loPedidoDet.cantidadPre = dataInput.readString();
                loPedidoDet.cantidadFrac = dataInput.readString();
                loPedidoDet.precioFrac = dataInput.readString();
                loPedidoDet.montoSinDescuento = dataInput.readString();
                loPedidoDet._bonificacionFrac = dataInput.readString();
                loPedidoDet._tipoMoneda = dataInput.readString(); //@JBELVY


                loLstPedidoDets.Add(loPedidoDet);
            }
            return loLstPedidoDets;
        }

        public override void persist(DataOutputStream dataOutput, List<Persistent> list)
        {
            BeanPedidoDet loPedidoDet = null;

            dataOutput.writeInt(list.Count);            

            for (int i = 0; i < list.Count; i++)
            {
                loPedidoDet = (BeanPedidoDet)list[i];
                loPedidoDet.persistirItem(dataOutput);
            }
        }

        public void persistirTemp(DataOutputStream dataOutput, List<BeanPedidoDet> list)
        {
            BeanPedidoDet loPedidoDet = null;

            dataOutput.writeInt(list.Count);

            for (int i = 0; i < list.Count; i++)
            {
                loPedidoDet = list[i];
                loPedidoDet.persistirItem(dataOutput);
            }
        }

        public void persistirItem(DataOutputStream dataOutput)
        {
            dataOutput.writeLong(_codBonificacion);
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_empresa));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_codigoArticulo));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_nombreArticulo));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_cantidad));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_precioBase));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_precioBaseSoles)); //@JBELVY
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_precioBaseDolares)); //@JBELVY

            dataOutput.writeString(StringUtils.convertNullStringToBlank(_monto));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_montoSoles)); //@JBELVY
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_montoDolar)); //@JBELVY
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_stock));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_descuento));
            dataOutput.writeInt(_codAlmacen);
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_flete));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_cantidadPre));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_cantidadFrac));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_precioFrac));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_montoSinDescuento));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_bonificacionFrac));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_tipoMoneda)); //@JBELVY
        }
        #endregion

        #region Otros métodos
        public void setDescuento(String psDescuento)
        {
            _descuento = (".".Equals(psDescuento)) ? "" : psDescuento;
        }

        public void setCantidad(String psCantidad)
        {
            _cantidad = (".".Equals(psCantidad)) ? "" : psCantidad;
        }
        #endregion
    }
}
