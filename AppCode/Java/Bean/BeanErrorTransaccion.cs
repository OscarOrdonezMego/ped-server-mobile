#region Referencias
using System;
using System.Data;
using System.Configuration;
using System.Xml;
using System.Collections.Generic;
#endregion
#region Referencias Nextel
using NextlibServer.Utils;
//using NextlibServer.RMS;
//using Java.binario.rms;
using Java.Bean;
#endregion

namespace Java.Bean
{
    public class BeanErrorTransaccion
    {
        #region Atributos
        private String _error = string.Empty;
        private Int32 _total = 0;
        private Int32 _insertados = 0;
        private Int32 _errorBD = 0;
        private Int32 _errorDuplicado = 0;
        private String _mensaje = string.Empty;

        private String _mensajeErrorInsertar = string.Empty;
        private String _mensajeInsertados = string.Empty;
        #endregion

        #region Constructor
        public BeanErrorTransaccion(){ }
        #endregion

        #region Propiedades
        public String error
        {
            get { return _error; }
            set { _error = value; }
        }

        public Int32 total
        {
            get { return _total; }
            set { _total = value; }
        }

        public Int32 insertados
        {
            get { return _insertados; }
            set { _insertados = value; }
        }

        public Int32 errorBD
        {
            get { return _errorBD; }
            set { _errorBD = value; }
        }

        public Int32 errorDuplicado
        {
            get { return _errorDuplicado; }
            set { _errorDuplicado = value; }
        }

        public String mensaje
        {
            get { return _mensaje; }
            set { _mensaje = value; }
        }

        public String mensajeErrorInsertar
        {
            get { return _mensajeErrorInsertar; }
            set { _mensajeErrorInsertar = value; }
        }

        public String mensajeInsertados
        {
            get { return _mensajeInsertados; }
            set { _mensajeInsertados = value; }
        }
        #endregion
    }
}
