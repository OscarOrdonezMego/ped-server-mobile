using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using NextlibServer.RMS;
using NextlibServer.Utils;

namespace Java.Bean
{
    public class BeanCanjeDet : Persistent
    {
        #region Atributos
        private String _idArticulo;
        private String _codigoArticulo;
        private String _cantidad;
        private String _codigoMotivo;
        private String _fechaVencimiento;
        private String _observacion;
        private String _codAlmacen;
        private String _cantidadPre;
        private String _cantidadFrac;
        private String _tipoArticulo;
        private Int32 _idCanje;
        #endregion

        #region Propiedades
        public String idArticulo
        {
            get { return _idArticulo; }
            set { _idArticulo = value; }
        }
        public String tipoArticulo
        {
            get { return _tipoArticulo; }
            set { _tipoArticulo = value; }
        }
        public String cantidadFrac
        {
            get { return _cantidadFrac; }
            set { _cantidadFrac = value; }
        }
        public String cantidadPre
        {
            get { return _cantidadPre; }
            set { _cantidadPre = value; }
        }
        public String codAlmacen
        {
            get { return _codAlmacen; }
            set { _codAlmacen = value; }
        }
        public Int32 idCanje
        {
            get { return _idCanje; }
            set { _idCanje = value; }
        }        

        public String observacion
        {
            get { return _observacion; }
            set { _observacion = value; }
        }

        public String codigoArticulo
        {
            get { return _codigoArticulo; }
            set { _codigoArticulo = value; }
        }

        public String cantidad
        {
            get { return _cantidad; }
            set { _cantidad = value; }
        }

        public String codigoMotivo
        {
            get { return _codigoMotivo; }
            set { _codigoMotivo = value; }
        }

        public String fechaVencimiento
        {
            get { return _fechaVencimiento; }
            set { _fechaVencimiento = value; }
        }
        #endregion

        #region Constructor
        public BeanCanjeDet() { }
        #endregion

        #region Implementación métodos "Persistente"
        public List<BeanCanjeDet> recuperar(DataInputStream dataInput)
        {
            List<BeanCanjeDet> list = new List<BeanCanjeDet>();
            BeanCanjeDet detalle = null;
            int cant = dataInput.readInt();
            for (int i = 0; i < cant; i++)
            {
                detalle = new BeanCanjeDet();
                detalle.recuperarItem(dataInput);
                list.Add(detalle);
            }
            return list;
        }

        public void recuperarItem(DataInputStream dataInput)
        {
            _idArticulo = dataInput.readString();
            _codigoArticulo = dataInput.readString();
            _cantidad = dataInput.readString();
            _codigoMotivo = dataInput.readString();
            _fechaVencimiento = dataInput.readString();
            _observacion = dataInput.readString();
            _codAlmacen = dataInput.readString();
            _cantidadPre = dataInput.readString();
            _cantidadFrac = dataInput.readString();
            _tipoArticulo = dataInput.readString();

        }

        public override void persist(DataOutputStream dataOutput, List<Persistent> list)
        {
            dataOutput.writeInt(list.Count);
            BeanCanjeDet loCanjeDet;

            for (int i = 0; i < list.Count; i++)
            {
                loCanjeDet = (BeanCanjeDet)list[i];
                loCanjeDet.persistirItem(dataOutput);
            }
        }

        public void persistirTemp(DataOutputStream dataOutput, List<BeanCanjeDet> list)
        {
            dataOutput.writeInt(list.Count);

            BeanCanjeDet loCanjeDet;

            for (int i = 0; i < list.Count; i++)
            {
                loCanjeDet = list[i];
                loCanjeDet.persistirItem(dataOutput);
            }
        }

        public void persistirItem(DataOutputStream dataOutput)
        {
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_idArticulo));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_codigoArticulo));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_cantidad));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_codigoMotivo));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_fechaVencimiento));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_observacion));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_cantidadFrac));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_cantidadPre));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_codAlmacen));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_tipoArticulo));
        }
        #endregion
    }
}