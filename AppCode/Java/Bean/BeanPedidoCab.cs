using System.Collections.Generic;
using NextlibServer.Utils;
using NextlibServer.RMS;
using System;
using System.Xml.Serialization;
using Nextel.BE;

namespace Java.Bean
{
    public class BeanPedidoCab : Persistent
    {
        #region Atributos
        private String _empresa;
        private String _codigoUsuario;
        private String _codigoPedido;
        private String _fechaInicio;
        private String _fechaFin;
        private String _codigoEstablecimiento;
        private String _condicionVenta;
        private String _precio;
        private String _precioSoles; //@JBELVY
        private String _precioDolar; //@JBELVY
        private String _ruta;
        private String _codigoCliente;
        private String _codigoMotivo;
        private String _montoTotal;
        private String _montoTotalSoles; //@JBELVY
        private String _montoTotalDolar; //@JBELVY
        private int _latitud;
        private int _longitud;
        private Double _doubleLatitud;
        private Double _doubleLongitud;
        private String _direccion;
        private String _direccionDespacho;
        private String _observacion; //aMendiola 20/09/2010
        private String _celda;
        private int _errorConexion;
        private int _errorPosicion;
        private String _cantidadTotal;
        
        private List<BeanPedidoDet> _lstPedidoDetalle;
        private List<BEEnvCtrlPedido> _listaCtrlPedido;

        private String flgEnCobertura;

        private String flgTerminado;
        private String _codigoPedidoServidor;

        private String _bonificacionTotal;
        private String _flgTipo;
        private int _codAlmacen;
        private String _fleteTotal;
        private String _tipoArticulo;
        private String _creditoUtilizado;
        private String _fechaRegistro;
        private bool _enEdicion = false;

        private String _tipoCambioSoles; //@JBELVY
        private String _tipoCambioDolar; //@JBELVY
        

        #endregion

        #region Propiedades
        public Double doubleLatitud
        {
            get { return _doubleLatitud; }
            set { _doubleLatitud = value; }
        }

        public Double doubleLongitud
        {
            get { return _doubleLongitud; }
            set { _doubleLongitud = value; }
        }

        public String empresa
        {
            get { return _empresa; }
            set { _empresa = value; }
        }

        public String codigoUsuario
        {
            get { return _codigoUsuario; }
            set { _codigoUsuario = value; }
        }

        public String codigoPedido
        {
            get { return _codigoPedido; }
            set { _codigoPedido = value; }
        }

        public String fechaInicio
        {
            get { return _fechaInicio; }
            set { _fechaInicio = value; }
        }

        public String fechaFin
        {
            get { return _fechaFin; }
            set { _fechaFin = value; }
        }

        public String codigoEstablecimiento
        {
            get { return _codigoEstablecimiento; }
            set { _codigoEstablecimiento = value; }
        }

        public String condicionVenta
        {
            get { return _condicionVenta; }
            set { _condicionVenta = value; }
        }

        public String precio
        {
            get { return _precio; }
            set { _precio = value; }
        }

        public String precioSoles
        {
            get { return _precioSoles; }
            set { _precioSoles = value; }
        } //@JBELVY

        public String precioDolar
        {
            get { return _precioDolar; }
            set { _precioDolar = value; }
        } //@JBELVY

        public String ruta
        {
            get { return _ruta; }
            set { _ruta = value; }
        }

        public String codigoCliente
        {
            get { return _codigoCliente; }
            set { _codigoCliente = value; }
        }

        public String codigoMotivo
        {
            get { return _codigoMotivo; }
            set { _codigoMotivo = value; }
        }

        public String montoTotal
        {
            get { return _montoTotal; }
            set { _montoTotal = value; }
        }

        public String montoTotalSoles
        {
            get { return _montoTotalSoles; }
            set { _montoTotalSoles = value; }
        } //@JBELVY

        public String montoTotalDolar
        {
            get { return _montoTotalDolar; }
            set { _montoTotalDolar = value; }
        } //@JBELVY

        public int latitud
        {
            get { return _latitud; }
            set { _latitud = value; }
        }

        public int longitud
        {
            get { return _longitud; }
            set { _longitud = value; }
        }

        public String direccion
        {
            get { return _direccion; }
            set { _direccion = value; }
        }
        public String direccionDespacho
        {
            get { return _direccionDespacho; }
            set { _direccionDespacho = value; }
        }

        public String observacion
        {
            get { return _observacion; }
            set { _observacion = value; }
        }

        public String celda
        {
            get { return _celda; }
            set { _celda = value; }
        }

        public int errorConexion
        {
            get { return _errorConexion; }
            set { _errorConexion = value; }
        }

        public int errorPosicion
        {
            get { return _errorPosicion; }
            set { _errorPosicion = value; }
        }

        public String cantidadTotal
        {
            get { return _cantidadTotal; }
            set { _cantidadTotal = value; }
        }

        public List<BeanPedidoDet> lstPedidoDetalle
        {
            get { return _lstPedidoDetalle; }
            set { _lstPedidoDetalle = value; }
        }
        public String FlgEnCobertura
        {
            get { return flgEnCobertura; }
            set { flgEnCobertura = value; }
        }
        public String FlgTerminado
        {
            get { return flgTerminado; }
            set { flgTerminado = value; }
        }
        public String CodigoPedidoServidor
        {
            get { return _codigoPedidoServidor; }
            set { _codigoPedidoServidor = value; }
        }

        public String bonificacionTotal
        {
            get { return _bonificacionTotal; }
            set { _bonificacionTotal = value; }
        }
        public String flgTipo
        {
            get { return _flgTipo; }
            set { _flgTipo = value; }
        }
        public int codAlmacen
        {
            get { return _codAlmacen; }
            set { _codAlmacen = value; }
        }
        public String fleteTotal
        {
            get { return _fleteTotal; }
            set { _fleteTotal = value; }
        }

        public String tipoArticulo
        {
            get { return _tipoArticulo; }
            set { _tipoArticulo = value; }
        }

        public String creditoUtilizado
        {
            get { return _creditoUtilizado; }
            set { _creditoUtilizado = value; }
        }

        public List<BEEnvCtrlPedido> listaCtrlPedido
        {
            get { return _listaCtrlPedido; }
            set { _listaCtrlPedido = value; }
        }

        public String fechaRegistro
        {
            get { return _fechaRegistro; }
            set { _fechaRegistro = value; }
        }

        public bool enEdicion
        {
            get { return _enEdicion; }
            set { _enEdicion = value; }
        }

        public String tipoCambioSoles
        {
            get { return _tipoCambioSoles; }
            set { _tipoCambioSoles = value; }
        } //@JBELVY

        public String tipoCambioDolar
        {
            get { return _tipoCambioDolar; }
            set { _tipoCambioDolar = value; }
        } //@JBELVY
        #endregion

        #region Implementación métodos "Persistente"
        public List<BeanPedidoCab> recuperar(DataInputStream dataInput)
        {
            List<BeanPedidoCab> loLstPedidosCab = new List<BeanPedidoCab>();
            BeanPedidoCab loPedidoCab = null;
            BeanPedidoDet loPedidoDet = null;

            int liCantidadPedidosCab = dataInput.readInt();
            int liCantidadPedidoDets = 0;

            for (int i = 0; i < liCantidadPedidosCab; i++)
            {
                loPedidoCab = new BeanPedidoCab();

                loPedidoCab.empresa = dataInput.readString();
                loPedidoCab.codigoUsuario = dataInput.readString();
                loPedidoCab.fechaInicio = dataInput.readLong().ToString();
                loPedidoCab.fechaFin = dataInput.readLong().ToString();
                loPedidoCab.condicionVenta = dataInput.readString();

                //cabPedido.precio = dataInput.readString();
                loPedidoCab.codigoCliente = dataInput.readString();
                loPedidoCab.codigoMotivo = dataInput.readString();
                loPedidoCab.montoTotal = dataInput.readString();
                loPedidoCab.montoTotalSoles = dataInput.readString(); //@JBELVY
                loPedidoCab.montoTotalDolar = dataInput.readString(); //@JBELVY

                loPedidoCab.latitud = dataInput.readInt();
                loPedidoCab.longitud = dataInput.readInt();
                loPedidoCab.celda = dataInput.readString();
                loPedidoCab.errorConexion = dataInput.readInt();
                loPedidoCab.errorPosicion = dataInput.readInt();

                loPedidoCab.observacion = dataInput.readString(); //aMendiola 21/09/2010
                loPedidoCab.cantidadTotal = dataInput.readString();
                loPedidoCab.codAlmacen = dataInput.readInt();
                loPedidoCab.fleteTotal = dataInput.readString();
                loPedidoCab._tipoArticulo = dataInput.readString();
                loPedidoCab.creditoUtilizado = dataInput.readString();

                loPedidoCab.tipoCambioSoles = dataInput.readString(); //@JBELVY
                loPedidoCab.tipoCambioDolar = dataInput.readString(); //@JBELVY

                liCantidadPedidoDets = dataInput.readInt();

                if (liCantidadPedidoDets > 0)
                {
                    loPedidoDet = new BeanPedidoDet();
                    loPedidoCab.lstPedidoDetalle = loPedidoDet.recuperar(dataInput);
                }
                loLstPedidosCab.Add(loPedidoCab);
            }
            return loLstPedidosCab;
        }

        public override void persist(DataOutputStream dataOutput, List<Persistent> list)
        {
            BeanPedidoCab loPedidoCab = null;

            dataOutput.writeInt(list.Count);

            for (int i = 0; i < list.Count; i++)
            {
                loPedidoCab = (BeanPedidoCab)list[i];

                loPedidoCab.persistirItem(dataOutput);
            }
        }

        public void persistirItem(DataOutputStream dataOutput)
        {
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_empresa));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_codigoUsuario));
            dataOutput.writeString(_fechaInicio);
            dataOutput.writeString(_fechaFin);
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_condicionVenta));

            //dataOutput.writeString(StringUtils.stringNullToBlank(precio));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_codigoCliente));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_codigoMotivo));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_montoTotal));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_montoTotalSoles)); //@JBELVY
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_montoTotalDolar)); //@JBELVY

            dataOutput.writeInt(_latitud);
            dataOutput.writeInt(_longitud);

            dataOutput.writeString(StringUtils.convertNullStringToBlank(_celda));
            dataOutput.writeInt(_errorConexion);
            dataOutput.writeInt(_errorPosicion);

            dataOutput.writeString(StringUtils.convertNullStringToBlank(_observacion)); //aMendiola 20/09/2010

            dataOutput.writeString(StringUtils.convertNullStringToBlank(_cantidadTotal));

            dataOutput.writeString(StringUtils.convertNullStringToBlank(_fleteTotal));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_tipoArticulo));
            dataOutput.writeInt(_codAlmacen);

            dataOutput.writeString(StringUtils.convertNullStringToBlank(_tipoCambioSoles)); //@JBELVY
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_tipoCambioDolar)); //@JBELV

            dataOutput.writeInt(1);
            BeanPedidoDet loPedidoDet = new BeanPedidoDet();
            loPedidoDet.persistirTemp(dataOutput, _lstPedidoDetalle);
        }
        #endregion
        //private Int64 fnFechaToLong(String psFecha)
        //{
        //    Int64 liResultado;
        //    DateTime lo = DateTime.Now;
        //    liResultado = lo.Second;
        //    return liResultado;
        //}

        public string ToXML()
        {
            var stringwriter = new System.IO.StringWriter();
            var serializer = new XmlSerializer(this.GetType());
            serializer.Serialize(stringwriter, this);
            return stringwriter.ToString();
        }
    }
}

