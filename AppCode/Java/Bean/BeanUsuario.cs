using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NextlibServer.Utils;

namespace Java.Bean
{
    public class BeanUsuario
    {
        #region Atributos
        private String _codigo;
        private String _password;
        private String _empresa;
        private String _nombre;
        private String _fechaMovil;
        private String _numero;
        #endregion

        #region Propiedades
        public String fechaMovil
        {
            get { return _fechaMovil.Trim(); }
            set { _fechaMovil = value; }
        }

        public String codigo
        {
            get { return _codigo.Trim(); }
            set { _codigo = value; }
        }

        public String password
        {
            get { return _password.Trim(); }
            set { _password = value; }
        }
        public String numero
        {
            get { return _numero.Trim(); }
            set { _numero = value; }
        }
        public String empresa
        {
            get { return _empresa.Trim(); }
            set { _empresa = value; }
        }

        public String nombre
        {
            get { return _nombre.Trim(); }
            set { _nombre = value; }
        }
        #endregion

        #region Constructor
        public BeanUsuario() {}
        #endregion
    }
}