using System;
using System.Collections.Generic;
using System.Text;
using NextlibServer.RMS;
using Java.Bean;
using System.Configuration;
using NextlibServer.Utils;

namespace Java.Bean
{
    public class BeanIdioma : Persistent
    {
        #region Atributos
        private String _valor;
        private String _id;
        #endregion

        #region Propiedades
        public String valor
        {
            get { return _valor; }
            set { _valor = value; }
        }

        public String id
        {
            get { return _id; }
            set { _id = value; }
        }
        #endregion

        #region Implementación métodos "Persistente"
        public override void persist(DataOutputStream dataOutput, List<Persistent> list)
        {
            BeanIdioma loIdioma = null;
            dataOutput.writeInt(list.Count);

            for (int i = 0; i < list.Count; i++)
            {
                loIdioma = (BeanIdioma)list[i];
                loIdioma.persistirItem(dataOutput);
            }
        }

        public void persistirItem(DataOutputStream dataOutput)
        {
            dataOutput.writeStringShort(StringUtils.convertNullStringToBlank(_valor));
            dataOutput.writeStringByte(StringUtils.convertNullStringToBlank(_id));
        }
        #endregion
    }
}
