using System;
using System.Collections.Generic;
using NextlibServer.RMS;
using NextlibServer.Utils;

namespace Java.Bean
{
    public class BeanDevolucionDet : Persistent
    {
        #region Atributos
        private String _idArticulo;
        private String _codigoArticulo;
        private String _cantidad;
        private String _codigoMotivo;
        private String _fechaVencimiento;
        private String _observacion;
        private String _codAlmacen;
        private String _cantidadPre;
        private String _cantidadFrac;
        private String _tipoArticulo;
        #endregion

        #region Propiedades
        public String idArticulo
        {
            get { return _idArticulo; }
            set { _idArticulo = value; }
        }
        public String tipoArticulo
        {
            get { return _tipoArticulo; }
            set { _tipoArticulo = value; }
        }
        public String cantidadFrac
        {
            get { return _cantidadFrac; }
            set { _cantidadFrac = value; }
        }
        public String cantidadPre
        {
            get { return _cantidadPre; }
            set { _cantidadPre = value; }
        }
        public String codAlmacen
        {
            get { return _codAlmacen; }
            set { _codAlmacen = value; }
        }

        public String observacion
        {
            get { return _observacion; }
            set { _observacion = value; }
        }

        public String codigoArticulo
        {
            get { return _codigoArticulo; }
            set { _codigoArticulo = value; }
        }

        public String cantidad
        {
            get { return _cantidad; }
            set { _cantidad = value; }
        }

        public String codigoMotivo
        {
            get { return _codigoMotivo; }
            set { _codigoMotivo = value; }
        }

        public String fechaVencimiento
        {
            get { return _fechaVencimiento; }
            set { _fechaVencimiento = value; }
        }
        #endregion

        #region Constructor
        public BeanDevolucionDet() { }
        #endregion

        #region Implementación métodos "Persistente"
        public List<BeanDevolucionDet> recuperar(DataInputStream dataInput)
        {
            List<BeanDevolucionDet> lstDevolucionDet = new List<BeanDevolucionDet>();
            BeanDevolucionDet loDevolucionDet = null;
            int liCantidadDevolucionDet = dataInput.readInt();

            for (int i = 0; i < liCantidadDevolucionDet; i++)
            {
                loDevolucionDet = new BeanDevolucionDet();
                loDevolucionDet.recuperarItem(dataInput);
                lstDevolucionDet.Add(loDevolucionDet);
            }
            return lstDevolucionDet;
        }

        public void recuperarItem(DataInputStream dataInput)
        {
            _idArticulo = dataInput.readString();
            _codigoArticulo = dataInput.readString();
            _cantidad = dataInput.readString();
            _codigoMotivo = dataInput.readString();
            _fechaVencimiento = dataInput.readString();
            _observacion = dataInput.readString();
            _codAlmacen = dataInput.readString();
            _cantidadPre = dataInput.readString();
            _cantidadFrac = dataInput.readString();
            _tipoArticulo = dataInput.readString();
        }

        public override void persist(DataOutputStream dataOutput, List<Persistent> list)
        {
            /*
            dataOutput.writeInt(list.Count);

            BeanDetalle loDetalle = null;

            for (int i = 0; i < list.Count; i++)
            {
                loDetalle = (BeanDetalle)list[i];
                loDetalle.persistirItem(dataOutput);
            }
            */ 
        }

        public void persistirTemp(DataOutputStream dataOutput, List<BeanDevolucionDet> list)
        {
            dataOutput.writeInt(list.Count);
            BeanDevolucionDet loDevolucionDet = null;

            for (int i = 0; i < list.Count; i++)
            {
                loDevolucionDet = list[i];
                loDevolucionDet.persistirItem(dataOutput);
            }
        }

        public void persistirItem(DataOutputStream dataOutput)
        {
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_idArticulo));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_codigoArticulo));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_cantidad));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_codigoMotivo));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_fechaVencimiento));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_observacion));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_cantidadFrac));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_cantidadPre));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_codAlmacen));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_tipoArticulo));
        }
        #endregion
    }
}
