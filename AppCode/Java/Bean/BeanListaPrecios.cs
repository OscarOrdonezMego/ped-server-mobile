using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Collections.Generic;
using System.Collections;
using NextlibServer.RMS;
using NextlibServer.Utils;

namespace Java.Bean
{
    public class BeanListaPrecios : Persistent
    {
        #region Atributso
        private String _codigoCanal;
        private String _precioBase;
        private String _codigoProducto;
        private String _codigoCondVta;
        #endregion

        #region Propiedades
        public String codigoCanal
        {
            get { return _codigoCanal; }
            set { _codigoCanal = value; }
        }

        public String precioBase
        {
            get { return _precioBase; }
            set { _precioBase = value; }
        }

        public String codigoProducto
        {
            get { return _codigoProducto; }
            set { _codigoProducto = value; }
        }

        public String codigoCondVta
        {
            get { return _codigoCondVta; }
            set { _codigoCondVta = value; }
        }
        #endregion

        #region Construtor
        public BeanListaPrecios(){ }
        #endregion

        #region Implementación método "Persistente"
        public override void persist(DataOutputStream dataOutput, List<Persistent> list)
        {
            BeanListaPrecios loListaPrecios = null;

            dataOutput.writeInt(list.Count);

            for (int i = 0; i < list.Count; i++)
            {
                loListaPrecios = (BeanListaPrecios)list[i];
                loListaPrecios.persistirItem(dataOutput);
            }
        }

        public void persistirItem(DataOutputStream dataOutput)
        {
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_codigoCanal));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_precioBase));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_codigoCondVta));
        }
        #endregion
    }
}