﻿using NextlibServer.RMS;
using NextlibServer.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Java.Bean
{
    public class BeanDocumento : Persistent
    {

        #region Atributos
        private String _idDocTipo;
        private String _idDocTipoMotivo;
        private String _fechaMovil;
        private String _idUsuario;
        private String _idCliente;
        private String _latitud;
        private String _longitud;
        private String _serie;
        private String _correlativo;
        private String _idPago;
        private String _montosoles;
        private String _montodolares;
        private long _rangocorrelativo;

        private int _errorConexion;
        private int _errorPosicion;
        #endregion

        #region Propiedades
        public String idDocTipo
        {
            get { return _idDocTipo; }
            set { _idDocTipo = value; }
        }
        public String idDocTipoMotivo
        {
            get { return _idDocTipoMotivo; }
            set { _idDocTipoMotivo = value; }
        }

        public String fechaMovil
        {
            get { return _fechaMovil; }
            set { _fechaMovil = value; }
        }
        public String idUsuario
        {
            get { return _idUsuario; }
            set { _idUsuario = value; }
        }
        public String idCliente
        {
            get { return _idCliente; }
            set { _idCliente = value; }
        }
        public String latitud
        {
            get { return _latitud; }
            set { _latitud = value; }
        }
        public String longitud
        {
            get { return _longitud; }
            set { _longitud = value; }
        }
        public String serie
        {
            get { return _serie; }
            set { _serie = value; }
        }
        public String correlativo
        {
            get { return _correlativo; }
            set { _correlativo = value; }
        }
        public String idPago
        {
            get { return _idPago; }
            set { _idPago = value; }
        }
        public String montosoles
        {
            get { return _montosoles; }
            set { _montosoles = value; }
        }
        public String montodolares
        {
            get { return _montodolares; }
            set { _montodolares = value; }
        }
        public long rangocorrelativo
        {
            get { return _rangocorrelativo; }
            set { _rangocorrelativo = value; }
        }

        #endregion

        #region Constructor
        public BeanDocumento() { }
        #endregion

        #region Implementación métoos "Persistenet"
        public List<BeanDocumento> recuperar(DataInputStream dataInput)
        {
            List<BeanDocumento> loLstPago = new List<BeanDocumento>();
            BeanDocumento loPago = null;

            int loCantidadLstPagos = dataInput.readInt();

            for (int i = 0; i < loCantidadLstPagos; i++)
            {
                loPago = new BeanDocumento();
                loPago.idDocTipo = dataInput.readString();
                loPago.idDocTipoMotivo = dataInput.readString();
                loPago.fechaMovil = dataInput.readString();
                loPago.idUsuario = dataInput.readString();
                loPago.idCliente = dataInput.readString();
                loPago.latitud = dataInput.readString();
                loPago.longitud = dataInput.readString();
                loPago.serie = dataInput.readString();
                loPago.correlativo = dataInput.readString();
                loPago.idPago = dataInput.readString();
                loPago.montosoles = dataInput.readString();
                loPago.montodolares = dataInput.readString();
                loPago.rangocorrelativo = dataInput.readInt();
                loLstPago.Add(loPago);
            }
            return loLstPago;
        }

        public override void persist(DataOutputStream dataOutput, List<Persistent> list)
        {
            BeanDocumento loPago = null;

            dataOutput.writeInt(list.Count);

            for (int i = 0; i < list.Count; i++)
            {
                loPago = (BeanDocumento)list[i];
                loPago.persistirItem(dataOutput);
            }
        }

        public void persistirItem(DataOutputStream dataOutput)
        {
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_idDocTipo));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_idDocTipoMotivo));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_fechaMovil));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_idUsuario));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_idCliente));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_latitud));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_longitud));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_serie));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_correlativo));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_idPago));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_montosoles));
            dataOutput.writeString(StringUtils.convertNullStringToBlank(_montodolares));
            dataOutput.writeLong(_rangocorrelativo);
            dataOutput.writeInt(_errorConexion);
            dataOutput.writeInt(_errorPosicion);
        }
        #endregion
    }
}