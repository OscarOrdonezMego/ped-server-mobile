using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;
using System.Collections.Specialized;
using System.IO.Compression;
using System.Drawing;
using Java.Configuracion;
using System.Configuration;
using System.Web;
using System.Xml;

namespace Nextel.BE
{
    public static class Util
    {
        #region CONSTANTES
        public static int CONSRMSCANTREG = 2;
        public static int CONSRMSCANTREGBUSQUEDA = 5;
        public static Double CONSDIVGPS = 6000000;
        public static String CONSCARIMAGEN = "CargaJava\\Images";
        public static String CONSRESSCRIPT = "DROP TABLE IF EXISTS CFRespuesta;" + Environment.NewLine + "CREATE TABLE IF NOT EXISTS CFRespuesta(IdRespuesta INTEGER not null,Respuesta VARCHAR(100) not null);" + Environment.NewLine + "INSERT INTO CFRespuesta VALUES(";
        public static String CONSRESSCRIPT2 = "INSERT INTO CFRespuesta VALUES(";
        
        #endregion

        #region METODOS
        
        
        public static String fnListatoString(List<String> poLista)
        {
            StringBuilder loSb = new StringBuilder();
            foreach (String ls in poLista)
            {
                loSb.Append("," + ls);
            }
            return loSb.ToString().Substring(1);
        }
        public static String fnFecha(String psFecha,String psFormatoIn,String psFormatoOut, out String psOutfecha, Boolean pbAumentarDia)
        {
            String lsResultado;
            Int32 liNumDia = (pbAumentarDia) ? 1 : 0;
            DateTime lfFecha;
            lsResultado = (DateTime.TryParseExact(psFecha, psFormatoIn, System.Globalization.CultureInfo.CurrentCulture, DateTimeStyles.None, out lfFecha)) ? lfFecha.AddDays(liNumDia).ToString(psFormatoOut) : DateTime.Now.ToString(psFormatoOut);
            psOutfecha = DateTime.ParseExact(lsResultado, psFormatoOut, System.Globalization.CultureInfo.CurrentCulture).AddDays(-1*liNumDia).ToString(psFormatoIn);
            return lsResultado;
        }

        public static String fnCambioPipexBr(String psCadena)
        {
            return psCadena.Replace("|", "<br />");
        }

        public static byte[] comprimir(byte[] data)
        {
            MemoryStream stream = new MemoryStream();
            ZipOutputStream oZipStream = new ZipOutputStream(stream);

            ZipEntry entry = new ZipEntry("datos.bin");
            entry.Size = data.Length;
            oZipStream.PutNextEntry(entry);
            oZipStream.Write(data, 0, data.Length);
            oZipStream.CloseEntry();
            oZipStream.Finish();
            oZipStream.Close();

            byte[] oBuffer = stream.ToArray();
            return oBuffer;
        }

        public static byte[] desComprimir(byte[] data)
        {
            MemoryStream mem = new MemoryStream(data);
            ZipInputStream zin = new ZipInputStream(mem);
            ZipEntry zen = zin.GetNextEntry();
            byte[] buf = new byte[zen.Size];
            zin.Read(buf, 0, buf.Length);
            return buf;
        }


        public static DateTime fnLongToFecha(Int64 piFecha)
        {
            DateTime lfRetorno;
            DateTime lfFecBase = new DateTime(1970, 1, 1, 0, 0, 0);
            lfRetorno = new DateTime((piFecha * 10000) + lfFecBase.Ticks);
            return lfRetorno;
        }
        public static Dictionary<String,String> fnListaRequest(String psString)
        {
            Dictionary<String, String> loDic = new Dictionary<string, string>();
            String[] laCadena=psString.Split('&');
            for (int i = 0; i < laCadena.Length; i++)
            {
                loDic.Add(laCadena[i].Split('=')[0], laCadena[i].Split('=')[1]);
            }
            return loDic;
        }

        public static String fnResScript(int liIdRespuesta, String psMensaje)
        {
            return CONSRESSCRIPT + liIdRespuesta + ",'" + fnReplace(psMensaje) + "');";
        }
        public static String fnReplace(String psString)
        {
            String lsRespuesta ;
            lsRespuesta = psString.Replace("'", "_");
            lsRespuesta = lsRespuesta.Replace("\"", "_");
            return lsRespuesta;
        }
        #region Metodo sincronizar imagenes
        public static byte[] Image2Bytes(String sTemp, Image img)
        {
            //string sTemp = Path.GetTempFileName();
            FileStream fs = new FileStream(sTemp, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            img.Save(fs, System.Drawing.Imaging.ImageFormat.Png);
            fs.Position = 0;
            //
            int imgLength = Convert.ToInt32(fs.Length);
            byte[] bytes = new byte[imgLength];
            fs.Read(bytes, 0, imgLength);
            fs.Close();
            return bytes;
        }

        public static Image Bytes2Image(byte[] bytes)
        {
            if (bytes == null) return null;
            //
            MemoryStream ms = new MemoryStream(bytes);
            Bitmap bm = null;
            try
            {
                bm = new Bitmap(ms);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
            return bm;
        }

        public static bool verificaDirectorio(string directorio)
        {
            try
            {
                DirectoryInfo di = new DirectoryInfo(directorio);
                if (!di.Exists)
                {
                    di.Create();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool borrarDirectorio(string directorio)
        {
            try
            {
                DirectoryInfo di = new DirectoryInfo(directorio);
                if (di.Exists)
                {
                    di.Delete(true);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #endregion

        public static String fnMobileVersion()
        {
            string ubicacion_padre = "";
            string nombre = "";
            try
            {
                string sFilename = HttpContext.Current.Server.MapPath("~");
                ubicacion_padre = ConfigurationManager.AppSettings["ubicacion_version"];
                string[] niveles = ubicacion_padre.Split('/');
                if (niveles.Length == 0)
                {
                    throw new Exception("parámetro sin datos");
                }
                else
                {
                    nombre = niveles[niveles.Length - 1];
                    for (int i = 0; i < niveles.Length - 1; i++)
                    {
                        if (niveles[i].Equals(".."))
                        {
                            sFilename = Path.GetDirectoryName(sFilename);
                        }
                    }

                }
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(sFilename + "/" + nombre);
                XmlElement rootElem = xmlDoc.DocumentElement;
                if (rootElem != null)
                {
                    foreach (XmlElement elem in rootElem.ChildNodes)
                    {
                        if (elem.Name.Equals("objeto") && elem.Attributes["name"].Value.Equals("android"))
                        {
                            foreach (XmlElement elem1 in elem.ChildNodes)
                            {
                                if (elem1.Name.Equals("propiedad") && elem1.Attributes["name"].Value.Equals("version"))
                                {
                                    return elem1.Attributes["value"].Value;
                                }
                            }
                        }
                    }
                    return "0.0.0";
                }
                else
                {
                    return "0.0.0";
                }
            }
            catch (Exception e)
            {
                return "0.0.0";
            }
        }



    }

    #region ENUM
    public enum EnumResultado
    {
        IncUsuario = 1,
        IncPassword = 2,
        Ok = 3,
        ConexionDB=4
    }
    public enum EnumResGrabJava
    {
        TodoMal = -1,
        AlgunoMal = 0,
        TodoOk = 1,
        NoMsg = 2
    }
    public enum EnumTipoMensaje//para los mensajes del masterpage
    {
        OK,
        Error
    }
    public enum EnumTipoPagina//para ver si es Nuevo o editar (Mantenimiento)
    {
        Nuevo,
        Editar
    }
    public enum EnumTipoTransaccion//para el manejo de las imagenes en el tracking
    {
        General = 1,
    }
    #endregion

 
}
