using System;
using System.Data;
using System.Collections.Generic;
using NextlibServer.Main;
using Java.Bean;
using Java.DatabaseAccess;
using Nextel.BE;
using HttpUtils;
using Pedidos_Mobile.AppCode.Java.BeanAndroid;

namespace Java.Control
{
    public class ControlJava : ControlBase
    {
        #region M�todos de interacci�n con el m�vil

        /// <summary>
        /// M�todo de Validaci�n de usuario
        /// </summary>
        /// <param name="psXml">XML con los datos del usuario y el equipo utilizado</param>
        /// <returns>XML con los datos del usuario (incluyendo el nombre) y los permisos de aplicaci�n</returns>
        public static BEUsuario fnValidarUsuarioAndroid(BEUsuario psUsuario)
        {
            BEUsuario psUsuariorp = new BEUsuario();

            #region CONSULTA A GENERADOR

            //obtiene los permisos
            psUsuariorp.configuracion = DBJava.obtenerConfiguracionDB();

            #endregion CONSULTA A GENERADOR
            try
            {
                BeanUsuario lousuario = new BeanUsuario();
                lousuario.codigo = psUsuario.usuario;
                lousuario.password = psUsuario.clave.ToUpper();
                lousuario.numero = psUsuario.numero;
                DataSet data = DBJava.fnSelValidarUsuario(lousuario);

                foreach (DataRow drow in data.Tables[0].Rows)
                {

                    psUsuariorp.codVendedor = drow["CODIGO"].ToString().Trim();
                    psUsuariorp.nombre = drow["NOMBRE"].ToString().Trim();
                    psUsuariorp.perfil = drow["PERFIL"].ToString().Trim();
                    psUsuariorp.flgNServicesObligatorio = drow["flgNServicesObligatorio"].ToString().Trim();

                }

                if (!psUsuariorp.codVendedor.Equals(""))
                {
                    DBJava.fnRegistrarNumeroUsuario(lousuario);
                    psUsuariorp.usuario = psUsuario.usuario;
                    psUsuariorp.clave = psUsuario.clave;
                    psUsuariorp.idResultado = 1;
                    psUsuariorp.resultado = "Bienvenido " + psUsuariorp.nombre;
                }
                else
                {
                    psUsuariorp = new BEUsuario();
                    psUsuariorp.usuario = psUsuario.usuario;
                    psUsuariorp.clave = psUsuario.clave;
                    psUsuariorp.idResultado = -1;
                    psUsuariorp.resultado = "Usuario/Password Incorrecto";

                }
                psUsuariorp.currentVersion = Util.fnMobileVersion();

            }
            catch (Exception ex)
            {
                psUsuariorp = new BEUsuario();
                psUsuariorp.usuario = psUsuario.usuario;
                psUsuariorp.clave = psUsuario.clave;
                psUsuariorp.idResultado = 0;
                psUsuariorp.resultado = ex.Message;

                registerLog("fnValidarUsuario.Error: " + ex);
            }



            return psUsuariorp;
        }

        #region Sincronizacion

        /// <summary>
        /// Funci�n de sincronizacion para Android
        /// </summary>
        /// <param name="picodUsuario">el usuario que va a sincronizar data</param>
        /// <returns></returns>
        public static BESincronizar fnSinScriptString(String picodUsuario, String piTelefono)
        {
            BESincronizar resultado = DBJava.fnSinScriptString(picodUsuario, piTelefono);
            return resultado;
        }

        public static BESincronizar fnSincronizarConfiguracion()
        {
            var resultado = DBJava.fnSincronizarConfiguracion();
            return resultado;
        }

        #endregion

        #region Registro de transaccion

        /// <summary>
        /// Funci�n que registra el pedido Android y devuelve mensaje de retorno 
        /// del resultado del registro
        /// </summary>
        /// <param name="polistapedidos">Pedido con detalle de productos</param>
        /// <returns>respuesta del registro de pedido</returns>
        public static String fnRegistrarPedidoAndroid(BEEnvCabPedido poPedidoCab)
        {
            registerLog("fnRegistrarPedidoAndroid.Input: " + poPedidoCab);
            BeanPedidoCab lobeancabfinal;
            try
            {

                #region Insertar Transaccion
                registerLog("poPedidoCab.codDireccion: " + poPedidoCab.codDireccion);

                lobeancabfinal = poPedidoCab.cambiabeanes();
                registerLog("lobeancabfinal.direccion: " + lobeancabfinal.direccion);
                List<Object> lbLstResultado = DBJava.fnInsPedidoUpdate(lobeancabfinal, "");

                if ((Boolean)lbLstResultado[0])
                {
                    return "1;Grabado correctamente";
                }
                else
                {
                    return "-1;" + lbLstResultado[1];
                }
                #endregion

            }
            catch (Exception ex)
            {
                registerLog("fnRegistrarPedidoAndroid.Error: " + ex.Message + " : " + ex.StackTrace);
                return "-1;" + ex.Message;
            }
        }

        public static String fnRegistrarProspectoAndroid(BEProspecto poProspecto)
        {
            String resultado = "";
            Int32 idResultado = 0;
            //BEResponseGrabarProspecto beanResponse;

            registerLog("fnRegistrarProspectoAndroid.Input: " + poProspecto);

            try
            {
                #region Insertar Transaccion

                List<object> lbLstResultado = DBJava.fnInsProspecto(poProspecto);

                if (lbLstResultado[0].Equals("") && lbLstResultado[1].Equals(""))
                {
                    idResultado = (Int32)EnumResGrabJava.TodoOk;
                    resultado = Constantes.ValidarGrabar.GRABO_CORRECTO;
                }
                else
                {
                    idResultado = (Int32)EnumResGrabJava.TodoMal;
                    resultado = "" + lbLstResultado[1];
                }

                //beanResponse = new BEResponseGrabarProspecto(resultado,idResultado);

                #endregion
            }
            catch (Exception ex)
            {
                registerLog("fnRegistrarProspectoAndroid.Error: " + ex.Message + " : " + ex.StackTrace);
                idResultado = (Int32)EnumResGrabJava.TodoMal;
                resultado = ex.Message;
                //beanResponse = new BEResponseGrabarProspecto(ex.Message, (Int32)EnumResGrabJava.TodoMal);
            }

            //return beanResponse;
            return idResultado + ";" + resultado;
        }

        #region Validacion Android

        /// <summary>
        /// Funci�n que determina si se valida o registra el pedido Android        
        /// </summary>
        /// <param name="poPedidoCab">Pedido con detalle de productos</param>
        /// <returns>lista de detalles actualizados</returns>
        public static BEValidaPedidoAndroid fnRegistrarPedidoAndroid_Validar(BEEnvCabPedido poPedidoCab)
        {
            BEValidaPedidoAndroid loBEValidaPedidoAndroid;
            registerLog("fnValidarRegistroPedidoAndroid.Input: " + poPedidoCab);
            BeanPedidoCab lobeancabfinal;
            String resultado = "";
            Int32 idResultado = 0;
            try
            {
                if (poPedidoCab.FlgEliminar.Equals("T"))
                {
                    List<Boolean> lbLstResultado = DBJava.fnDelPedidoDir(poPedidoCab.CodigoPedidoServidor, poPedidoCab.tipoArticulo, poPedidoCab.enEdicion);
                    if (lbLstResultado[0])
                    {
                        idResultado = (Int32)EnumResGrabJava.TodoOk;
                        resultado = Constantes.ValidarGrabar.GRABO_CORRECTO;
                    }
                    else
                    {
                        idResultado = (Int32)EnumResGrabJava.TodoMal;
                        resultado = "" + lbLstResultado[1];
                    }
                    //crear bean de respuesta
                    loBEValidaPedidoAndroid = new BEValidaPedidoAndroid(resultado, idResultado);
                }
                else
                {
                    #region Insertar Transaccion
                    registerLog("poPedidoCab.codDireccion: " + poPedidoCab.codDireccion);
                    lobeancabfinal = poPedidoCab.cambiabeanes();
                    registerLog("lobeancabfinal.direccion: " + lobeancabfinal.direccion);


                    if (poPedidoCab.FlgTerminado.Equals("T"))//pedido ya validado y sin editar
                    {//grabar pedido
                        //List<Object> lbLstResultado = DBJava.fnInsPedidoDir(lobeancabfinal, "");
                        //String xml = lobeancabfinal.ToXML();
                        List<Object> lbLstResultado = DBJava.fnInsPedidoUpdate(lobeancabfinal, "");

                        if ((Boolean)lbLstResultado[0])
                        {
                            idResultado = (Int32)EnumResGrabJava.TodoOk;
                            resultado = Constantes.ValidarGrabar.GRABO_CORRECTO;
                        }
                        else
                        {
                            idResultado = (Int32)EnumResGrabJava.TodoMal;
                            resultado = "" + lbLstResultado[1];
                        }
                        //crear bean de respuesta
                        loBEValidaPedidoAndroid = new BEValidaPedidoAndroid(resultado, idResultado);
                    }
                    else
                    {//cotizar                                     
                        loBEValidaPedidoAndroid = DBJava.fnInsPedidoDir_Validar(lobeancabfinal, "");
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                registerLog("fnValidarRegistroPedidoAndroid.Error: " + ex.Message + " : " + ex.StackTrace);
                //return "-1;" + ex.Message;                
                loBEValidaPedidoAndroid = new BEValidaPedidoAndroid(ex.Message, (Int32)EnumResGrabJava.TodoMal);
            }
            return loBEValidaPedidoAndroid;
        }


        public static String fnEliminarPedidoAndroid(String poCodigoPedido, String psTipoArticulo, bool enEdicion)
        {
            registerLog("fnEliminarPedidoAndroid.Input: " + poCodigoPedido);
            try
            {
                #region Eliminar
                List<Boolean> lbLstResultado = DBJava.fnDelPedidoDir(poCodigoPedido, psTipoArticulo, enEdicion);

                if (lbLstResultado[0])
                {
                    return "1;Eliminado correctamente";
                }
                else
                {
                    return "-1;" + lbLstResultado[1];
                }
                #endregion
            }
            catch (Exception ex)
            {
                registerLog("fnEliminarPedidoAndroid.Error: " + ex.Message + " : " + ex.StackTrace);
                return "-1;" + ex.Message;
            }
        }
        #endregion

        /// <summary>
        /// Funci�n que registra la cobranza Android y devuelve mensaje de retorno 
        /// del resultado del registro
        /// </summary>
        /// <param name="poPago">Detalle de la cobranza</param>
        /// <returns>respuesta del registro de cobranza</returns>
        public static String fnRegistrarCobranzaAndroid(BeanPago poPago)
        {
            registerLog("fnRegistrarCobranzaAndroid.Input: " + poPago);
            try
            {
                #region Insertar Transaccion

                List<bool> lbLstResultado = DBJava.fnInsPago(poPago, "");

                if (lbLstResultado[0])
                {
                    return "1;Grabado correctamente";
                }
                else
                {
                    return "-1;Error de Base de Datos";
                }
                return lbLstResultado[0] + ";" + lbLstResultado[1];
                #endregion
            }
            catch (Exception ex)
            {
                registerLog("fnRegistrarCobranzaAndroid.Error: " + ex.Message + " : " + ex.StackTrace);
                return "-1;" + ex.Message;
            }
        }

        public static BEResponsePendientes fnRegistrarPendientesAndroid(BEPendientes poListasPendientes)
        {
            BEResponsePendientes loBEResponsePendientes = new BEResponsePendientes();

            String lstempo = "";
            String error = "";

            List<BeanClienteDireccion> loListaDirecciones = poListasPendientes.listDirecciones;
            loBEResponsePendientes.listDirecciones = fnRegistrarPendientesDireccionesAndroid(loListaDirecciones);
            poListasPendientes.subActualizarDireccionPedido(loBEResponsePendientes.listDirecciones);

            List<BEEnvCabPedido> loListaPedidos = poListasPendientes.listPedidos;
            List<BeanPago> loListaPagos = poListasPendientes.listPagos;
            List<BeanCanjeCab> loListaCanjes = poListasPendientes.listCanjes;
            List<BeanDevolucionCab> loListaDevoluciones = poListasPendientes.listDevoluciones;
            List<BEProspecto> loListaProspectos = poListasPendientes.listProspectos;


            //Registro de Pedidos
            lstempo = fnRegistrarPendientesPedidosAndroid(loListaPedidos);
            if (lstempo.Substring(0, 1).Equals("-"))
            {
                error = lstempo.Substring(lstempo.IndexOf(';'), lstempo.Length);
            }
            //Registro de Pagos
            lstempo = fnRegistrarPendientesCobranzaAndroid(loListaPagos);
            if (lstempo.Substring(0, 1).Equals("-"))
            {
                if (!error.Equals(""))
                {
                    error = error + "|";
                }
                error = error + lstempo.Substring(lstempo.IndexOf(';'), lstempo.Length); ;
            }
            //Registro de Canjes
            lstempo = fnRegistrarPendientesCanjeAndroid(loListaCanjes);
            if (lstempo.Substring(0, 1).Equals("-"))
            {
                if (!error.Equals(""))
                {
                    error = error + "|";
                }
                error = error + lstempo.Substring(lstempo.IndexOf(';'), lstempo.Length); ;
            }
            //Registro de Devoluciones
            lstempo = fnRegistrarPendientesDevolucionAndroid(loListaDevoluciones);
            if (lstempo.Substring(0, 1).Equals("-"))
            {
                if (!error.Equals(""))
                {
                    error = error + "|";
                }
                error = error + lstempo.Substring(lstempo.IndexOf(';'), lstempo.Length); ;
            }
            //Registro de Prospectos
            lstempo = fnRegistrarPendientesProspectoAndroid(loListaProspectos);
            if (lstempo.Substring(0, 1).Equals("-"))
            {
                if (!error.Equals(""))
                {
                    error = error + "|";
                }
                error = error + lstempo.Substring(lstempo.IndexOf(';'), lstempo.Length); ;
            }

            if (error.Equals(""))
            {
                loBEResponsePendientes.error = "1;Pendientes procesados correctamente";

                return loBEResponsePendientes;
            }
            else
            {
                loBEResponsePendientes.error = error;
                return loBEResponsePendientes;
            }
        }

        public static List<BeanClienteDireccion> fnRegistrarPendientesDireccionesAndroid(List<BeanClienteDireccion> poListaDirecciones)
        {
            List<BeanClienteDireccion> loListaDirecciones = new List<BeanClienteDireccion>();

            foreach (BeanClienteDireccion loDir in poListaDirecciones)
            {
                loListaDirecciones.Add(DBJava.fnInsDireccion(loDir));

            }
            return loListaDirecciones;
        }

        public static String fnRegistrarPendientesPedidosAndroid(List<BEEnvCabPedido> poListaBETRCabPedido)
        {
            String lstempo = "";
            String error = "";
            foreach (BEEnvCabPedido loBE in poListaBETRCabPedido)
            {   //loSb.Append(ioDALTRCabPedido.fnPerInsertCabDetalle(loBE));

                if (loBE.FlgTerminado.Equals("T"))
                {
                    lstempo = fnRegistrarPedidoAndroid(loBE);
                }
                else
                {
                    lstempo = fnEliminarPedidoAndroid(loBE.CodigoPedidoServidor, loBE.tipoArticulo, loBE.enEdicion);
                }

                if (lstempo.Substring(0, 1).Equals("-"))
                {
                    error = lstempo;
                }
            }
            if (error.Equals(""))
            {
                return "1;Pendientes grabados correctamente";
            }
            else
            {
                return lstempo;
            }
        }

        public static String fnRegistrarPendientesCobranzaAndroid(List<BeanPago> poListaPagos)
        {
            String lstempo = "";
            String error = "";
            foreach (BeanPago loBE in poListaPagos)
            {
                lstempo = fnRegistrarCobranzaAndroid(loBE);
                if (lstempo.Substring(0, 1).Equals("-"))
                {
                    error = lstempo;
                }
            }
            if (error.Equals(""))
            {
                return "1;Pendientes grabados correctamente";
            }
            else
            {
                return lstempo;
            }
        }

        public static String fnRegistrarPendientesCanjeAndroid(List<BeanCanjeCab> poListaCanjes)
        {
            String lstempo = "";
            String error = "";
            foreach (BeanCanjeCab loBE in poListaCanjes)
            {
                lstempo = fnRegistrarCanjeAndroid(loBE);
                if (lstempo.Substring(0, 1).Equals("-"))
                {
                    error = lstempo;
                }
            }
            if (error.Equals(""))
            {
                return "1;Pendientes grabados correctamente";
            }
            else
            {
                return lstempo;
            }

        }

        public static String fnRegistrarPendientesDevolucionAndroid(List<BeanDevolucionCab> poListaDevolucion)
        {
            String lstempo = "";
            String error = "";
            foreach (BeanDevolucionCab loBE in poListaDevolucion)
            {
                lstempo = fnRegistrarDevolucionAndroid(loBE);
                if (lstempo.Substring(0, 1).Equals("-"))
                {
                    error = lstempo;
                }
            }
            if (error.Equals(""))
            {
                return "1;Pendientes grabados correctamente";
            }
            else
            {
                return lstempo;
            }
        }

        public static String fnRegistrarPendientesProspectoAndroid(List<BEProspecto> poListaProspectos)
        {
            String lstempo = "";
            String error = "";
            foreach (BEProspecto loBE in poListaProspectos)
            {
                lstempo = fnRegistrarProspectoAndroid(loBE);
                if (lstempo.Substring(0, 1).Equals("-"))
                {
                    error = lstempo;
                }
            }
            if (error.Equals(""))
            {
                return "1;Pendientes grabados correctamente";
            }
            else
            {
                return lstempo;
            }

        }

        /// <summary>
        /// Funci�n que registra la Direccion Android y devuelve mensaje de retorno 
        /// del resultado del registro
        /// </summary>
        /// <param name="poCanjeCa">Bean de Direccion</param>
        /// <returns>respuesta del registro de Direccion</returns>
        public static BeanClienteDireccion fnRegistrarDireccionAndroid(BeanClienteDireccion poClienteDireccion)
        {
            return DBJava.fnInsDireccion(poClienteDireccion);
        }

        /// <summary>
        /// Funci�n que registra el canje Android y devuelve mensaje de retorno 
        /// del resultado del registro
        /// </summary>
        /// <param name="poCanjeCab">Canje con detalle de productos</param>
        /// <returns>respuesta del registro de Canje</returns>
        public static String fnRegistrarCanjeAndroid(BeanCanjeCab poCanjeCab)
        {
            registerLog("fnRegistrarCanjeAndroid.Input: " + poCanjeCab);
            try
            {
                #region Insertar Transaccion

                List<Boolean> lbLstResultado = DBJava.fnInsCanje(poCanjeCab, "");

                if (lbLstResultado[0])
                {
                    return "1;Grabado correctamente";
                }
                else
                {
                    return "-1;" + lbLstResultado[1];
                }
                #endregion
            }
            catch (Exception ex)
            {
                registerLog("fnRegistrarCanjeAndroid.Error: " + ex.Message + " : " + ex.StackTrace);
                return "-1;" + ex.Message;
            }
        }

        /// <summary>
        /// Funci�n que registra la devolucion Android y devuelve mensaje de retorno 
        /// del resultado del registro
        /// </summary>
        /// <param name="poDevolucionCab">Devolucion con detalle de productos</param>
        /// <returns>respuesta del registro de devolucion</returns>
        public static String fnRegistrarDevolucionAndroid(BeanDevolucionCab poDevolucionCab)
        {
            registerLog("fnRegistrarDevolucionAndroid.Input: " + poDevolucionCab);
            try
            {
                #region Insertar Transaccion

                List<Boolean> lbLstResultado = DBJava.fnInsDevolucion(poDevolucionCab, "");

                if (lbLstResultado[0])
                {
                    return "1;Grabado correctamente";
                }
                else
                {
                    return "-1;" + lbLstResultado[1];
                }
                #endregion
            }
            catch (Exception ex)
            {
                registerLog("fnRegistrarDevolucionAndroid.Error: " + ex.Message + " : " + ex.StackTrace);
                return "-1;" + ex.Message;
            }
        }

        #endregion

        #region Consultas Online

        /// <summary>
        /// Funci�n que obtiene la cantidad de stock del art�culo. 
        /// El art�culo es buscado por c�digo o nombre
        /// </summary>
        /// <param name="BEArticuloOnline">Bean articulo con el c�digo y/o nombre para buscar</param>
        /// <returns>BEListaArticuloOnline con articulos de la busqueda</returns>
        public static BEListaArticuloOnline fnObtenerStockAndroid(BEArticuloOnline psartiOnline)
        {
            registerLog("fnObtenerStockAndroid.Input: " + psartiOnline.codigo);
            BeanArticulo loArticulo = new BeanArticulo();
            BEArticuloOnline loartresul = null;
            List<BEArticuloOnline> lolista = new List<BEArticuloOnline>();
            BEListaArticuloOnline loResultado = new BEListaArticuloOnline();
            loArticulo.codigo = psartiOnline.codigo;
            loArticulo.nombre = psartiOnline.nombre;
            loArticulo.tipoBusqueda = psartiOnline.tipoBusqueda;

            try
            {
                loArticulo.codAlmacen = Int16.Parse(psartiOnline.codAlmacen);
            }
            catch (Exception e)
            {
                loArticulo.codAlmacen = 0;
            }

            try
            {
                DataSet loDsStockArticulos = null;
                if (psartiOnline.tipoArticulo.Equals("PRO"))
                {
                    loDsStockArticulos = DBJava.fnSelStockArticulo(loArticulo);
                }
                else
                {
                    loDsStockArticulos = DBJava.fnSelStockPresentacion(loArticulo);
                }
                foreach (DataRow loDrStockArticulos in loDsStockArticulos.Tables[0].Rows)
                {
                    loartresul = new BEArticuloOnline();
                    if (psartiOnline.tipoArticulo.Equals("PRO"))
                    {
                        loartresul.codigo = loDrStockArticulos["CODIGOPRODUCTO"].ToString().Trim();
                        loartresul.nombre = loDrStockArticulos["NOMBREPRODUCTO"].ToString().Trim();
                    }
                    else
                    {
                        loartresul.codigo = loDrStockArticulos["CODIGOPRESENTACION"].ToString().Trim();
                        loartresul.nombre = loDrStockArticulos["NOMBREPRESENTACION"].ToString().Trim();
                        loartresul.nombreProducto = loDrStockArticulos["NOMBREPRODUCTO"].ToString().Trim();
                        loartresul.codigoProducto = loDrStockArticulos["CODIGOPRODUCTO"].ToString().Trim();
                        loartresul.cantidadPresentacion = loDrStockArticulos["CANTIDADPRESENTACION"].ToString().Trim();
                        loartresul.unidadDefecto = loDrStockArticulos["UNIDADDEFECTO"].ToString().Trim();
                    }
                    loartresul.stock = loDrStockArticulos["STOCK"].ToString().Trim();
                    loartresul.codAlmacen = loDrStockArticulos["PKALMACEN"].ToString().Trim();
                    loartresul.nombreAlmacen = loDrStockArticulos["NOMBREALMACEN"].ToString().Trim();
                    loartresul.tipoArticulo = psartiOnline.tipoArticulo;
                    lolista.Add(loartresul);
                }
                loResultado.idResultado = (Int32)EnumResGrabJava.NoMsg;
                loResultado.resultado = "";
                loResultado.listaProd = lolista;
            }
            catch (Exception ex)
            {
                registerLog("fnObtenerStockAndroid.Error: " + ex.ToString());
                //psClienteOnline.idResultado = 0;
                //psClienteOnline.resultado = ex.ToString();
                lolista = new List<BEArticuloOnline>();
                loResultado.idResultado = (Int32)EnumResGrabJava.TodoMal;
                //loResultado.resultado = ex.ToString();
                loResultado.resultado = Constantes.ValidarConsulta.CONSULTA_INCORRECTA;
            }

            return loResultado;
        }


        /// <summary>
        /// Funci�n que obtiene el ultimo pedido realizado por el cliente. 
        /// </summary>
        /// <param name="BEEnvCabPedido">Bean pedido con el id (PK) del cliente para buscar en el campo Codigo Pedido de la clase BEEnvCabPedido </param>
        /// <returns>BEEnvCabPedido con el ultimo pedido</returns>
        public static BEListaPedidoOnline fnObtenerUltimoPedidoAndroid(BEEnvCabPedido loBEEnvCabPedido)
        {
            registerLog("fnObtenerUltimoPedidoAndroid.Input: " + loBEEnvCabPedido.codigoCliente);

            BeanPedidoCab lobeancabfinal;
            lobeancabfinal = loBEEnvCabPedido.cambiabeanes();

            List<BEEnvCabPedido> lolista = new List<BEEnvCabPedido>();
            List<BEEnvDetPedido> lolistaDet = new List<BEEnvDetPedido>();
            BEListaPedidoOnline loResultado = new BEListaPedidoOnline();

            BEEnvCabPedido loartresul = null;
            BEEnvDetPedido loartresulDet = null;

            try
            {
                DataSet loDsPedidos = null;
                if (loBEEnvCabPedido.tipoArticulo.Equals(Constantes.TipoArticulo.PRODUCTO))
                {
                    loDsPedidos = DBJava.fnSelPedido(lobeancabfinal.codigoCliente);
                }
                else
                {
                    loDsPedidos = DBJava.fnSelPedidoPresentacion(lobeancabfinal.codigoCliente);
                }

                foreach (DataRow loDrPedidosDet in loDsPedidos.Tables[1].Rows)
                {
                    loartresulDet = new BEEnvDetPedido();

                    if (loBEEnvCabPedido.tipoArticulo.Equals(Constantes.TipoArticulo.PRODUCTO))
                    {
                        loartresulDet.idArticulo = loDrPedidosDet["PRO_PK"].ToString().Trim();
                        loartresulDet.nombreArticulo = loDrPedidosDet["PRO_NOMBRE"].ToString().Trim();
                        loartresulDet.codigoArticulo = loDrPedidosDet["PRO_CODIGO"].ToString().Trim();
                    }
                    else
                    {
                        loartresulDet.idArticulo = loDrPedidosDet["PRE_PK"].ToString().Trim();
                        loartresulDet.nombreArticulo = loDrPedidosDet["NOMBRE"].ToString().Trim();
                        loartresulDet.codigoArticulo = loDrPedidosDet["PRE_CODIGO"].ToString().Trim();
                        loartresulDet.cantidadPre = loDrPedidosDet["PRE_CANTIDAD"].ToString().Trim();
                        loartresulDet.cantidadFrac = loDrPedidosDet["DET_CANTIDAD_FRAC"].ToString().Trim();
                        loartresulDet.bonificacionFrac = loDrPedidosDet["DET_BONIFICACION_FRAC"].ToString().Trim();
                        loartresulDet.precioFrac = loDrPedidosDet["DET_PRECIO_FRAC"].ToString().Trim();


                    }
                    loartresulDet.codPedido = loDrPedidosDet["PED_PK"].ToString().Trim();
                    loartresulDet.cantidad = loDrPedidosDet["DET_CANTIDAD"].ToString().Trim();
                    loartresulDet.descuento = loDrPedidosDet["DET_DESCUENTO"].ToString().Trim();
                    loartresulDet.monto = loDrPedidosDet["DET_MONTO"].ToString().Trim();
                    loartresulDet.montoSoles = loDrPedidosDet["MontoSoles"].ToString().Trim();
                    loartresulDet.montoDolar = loDrPedidosDet["MontoDolares"].ToString().Trim();
                    loartresulDet.precioBase = loDrPedidosDet["DET_PRECIOBASE"].ToString().Trim();
                    loartresulDet.precioBaseSoles = loDrPedidosDet["PrecioBaseSoles"].ToString().Trim();
                    loartresulDet.precioBaseDolares = loDrPedidosDet["PrecioBaseDolares"].ToString().Trim();
                    loartresulDet.tipoMoneda = loDrPedidosDet["TM_PK"].ToString().Trim();
                    loartresulDet.tipoDescuento = loDrPedidosDet["DET_TIPODESC"].ToString().Trim();
                    loartresulDet.bonificacion = loDrPedidosDet["DET_BONIFICACION"].ToString().Trim();
                    loartresulDet.codListaArticulo = loDrPedidosDet["LPR_CODIGO"].ToString().Trim();
                    loartresulDet.observacion = loDrPedidosDet["DET_DESCRIPCION"].ToString().Trim();
                    loartresulDet.flete = loDrPedidosDet["DET_FLETE"].ToString().Trim();
                    loartresulDet.codAlmacen = loDrPedidosDet["ALM_PK"].ToString().Trim();
                    loartresulDet.stock = loDrPedidosDet["STOCK"].ToString().Trim();
                    loartresulDet.codBonificacion = Convert.ToInt64(loDrPedidosDet["BON_PK"].ToString().Trim());
                    loartresulDet.montoSinDescuento = loDrPedidosDet["DET_MONTO_SIN_DESCUENTO"].ToString().Trim();
                    lolistaDet.Add(loartresulDet);
                }

                foreach (DataRow loDrPedidos in loDsPedidos.Tables[0].Rows)
                {
                    loartresul = new BEEnvCabPedido();

                    loartresul.codigoCliente = loDrPedidos["CLI_CODIGO"].ToString().Trim();
                    loartresul.fechaInicio = loDrPedidos["PED_FECINICIO"].ToString().Trim();
                    loartresul.fechaFin = loDrPedidos["PED_FECFIN"].ToString().Trim();
                    loartresul.montoTotal = loDrPedidos["PED_MONTOTOTAL"].ToString().Trim();
                    loartresul.montoTotalSoles = loDrPedidos["MontoTotalSoles"].ToString().Trim();
                    loartresul.montoTotalDolar = loDrPedidos["MontoTotalDolares"].ToString().Trim();
                    loartresul.cantidadTotal = loDrPedidos["CANT_TOTAL"].ToString().Trim();
                    loartresul.condicionVenta = loDrPedidos["PED_CONDVENTA"].ToString().Trim();
                    loartresul.flgTipo = loDrPedidos["FLGTIPO"].ToString().Trim();
                    loartresul.fleteTotal = loDrPedidos["FLETE_TOTAL"].ToString().Trim();
                    loartresul.codAlmacen = loDrPedidos["ALM_PK"].ToString().Trim();
                    loartresul.tipoArticulo = loBEEnvCabPedido.tipoArticulo;
                    lolista.Add(loartresul);
                }

                if (lolista.Count > 0)
                {
                    lolista[0].listaDetPedido = lolistaDet;
                    loResultado.idResultado = (Int32)EnumResGrabJava.NoMsg;
                    loResultado.resultado = "";
                    loResultado.pedido = lolista[0];
                }
                else
                {
                    loResultado.idResultado = (Int32)EnumResGrabJava.TodoMal;
                    loResultado.resultado = "No tiene �ltimos Pedidos";
                }
            }
            catch (Exception ex)
            {
                registerLog("fnObtenerUltimoPedido.Error: " + ex.ToString());
                //psClienteOnline.idResultado = 0;
                //psClienteOnline.resultado = ex.ToString();
                lolista = new List<BEEnvCabPedido>();
                loResultado.idResultado = (Int32)EnumResGrabJava.TodoMal;
                loResultado.resultado = ex.ToString();
            }

            return loResultado;
        }

        public static BEListaEficienciaOnline fnObtenerEficienciaAndroid(String psCodigo, String perfilUsuario)
        {
            registerLog("fnObtenerEficienciaAndroid.Input: " + psCodigo);
            BEListaEficienciaOnline listaEficiencia = new BEListaEficienciaOnline();
            List<BEEficienciaOnline> lista = new List<BEEficienciaOnline>();
            BEEficienciaOnline loresul;
            DataSet loLstEficiencias = null;

            try
            {
                if (perfilUsuario == Constantes.Perfiles.supervisor)
                {
                    loLstEficiencias = DBJava.fnObtenerEficienciaSupervisor(psCodigo);
                    listaEficiencia.usuariosVendedores = DBJava.obtenerVendedores(psCodigo);
                }
                else
                {
                    if (perfilUsuario == Constantes.Perfiles.vendedor)
                    {
                        loLstEficiencias = DBJava.fnObtenerEficienciaVendedor(psCodigo);
                    }
                }
                foreach (DataRow loEficiencia in loLstEficiencias.Tables[0].Rows)
                {
                    loresul = new BEEficienciaOnline();
                    loresul.descripcion = loEficiencia["descripcion"].ToString();
                    loresul.valor = loEficiencia["valor"].ToString();
                    lista.Add(loresul);
                }

                BEKpi kpi = new BEKpi();
                if (loLstEficiencias.Tables.Count > 1 && loLstEficiencias.Tables[1].Rows.Count > 0)
                {
                    kpi.numPedidos = loLstEficiencias.Tables[1].Rows[0]["totalPedidos"].ToString();
                    kpi.numCobranzas = loLstEficiencias.Tables[1].Rows[0]["totalCobranzas"].ToString();
                    kpi.numNoPedidos = loLstEficiencias.Tables[1].Rows[0]["TotalNoPedidos"].ToString();
                    kpi.numCanjes = loLstEficiencias.Tables[1].Rows[0]["TotalCanjes"].ToString();
                    kpi.numDevoluciones = loLstEficiencias.Tables[1].Rows[0]["TotalDevoluciones"].ToString();
                }

                listaEficiencia.listaEficiencia = lista;
                listaEficiencia.kpi = kpi;
                listaEficiencia.idResultado = 2;
            }
            catch (Exception ex)
            {
                registerLog("fnObtenerEficienciaAndroid.Error: " + ex.ToString());
                listaEficiencia.idResultado = 0;
                listaEficiencia.resultado = ex.ToString();
            }

            return listaEficiencia;
        }


        /// <summary>
        /// Funci�n que busca al usuario seg�n el c�digo
        /// </summary>
        /// <param name="BEClienteOnline">Bean usuario con el c�digo para buscar</param>
        /// <returns>BEClienteOnline con el detalle del usuario</returns>
        public static BEObtenerClienteOnline fnObtenerClienteCobranzasAndroid(BEClienteOnline psClienteOnline)
        {
            registerLog("fnObtenerClienteCobranzasAndroid.Input: " + psClienteOnline.clicod);
            BEClienteOnline loClienteOnline = new BEClienteOnline();
            BEObtenerClienteOnline loObtenerOnline = new BEObtenerClienteOnline();
            try
            {
                List<BEClienteOnline> loListaCliente = fnSelCliente(psClienteOnline.clicod);

                if (loListaCliente.Count == 0)
                {
                    loObtenerOnline.idResultado = 2;
                    loObtenerOnline.resultado = "No encontrado";
                }
                else
                {
                    String flgCobranzasFueraRuta = DBJava.fnSelValorConfiguracionDB("TPCF");
                    List<BECobranzaOnline> loListaCobranza;

                    if (flgCobranzasFueraRuta.Equals("T"))
                    {
                        loListaCobranza = fnSelClienteCobranza(psClienteOnline.clicod, psClienteOnline.codUsuario);
                    }
                    else
                    {
                        loListaCobranza = null;
                    }

                    loListaCliente[0].codUsuario = psClienteOnline.codUsuario;
                    loObtenerOnline.clienteOnline = loListaCliente[0];
                    loObtenerOnline.listaCobranzasOnline = loListaCobranza;
                    loObtenerOnline.idResultado = 1;
                    loObtenerOnline.resultado = "Encontrado";
                }
            }
            catch (Exception ex)
            {
                registerLog("fnObtenerClienteCobranzasAndroid.Error: " + ex.ToString());
                loObtenerOnline.idResultado = 0;
                loObtenerOnline.resultado = ex.ToString();
            }

            return loObtenerOnline;
        }

        public static List<BEClienteOnline> fnSelCliente(String psCodigoCliente)
        {
            BEClienteOnline loCliente;
            List<BEClienteOnline> loListaCliente = new List<BEClienteOnline>();
            DataSet loDsClientes = DBJava.fnSelCliente(psCodigoCliente);
            foreach (DataRow loDrCliente in loDsClientes.Tables[0].Rows)
            {
                loCliente = new BEClienteOnline();

                loCliente.clipk = loDrCliente["CLI_PK"].ToString().ToUpper().Trim();
                loCliente.clicod = loDrCliente["CLI_CODIGO"].ToString().ToUpper().Trim();
                loCliente.clinom = loDrCliente["CLI_NOMBRE"].ToString().ToUpper().Trim();
                loCliente.tclicod = loDrCliente["TCLI_COD"].ToString().ToUpper().Trim();
                loCliente.tclinombre = loDrCliente["TCLI_NOMBRE"].ToString().ToUpper().Trim();
                loCliente.cligiro = loDrCliente["CLI_GIRO"].ToString().ToUpper().Trim();
                loCliente.secuencia = loDrCliente["CLI_SECUENCIA"].ToString().ToUpper().Trim();
                loCliente.clidireccion = loDrCliente["CLI_DIRECCION"].ToString().ToUpper().Trim();
                loCliente.clideuda = loDrCliente["CLI_DEUDA"].ToString().ToUpper().Trim();
                loCliente.estado = loDrCliente["ESTADO"].ToString().ToUpper().Trim();

                loCliente.campoAdicional1 = loDrCliente["CAMPO1"].ToString().ToUpper().Trim();
                loCliente.campoAdicional2 = loDrCliente["CAMPO2"].ToString().ToUpper().Trim();
                loCliente.campoAdicional3 = loDrCliente["CAMPO3"].ToString().ToUpper().Trim();
                loCliente.campoAdicional4 = loDrCliente["CAMPO4"].ToString().ToUpper().Trim();
                loCliente.campoAdicional5 = loDrCliente["CAMPO5"].ToString().ToUpper().Trim();
                loCliente.saldoCredito = loDrCliente["SALDO_CREDITO"].ToString().ToUpper().Trim();

                loListaCliente.Add(loCliente);
            }
            return loListaCliente;
        }

        public static List<BECobranzaOnline> fnSelClienteCobranza(String psCodigoCliente, String psCodigoUsuario)
        {
            BECobranzaOnline loCobranza;
            List<BECobranzaOnline> lstCobranzas = new List<BECobranzaOnline>();

            DataSet loLstCobranzas = DBJava.fnSelClienteCobranza(psCodigoCliente, psCodigoUsuario);

            foreach (DataRow loDrClienteCob in loLstCobranzas.Tables[0].Rows)
            {
                loCobranza = new BECobranzaOnline();

                loCobranza.cobPk = loDrClienteCob["COB_PK"].ToString().ToUpper().Trim();
                loCobranza.cobCodigo = loDrClienteCob["COB_CODIGO"].ToString().ToUpper().Trim();
                loCobranza.cobNumDocumento = loDrClienteCob["COB_NUMDOCUMENTO"].ToString().ToUpper().Trim();
                loCobranza.cobTipoDocumento = loDrClienteCob["COB_TIPODOCUMENTO"].ToString().ToUpper().Trim();
                loCobranza.cobMontoPagado = loDrClienteCob["COB_MONTOPAGADO"].ToString().ToUpper().Trim();
                loCobranza.cobMontoTotal = loDrClienteCob["COB_MONTOTOTAL"].ToString().ToUpper().Trim();
                loCobranza.cobFecVencimiento = loDrClienteCob["COB_FECVENCIMIENTO"].ToString().ToUpper().Trim();
                loCobranza.cobSerie = loDrClienteCob["COB_SERIE"].ToString().ToUpper().Trim();
                loCobranza.cliCodigo = loDrClienteCob["CLI_CODIGO"].ToString().ToUpper().Trim();
                loCobranza.cobEmpresa = loDrClienteCob["COB_EMPRESA"].ToString().ToUpper().Trim();
                loCobranza.cobUsuario = loDrClienteCob["COB_USUARIO"].ToString().ToUpper().Trim();
                loCobranza.cliPK = loDrClienteCob["CLI_PK"].ToString().ToUpper().Trim();
                loCobranza.saldo = loDrClienteCob["SALDO"].ToString().ToUpper().Trim();

                lstCobranzas.Add(loCobranza);
            }
            return lstCobranzas;
        }


        public static BESincronizar fnConsultaPedidosScript(String picodUsuario, String piFecha, String psTipoArticulo, String pscodCliente)
        {
            BESincronizar resultado = DBJava.fnConsultaPedidosScript(picodUsuario, piFecha, psTipoArticulo, pscodCliente);
            return resultado;
        }

        public static BESincronizar fnConsultaPagosScript(String picodUsuario, String piFecha, String pscodCliente)
        {
            #endregion
            BESincronizar resultado = DBJava.fnConsultaPagosScript(picodUsuario, piFecha, pscodCliente);
            return resultado;
        }

        public static String fnActualizarEstadoProcesado(BEEnvCabPedido pedido)
        {
            registerLog("fnActualizarEstadoProcesado.Input: " + pedido);
            try
            {
                #region Insertar Transaccion

                List<Boolean> lbLstResultado = DBJava.fnActualizarEstadoPedido(pedido);

                if (lbLstResultado[0])
                {
                    return "2;Grabado correctamente";
                }
                else
                {
                    return "-1;" + lbLstResultado[1];
                }
                #endregion
            }
            catch (Exception ex)
            {
                registerLog("fnActualizarEstadoProcesado.Error: " + ex.Message + " : " + ex.StackTrace);
                return "-1;" + ex.Message;
            }
        }

        #region Verificaci�n de Servicios
        public static String fnVerificar(String parametros)
        {
            String lsRespuesta = String.Empty;
            String endPoint = @"http://demosdata.entel.pe/ws_suscriptor/Service1.svc/obtenerInfo";
            // String endPoint = @"http://solucionesmoviles3.entel.pe:8080/ws_subscriptor/Service1.svc/obtenerInfo";

            try
            {
                RestClient cliente = new RestClient(endPoint, HttpVerb.POST, parametros);
                lsRespuesta = cliente.MakeRequest("");
            }
            catch (Exception ex)
            {
                registerLog("fnVerificar REST.Error: " + ex);
                lsRespuesta = ex.Message;
            }
            return lsRespuesta;
        }
        #endregion


        #region Consulta Recibo (Serie - Correlativo)
        public static BEUsuarioReciboOnline fnObtenerReciboAndroid(BEUsuario psartiOnline)
        {
            registerLog("fnObtenerReciboUsuarioAndroid.Input: " + psartiOnline.codVendedor);
            BEUsuarioReciboOnline loResultado = new BEUsuarioReciboOnline();
            try
            {
                DataSet loDsRecibo = null;
                loDsRecibo = DBJava.fnObtenerReciboCobranza(psartiOnline.codVendedor);
                foreach (DataRow loDrRecibo in loDsRecibo.Tables[0].Rows)
                {
                    loResultado.serie = loDrRecibo["Serie"].ToString().Trim();
                    loResultado.correlativo = loDrRecibo["Correlativo"].ToString().Trim();
                }
                loResultado.idResultado = (Int32)EnumResGrabJava.NoMsg;
                loResultado.resultado = "";
            }
            catch (Exception ex)
            {
                registerLog("fnObtenerReciboUsuarioAndroid.Error: " + ex.ToString());
                loResultado.idResultado = (Int32)EnumResGrabJava.TodoMal;
                loResultado.resultado = Constantes.ValidarConsulta.CONSULTA_INCORRECTA;
            }

            return loResultado;
        }
        #endregion

        public static String fnRegistrarDocumentoAndroid(BeanDocumento poDocumento)
        {
            registerLog("fnRegistrarDocumentoAndroid.Input: " + poDocumento);
            try
            {
                #region Insertar Transaccion
                List<bool> lbLstResultado = DBJava.fnInsDocumento(poDocumento, "");

                if (lbLstResultado[0])
                {
                    return "1;Grabado correctamente";
                }
                else
                {
                    return "-1;Error de Base de Datos";
                }
                #endregion
            }
            catch (Exception ex)
            {
                registerLog("fnRegistrarCobranzaAndroid.Error: " + ex.Message + " : " + ex.StackTrace);
                return "-1;" + ex.Message;
            }
        }
        #endregion
    }
}

