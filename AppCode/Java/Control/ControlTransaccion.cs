#region Referencias
using System;
using System.Data;
using System.Configuration;
using System.Xml;
using System.Collections.Generic;
using Java.DatabaseAccess;

#endregion
#region Referencias Nextel
using NextlibServer.Main;
//using NextlibServer.RMS;
//using Java.RMS;
using Java.Bean;
#endregion

namespace Java.Control
{
    public class ControlTransaccion : ControlBase
    {
        /// <summary>
        /// Funci�n que registra pedidos
        /// </summary>
        /// <param name="poLstPedidosCab">Lista de pedidos</param>
        /// <returns>Todos los detalles de error</returns>
        public static BeanErrorTransaccion fnRegistrarPedido(List<BeanPedidoCab> poLstPedidosCab, String psPlataforma)
        {
            BeanErrorTransaccion loErrorTransaccion = new BeanErrorTransaccion();

            loErrorTransaccion.mensajeErrorInsertar = "Error al insertar pedido";
            loErrorTransaccion.mensajeInsertados = " pedidos insertados de ";

            try
            {
                Boolean lbVarios = (poLstPedidosCab.Count == 1) ? false : true;
                loErrorTransaccion.total = poLstPedidosCab.Count;

                foreach (BeanPedidoCab loPedidoCab in poLstPedidosCab)
                {

                    List<Boolean> lbLstResultado = DBJava.fnInsPedido(loPedidoCab, psPlataforma);
                    if (lbLstResultado[0])
                    {
                        loErrorTransaccion.insertados++;
                    }
                    else
                    {
                        if (lbLstResultado[1])
                        {
                            loErrorTransaccion.errorBD++;
                        }
                        else
                        {
                            loErrorTransaccion.errorDuplicado++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                loErrorTransaccion.error = ex.Message;

                loErrorTransaccion.mensaje = ex.Message;
                loErrorTransaccion.errorBD++;

                registerLog("ControlTransaccion.fnRegistrarPedido.Error: " + ex.Message + " : " + ex.StackTrace);
            }

            loErrorTransaccion.mensaje = fnObtenerMensajeError(loErrorTransaccion);

            return loErrorTransaccion;
        }

        /// <summary>
        /// Funci�n que registra cobranzas
        /// </summary>
        /// <param name="poLstPedidosCab">Lista de cobranzas</param>
        /// <returns>Todos los detalles de error</returns>
        public static BeanErrorTransaccion fnRegistrarCobranza(List<BeanPago> poLstPago, String psPlataforma)
        {
            BeanErrorTransaccion loErrorTransaccion = new BeanErrorTransaccion();
            
            loErrorTransaccion.mensajeErrorInsertar = "Error al insertar pago";
            loErrorTransaccion.mensajeInsertados = " pagos insertados de ";

            try
            {
                Boolean lbVarios = (poLstPago.Count == 1) ? false : true;

                foreach (BeanPago loBeanPago in poLstPago)
                {
                    #region Insertar Transaccion
                    List<bool> lbLstResultado = DBJava.fnInsPago(loBeanPago, psPlataforma);
                    if (lbLstResultado[0])
                    {
                        loErrorTransaccion.insertados++;
                    }
                    else
                    {
                        if (lbLstResultado[1])
                        {
                            loErrorTransaccion.errorBD++;
                        }
                        else
                        {
                            loErrorTransaccion.errorDuplicado++;
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                loErrorTransaccion.error = ex.Message;
                loErrorTransaccion.mensaje = ex.Message;
                loErrorTransaccion.errorBD++;

                registerLog("ControlTransaccion.fnRegistrarCobranza.ERROR: " + ex.Message + " : " + ex.StackTrace);
            }

            loErrorTransaccion.mensaje = fnObtenerMensajeError(loErrorTransaccion);            

            return loErrorTransaccion;
        }

        /// <summary>
        /// Funci�n que registra canjes
        /// </summary>
        /// <param name="poLstPedidosCab">Lista de canjes</param>
        /// <returns>Todos los detalles de error</returns>
        public static BeanErrorTransaccion fnRegistrarCanje(List<BeanCanjeCab> poLstCanje, String psPlataforma)
        {
            BeanErrorTransaccion loErrorTransaccion = new BeanErrorTransaccion();

            loErrorTransaccion.mensajeErrorInsertar = "Error al insertar pago";
            loErrorTransaccion.mensajeInsertados = " canjes insertados de ";

            try
            {
                Boolean lbVarios = (poLstCanje.Count == 1) ? false : true;

                foreach (BeanCanjeCab loCanjeCab in poLstCanje)
                {
                    #region Insertar Transaccion
                    List<Boolean> lbLstResultado = DBJava.fnInsCanje(loCanjeCab, psPlataforma);
                    if (lbLstResultado[0])
                    {
                        loErrorTransaccion.insertados++;
                    }
                    else
                    {
                        if (lbLstResultado[1])
                        {
                            loErrorTransaccion.errorBD++;
                        }
                        else
                        {
                            loErrorTransaccion.errorDuplicado++;
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                loErrorTransaccion.error = ex.Message;
                loErrorTransaccion.mensaje = ex.Message;
                loErrorTransaccion.errorBD++;

                registerLog("ControlTransaccion.fnRegistrarCanje.ERROR: " + ex.Message + " : " + ex.StackTrace);
            }

            loErrorTransaccion.mensaje = fnObtenerMensajeError(loErrorTransaccion);

            return loErrorTransaccion;
        }

        /// <summary>
        /// Funci�n que registra devoluciones
        /// </summary>
        /// <param name="poLstPedidosCab">Lista de devoluciones</param>
        /// <returns>Todos los detalles de error</returns>
        public static BeanErrorTransaccion fnRegistrarDevolucion(List<BeanDevolucionCab> poLstDevolucionesCab, String psPlataforma)
        {
            BeanErrorTransaccion loErrorTransaccion = new BeanErrorTransaccion();

            loErrorTransaccion.mensajeErrorInsertar = "Error al insertar pago";
            loErrorTransaccion.mensajeInsertados = " devoluciones insertados de ";

            try
            {
                Boolean lbVarios = (poLstDevolucionesCab.Count == 1) ? false : true;

                foreach (BeanDevolucionCab loDevolucionCab in poLstDevolucionesCab)
                {
                    #region Insertar Transaccion
                    List<Boolean> lbLstResultado = DBJava.fnInsDevolucion(loDevolucionCab, psPlataforma);
                    if (lbLstResultado[0])
                    {
                        loErrorTransaccion.insertados++;
                    }
                    else
                    {
                        if (lbLstResultado[1])
                        {
                            loErrorTransaccion.errorBD++;
                        }
                        else
                        {
                            loErrorTransaccion.errorDuplicado++;
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                loErrorTransaccion.error = ex.Message;
                loErrorTransaccion.mensaje = ex.Message;
                loErrorTransaccion.errorBD++;

                registerLog("ControlTransaccion.fnRegistrarDevolucion.ERROR: " + ex.Message + " : " + ex.StackTrace);
            }

            loErrorTransaccion.mensaje = fnObtenerMensajeError(loErrorTransaccion);

            return loErrorTransaccion;
        }

        /// <summary>
        /// Funci�n que asigna el mensaje de retorno. Muestra la cantidad de transacciones insertadas
        /// </summary>
        /// <param name="poLstPedidosCab">Lista de devoluciones</param>
        /// <returns>Todos los detalles de error</returns>
        public static String fnObtenerMensajeError(BeanErrorTransaccion poErrorTransaccion)
        {
            String lsMensaje = poErrorTransaccion.mensaje;

            if (poErrorTransaccion.total == 1)
            {
                lsMensaje = (poErrorTransaccion.insertados == 1) ? String.Empty : poErrorTransaccion.mensajeErrorInsertar;
            }
            else
            {
                lsMensaje = (poErrorTransaccion.insertados == poErrorTransaccion.total) ? String.Empty : poErrorTransaccion.insertados.ToString() + poErrorTransaccion.mensajeInsertados + poErrorTransaccion.total.ToString();
            }

            return lsMensaje;
        }
    }
}