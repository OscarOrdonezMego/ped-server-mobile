﻿using System;
using System.Collections.Generic;
using System.Web;

namespace Java.Control
{
public class Constantes
    {

        public class TipoArticulo
        {
            public static String PRODUCTO = "PRO";
            public static String PRESENTACION = "PRE";
        }
        public class Usuario
        {
            public static String USUARIO_EXISTE = "Se Identifico Correctamente";
            public static String USUARIO_NO_EXISTE = "El Usuario y/o Clave son incorrectos";
            public static String USUARIO_NO_ENCONTRADO = "No se encontro el numero de documento";
            public static String USUARIO_ENCONTRADO = "Encontrado";
        }
       
        public class ValidarGrabar
        {
            public static String GRABO_CORRECTO = "Se registro correctamente";
            public static String GRABO_INCORRECTO = "No se pudo registrar";
            public static String RESERVO_CORRECTO = "Reserva correcta";
        }

        public class ValidarGrabarPendientes
        {
            public static String GRABO_CORRECTO = "Se registraron correctamente los pendientes";
            public static String GRABO_INCORRECTO = "No se registraron todos los pendientes";
        }

        public class ValidarConsulta {
            public static String CONSULTA_CORRECTA = "Se realizo la consulta correctamente";
            public static String CONSULTA_INCORRECTA = "Problemas en el servidor, consulte más tarde";
        }

        public class Sincronizar
        {
            public static String CORRECTO = "Se sincronizo correctamente la informacion";
            public static String CORRECTOPTOVENTA = "Puntos de Venta Sincronizados";
            public static String INCORRECTO = "No se puedo sincronizar la informacion";
            public static String SIN_DATA = "No existe data para poder sincronizar";

        }
        public class Perfiles
        {
            public static String vendedor = "2";
            public static String supervisor = "4";

        }

        public class Flag
        {
            public static String TRUE = "T";
            public static String FALSE = "F";
        }

    }
}