using System;
using System.Collections;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Data.SqlClient;
using NextlibServer.Main;
using NextlibServer.DAO;
using NextlibServer.RMS;
using Java.Bean;
using NextlibServer.Utils;
using NextlibServer.GPS;
using Nextel.BE;
using System.Text;
using Java.Control;
using Microsoft.ApplicationBlocks.Data;
using Pedidos_Mobile.App_Code.Tools;

namespace Java.DatabaseAccess
{
    public class DBJava : DALBase
    {
        public static List<String> obtenerConfiguracionDB()
        {
            ArrayList loLstParametros = new ArrayList();
            List<String> loLstConfiguracion = new List<String>();

            DataSet loDsManager = DBUtilSQLServer.getDataset("USPS_OBTENER_CONFIGURACION_MOVIL", loLstParametros);

            foreach (DataRow loDrManager in loDsManager.Tables[0].Rows)
            {

                loLstConfiguracion.Add(loDrManager["SCRIPT"].ToString().Trim());
            }
            return loLstConfiguracion;
        }
        public static List<String> obtenerVendedores(String cod_supervisor)
        {
            ArrayList loLstParametros = new ArrayList();
            List<String> loLstVendedores = new List<String>();
            SqlParameter loParametro;
            loParametro = new SqlParameter("@cod_supervisor", SqlDbType.VarChar, 20);
            loParametro.Value = cod_supervisor;
            loLstParametros.Add(loParametro);
            DataSet loDsManager = DBUtilSQLServer.getDataset("USPS_OBTENER_VENDEDORES", loLstParametros);

            foreach (DataRow loDrManager in loDsManager.Tables[0].Rows)
            {
                loLstVendedores.Add(loDrManager["VENDEDORES"].ToString().Trim());
            }
            return loLstVendedores;
        }
        /// <summary>
        /// Funci�n que obtiene el valor de un codigo de configuracion asignada
        /// </summary>
        /// <param name="codigo">El codigo de la configuracion</param>
        /// <returns></returns>
        public static String fnSelValorConfiguracionDB(string codigo)
        {
            String valor = String.Empty;
            SqlParameter loParametro;
            ArrayList loLstParametros = new ArrayList();

            loParametro = new SqlParameter("@PAR_CODIGO", SqlDbType.VarChar, 20);
            loParametro.Value = codigo;
            loLstParametros.Add(loParametro);

            DataSet loDsManager = DBUtilSQLServer.getDataset("USPS_OBTENER_VALOR_CONFIGURACION", loLstParametros);

            foreach (DataRow loDrManager in loDsManager.Tables[0].Rows)
            {
                valor = loDrManager["Valor"].ToString().Trim();

            }

            return valor;
        }

        /// <summary>
        /// Funci�n que obtiene las etiquetas de idiomas a utilizar en las aplicaciones en formato List
        /// </summary>
        /// <param name="psTipo">Tipo de aplicaci�n: m�vil (J) o web(W)</param>
        /// <returns></returns>
        public static List<BeanIdioma> fnSelCultureIdiomaLista(string psTipo)
        {
            List<BeanIdioma> loLstIdiomas = new List<BeanIdioma>();

            ArrayList loLstParametros = new ArrayList();
            SqlParameter loParametro;
            BeanIdioma loIdioma = null;

            loParametro = new SqlParameter("@TIPO", SqlDbType.VarChar, 20);
            loParametro.Value = psTipo;
            loLstParametros.Add(loParametro);

            DataSet loDsIdioma = DBUtilSQLServer.getDataset("USPW_PRINCIPAL_S_IDIOMA", loLstParametros);

            foreach (DataRow loDrIdioma in loDsIdioma.Tables[0].Rows)
            {
                loIdioma = new BeanIdioma();

                loIdioma.id = loDrIdioma["CODIGO"].ToString().ToString().Trim().ToUpper();
                loIdioma.valor = loDrIdioma["DESCRIPCION"].ToString().Trim().ToUpper().Replace("\\N", "\n");

                loLstIdiomas.Add(loIdioma);
            }

            return loLstIdiomas;
        }



        /// <summary>
        /// M�todo de acceso a BD para validar el usuario
        /// </summary>
        /// <param name="poUsuario">Bean con los datos de usuario</param>
        /// <returns>Dataset con los datos del usuario</returns>
        /// 
        public static DataSet fnRegistrarNumeroUsuario(BeanUsuario poUsuario)
        {
            ArrayList loLstParametros = new ArrayList();
            SqlParameter loParametro;

            loParametro = new SqlParameter("@PAR_CODUSUARIO", SqlDbType.VarChar, 20);
            loParametro.Value = poUsuario.codigo;
            loLstParametros.Add(loParametro);

            loParametro = new SqlParameter("@PAR_NUMERO", SqlDbType.VarChar, 100);
            loParametro.Value = poUsuario.numero;
            loLstParametros.Add(loParametro);

            return DBUtilSQLServer.getDataset("USPU_REGISTRAR_NUMERO_USUARIO", loLstParametros);

        }
        public static DataSet fnSelValidarUsuario(BeanUsuario poUsuario)
        {
            ArrayList loLstParametros = new ArrayList();
            SqlParameter loParametro;

            loParametro = new SqlParameter("@PAR_CODUSUARIO", SqlDbType.VarChar, 20);
            loParametro.Value = poUsuario.codigo;
            loLstParametros.Add(loParametro);

            loParametro = new SqlParameter("@PAR_PASSWORD", SqlDbType.VarChar, 100);
            loParametro.Value = poUsuario.password;
            loLstParametros.Add(loParametro);

            return DBUtilSQLServer.getDataset("USPS_VALIDAR_USUARIO", loLstParametros);

        }

        #region Sincronizacion


        public static BESincronizar fnSinScriptString(String piIdUsuario, String piTelefono)
        {

            BESincronizar loBESincronizar = null;
            StringBuilder loStringScript = new StringBuilder();
            SqlParameter[] par = new SqlParameter[1];
            par[0] = new SqlParameter("@PAR_CODUSUARIO", System.Data.SqlDbType.VarChar, 20);
            par[0].Value = piIdUsuario;

            //Integracion NTrack
            List<String> scriptsNServices = null;

            int liCount = 0;
            //obtiene los permisos
            String flgNServices = DBJava.fnSelValorConfiguracionDB("GESR"); //INDICAR EL FLGNSERVICES

            try
            {
                if (flgNServices.Equals("T"))
                {
                    scriptsNServices = fnSinScriptStringNServices(piTelefono);
                }


                SqlDataReader loDr = SqlHelper.ExecuteReader(new SqlConnection(connectionString), CommandType.StoredProcedure, "USPS_JSON_SINCRONIZAR_TODO", par);
                while (loDr.Read())
                {

                    loStringScript.Append((String)loDr.GetValue(loDr.GetOrdinal("SCRIPT")));
                    loStringScript.Append(Environment.NewLine);
                    liCount++;



                }
                loDr.NextResult();
                StringBuilder loSbResultado = new StringBuilder();
                while (loDr.Read())
                {
                    loSbResultado.Append(",");
                    loSbResultado.Append((String)loDr.GetValue(loDr.GetOrdinal("Total")));
                    //loSbResultado.Append(Environment.NewLine);
                }

                //Integracion Ntrack
                if (scriptsNServices != null)
                {
                    foreach (String lsScriptNS in scriptsNServices)
                    {
                        loStringScript.Append(lsScriptNS);
                        loStringScript.Append(Environment.NewLine);
                    }
                }


                loDr.Close();
                loBESincronizar = new BESincronizar(true, loStringScript.ToString(), EnumResGrabJava.TodoOk, (loSbResultado.Length > 0) ? loSbResultado.ToString().Substring(1) : "");
            }
            catch (Exception e)
            {
                ControlBase.registerLog("fnSinScriptString.Error sincro: " + e.ToString());
                int x = liCount;
                loBESincronizar = new BESincronizar(true, "", EnumResGrabJava.TodoMal, e.Message);
            }
            finally
            {

            }
            return loBESincronizar;
        }


        public static BESincronizar fnSincronizarConfiguracion()
        {
            BESincronizar loBESincronizar = null;
            var loStringScript = new StringBuilder();


            try
            {
                SqlDataReader loDr = SqlHelper.ExecuteReader(new SqlConnection(connectionString), CommandType.StoredProcedure, "USPS_OBTENER_CONFIGURACION_MOVIL", null);
                while (loDr.Read())
                {
                    loStringScript.Append((String)loDr.GetValue(loDr.GetOrdinal("SCRIPT")));
                    loStringScript.Append(Environment.NewLine);

                }
                loDr.NextResult();
                var loSbResultado = new StringBuilder();
                while (loDr.Read())
                {
                    loSbResultado.Append(",");
                    loSbResultado.Append((String)loDr.GetValue(loDr.GetOrdinal("Total")));

                }

                loDr.Close();
                loBESincronizar = new BESincronizar(true, loStringScript.ToString(), EnumResGrabJava.TodoOk, (loSbResultado.Length > 0) ? loSbResultado.ToString().Substring(1) : "");


            }
            catch (Exception e)
            {
                loBESincronizar = new BESincronizar(true, "", EnumResGrabJava.TodoMal, e.Message);
            }

            return loBESincronizar;
        }


        #endregion

        #region Pedidos

        public static List<Boolean> fnInsPedido(BeanPedidoCab poPedidoCab, String psPlataforma)
        {
            List<Boolean> loLstResultadosInserciones = new List<bool>();
            Boolean lbResultado = true;
            Boolean lbErrorBD = false;
            Int32 liResultado = 0;

            SqlConnection loConnection = new SqlConnection();
            loConnection.ConnectionString = connectionString;

            #region Parametros
            SqlParameter[] loArrParamPedidosCab = new SqlParameter[15];

            loArrParamPedidosCab[0] = new SqlParameter("@PAR_CODUSUARIO", System.Data.SqlDbType.VarChar, 20);
            loArrParamPedidosCab[0].Value = poPedidoCab.codigoUsuario;
            loArrParamPedidosCab[1] = new SqlParameter("@PAR_FECHA_INICIO", System.Data.SqlDbType.VarChar, 20);
            loArrParamPedidosCab[1].Value = DateUtils.getStringDateYYMMDDHHMM(poPedidoCab.fechaInicio);
            loArrParamPedidosCab[2] = new SqlParameter("@PAR_FECHA_FIN", System.Data.SqlDbType.VarChar, 20);
            loArrParamPedidosCab[2].Value = DateUtils.getStringDateYYMMDDHHMM(poPedidoCab.fechaFin);
            loArrParamPedidosCab[3] = new SqlParameter("@PAR_MONTO_TOTAL", System.Data.SqlDbType.VarChar, 20);
            loArrParamPedidosCab[3].Value = poPedidoCab.montoTotal;
            loArrParamPedidosCab[4] = new SqlParameter("@CLI_CODIGO", System.Data.SqlDbType.VarChar, 20);
            loArrParamPedidosCab[4].Value = poPedidoCab.codigoCliente;
            loArrParamPedidosCab[5] = new SqlParameter("@PED_NOPEDIDO", System.Data.SqlDbType.VarChar, 20);
            loArrParamPedidosCab[5].Value = poPedidoCab.codigoMotivo;
            loArrParamPedidosCab[6] = new SqlParameter("@PED_CONDVENTA", System.Data.SqlDbType.VarChar, 20);
            loArrParamPedidosCab[6].Value = poPedidoCab.condicionVenta;
            if (psPlataforma.Equals("2G"))
            {
                loArrParamPedidosCab[7] = new SqlParameter("@LATITUD", System.Data.SqlDbType.VarChar, 20);
                loArrParamPedidosCab[7].Value = (Double.Parse(poPedidoCab.latitud.ToString()) / GPSConfiguration.gpsConstant).ToString();
                loArrParamPedidosCab[8] = new SqlParameter("@LONGITUD", System.Data.SqlDbType.VarChar, 20);
                loArrParamPedidosCab[8].Value = (Double.Parse(poPedidoCab.longitud.ToString()) / GPSConfiguration.gpsConstant).ToString();
            }
            else if (psPlataforma.Equals("3G"))
            {
                loArrParamPedidosCab[7] = new SqlParameter("@LATITUD", System.Data.SqlDbType.VarChar, 20);
                loArrParamPedidosCab[7].Value = poPedidoCab.doubleLatitud;
                loArrParamPedidosCab[8] = new SqlParameter("@LONGITUD", System.Data.SqlDbType.VarChar, 20);
                loArrParamPedidosCab[8].Value = poPedidoCab.doubleLongitud;
            }
            else //Android
            {
                loArrParamPedidosCab[7] = new SqlParameter("@LATITUD", System.Data.SqlDbType.VarChar, 20);
                loArrParamPedidosCab[7].Value = poPedidoCab.doubleLatitud;
                loArrParamPedidosCab[8] = new SqlParameter("@LONGITUD", System.Data.SqlDbType.VarChar, 20);
                loArrParamPedidosCab[8].Value = poPedidoCab.doubleLongitud;
            }
            loArrParamPedidosCab[9] = new SqlParameter("@PAR_FECHAACTUAL", System.Data.SqlDbType.VarChar, 20);
            loArrParamPedidosCab[9].Value = DateUtils.getStringDateYYMMDDHHMM(DateUtils.getCurrentCompleteDate());
            loArrParamPedidosCab[10] = new SqlParameter("@PED_OBSERVACION", System.Data.SqlDbType.VarChar, 100);
            loArrParamPedidosCab[10].Value = poPedidoCab.observacion;
            loArrParamPedidosCab[11] = new SqlParameter("@CELDA", System.Data.SqlDbType.VarChar, 20);
            loArrParamPedidosCab[11].Value = poPedidoCab.celda;
            loArrParamPedidosCab[12] = new SqlParameter("@ERRORCONEXION", System.Data.SqlDbType.Int);
            loArrParamPedidosCab[12].Value = poPedidoCab.errorConexion;
            loArrParamPedidosCab[13] = new SqlParameter("@ERRORPOSICION", System.Data.SqlDbType.Int);
            loArrParamPedidosCab[13].Value = poPedidoCab.errorPosicion;
            loArrParamPedidosCab[14] = new SqlParameter("@CANT_TOTAL", System.Data.SqlDbType.VarChar, 20);
            loArrParamPedidosCab[14].Value = poPedidoCab.cantidadTotal;
            #endregion

            try
            {
                liResultado = Convert.ToInt32(SqlHelper.ExecuteScalar(loConnection, "USPS_GUARDAR_PEDIDO", loArrParamPedidosCab));

                if (poPedidoCab.lstPedidoDetalle != null && liResultado != null && liResultado > -1)
                {
                    #region Insertar detalle
                    int liSecuencia = 1;
                    foreach (BeanPedidoDet loPedidoDet in poPedidoCab.lstPedidoDetalle)
                    {
                        loPedidoDet.codigoPedido = liResultado.ToString();
                       
                        //parametros
                        SqlParameter[] loArrParamPedidosDet = new SqlParameter[21];

                        loArrParamPedidosDet[0] = new SqlParameter("@PAR_CODPEDIDO", SqlDbType.VarChar, 10);
                        loArrParamPedidosDet[0].Value = loPedidoDet.codigoPedido;
                        loArrParamPedidosDet[1] = new SqlParameter("@PAR_CODARTICULO", SqlDbType.VarChar, 10);
                        loArrParamPedidosDet[1].Value = loPedidoDet.codigoArticulo;
                        loArrParamPedidosDet[2] = new SqlParameter("@PAR_CANTIDAD", SqlDbType.VarChar, 10);
                        loArrParamPedidosDet[2].Value = loPedidoDet.cantidad;
                        loArrParamPedidosDet[3] = new SqlParameter("@PAR_MONTO", SqlDbType.VarChar, 15);
                        loArrParamPedidosDet[3].Value = loPedidoDet.monto;
                        loArrParamPedidosDet[4] = new SqlParameter("@PAR_PRECIO", SqlDbType.VarChar, 20);
                        loArrParamPedidosDet[4].Value = loPedidoDet.precioBase;
                        loArrParamPedidosDet[5] = new SqlParameter("@PAR_DESCUENTO", SqlDbType.VarChar, 20);
                        loArrParamPedidosDet[5].Value = loPedidoDet.descuento;
                        loArrParamPedidosDet[6] = new SqlParameter("@PAR_DESCRIPCION", SqlDbType.VarChar, 100);
                        loArrParamPedidosDet[6].Value = loPedidoDet.descripcion;
                        loArrParamPedidosDet[7] = new SqlParameter("@PAR_TABLA", SqlDbType.VarChar, 30);
                        loArrParamPedidosDet[7].Value = loPedidoDet.tabla;
                        loArrParamPedidosDet[8] = new SqlParameter("@PAR_TIPODESC", SqlDbType.VarChar, 10);
                        loArrParamPedidosDet[8].Value = loPedidoDet.tipoDescuento;
                        loArrParamPedidosDet[9] = new SqlParameter("@PAR_BONIFICACION", SqlDbType.VarChar, 20);
                        loArrParamPedidosDet[9].Value = loPedidoDet.bonificacion;
                        loArrParamPedidosDet[10] = new SqlParameter("@PAR_CODLISTAPRECIO", SqlDbType.VarChar, 20);
                        loArrParamPedidosDet[10].Value = loPedidoDet.codListaArticulo;
                        loArrParamPedidosDet[11] = new SqlParameter("@PAR_FLETE", SqlDbType.VarChar, 20);
                        loArrParamPedidosDet[11].Value = loPedidoDet.flete;
                        loArrParamPedidosDet[12] = new SqlParameter("@PAR_CODALMACEN", SqlDbType.Int);
                        loArrParamPedidosDet[12].Value = loPedidoDet.codAlmacen;
                        loArrParamPedidosDet[13] = new SqlParameter("@PAR_SECUENCIA", SqlDbType.Int);
                        loArrParamPedidosDet[13].Value = liSecuencia;
                        loArrParamPedidosDet[14] = new SqlParameter("@PAR_CODBONIFICACION", SqlDbType.Int);
                        loArrParamPedidosDet[14].Value = loPedidoDet.codBonificacion.ToString();
                        loArrParamPedidosDet[15] = new SqlParameter("@PAR_MONTO_SOLES", SqlDbType.VarChar, 15);
                        loArrParamPedidosDet[15].Value = loPedidoDet.montoSoles;
                        loArrParamPedidosDet[16] = new SqlParameter("@PAR_MONTO_DOLAR", SqlDbType.VarChar, 15);
                        loArrParamPedidosDet[16].Value = loPedidoDet.montoDolar;
                        loArrParamPedidosDet[17] = new SqlParameter("@PAR_PRECIO_SOLES", SqlDbType.VarChar, 20);
                        loArrParamPedidosDet[17].Value = loPedidoDet.precioBaseSoles;
                        loArrParamPedidosDet[18] = new SqlParameter("@PAR_PRECIO_DOLAR", SqlDbType.VarChar, 20);
                        loArrParamPedidosDet[18].Value = loPedidoDet.precioBaseDolares;
                        loArrParamPedidosDet[19] = new SqlParameter("@TM_PK", SqlDbType.Char, 1);
                        loArrParamPedidosDet[19].Value = loPedidoDet.tipoMoneda;
                        loArrParamPedidosDet[20] = new SqlParameter("@PAR_MONTO_SIN_DESCUENTO", SqlDbType.VarChar, 100);
                        loArrParamPedidosDet[20].Value = loPedidoDet.montoSinDescuento;
                        SqlHelper.ExecuteNonQuery(loConnection, "USPS_GUARDAR_DETALLEPEDIDO", loArrParamPedidosDet);
                        liSecuencia++;
                    }
                    #endregion
                }

                #region DEPRECADO - si se considera esto aparece mensaje de error al enviar pedido repetido
                //if (liResultado == -1)//repetido
                //{
                //    lbResultado = false;
                //}
                #endregion
            }
            catch (Exception exc)
            {
                ControlBase.registerLog("fnInsPedido.Error: " + exc.Message + " : " + exc.StackTrace);

                subDelPedido(loConnection, liResultado, poPedidoCab.tipoArticulo);

                String ls = exc.Message;
                lbResultado = false;
                lbErrorBD = true;
            }

            loLstResultadosInserciones.Add(lbResultado);
            loLstResultadosInserciones.Add(lbErrorBD);

            return loLstResultadosInserciones;
        }

        public static List<object> fnInsProspecto(BEProspecto poProspecto)
        {
            List<object> loLstResultadosInserciones = new List<object>();
            String lbResultado = "";
            String lbErrorBD = "";
            Int32 liResultado = 0;

            SqlConnection loConnection = new SqlConnection();
            loConnection.ConnectionString = connectionString;

            #region Parametros
            SqlParameter[] loArrParamProspecto = new SqlParameter[15];

            loArrParamProspecto[0] = new SqlParameter("@CODIGO", System.Data.SqlDbType.VarChar, 20);
            loArrParamProspecto[0].Value = poProspecto.codigo;
            loArrParamProspecto[1] = new SqlParameter("@NOMBRE", System.Data.SqlDbType.VarChar, 100);
            loArrParamProspecto[1].Value = poProspecto.nombre;
            loArrParamProspecto[2] = new SqlParameter("@DIRECCION", System.Data.SqlDbType.VarChar, 100);
            loArrParamProspecto[2].Value = poProspecto.direccion;
            loArrParamProspecto[3] = new SqlParameter("@TCLI_COD", System.Data.SqlDbType.VarChar, 30);
            loArrParamProspecto[3].Value = poProspecto.tcli_cod;
            loArrParamProspecto[4] = new SqlParameter("@GIRO", System.Data.SqlDbType.VarChar, 100);
            loArrParamProspecto[4].Value = poProspecto.giro;
            loArrParamProspecto[5] = new SqlParameter("@LATITUD", System.Data.SqlDbType.VarChar, 50);
            loArrParamProspecto[5].Value = poProspecto.latitud;
            loArrParamProspecto[6] = new SqlParameter("@LONGITUD", System.Data.SqlDbType.VarChar, 50);
            loArrParamProspecto[6].Value = poProspecto.longitud;
            loArrParamProspecto[7] = new SqlParameter("@USER_COD", System.Data.SqlDbType.VarChar, 20);
            loArrParamProspecto[7].Value = poProspecto.codUsuario;
            loArrParamProspecto[8] = new SqlParameter("@CAMPO_ADICIONAL_1", System.Data.SqlDbType.VarChar, 100);
            loArrParamProspecto[8].Value = poProspecto.campoAdicional1;
            loArrParamProspecto[9] = new SqlParameter("@CAMPO_ADICIONAL_2", System.Data.SqlDbType.VarChar, 100);
            loArrParamProspecto[9].Value = poProspecto.campoAdicional2;
            loArrParamProspecto[10] = new SqlParameter("@CAMPO_ADICIONAL_3", System.Data.SqlDbType.VarChar, 100);
            loArrParamProspecto[10].Value = poProspecto.campoAdicional3;
            loArrParamProspecto[11] = new SqlParameter("@CAMPO_ADICIONAL_4", System.Data.SqlDbType.VarChar, 100);
            loArrParamProspecto[11].Value = poProspecto.campoAdicional4;
            loArrParamProspecto[12] = new SqlParameter("@CAMPO_ADICIONAL_5", System.Data.SqlDbType.VarChar, 100);
            loArrParamProspecto[12].Value = poProspecto.campoAdicional5;
            loArrParamProspecto[13] = new SqlParameter("@OBSERVACION", System.Data.SqlDbType.VarChar, 100);
            loArrParamProspecto[13].Value = poProspecto.observacion;
            loArrParamProspecto[14] = new SqlParameter("@FECHAMOVIL", System.Data.SqlDbType.VarChar, 20);
            loArrParamProspecto[14].Value = DateUtils.getStringDateYYMMDDHHMM(poProspecto.fecha);

            #endregion

            try
            {
                liResultado = Convert.ToInt32(SqlHelper.ExecuteScalar(loConnection, "USPS_GUARDAR_PROSPECTO", loArrParamProspecto));

                if (liResultado == -1)
                {
                    lbErrorBD = "Codigo ya existe";
                }
            }
            catch (Exception exc)
            {
                ControlBase.registerLog("fnInsProspecto.Error: " + exc.Message + " : " + exc.StackTrace);

                lbResultado = "false";
                lbErrorBD = exc.Message;
            }

            loLstResultadosInserciones.Add(lbResultado);
            loLstResultadosInserciones.Add(lbErrorBD);

            return loLstResultadosInserciones;
        }

        public static List<object> fnInsPedidoUpdate(BeanPedidoCab pedido, String plataforma)
        {
            List<object> loLstResultadosInserciones = new List<object>();

            if (pedido.enEdicion)
            {
                loLstResultadosInserciones = fnUpdPedido(pedido);
            }
            else
            {
                loLstResultadosInserciones = fnInsPedidoDir(pedido, plataforma);
            }

            return loLstResultadosInserciones;
        }

        public static List<object> fnUpdPedido(BeanPedidoCab pedido)
        {
            //construir xml con el pedido
            //construir xml con el los detalles del pedido
            //construir xml con el detalle de los controles del pedido
            List<object> loLstResultadosInserciones = new List<object>();
            Boolean lbResultado = true;
            Boolean lbErrorBD = false;
            Int32 liResultado = 0;

            SqlConnection loConnection = new SqlConnection();
            loConnection.ConnectionString = connectionString;

            #region Parametros
            SqlParameter[] loArrParamPedidosCab = new SqlParameter[1];
            pedido.fechaRegistro = DateUtils.getStringDateYYMMDDHHMM(DateUtils.getCurrentCompleteDate());
            loArrParamPedidosCab[0] = new SqlParameter("@PEDIDO", System.Data.SqlDbType.Xml);
            loArrParamPedidosCab[0].Value = pedido.ToXML();

            #endregion
            String ls = "";
            try
            {
                if (pedido.tipoArticulo.Equals("PRE"))
                    liResultado = Convert.ToInt32(SqlHelper.ExecuteScalar(loConnection, "USPS_ACTUALIZAR_PEDIDO_PRESENTACION", loArrParamPedidosCab));
                else
                    liResultado = Convert.ToInt32(SqlHelper.ExecuteScalar(loConnection, "USPS_ACTUALIZAR_PEDIDO", loArrParamPedidosCab));
            }
            catch (Exception exc)
            {
                ControlBase.registerLog("fnUpdPedido.Error: " + exc.Message + " : " + exc.StackTrace);
                ls = exc.Message;
                lbResultado = false;
                lbErrorBD = true;
            }

            loLstResultadosInserciones.Add(lbResultado);
            loLstResultadosInserciones.Add(ls);

            return loLstResultadosInserciones;
        }

        public static List<object> fnInsPedidoDir(BeanPedidoCab poPedidoCab, String psPlataforma)
        {
            List<object> loLstResultadosInserciones = new List<object>();
            Boolean lbResultado = true;
            Boolean lbErrorBD = false;
            Int32 liResultado = 0;

            SqlConnection loConnection = new SqlConnection();
            loConnection.ConnectionString = connectionString;

            #region Parametros
            SqlParameter[] loArrParamPedidosCab = new SqlParameter[29];

            loArrParamPedidosCab[0] = new SqlParameter("@PAR_CODUSUARIO", System.Data.SqlDbType.VarChar, 20);
            loArrParamPedidosCab[0].Value = poPedidoCab.codigoUsuario;
            loArrParamPedidosCab[1] = new SqlParameter("@PAR_FECHA_INICIO", System.Data.SqlDbType.VarChar, 20);
            loArrParamPedidosCab[1].Value = DateUtils.getStringDateYYMMDDHHMM(poPedidoCab.fechaInicio);
            loArrParamPedidosCab[2] = new SqlParameter("@PAR_FECHA_FIN", System.Data.SqlDbType.VarChar, 20);
            loArrParamPedidosCab[2].Value = DateUtils.getStringDateYYMMDDHHMM(poPedidoCab.fechaFin);
            loArrParamPedidosCab[3] = new SqlParameter("@PAR_MONTO_TOTAL", System.Data.SqlDbType.VarChar, 20);
            loArrParamPedidosCab[3].Value = poPedidoCab.montoTotal;
            loArrParamPedidosCab[4] = new SqlParameter("@CLI_CODIGO", System.Data.SqlDbType.VarChar, 20);
            loArrParamPedidosCab[4].Value = poPedidoCab.codigoCliente;
            loArrParamPedidosCab[5] = new SqlParameter("@PED_NOPEDIDO", System.Data.SqlDbType.VarChar, 20);
            loArrParamPedidosCab[5].Value = poPedidoCab.codigoMotivo;
            loArrParamPedidosCab[6] = new SqlParameter("@PED_CONDVENTA", System.Data.SqlDbType.VarChar, 20);
            loArrParamPedidosCab[6].Value = poPedidoCab.condicionVenta;

            loArrParamPedidosCab[7] = new SqlParameter("@LATITUD", System.Data.SqlDbType.VarChar, 20);
            loArrParamPedidosCab[7].Value = poPedidoCab.doubleLatitud;
            loArrParamPedidosCab[8] = new SqlParameter("@LONGITUD", System.Data.SqlDbType.VarChar, 20);
            loArrParamPedidosCab[8].Value = poPedidoCab.doubleLongitud;

            loArrParamPedidosCab[9] = new SqlParameter("@PAR_FECHAACTUAL", System.Data.SqlDbType.VarChar, 20);
            loArrParamPedidosCab[9].Value = DateUtils.getStringDateYYMMDDHHMM(DateUtils.getCurrentCompleteDate());
            loArrParamPedidosCab[10] = new SqlParameter("@PED_OBSERVACION", System.Data.SqlDbType.VarChar, 100);
            loArrParamPedidosCab[10].Value = poPedidoCab.observacion;
            loArrParamPedidosCab[11] = new SqlParameter("@CELDA", System.Data.SqlDbType.VarChar, 20);
            loArrParamPedidosCab[11].Value = poPedidoCab.celda;
            loArrParamPedidosCab[12] = new SqlParameter("@ERRORCONEXION", System.Data.SqlDbType.Int);
            loArrParamPedidosCab[12].Value = poPedidoCab.errorConexion;
            loArrParamPedidosCab[13] = new SqlParameter("@ERRORPOSICION", System.Data.SqlDbType.Int);
            loArrParamPedidosCab[13].Value = poPedidoCab.errorPosicion;
            loArrParamPedidosCab[14] = new SqlParameter("@CANT_TOTAL", System.Data.SqlDbType.VarChar, 20);
            loArrParamPedidosCab[14].Value = poPedidoCab.cantidadTotal;
            loArrParamPedidosCab[15] = new SqlParameter("@DIRECCION", System.Data.SqlDbType.Int);
            loArrParamPedidosCab[15].Value = int.Parse(poPedidoCab.direccion);
            loArrParamPedidosCab[16] = new SqlParameter("@DIRECCION_DESPACHO", System.Data.SqlDbType.Int);
            loArrParamPedidosCab[16].Value = int.Parse(poPedidoCab.direccionDespacho);
            loArrParamPedidosCab[17] = new SqlParameter("@FLGCOBERTURA", System.Data.SqlDbType.Char);
            loArrParamPedidosCab[17].Value = poPedidoCab.FlgEnCobertura;
            loArrParamPedidosCab[18] = new SqlParameter("@FLGTERMINADO", System.Data.SqlDbType.Char);
            loArrParamPedidosCab[18].Value = poPedidoCab.FlgTerminado;
            loArrParamPedidosCab[19] = new SqlParameter("@BONIFICACION_TOTAL", System.Data.SqlDbType.VarChar, 20);
            loArrParamPedidosCab[19].Value = poPedidoCab.bonificacionTotal;
            loArrParamPedidosCab[20] = new SqlParameter("@FLGTIPO", System.Data.SqlDbType.Char);
            loArrParamPedidosCab[20].Value = poPedidoCab.flgTipo;
            loArrParamPedidosCab[21] = new SqlParameter("@PAR_FLETE_TOTAL", System.Data.SqlDbType.VarChar, 20);
            loArrParamPedidosCab[21].Value = poPedidoCab.fleteTotal;
            loArrParamPedidosCab[22] = new SqlParameter("@ALM_PK", System.Data.SqlDbType.Int);
            loArrParamPedidosCab[22].Value = poPedidoCab.codAlmacen;
            loArrParamPedidosCab[23] = new SqlParameter("@TIPO_ARTICULO", System.Data.SqlDbType.VarChar, 3);
            loArrParamPedidosCab[23].Value = poPedidoCab.tipoArticulo;
            loArrParamPedidosCab[24] = new SqlParameter("@CREDITO_UTILIZADO", System.Data.SqlDbType.VarChar, 50);
            loArrParamPedidosCab[24].Value = poPedidoCab.creditoUtilizado;
            loArrParamPedidosCab[25] = new SqlParameter("@PAR_MONTO_TOTAL_SOLES", System.Data.SqlDbType.VarChar, 20);
            loArrParamPedidosCab[25].Value = poPedidoCab.montoTotalSoles;
            loArrParamPedidosCab[26] = new SqlParameter("@PAR_MONTO_TOTAL_DOLAR", System.Data.SqlDbType.VarChar, 20);
            loArrParamPedidosCab[26].Value = poPedidoCab.montoTotalDolar;
            loArrParamPedidosCab[27] = new SqlParameter("@TC_S", System.Data.SqlDbType.Int);
            loArrParamPedidosCab[27].Value = poPedidoCab.tipoCambioSoles;
            loArrParamPedidosCab[28] = new SqlParameter("@TC_D", System.Data.SqlDbType.Int);
            loArrParamPedidosCab[28].Value = poPedidoCab.tipoCambioDolar;
            #endregion
            String ls = "";
            try
            {
                if (poPedidoCab.CodigoPedidoServidor.Equals("0"))
                    liResultado = Convert.ToInt32(SqlHelper.ExecuteScalar(loConnection, "USPS_GUARDAR_PEDIDO", loArrParamPedidosCab));
                else
                {
                    liResultado = Convert.ToInt32(poPedidoCab.CodigoPedidoServidor);
                    //actualizar flgterminado                    
                    subUpPedido(loConnection, liResultado, poPedidoCab);
                    //eliminar las reservas del pedido
                    subDelReserva(loConnection, liResultado, poPedidoCab.tipoArticulo);
                }
                //validar que se actualicen los datos del detalle tambien.
                if (poPedidoCab.lstPedidoDetalle != null && liResultado != null && liResultado > -1)
                {
                    int liSecuencia = 0;
                    #region Insertar detalle
                    foreach (BeanPedidoDet loPedidoDet in poPedidoCab.lstPedidoDetalle)
                    {
                        liSecuencia++;
                        loPedidoDet.codigoPedido = liResultado.ToString();
                        //parametros

                        SqlParameter[] loArrParamPedidosDet = null;
                        if (poPedidoCab.tipoArticulo.Equals("PRO"))
                        {
                            loArrParamPedidosDet = new SqlParameter[21];
                        }
                        else
                        {
                            loArrParamPedidosDet = new SqlParameter[25];
                        }


                        loArrParamPedidosDet[0] = new SqlParameter("@PAR_CODPEDIDO", SqlDbType.VarChar, 20);
                        loArrParamPedidosDet[0].Value = loPedidoDet.codigoPedido;
                        loArrParamPedidosDet[1] = new SqlParameter("@PAR_CODARTICULO", SqlDbType.VarChar, 20);
                        loArrParamPedidosDet[1].Value = loPedidoDet.codigoArticulo;
                        loArrParamPedidosDet[2] = new SqlParameter("@PAR_CANTIDAD", SqlDbType.VarChar, 20);
                        loArrParamPedidosDet[2].Value = loPedidoDet.cantidad;
                        loArrParamPedidosDet[3] = new SqlParameter("@PAR_MONTO", SqlDbType.VarChar, 20);
                        loArrParamPedidosDet[3].Value = loPedidoDet.monto;
                        loArrParamPedidosDet[4] = new SqlParameter("@PAR_PRECIO", SqlDbType.VarChar, 20);
                        loArrParamPedidosDet[4].Value = loPedidoDet.precioBase;
                        loArrParamPedidosDet[5] = new SqlParameter("@PAR_DESCUENTO", SqlDbType.VarChar, 20);
                        loArrParamPedidosDet[5].Value = loPedidoDet.descuento;
                        loArrParamPedidosDet[6] = new SqlParameter("@PAR_DESCRIPCION", SqlDbType.VarChar, 30);
                        loArrParamPedidosDet[6].Value = loPedidoDet.descripcion;
                        loArrParamPedidosDet[7] = new SqlParameter("@PAR_TABLA", SqlDbType.VarChar, 50);
                        loArrParamPedidosDet[7].Value = "";
                        loArrParamPedidosDet[8] = new SqlParameter("@PAR_TIPODESC", SqlDbType.VarChar, 1);
                        loArrParamPedidosDet[8].Value = loPedidoDet.tipoDescuento;
                        loArrParamPedidosDet[9] = new SqlParameter("@PAR_BONIFICACION", SqlDbType.VarChar, 20);
                        loArrParamPedidosDet[9].Value = loPedidoDet.bonificacion;
                        loArrParamPedidosDet[10] = new SqlParameter("@PAR_CODLISTAPRECIO", SqlDbType.VarChar, 20);
                        loArrParamPedidosDet[10].Value = loPedidoDet.codListaArticulo;
                        loArrParamPedidosDet[11] = new SqlParameter("@PAR_FLETE", SqlDbType.VarChar, 20);
                        loArrParamPedidosDet[11].Value = loPedidoDet.flete;
                        loArrParamPedidosDet[12] = new SqlParameter("@PAR_CODALMACEN", SqlDbType.Int);
                        loArrParamPedidosDet[12].Value = loPedidoDet.codAlmacen;

                        loArrParamPedidosDet[13] = new SqlParameter("@PAR_SECUENCIA", SqlDbType.Int);
                        loArrParamPedidosDet[13].Value = liSecuencia;

                        loArrParamPedidosDet[14] = new SqlParameter("@PAR_CODBONIFICACION", SqlDbType.VarChar, 20);
                        loArrParamPedidosDet[14].Value = (loPedidoDet.codBonificacion).ToString();

                        loArrParamPedidosDet[15] = new SqlParameter("@PAR_MONTO_SOLES", SqlDbType.VarChar, 20);
                        loArrParamPedidosDet[15].Value = loPedidoDet.montoSoles;
                        loArrParamPedidosDet[16] = new SqlParameter("@PAR_MONTO_DOLAR", SqlDbType.VarChar, 20);
                        loArrParamPedidosDet[16].Value = loPedidoDet.montoDolar;
                        loArrParamPedidosDet[17] = new SqlParameter("@PAR_PRECIO_SOLES", SqlDbType.VarChar, 20);
                        loArrParamPedidosDet[17].Value = loPedidoDet.precioBaseSoles;
                        loArrParamPedidosDet[18] = new SqlParameter("@PAR_PRECIO_DOLAR", SqlDbType.VarChar, 20);
                        loArrParamPedidosDet[18].Value = loPedidoDet.precioBaseDolares;
                        loArrParamPedidosDet[19] = new SqlParameter("@TM_PK", SqlDbType.Char, 1);
                        loArrParamPedidosDet[19].Value = loPedidoDet.tipoMoneda;

                        if (poPedidoCab.tipoArticulo.Equals("PRE"))
                        {
                            loArrParamPedidosDet[20] = new SqlParameter("@PAR_CANTIDAD_PRE", SqlDbType.VarChar, 20);
                            loArrParamPedidosDet[20].Value = loPedidoDet.cantidadPre;
                            loArrParamPedidosDet[21] = new SqlParameter("@PAR_CANTIDAD_FRAC", SqlDbType.VarChar, 20);
                            loArrParamPedidosDet[21].Value = loPedidoDet.cantidadFrac;
                            loArrParamPedidosDet[22] = new SqlParameter("@PAR_PRECIO_FRAC", SqlDbType.VarChar, 20);
                            loArrParamPedidosDet[22].Value = loPedidoDet.precioFrac;
                            loArrParamPedidosDet[23] = new SqlParameter("@PAR_MONTO_SIN_DESCUENTO", SqlDbType.VarChar, 100);
                            loArrParamPedidosDet[23].Value = loPedidoDet.montoSinDescuento;
                            loArrParamPedidosDet[24] = new SqlParameter("@PAR_BONIFICACION_FRAC", SqlDbType.VarChar, 20);
                            loArrParamPedidosDet[24].Value = loPedidoDet.bonificacionFrac;
                            SqlHelper.ExecuteNonQuery(loConnection, "USPS_GUARDAR_DETALLEPEDIDO_PRESENTACION", loArrParamPedidosDet);
                        }
                        else
                        {
                            loArrParamPedidosDet[20] = new SqlParameter("@PAR_MONTO_SIN_DESCUENTO", SqlDbType.VarChar, 100);
                            loArrParamPedidosDet[20].Value = loPedidoDet.montoSinDescuento;

                            SqlHelper.ExecuteNonQuery(loConnection, "USPS_GUARDAR_DETALLEPEDIDO", loArrParamPedidosDet);
                        }
                    }
                    #endregion
                }


                if (poPedidoCab.listaCtrlPedido != null && liResultado != null && liResultado > -1)
                {
                    #region Insertar Controles
                    foreach (BEEnvCtrlPedido loPedidoCtrl in poPedidoCab.listaCtrlPedido)
                    {
                        loPedidoCtrl.idPedido = liResultado.ToString();
                        //parametros
                        SqlParameter[] loArrParamPedidosCtrl = new SqlParameter[6];
                        loArrParamPedidosCtrl[0] = new SqlParameter("@PAR_IDPEDIDO", SqlDbType.VarChar, 20);
                        loArrParamPedidosCtrl[0].Value = loPedidoCtrl.idPedido;
                        loArrParamPedidosCtrl[1] = new SqlParameter("@PAR_ETIQUETACONTROL", SqlDbType.VarChar, 100);
                        loArrParamPedidosCtrl[1].Value = loPedidoCtrl.etiquetaControl;
                        loArrParamPedidosCtrl[2] = new SqlParameter("@PAR_IDTIPOCONTROL", SqlDbType.VarChar, 50);
                        loArrParamPedidosCtrl[2].Value = loPedidoCtrl.idTipoControl;
                        loArrParamPedidosCtrl[3] = new SqlParameter("@PAR_VALORCONTROL", SqlDbType.VarChar, 100);
                        loArrParamPedidosCtrl[3].Value = loPedidoCtrl.valorControl;
                        loArrParamPedidosCtrl[4] = new SqlParameter("@PAR_IDCONTROL", SqlDbType.VarChar, 20);
                        loArrParamPedidosCtrl[4].Value = loPedidoCtrl.idControl;
                        loArrParamPedidosCtrl[5] = new SqlParameter("@PAR_IDFORMULARIO", SqlDbType.VarChar, 20);
                        loArrParamPedidosCtrl[5].Value = loPedidoCtrl.idFormulario;

                        SqlHelper.ExecuteNonQuery(loConnection, "USPS_GUARDAR_CONTROLPEDIDO", loArrParamPedidosCtrl);
                    }

                    #endregion

                }


            }
            catch (Exception exc)
            {
                ControlBase.registerLog("fnInsPedido.Error: " + exc.Message + " : " + exc.StackTrace);

                //subDelPedido(loConnection, liResultado, poPedidoCab.tipoArticulo);

                ls = exc.Message;
                lbResultado = false;
                lbErrorBD = true;
            }

            loLstResultadosInserciones.Add(lbResultado);
            loLstResultadosInserciones.Add(ls);

            return loLstResultadosInserciones;
        }

        #region Procedimientos Android

        public static BEValidaPedidoAndroid fnInsPedidoDir_Validar(BeanPedidoCab poPedidoCab, String psPlataforma)
        {
            BEValidaPedidoAndroid loBEValidaPedidoAndroid;
            List<BeanPedidoDet> lolstDetallePedidos = new List<BeanPedidoDet>();
            BeanPedidoDet loBeanPedidoDet;
            Int32 liResultado = 0;
            SqlConnection loConnection = new SqlConnection();
            loConnection.ConnectionString = connectionString;
            int numReservados = 0;

            #region Parametros
            SqlParameter[] loArrParamPedidosCab = new SqlParameter[29];

            loArrParamPedidosCab[0] = new SqlParameter("@PAR_CODUSUARIO", System.Data.SqlDbType.VarChar, 20);
            loArrParamPedidosCab[0].Value = poPedidoCab.codigoUsuario;
            loArrParamPedidosCab[1] = new SqlParameter("@PAR_FECHA_INICIO", System.Data.SqlDbType.VarChar, 20);
            loArrParamPedidosCab[1].Value = DateUtils.getStringDateYYMMDDHHMM(poPedidoCab.fechaInicio);
            loArrParamPedidosCab[2] = new SqlParameter("@PAR_FECHA_FIN", System.Data.SqlDbType.VarChar, 20);
            loArrParamPedidosCab[2].Value = DateUtils.getStringDateYYMMDDHHMM(poPedidoCab.fechaFin);
            loArrParamPedidosCab[3] = new SqlParameter("@PAR_MONTO_TOTAL", System.Data.SqlDbType.VarChar, 20);
            loArrParamPedidosCab[3].Value = poPedidoCab.montoTotal;
            loArrParamPedidosCab[4] = new SqlParameter("@CLI_CODIGO", System.Data.SqlDbType.VarChar, 20);
            loArrParamPedidosCab[4].Value = poPedidoCab.codigoCliente;
            loArrParamPedidosCab[5] = new SqlParameter("@PED_NOPEDIDO", System.Data.SqlDbType.VarChar, 20);
            loArrParamPedidosCab[5].Value = poPedidoCab.codigoMotivo;
            loArrParamPedidosCab[6] = new SqlParameter("@PED_CONDVENTA", System.Data.SqlDbType.VarChar, 20);
            loArrParamPedidosCab[6].Value = poPedidoCab.condicionVenta;

            loArrParamPedidosCab[7] = new SqlParameter("@LATITUD", System.Data.SqlDbType.VarChar, 20);
            loArrParamPedidosCab[7].Value = poPedidoCab.doubleLatitud;
            loArrParamPedidosCab[8] = new SqlParameter("@LONGITUD", System.Data.SqlDbType.VarChar, 20);
            loArrParamPedidosCab[8].Value = poPedidoCab.doubleLongitud;

            loArrParamPedidosCab[9] = new SqlParameter("@PAR_FECHAACTUAL", System.Data.SqlDbType.VarChar, 20);
            loArrParamPedidosCab[9].Value = DateUtils.getStringDateYYMMDDHHMM(DateUtils.getCurrentCompleteDate());
            loArrParamPedidosCab[10] = new SqlParameter("@PED_OBSERVACION", System.Data.SqlDbType.VarChar, 100);
            loArrParamPedidosCab[10].Value = poPedidoCab.observacion;
            loArrParamPedidosCab[11] = new SqlParameter("@CELDA", System.Data.SqlDbType.VarChar, 20);
            loArrParamPedidosCab[11].Value = poPedidoCab.celda;
            loArrParamPedidosCab[12] = new SqlParameter("@ERRORCONEXION", System.Data.SqlDbType.Int);
            loArrParamPedidosCab[12].Value = poPedidoCab.errorConexion;
            loArrParamPedidosCab[13] = new SqlParameter("@ERRORPOSICION", System.Data.SqlDbType.Int);
            loArrParamPedidosCab[13].Value = poPedidoCab.errorPosicion;
            loArrParamPedidosCab[14] = new SqlParameter("@CANT_TOTAL", System.Data.SqlDbType.VarChar, 20);
            loArrParamPedidosCab[14].Value = poPedidoCab.cantidadTotal;
            loArrParamPedidosCab[15] = new SqlParameter("@DIRECCION", System.Data.SqlDbType.Int);
            loArrParamPedidosCab[15].Value = int.Parse(poPedidoCab.direccion);
            loArrParamPedidosCab[16] = new SqlParameter("@DIRECCION_DESPACHO", System.Data.SqlDbType.Int);
            loArrParamPedidosCab[16].Value = int.Parse(poPedidoCab.direccionDespacho);
            loArrParamPedidosCab[17] = new SqlParameter("@FLGCOBERTURA", System.Data.SqlDbType.Char);
            loArrParamPedidosCab[17].Value = poPedidoCab.FlgEnCobertura;
            loArrParamPedidosCab[18] = new SqlParameter("@FLGTERMINADO", System.Data.SqlDbType.Char);
            loArrParamPedidosCab[18].Value = poPedidoCab.FlgTerminado;
            loArrParamPedidosCab[19] = new SqlParameter("@BONIFICACION_TOTAL", System.Data.SqlDbType.VarChar, 20);
            loArrParamPedidosCab[19].Value = poPedidoCab.bonificacionTotal;
            loArrParamPedidosCab[20] = new SqlParameter("@FLGTIPO", System.Data.SqlDbType.Char);
            loArrParamPedidosCab[20].Value = poPedidoCab.flgTipo;
            loArrParamPedidosCab[21] = new SqlParameter("@PAR_FLETE_TOTAL", System.Data.SqlDbType.VarChar, 20);
            loArrParamPedidosCab[21].Value = poPedidoCab.fleteTotal;
            loArrParamPedidosCab[22] = new SqlParameter("@ALM_PK", System.Data.SqlDbType.Int);
            loArrParamPedidosCab[22].Value = poPedidoCab.codAlmacen;
            loArrParamPedidosCab[23] = new SqlParameter("@TIPO_ARTICULO", System.Data.SqlDbType.VarChar, 3);
            loArrParamPedidosCab[23].Value = poPedidoCab.tipoArticulo;
            loArrParamPedidosCab[24] = new SqlParameter("@CREDITO_UTILIZADO", System.Data.SqlDbType.VarChar, 50);
            loArrParamPedidosCab[24].Value = poPedidoCab.creditoUtilizado;
            loArrParamPedidosCab[25] = new SqlParameter("@PAR_MONTO_TOTAL_SOLES", System.Data.SqlDbType.VarChar, 20);
            loArrParamPedidosCab[25].Value = poPedidoCab.montoTotalSoles;
            loArrParamPedidosCab[26] = new SqlParameter("@PAR_MONTO_TOTAL_DOLAR", System.Data.SqlDbType.VarChar, 20);
            loArrParamPedidosCab[26].Value = poPedidoCab.montoTotalDolar;
            loArrParamPedidosCab[27] = new SqlParameter("@TC_S", System.Data.SqlDbType.Int);
            loArrParamPedidosCab[27].Value = poPedidoCab.tipoCambioSoles;
            loArrParamPedidosCab[28] = new SqlParameter("@TC_D", System.Data.SqlDbType.Int);
            loArrParamPedidosCab[28].Value = poPedidoCab.tipoCambioDolar;

            #endregion

            try
            {
                if (poPedidoCab.CodigoPedidoServidor.Equals("0"))
                {//pedido nuevo
                    liResultado = Convert.ToInt32(SqlHelper.ExecuteScalar(loConnection, "USPS_GUARDAR_PEDIDO", loArrParamPedidosCab));
                }
                else
                {//pedido ya tiene cabecera
                    liResultado = Convert.ToInt32(poPedidoCab.CodigoPedidoServidor);
                }

                //eliminar las reservas que tiene el pedido
                subDelReserva(loConnection, liResultado, poPedidoCab.tipoArticulo);

                //hacer las nuevas validaciones y reservas
                if (poPedidoCab.lstPedidoDetalle != null && liResultado > -1)
                {
                    #region Validar detalles
                    foreach (BeanPedidoDet loPedidoDet in poPedidoCab.lstPedidoDetalle)
                    {
                        loPedidoDet.codigoPedido = liResultado.ToString();
                        loBeanPedidoDet = fnReservar(loPedidoDet, poPedidoCab.tipoArticulo);
                        if (loBeanPedidoDet.atendible.Equals("1"))
                            numReservados++;
                        lolstDetallePedidos.Add(loBeanPedidoDet);
                    }
                    #endregion                  
                }

                loBEValidaPedidoAndroid = new BEValidaPedidoAndroid(lolstDetallePedidos, Constantes.ValidarGrabar.RESERVO_CORRECTO, Convert.ToString(liResultado), (Int32)EnumResGrabJava.TodoOk, numReservados);
            }
            catch (Exception exc)
            {
                ControlBase.registerLog("fnInsPedido.Error: " + exc.Message + " : " + exc.StackTrace);
                subDelPedido(loConnection, liResultado, poPedidoCab.tipoArticulo);
                subDelReserva(loConnection, liResultado, poPedidoCab.tipoArticulo);
                String ls = exc.Message;
                loBEValidaPedidoAndroid = new BEValidaPedidoAndroid(ls, (Int32)EnumResGrabJava.TodoMal);
            }
            return loBEValidaPedidoAndroid;
        }


        public static List<Boolean> fnDelPedidoDir(String psCodPedido, String psTipoArticulo, bool enEdicion)
        {
            SqlConnection loConnection = new SqlConnection();
            loConnection.ConnectionString = connectionString;

            List<Boolean> loLstResultados = new List<bool>();
            Boolean lbResultado = true;
            Boolean lbErrorBD = false;

            try
            {
                subDelReserva(loConnection, Convert.ToInt32(psCodPedido), psTipoArticulo);
                if (!enEdicion)
                    subDelPedido(loConnection, Convert.ToInt32(psCodPedido), psTipoArticulo);
            }
            catch (Exception exc)
            {
                ControlBase.registerLog("fnDelPedido.Error: " + exc.Message + " : " + exc.StackTrace);
                lbResultado = false;
                lbErrorBD = true;
            }

            loLstResultados.Add(lbResultado);
            loLstResultados.Add(lbErrorBD);

            return loLstResultados;
        }


        public static BeanPedidoDet fnReservar(BeanPedidoDet poPedidoDet, String psTipoArticulo)
        {
            BeanPedidoDet loBeanPedidoDet = poPedidoDet;
            ArrayList loLstParametros = new ArrayList();
            SqlParameter loParametro;

            loParametro = new SqlParameter("@PAR_CODPEDIDO", SqlDbType.VarChar, 20);
            loParametro.Value = poPedidoDet.codigoPedido;
            loLstParametros.Add(loParametro);

            if (psTipoArticulo.Equals("PRO"))
            {
                loParametro = new SqlParameter("@PAR_CODPRODUCTO", SqlDbType.VarChar, 20);
                loParametro.Value = poPedidoDet.codigoArticulo;
                loLstParametros.Add(loParametro);
            }
            else
            {
                loParametro = new SqlParameter("@PAR_CODPRESENTACION", SqlDbType.VarChar, 20);
                loParametro.Value = poPedidoDet.codigoArticulo;
                loLstParametros.Add(loParametro);
                loParametro = new SqlParameter("@PAR_CANTIDAD_PRE", SqlDbType.VarChar, 20);
                loParametro.Value = poPedidoDet.cantidadPre;
                loLstParametros.Add(loParametro);
                loParametro = new SqlParameter("@PAR_CANTIDAD_FRAC", SqlDbType.VarChar, 20);
                loParametro.Value = poPedidoDet.cantidadFrac;
                loLstParametros.Add(loParametro);
                loParametro = new SqlParameter("@PAR_BONIFICACION_FRAC", SqlDbType.VarChar, 20);
                loParametro.Value = poPedidoDet.bonificacionFrac;
                loLstParametros.Add(loParametro);

            }
            loParametro = new SqlParameter("@PAR_CANTIDAD", SqlDbType.VarChar, 20);
            loParametro.Value = poPedidoDet.cantidad;
            loLstParametros.Add(loParametro);
            loParametro = new SqlParameter("@FLGENABLE", SqlDbType.VarChar, 1);
            loParametro.Value = "0";//validar uso
            loLstParametros.Add(loParametro);
            loParametro = new SqlParameter("@PAR_BONIFICACION", SqlDbType.VarChar, 20);
            loParametro.Value = poPedidoDet.bonificacion;
            loLstParametros.Add(loParametro);
            loParametro = new SqlParameter("@PAR_CODALMACEN", SqlDbType.Int, 20);
            loParametro.Value = poPedidoDet.codAlmacen;
            loLstParametros.Add(loParametro);

            DataSet loDsConsulta = null;
            if (psTipoArticulo.Equals("PRO"))
            {
                loDsConsulta = DBUtilSQLServer.getDataset("USPS_RESERVAR_STOCK", loLstParametros);
            }
            else
            {
                loDsConsulta = DBUtilSQLServer.getDataset("USPS_RESERVAR_STOCK_PRESENTACION", loLstParametros);
            }

            foreach (DataRow loDrConsulta in loDsConsulta.Tables[0].Rows)
            {
                loBeanPedidoDet.stock = loDrConsulta["STOCK"].ToString().ToString().Trim().ToUpper();
                loBeanPedidoDet.atendible = loDrConsulta["ATENDIBLE"].ToString().ToString().Trim().ToUpper();
            }
            return loBeanPedidoDet;
        }

        public static void subDelReserva(SqlConnection poConnection, Int32 piResultadoInsPedido, String psTipoAlmacen)
        {
            if (piResultadoInsPedido > 0)
            {
                try
                {
                    SqlParameter[] loParameter = new SqlParameter[1];

                    loParameter[0] = new SqlParameter("@PAR_CODPEDIDO", SqlDbType.VarChar, 20);
                    loParameter[0].Value = piResultadoInsPedido.ToString();

                    if (psTipoAlmacen.Equals("PRO"))
                    {
                        SqlHelper.ExecuteNonQuery(poConnection, "USPD_ELIMINAR_RESERVA", loParameter);
                    }
                    else
                    {
                        SqlHelper.ExecuteNonQuery(poConnection, "USPD_ELIMINAR_RESERVA_PRESENTACION", loParameter);
                    }
                }
                catch (Exception e)
                {
                    ControlBase.registerLog("fnDelReserva.Error: " + e.Message + " : " + e.StackTrace);
                }
            }
        }

        public static void subUpPedido(SqlConnection poConnection, Int32 piResultadoInsPedido, BeanPedidoCab poBean)
        {
            if (piResultadoInsPedido > 0)
            {
                try
                {
                    SqlParameter[] loParameter = new SqlParameter[10];

                    loParameter[0] = new SqlParameter("@PAR_CODPEDIDO", SqlDbType.VarChar, 20);
                    loParameter[0].Value = piResultadoInsPedido.ToString();

                    loParameter[1] = new SqlParameter("@PAR_FECHA_FIN", SqlDbType.VarChar, 20);
                    loParameter[1].Value = DateUtils.getStringDateYYMMDDHHMM(poBean.fechaFin);

                    loParameter[2] = new SqlParameter("@FLGTERMIANDO", SqlDbType.Char, 1);
                    loParameter[2].Value = poBean.FlgTerminado;

                    loParameter[3] = new SqlParameter("@MONTO", SqlDbType.VarChar, 20);
                    loParameter[3].Value = poBean.montoTotal;

                    loParameter[4] = new SqlParameter("@CANTIDAD", SqlDbType.VarChar, 20);
                    loParameter[4].Value = poBean.cantidadTotal;

                    loParameter[5] = new SqlParameter("@OBSERVACION", SqlDbType.VarChar, 100);
                    loParameter[5].Value = poBean.observacion;

                    loParameter[6] = new SqlParameter("@CODDIRECCION", SqlDbType.VarChar, 20);
                    loParameter[6].Value = poBean.direccion;

                    loParameter[7] = new SqlParameter("@FLGENCOBERTURA", SqlDbType.Char);
                    loParameter[7].Value = poBean.FlgEnCobertura;

                    loParameter[8] = new SqlParameter("@BONIFICACION", SqlDbType.VarChar, 20);
                    loParameter[8].Value = poBean.bonificacionTotal;

                    loParameter[9] = new SqlParameter("@CREDITO_UTILIZADO", SqlDbType.VarChar, 20);
                    loParameter[9].Value = poBean.creditoUtilizado;



                    SqlHelper.ExecuteNonQuery(poConnection, "USPU_PEDIDO", loParameter);
                }
                catch (Exception e)
                {
                    ControlBase.registerLog("fnUpPedido.Error: " + e.Message + " : " + e.StackTrace);
                }
            }
        }





        #endregion


        public static void subDelPedido(SqlConnection poConnection, Int32 piResultadoInsPedido, String psTipoArticulo)
        {
            if (piResultadoInsPedido > 0)
            {
                try
                {
                    SqlParameter[] loParameter = new SqlParameter[1];

                    loParameter[0] = new SqlParameter("@PAR_CODPEDIDO", SqlDbType.VarChar, 20);
                    loParameter[0].Value = piResultadoInsPedido.ToString();

                    if (psTipoArticulo.Equals("PRO"))
                    {
                        SqlHelper.ExecuteNonQuery(poConnection, "USPD_ELIMINARPEDIDO", loParameter);
                    }
                    else
                    {
                        SqlHelper.ExecuteNonQuery(poConnection, "USPD_ELIMINARPEDIDO_PRESENTACION", loParameter);
                    }
                }
                catch (Exception e)
                {
                    ControlBase.registerLog("fnDelPedido.Error: " + e.Message + " : " + e.StackTrace);
                }
            }
        }
        #endregion

        #region Cobranzas
        public static List<bool> fnInsPago(BeanPago poPago, String psPlataforma)
        {
            List<bool> lbLstResultadoInsPago = new List<bool>();
            Boolean lbResultado = true;
            Boolean lbErrorBD = false;
            Int32 liResultado;

            #region Parametros
            SqlParameter[] loArrParametros = new SqlParameter[22];

            loArrParametros[0] = new SqlParameter("@cod_cobranza", System.Data.SqlDbType.VarChar, 20);
            loArrParametros[0].Value = poPago.idDocCobranza;
            loArrParametros[1] = new SqlParameter("@id_usuario", System.Data.SqlDbType.VarChar, 20);
            loArrParametros[1].Value = poPago.idUsuario;
            loArrParametros[2] = new SqlParameter("@id_cliente", System.Data.SqlDbType.VarChar, 20);
            loArrParametros[2].Value = poPago.idCliente;
            loArrParametros[3] = new SqlParameter("@pago", System.Data.SqlDbType.VarChar, 20);
            loArrParametros[3].Value = poPago.pago;
            loArrParametros[4] = new SqlParameter("@tipo_pago", System.Data.SqlDbType.VarChar, 20);
            loArrParametros[4].Value = poPago.idFormaPago;
            loArrParametros[5] = new SqlParameter("@nro_doc", System.Data.SqlDbType.VarChar, 20);
            loArrParametros[5].Value = poPago.nroDocumento;
            loArrParametros[6] = new SqlParameter("@cod_banco", System.Data.SqlDbType.VarChar, 10);
            loArrParametros[6].Value = poPago.idBanco;

            loArrParametros[7] = new SqlParameter("@Latitud", System.Data.SqlDbType.VarChar, 20);
            loArrParametros[7].Value = Double.Parse(poPago.latitud);
            loArrParametros[8] = new SqlParameter("@Longitud", System.Data.SqlDbType.VarChar, 20);
            loArrParametros[8].Value = Double.Parse(poPago.longitud);


            loArrParametros[9] = new SqlParameter("@FecMovil", System.Data.SqlDbType.VarChar, 20);
            loArrParametros[9].Value = DateUtils.getStringDateYYMMDDHHMM(poPago.fechaMovil);
            loArrParametros[10] = new SqlParameter("@par_fechaactual", System.Data.SqlDbType.VarChar, 20);
            loArrParametros[10].Value = DateUtils.getStringDateYYMMDDHHMM(DateUtils.getCurrentCompleteDate());
            loArrParametros[11] = new SqlParameter("@CELDA", System.Data.SqlDbType.VarChar, 20);
            loArrParametros[11].Value = poPago.celda;
            loArrParametros[12] = new SqlParameter("@ERRORCONEXION", System.Data.SqlDbType.Int);
            loArrParametros[12].Value = poPago.errorConexion;
            loArrParametros[13] = new SqlParameter("@ERRORPOSICION", System.Data.SqlDbType.Int);
            loArrParametros[13].Value = poPago.errorPosicion;
            loArrParametros[14] = new SqlParameter("@FLGCOBERTURA", System.Data.SqlDbType.Char);
            loArrParametros[14].Value = poPago.FlgEnCobertura;

            loArrParametros[15] = new SqlParameter("@FECHA_DIFERIDA", System.Data.SqlDbType.VarChar, 20);
            loArrParametros[15].Value = DateUtils.getStringDateYYMMDDHHMM(poPago.fechaDiferida);

            loArrParametros[16] = new SqlParameter("@pagosoles", System.Data.SqlDbType.VarChar, 20);
            loArrParametros[16].Value = poPago.pagosoles;
            loArrParametros[17] = new SqlParameter("@pagodolares", System.Data.SqlDbType.VarChar, 20);
            loArrParametros[17].Value = poPago.pagodolares;
            loArrParametros[18] = new SqlParameter("@TM_SOLES", System.Data.SqlDbType.Int);
            loArrParametros[18].Value = poPago.tipomonedasoles;
            loArrParametros[19] = new SqlParameter("@TM_DOLARES", System.Data.SqlDbType.Int);
            loArrParametros[19].Value = poPago.tipomonedadolares;
            loArrParametros[20] = new SqlParameter("@TC_SOLES", System.Data.SqlDbType.Int);
            loArrParametros[20].Value = poPago.idtipocambiosoles;
            loArrParametros[21] = new SqlParameter("@TC_DOLARES", System.Data.SqlDbType.Int);
            loArrParametros[21].Value = poPago.idtipocambiodolares;
            #endregion

            try
            {
                liResultado = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, "USPI_PAGO_COBRANZA_CARGATRANS", loArrParametros));

                #region DEPRECADO - si se considera esto aparece mensaje de error al enviar pedido repetido
                if (liResultado == -1)//repetido
                {
                    lbResultado = false;
                }

                #endregion
            }
            catch (Exception e)
            {
                ControlBase.registerLog("fnInsPago.Erro: " + e.Message + " : " + e.StackTrace);
                lbResultado = false;
                lbErrorBD = true;

            }

            lbLstResultadoInsPago.Add(lbResultado);
            lbLstResultadoInsPago.Add(lbErrorBD);

            return lbLstResultadoInsPago;
        }
        #endregion

        public static BeanClienteDireccion fnInsDireccion(BeanClienteDireccion poClienteDireccion)
        {
            BeanClienteDireccion loResponseClienteDireccion = new BeanClienteDireccion();
            //Int32 liResultado;

            ArrayList loLstParametros = new ArrayList();
            SqlParameter loParametro;

            #region Parametros ClienteDireccion

            loParametro = new SqlParameter("@CLI_CODIGO", System.Data.SqlDbType.VarChar, 20);
            loParametro.Value = poClienteDireccion.codCliente;
            loLstParametros.Add(loParametro);
            loParametro = new SqlParameter("@DIR_NOMBRE", System.Data.SqlDbType.VarChar, 100);
            loParametro.Value = poClienteDireccion.nombre;
            loLstParametros.Add(loParametro);
            loParametro = new SqlParameter("@LATITUD", System.Data.SqlDbType.VarChar, 50);
            loParametro.Value = poClienteDireccion.latitud;
            loLstParametros.Add(loParametro);
            loParametro = new SqlParameter("@LONGITUD", System.Data.SqlDbType.VarChar, 50);
            loParametro.Value = poClienteDireccion.longitud;
            loLstParametros.Add(loParametro);
            loParametro = new SqlParameter("@DIR_TIPO", System.Data.SqlDbType.Char);
            loParametro.Value = poClienteDireccion.tipo;
            loLstParametros.Add(loParametro);
            loParametro = new SqlParameter("@FECCREACION", System.Data.SqlDbType.VarChar, 20);
            loParametro.Value = DateUtils.getStringDateYYMMDDHHMM(poClienteDireccion.fechaCreacion);
            loLstParametros.Add(loParametro);

            #endregion

            try
            {

                DataSet loDsManager = DBUtilSQLServer.getDataset("USPW_CLIENTEDIRECCION_MOVIL_I", loLstParametros);

                if (loDsManager.Tables.Count > 0)
                {
                    loResponseClienteDireccion.pk = loDsManager.Tables[0].Rows[0]["DIR_PK"].ToString().Trim();
                    loResponseClienteDireccion.codDireccion = loDsManager.Tables[0].Rows[0]["DIR_CODIGO"].ToString().Trim();
                    loResponseClienteDireccion.codCliente = poClienteDireccion.codCliente;
                    loResponseClienteDireccion.fechaCreacion = poClienteDireccion.fechaCreacion;
                    loResponseClienteDireccion.error = 1;
                    loResponseClienteDireccion.mensaje = "Se Grabo Correctamente";
                }
                else
                {
                    loResponseClienteDireccion.error = -1;
                    loResponseClienteDireccion.mensaje = "Error en la BD";
                }


            }
            catch (Exception ex)
            {
                loResponseClienteDireccion.error = -1;
                loResponseClienteDireccion.mensaje = ex.Message;
                ControlBase.registerLog("fnInsCanje.Error: " + ex.Message + " : " + ex.StackTrace);

            }

            return loResponseClienteDireccion;
        }

        public static List<Boolean> fnInsCanje(BeanCanjeCab poCanjeCab, String psPlataforma)
        {
            List<Boolean> lbLstResultadosInsCanje = new List<bool>();
            Boolean lbResultado = true;
            Boolean lbErrorBD = false;
            Int32 liResultado;

            SqlConnection loConnection = new SqlConnection();
            loConnection.ConnectionString = connectionString;

            #region Parametros Canje cabecera
            SqlParameter[] loParametrosCanjeCab = new SqlParameter[13];

            loParametrosCanjeCab[0] = new SqlParameter("@COD_CANJE", System.Data.SqlDbType.VarChar, 30);
            loParametrosCanjeCab[0].Value = poCanjeCab.codigoCanje;
            loParametrosCanjeCab[1] = new SqlParameter("@ID_USUARIO", System.Data.SqlDbType.VarChar, 20);
            loParametrosCanjeCab[1].Value = poCanjeCab.idUsuario;
            loParametrosCanjeCab[2] = new SqlParameter("@ID_CLIENTE", System.Data.SqlDbType.VarChar, 20);
            loParametrosCanjeCab[2].Value = poCanjeCab.idCliente;
            loParametrosCanjeCab[3] = new SqlParameter("@CCAB_NRODOC", System.Data.SqlDbType.VarChar, 20);
            loParametrosCanjeCab[3].Value = poCanjeCab.numeroDocumento;

            loParametrosCanjeCab[4] = new SqlParameter("@Latitud", System.Data.SqlDbType.VarChar, 20);
            loParametrosCanjeCab[4].Value = Double.Parse(poCanjeCab.latitud);
            loParametrosCanjeCab[5] = new SqlParameter("@Longitud", System.Data.SqlDbType.VarChar, 20);
            loParametrosCanjeCab[5].Value = Double.Parse(poCanjeCab.longitud);

            loParametrosCanjeCab[6] = new SqlParameter("@FecMovil", System.Data.SqlDbType.VarChar, 20);
            loParametrosCanjeCab[6].Value = DateUtils.getStringDateYYMMDDHHMM(poCanjeCab.fechaMovil);
            loParametrosCanjeCab[7] = new SqlParameter("@PAR_FECHAACTUAL", System.Data.SqlDbType.VarChar, 20);
            loParametrosCanjeCab[7].Value = DateUtils.getStringDateYYMMDDHHMM(DateUtils.getCurrentCompleteDate());
            loParametrosCanjeCab[8] = new SqlParameter("@CELDA", System.Data.SqlDbType.VarChar, 20);
            loParametrosCanjeCab[8].Value = poCanjeCab.celda;
            loParametrosCanjeCab[9] = new SqlParameter("@ERRORCONEXION", System.Data.SqlDbType.Int);
            loParametrosCanjeCab[9].Value = poCanjeCab.errorConexion;
            loParametrosCanjeCab[10] = new SqlParameter("@ERRORPOSICION", System.Data.SqlDbType.Int);
            loParametrosCanjeCab[10].Value = poCanjeCab.errorPosicion;
            loParametrosCanjeCab[11] = new SqlParameter("@FLGCOBERTURA", System.Data.SqlDbType.Char);
            loParametrosCanjeCab[11].Value = poCanjeCab.FlgEnCobertura;
            loParametrosCanjeCab[12] = new SqlParameter("@TIPO_ARTICULO", System.Data.SqlDbType.Char, 3);
            loParametrosCanjeCab[12].Value = poCanjeCab.tipoArticulo;
            #endregion

            try
            {
                liResultado = Convert.ToInt32(SqlHelper.ExecuteScalar(loConnection, "USPI_CAB_CANJE", loParametrosCanjeCab));
                if (liResultado != -1)
                {
                    #region Insertar detalle
                    foreach (BeanCanjeDet loCanjeDet in poCanjeCab.lstCanjeDet)
                    {
                        loCanjeDet.idCanje = liResultado;

                        #region Parametros Canje cabecera
                        SqlParameter[] loParametrosCanjeDet = null;
                        if (Constantes.TipoArticulo.PRODUCTO.Equals(poCanjeCab.tipoArticulo))
                        {
                            loParametrosCanjeDet = new SqlParameter[7];
                        }
                        else
                        {
                            loParametrosCanjeDet = new SqlParameter[10];
                        }
                        loParametrosCanjeDet[0] = new SqlParameter("@ID_CAB_CANJE", SqlDbType.Int);
                        loParametrosCanjeDet[0].Value = loCanjeDet.idCanje;
                        loParametrosCanjeDet[1] = new SqlParameter("@DETC_CODARTICULO", SqlDbType.VarChar, 20);
                        loParametrosCanjeDet[1].Value = loCanjeDet.codigoArticulo;
                        loParametrosCanjeDet[2] = new SqlParameter("@DETC_CANTIDAD", SqlDbType.VarChar, 20);
                        loParametrosCanjeDet[2].Value = loCanjeDet.cantidad;
                        loParametrosCanjeDet[3] = new SqlParameter("@DETC_OBSERVACION", SqlDbType.VarChar, 50);
                        loParametrosCanjeDet[3].Value = loCanjeDet.observacion;
                        loParametrosCanjeDet[4] = new SqlParameter("@DETC_FECVCMTO", SqlDbType.VarChar, 20);
                        loParametrosCanjeDet[4].Value = DateUtils.getStringDateYYMMDDHHMM(loCanjeDet.fechaVencimiento);
                        loParametrosCanjeDet[5] = new SqlParameter("@DETC_MOTIVO", SqlDbType.VarChar, 20);
                        loParametrosCanjeDet[5].Value = loCanjeDet.codigoMotivo;
                        loParametrosCanjeDet[6] = new SqlParameter("@ALM_PK", SqlDbType.Int);
                        loParametrosCanjeDet[6].Value = Int32.Parse(loCanjeDet.codAlmacen);

                        if (Constantes.TipoArticulo.PRESENTACION.Equals(poCanjeCab.tipoArticulo))
                        {
                            loParametrosCanjeDet[7] = new SqlParameter("@ID_PRE_PK", SqlDbType.Int);
                            loParametrosCanjeDet[7].Value = Int32.Parse(loCanjeDet.idArticulo);
                            loParametrosCanjeDet[8] = new SqlParameter("@DETC_CANTIDAD_PRE", SqlDbType.Int);
                            loParametrosCanjeDet[8].Value = Int32.Parse(loCanjeDet.cantidadPre);
                            loParametrosCanjeDet[9] = new SqlParameter("@DETC_CANTIDAD_FRAC", SqlDbType.Int);
                            loParametrosCanjeDet[9].Value = Int32.Parse(loCanjeDet.cantidadFrac);

                            SqlHelper.ExecuteNonQuery(loConnection, "USPI_DET_CANJE_PRESENTACION", loParametrosCanjeDet);

                        }
                        else
                        {
                            SqlHelper.ExecuteNonQuery(loConnection, "USPI_DET_CANJE", loParametrosCanjeDet);
                        }


                        #endregion


                    }

                    #endregion
                }


            }
            catch (Exception ex)
            {
                ControlBase.registerLog("fnInsCanje.Error: " + ex.Message + " : " + ex.StackTrace);
                lbResultado = false;
                lbErrorBD = true;
            }
            finally
            {
                loConnection.Close();
                loConnection.Dispose();
            }

            lbLstResultadosInsCanje.Add(lbResultado);
            lbLstResultadosInsCanje.Add(lbErrorBD);

            return lbLstResultadosInsCanje;
        }

        public static List<Boolean> fnInsDevolucion(BeanDevolucionCab poDevolucionCab, String psPlataforma)
        {
            List<Boolean> lbLstResultadosInsDevolucion = new List<bool>();
            Boolean lbResultado = true;
            Boolean lbErrorBD = false;
            Int32 liResultado;

            SqlConnection loConnection = new SqlConnection();
            loConnection.ConnectionString = connectionString;

            #region Parametros devoluci�n cabecera
            SqlParameter[] loArrParamDevolucionCab = new SqlParameter[12];
            loArrParamDevolucionCab[0] = new SqlParameter("@COD_DEVOLUCION", System.Data.SqlDbType.VarChar, 10);
            loArrParamDevolucionCab[0].Value = poDevolucionCab.codigoDevolucion;
            loArrParamDevolucionCab[1] = new SqlParameter("@ID_USUARIO", System.Data.SqlDbType.VarChar, 20);
            loArrParamDevolucionCab[1].Value = poDevolucionCab.idUsuario;
            loArrParamDevolucionCab[2] = new SqlParameter("@ID_CLIENTE", System.Data.SqlDbType.VarChar, 20);
            loArrParamDevolucionCab[2].Value = poDevolucionCab.idCliente;

            loArrParamDevolucionCab[3] = new SqlParameter("@LATITUD", System.Data.SqlDbType.VarChar, 20);
            loArrParamDevolucionCab[3].Value = Double.Parse(poDevolucionCab.latitud);
            loArrParamDevolucionCab[4] = new SqlParameter("@LONGITUD", System.Data.SqlDbType.VarChar, 20);
            loArrParamDevolucionCab[4].Value = Double.Parse(poDevolucionCab.longitud);


            loArrParamDevolucionCab[5] = new SqlParameter("@FecMovil", System.Data.SqlDbType.VarChar, 20);
            loArrParamDevolucionCab[5].Value = DateUtils.getStringDateYYMMDDHHMM(poDevolucionCab.fechaMovil);
            loArrParamDevolucionCab[6] = new SqlParameter("@par_fechaactual", System.Data.SqlDbType.VarChar, 20);
            loArrParamDevolucionCab[6].Value = DateUtils.getStringDateYYMMDDHHMM(DateUtils.getCurrentCompleteDate());
            loArrParamDevolucionCab[7] = new SqlParameter("@CELDA", System.Data.SqlDbType.VarChar, 20);
            loArrParamDevolucionCab[7].Value = poDevolucionCab.celda;
            loArrParamDevolucionCab[8] = new SqlParameter("@ERRORCONEXION", System.Data.SqlDbType.Int);
            loArrParamDevolucionCab[8].Value = poDevolucionCab.errorConexion;
            loArrParamDevolucionCab[9] = new SqlParameter("@ERRORPOSICION", System.Data.SqlDbType.Int);
            loArrParamDevolucionCab[9].Value = poDevolucionCab.errorPosicion;
            loArrParamDevolucionCab[10] = new SqlParameter("@FLGCOBERTURA", System.Data.SqlDbType.Char);
            loArrParamDevolucionCab[10].Value = poDevolucionCab.FlgEnCobertura;
            loArrParamDevolucionCab[11] = new SqlParameter("@TIPO_ARTICULO", System.Data.SqlDbType.Char, 3);
            loArrParamDevolucionCab[11].Value = poDevolucionCab.tipoArticulo;
            #endregion

            try
            {
                liResultado = Convert.ToInt32(SqlHelper.ExecuteScalar(loConnection, "USPI_CAB_DEVOLUCION", loArrParamDevolucionCab));
                if (liResultado != -1)//repetido
                {
                    #region Insertar detalle
                    foreach (BeanDevolucionDet loDevolucionDet in poDevolucionCab.lstDevolucionDet)
                    {
                        #region Parametros Devolucion Detalle
                        SqlParameter[] loArrParamDevolucionDet = null;
                        if (Constantes.TipoArticulo.PRODUCTO.Equals(loDevolucionDet.tipoArticulo))
                        {
                            loArrParamDevolucionDet = new SqlParameter[7];
                        }
                        else
                        {
                            loArrParamDevolucionDet = new SqlParameter[10];
                        }


                        loArrParamDevolucionDet[0] = new SqlParameter("@ID_CAB_DEVOLUCION", SqlDbType.BigInt);
                        loArrParamDevolucionDet[0].Value = liResultado;
                        loArrParamDevolucionDet[1] = new SqlParameter("@DEV_CODARTICULO", SqlDbType.VarChar, 20);
                        loArrParamDevolucionDet[1].Value = loDevolucionDet.codigoArticulo;
                        loArrParamDevolucionDet[2] = new SqlParameter("@DEV_CANTIDAD", SqlDbType.VarChar, 20);
                        loArrParamDevolucionDet[2].Value = loDevolucionDet.cantidad;
                        loArrParamDevolucionDet[3] = new SqlParameter("@DEV_CODMOTIVO", SqlDbType.VarChar, 10);
                        loArrParamDevolucionDet[3].Value = loDevolucionDet.codigoMotivo;
                        loArrParamDevolucionDet[4] = new SqlParameter("@DEV_FECVCMTO", SqlDbType.VarChar, 20);
                        loArrParamDevolucionDet[4].Value = DateUtils.getStringDateYYMMDDHHMM(loDevolucionDet.fechaVencimiento);
                        loArrParamDevolucionDet[5] = new SqlParameter("@OBSERVACION", SqlDbType.VarChar, 100);
                        loArrParamDevolucionDet[5].Value = loDevolucionDet.observacion;
                        loArrParamDevolucionDet[6] = new SqlParameter("@ALM_PK", SqlDbType.Int);
                        loArrParamDevolucionDet[6].Value = loDevolucionDet.codAlmacen;
                        #endregion
                        if (Constantes.TipoArticulo.PRODUCTO.Equals(loDevolucionDet.tipoArticulo))
                        {
                            SqlHelper.ExecuteNonQuery(loConnection, "USPI_DET_DEVOLUCION", loArrParamDevolucionDet);
                        }
                        else
                        {
                            loArrParamDevolucionDet[7] = new SqlParameter("@ID_PRE_PK", SqlDbType.Int);
                            loArrParamDevolucionDet[7].Value = loDevolucionDet.idArticulo;
                            loArrParamDevolucionDet[8] = new SqlParameter("@DEV_CANTIDAD_PRE", SqlDbType.Int);
                            loArrParamDevolucionDet[8].Value = loDevolucionDet.cantidadPre;
                            loArrParamDevolucionDet[9] = new SqlParameter("@DEV_CANTIDAD_FRAC", SqlDbType.Int);
                            loArrParamDevolucionDet[9].Value = loDevolucionDet.cantidadFrac;

                            SqlHelper.ExecuteNonQuery(loConnection, "USPI_DET_DEVOLUCION_PRESENTACION", loArrParamDevolucionDet);
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                ControlBase.registerLog("fnInsDevolucion.Erro: " + ex.Message + " : " + ex.StackTrace);
                lbResultado = false;
                lbErrorBD = true;

            }
            finally
            {
                loConnection.Close();
                loConnection.Dispose();
            }

            lbLstResultadosInsDevolucion.Add(lbResultado);
            lbLstResultadosInsDevolucion.Add(lbErrorBD);

            return lbLstResultadosInsDevolucion;
        }

        #region Consultas Online
        public static DataSet fnSelCliente(String psCodigoCliente)
        {
            ArrayList loLstParametros = new ArrayList();
            SqlParameter loParametro;

            List<Persistent> loLstClientes = new List<Persistent>();

            loParametro = new SqlParameter("@PAR_CODCLIENTE", SqlDbType.VarChar, 20);
            loParametro.Value = psCodigoCliente;
            loLstParametros.Add(loParametro);

            return DBUtilSQLServer.getDataset("JAVA_USPS_CLIENTEONLINE", loLstParametros);

        }

        public static DataSet fnSelClienteCobranza(String psCodigoCliente, String psCodigoUsuario)
        {
            ArrayList loLstParametros = new ArrayList();
            SqlParameter loParametro;

            loParametro = new SqlParameter("@PAR_CODCLIENTE", SqlDbType.VarChar, 20);
            loParametro.Value = psCodigoCliente;
            loLstParametros.Add(loParametro);

            loParametro = new SqlParameter("@PAR_CODUSUARIO", SqlDbType.VarChar, 20);
            loParametro.Value = psCodigoUsuario;
            loLstParametros.Add(loParametro);

            return DBUtilSQLServer.getDataset("JAVA_USPS_CLIENTEONLINE_COBRANZA", loLstParametros);

        }

        public static DataSet fnSelReporteEficiencia(BeanUsuario poUsuario)
        {
            ArrayList loLstParametros = new ArrayList();
            SqlParameter loParametro;

            loParametro = new SqlParameter("@PAR_CODVENDEDOR", SqlDbType.VarChar, 20);
            loParametro.Value = poUsuario.codigo;
            loLstParametros.Add(loParametro);

            loParametro = new SqlParameter("@PAR_FECHAMOVIL", SqlDbType.VarChar, 20);
            loParametro.Value = DateUtils.getStringDateYYMMDDHHMM(poUsuario.fechaMovil);
            loLstParametros.Add(loParametro);

            return DBUtilSQLServer.getDataset("JAVA_USPS_PEDIDOCANTIDAD", loLstParametros);
        }

        public static DataSet fnSelStockArticulo(BeanArticulo poArticulo)
        {
            ArrayList loLstParametros = new ArrayList();
            SqlParameter loParametro;

            loParametro = new SqlParameter("@PAR_CODARTICULO", SqlDbType.VarChar, 20);
            loParametro.Value = poArticulo.codigo;
            loLstParametros.Add(loParametro);

            loParametro = new SqlParameter("@PAR_ARTICULO", SqlDbType.VarChar, 100);
            loParametro.Value = poArticulo.nombre;
            loLstParametros.Add(loParametro);

            loParametro = new SqlParameter("@PAR_COD_ALMACEN", SqlDbType.Int);
            loParametro.Value = poArticulo.codAlmacen;
            loLstParametros.Add(loParametro);

            return DBUtilSQLServer.getDataset("JAVA_USPS_STOCKCANTIDA", loLstParametros);
        }

        public static DataSet fnSelStockPresentacion(BeanArticulo poArticulo)
        {
            ArrayList loLstParametros = new ArrayList();
            SqlParameter loParametro;

            loParametro = new SqlParameter("@PAR_CODARTICULO", SqlDbType.VarChar, 20);
            loParametro.Value = poArticulo.codigo;
            loLstParametros.Add(loParametro);

            loParametro = new SqlParameter("@PAR_ARTICULO", SqlDbType.VarChar, 100);
            loParametro.Value = poArticulo.nombre;
            loLstParametros.Add(loParametro);


            loParametro = new SqlParameter("@PAR_COD_ALMACEN", SqlDbType.Int);
            loParametro.Value = poArticulo.codAlmacen;
            loLstParametros.Add(loParametro);

            loParametro = new SqlParameter("@PAR_BUS_TIPO", SqlDbType.Char, 3);
            loParametro.Value = poArticulo.tipoBusqueda;
            loLstParametros.Add(loParametro);

            return DBUtilSQLServer.getDataset("JAVA_USPS_STOCKCANTIDA_PRESENTACION", loLstParametros);
        }


        public static DataSet fnSelPedido(String codCliente)
        {
            ArrayList loLstParametros = new ArrayList();
            SqlParameter loParametro;

            loParametro = new SqlParameter("@PAR_CODCLIENTE", SqlDbType.VarChar, 50);
            loParametro.Value = codCliente;
            loLstParametros.Add(loParametro);

            return DBUtilSQLServer.getDataset("JAVA_USPS_ULTIMOPEDIDOCLIENTE", loLstParametros);
        }

        public static DataSet fnSelPedidoPresentacion(String codCliente)
        {
            ArrayList loLstParametros = new ArrayList();
            SqlParameter loParametro;

            loParametro = new SqlParameter("@PAR_CODCLIENTE", SqlDbType.VarChar, 50);
            loParametro.Value = codCliente;
            loLstParametros.Add(loParametro);

            return DBUtilSQLServer.getDataset("JAVA_USPS_ULTIMOPEDIDOCLIENTE_PRESENTACION", loLstParametros);
        }

        public static BESincronizar fnConsultaPedidosScript(String piIdUsuario, String fecha, String psTipoArticulo, String pscodCliente)
        {
            BESincronizar loBESincronizar = null;
            StringBuilder loStringScript = new StringBuilder();

            SqlParameter[] par = new SqlParameter[3];
            par[0] = new SqlParameter("@PAR_CODUSUARIO", System.Data.SqlDbType.VarChar, 50);
            par[0].Value = piIdUsuario;
            par[1] = new SqlParameter("@PAR_FECHA", System.Data.SqlDbType.VarChar, 50);
            par[1].Value = fecha;
            par[2] = new SqlParameter("@PAR_CODCLIENTE", System.Data.SqlDbType.VarChar, 20);
            par[2].Value = pscodCliente;

            int liCount = 0;
            try
            {
                SqlDataReader loDr = null;
                if (psTipoArticulo.Equals(Constantes.TipoArticulo.PRODUCTO))
                {
                    loDr = SqlHelper.ExecuteReader(new SqlConnection(connectionString),
                        CommandType.StoredProcedure, "USPS_CONSULTA_PEDIDOS_SCRIPT", par);
                }
                else
                {
                    loDr = SqlHelper.ExecuteReader(new SqlConnection(connectionString),
                        CommandType.StoredProcedure, "USPS_CONSULTA_PEDIDOS_PRESENTACION_SCRIPT", par);
                }

                while (loDr.Read())
                {
                    loStringScript.Append((String)loDr.GetValue(loDr.GetOrdinal("SCRIPT")));
                    loStringScript.Append(Environment.NewLine);
                    liCount++;
                }
                loDr.NextResult();
                StringBuilder loSbResultado = new StringBuilder();
                while (loDr.Read())
                {
                    loSbResultado.Append(",");
                    loSbResultado.Append((String)loDr.GetValue(loDr.GetOrdinal("Total")));
                }
                loDr.Close();
                loBESincronizar = new BESincronizar(true, loStringScript.ToString(), EnumResGrabJava.TodoOk, (loSbResultado.Length > 0) ? loSbResultado.ToString().Substring(1) : "");
            }
            catch (Exception e)
            {
                int x = liCount;
                loBESincronizar = new BESincronizar(true, "", EnumResGrabJava.TodoMal, e.Message);
            }
            finally
            {

            }
            return loBESincronizar;
        }

        public static BESincronizar fnConsultaPagosScript(String piIdUsuario, String fecha, String pscodCliente)
        {
            BESincronizar loBESincronizar = null;
            StringBuilder loStringScript = new StringBuilder();

            SqlParameter[] par = new SqlParameter[3];
            par[0] = new SqlParameter("@PAR_CODUSUARIO", System.Data.SqlDbType.VarChar, 50);
            par[0].Value = piIdUsuario;
            par[1] = new SqlParameter("@PAR_FECHA", System.Data.SqlDbType.VarChar, 50);
            par[1].Value = fecha;
            par[2] = new SqlParameter("@PAR_CODCLIENTE", System.Data.SqlDbType.VarChar, 20);
            par[2].Value = pscodCliente;

            int liCount = 0;
            try
            {
                SqlDataReader loDr = null;
                loDr = SqlHelper.ExecuteReader(new SqlConnection(connectionString),
                        CommandType.StoredProcedure, "USPS_CONSULTA_PAGOS_ONLINE", par);

                while (loDr.Read())
                {
                    loStringScript.Append((String)loDr.GetValue(loDr.GetOrdinal("SCRIPT")));
                    loStringScript.Append(Environment.NewLine);
                    liCount++;
                }
                loDr.NextResult();
                StringBuilder loSbResultado = new StringBuilder();
                while (loDr.Read())
                {
                    loSbResultado.Append(",");
                    loSbResultado.Append((String)loDr.GetValue(loDr.GetOrdinal("Total")));
                }
                loDr.Close();
                loBESincronizar = new BESincronizar(true, loStringScript.ToString(), EnumResGrabJava.TodoOk, (loSbResultado.Length > 0) ? loSbResultado.ToString().Substring(1) : "");
            }
            catch (Exception e)
            {
                int x = liCount;
                loBESincronizar = new BESincronizar(true, "", EnumResGrabJava.TodoMal, e.Message);
            }
            finally
            {

            }
            return loBESincronizar;
        }

        public static DataSet fnObtenerEficienciaSupervisor(String codigo)
        {
            ArrayList loLstParametros = new ArrayList();
            SqlParameter loParametro;

            loParametro = new SqlParameter("@cod_supervisor", SqlDbType.VarChar, 20);
            loParametro.Value = codigo;
            loLstParametros.Add(loParametro);


            return DBUtilSQLServer.getDataset("spS_ObtenerInformacionSupervisor", loLstParametros);
        }
        public static DataSet fnObtenerEficienciaVendedor(String codigo)
        {
            ArrayList loLstParametros = new ArrayList();
            SqlParameter loParametro;

            loParametro = new SqlParameter("@cod_vendedor", SqlDbType.VarChar, 20);
            loParametro.Value = codigo;
            loLstParametros.Add(loParametro);


            return DBUtilSQLServer.getDataset("spS_ObtenerInformacionVendedor", loLstParametros);
        }
        #endregion

        public static List<Boolean> fnActualizarEstadoPedido(BEEnvCabPedido pedido)
        {
            List<Boolean> lbLstResultados = new List<bool>();
            Boolean lbResultado = true;
            Boolean lbErrorBD = false;
            Int32 liResultado;

            SqlConnection loConnection = new SqlConnection();
            loConnection.ConnectionString = connectionString;


            SqlParameter[] loParametros = new SqlParameter[2];
            loParametros[0] = new SqlParameter("@CODIGO_PEDIDO", System.Data.SqlDbType.VarChar, 20);
            loParametros[0].Value = pedido.CodigoPedidoServidor;
            loParametros[1] = new SqlParameter("@PROCESADO", System.Data.SqlDbType.Char, 1);
            loParametros[1].Value = pedido.procesado ? Constantes.Flag.TRUE : Constantes.Flag.FALSE;

            try
            {
                Convert.ToInt32(SqlHelper.ExecuteScalar(loConnection, "USPS_ACTUALIZAR_ESTADO_PROCESADO", loParametros));
            }
            catch (Exception ex)
            {
                ControlBase.registerLog("fnActualizarEstadoPedido.Error: " + ex.Message + " : " + ex.StackTrace);
                lbResultado = false;
                lbErrorBD = true;
            }
            finally
            {
                loConnection.Close();
                loConnection.Dispose();
            }

            lbLstResultados.Add(lbResultado);
            lbLstResultados.Add(lbErrorBD);

            return lbLstResultados;
        }


        /// <summary>
        /// M�todo que permite registrar en el log el mensaje ingresado
        /// </summary>
        /// <param name="message">Mensaje a registrar</param>
        public static void registerLog(String message)
        {
            string logActive = ConfigurationManager.AppSettings["LOG_ACTIVO"];

            if (logActive == "1")
            {
                string path = ConfigurationManager.AppSettings["rutaLog"];
                //Log.Write(HttpContext.Current.Server.MapPath("~") + path, message);
                new LogHelper().Debug("DBJava -> registerLog", message);
            }
        }

        //Integracion NTrack
        public static List<Java.Bean.BeanIdioma> HASHIDIOMA = null;

        public static List<String> fnSinScriptStringNServices(String psNumtelefono)
        {
            SqlParameter[] par = new SqlParameter[1];
            par[0] = new SqlParameter("@NUMTELEFONO", System.Data.SqlDbType.VarChar, 20);
            par[0].Value = psNumtelefono;
            List<String> loScript = new List<String>();
            loScript.Add("DROP TABLE IF EXISTS CFNServices;");
            loScript.Add("CREATE TABLE IF NOT EXISTS CFNServices(CodModulo VARCHAR(4) NOT NULL, Descripcion VARCHAR(50) NULL);");
            String urlAPK = null;
            bool nServicesScreen = false;
            try
            {
                urlAPK = fnObtenerNServicesAPK();
                SqlDataReader loDr = SqlHelper.ExecuteReader(new SqlConnection(connectionNServices), CommandType.StoredProcedure, "spM_ConsultaModulo", par);
                while (loDr.Read())
                {
                    nServicesScreen = ((String)loDr.GetValue(loDr.GetOrdinal("EXISTE"))).Equals("T");
                }
                loDr.Close();
                if (nServicesScreen)
                {
                    loScript.Add("INSERT INTO CFNServices VALUES('ETQ', '" + fnRecuperaDescripcion("WEB_NSERVICES") + "');");
                }
                loScript.Add("INSERT INTO CFNServices VALUES('APK', " + (urlAPK != null ? "'" + urlAPK + "'" : "NULL") + ");");


            }
            catch (NullReferenceException e)
            {
                loScript = new List<String>();
                loScript.Add("DROP TABLE IF EXISTS CFNServices;");
                loScript.Add("CREATE TABLE IF NOT EXISTS CFNServices(CodModulo VARCHAR(4) NOT NULL, Descripcion VARCHAR(50) NULL);");
                loScript.Add("INSERT INTO CFNServices VALUES('ERR', '" + fnRecuperaDescripcion("WEB_NSERVICES_DESACTIVADO").Replace("{0}", fnRecuperaDescripcion("WEB_OPERADOR" + obtenerTemaActual())) + "');");
            }
            catch (Exception e)
            {
                loScript = new List<String>();
                loScript.Add("DROP TABLE IF EXISTS CFNServices;");
                loScript.Add("CREATE TABLE IF NOT EXISTS CFNServices(CodModulo VARCHAR(4) NOT NULL, Descripcion VARCHAR(50) NULL);");
                loScript.Add("INSERT INTO CFNServices VALUES('ERR', '" + e.Message + "');");
            }
            return loScript;
        }

        public static String obtenerTemaActual()
        {
            WS.Service.Service loService = new WS.Service.Service();
            try
            {
                return loService.obtenerTemaActual();
            }
            catch (Exception e)
            {
                return ConfigurationManager.AppSettings["temaDefault"].ToString();
            }
            finally
            {
                loService = null;
            }
        }

        public static string fnObtenerNServicesAPK()
        {
            try
            {
                if (HASHIDIOMA == null)
                {
                    HASHIDIOMA = fnSelCultureIdiomaLista("W");
                }
                return ConfigurationManager.AppSettings["NSERVICES_APK"].ToString();
            }
            catch (NullReferenceException e)
            {
                throw new Exception(fnRecuperaDescripcion("WEB_NSERVICES_DESACTIVADO").Replace("{0}", fnRecuperaDescripcion("WEB_OPERADOR" + obtenerTemaActual())));
            }
        }

        public static string fnRecuperaDescripcion(String llave)
        {
            try
            {
                foreach (Java.Bean.BeanIdioma beidioma in HASHIDIOMA)
                {
                    if (beidioma.id.Equals(llave))
                    {
                        return beidioma.valor;
                    }
                }
                return "";
            }
            catch (Exception e)
            {
                return "";
            }
        }

        public static DataSet fnObtenerTipoCambio()
        {
             ArrayList loLstParametros = new ArrayList();
            SqlParameter loParametro;

            loParametro = new SqlParameter("@TC_CODIGO", SqlDbType.VarChar, 10);
            loParametro.Value = "-1";
            loLstParametros.Add(loParametro);

            loParametro = new SqlParameter("@HABILITADO", SqlDbType.Char, 1);
            loParametro.Value = "T";
            loLstParametros.Add(loParametro);

            loParametro = new SqlParameter("@PAGE", SqlDbType.Int);
            loParametro.Value = 10;
            loLstParametros.Add(loParametro);

            loParametro = new SqlParameter("@ROWS", SqlDbType.Int);
            loParametro.Value = 1;
            loLstParametros.Add(loParametro);

            return DBUtilSQLServer.getDataset("USPS_TIPO_CAMBIO", loLstParametros);
        }

        public static DataSet fnObtenerReciboCobranza(String codVendedor)
        {
            ArrayList loLstParametros = new ArrayList();
            SqlParameter loParametro;

            loParametro = new SqlParameter("@PAR_USR_CODIGO", SqlDbType.VarChar, 20);
            loParametro.Value = codVendedor;
            loLstParametros.Add(loParametro);

            return DBUtilSQLServer.getDataset("JAVA_USPS_RECIBOUSER", loLstParametros);
        }

        #region Documento Anulado/Modificado

        public static List<bool> fnInsDocumento(BeanDocumento poDocumento, String psPlataforma)
        {
            List<bool> lbLstResultadoInsPago = new List<bool>();
            Boolean lbResultado = true;
            Boolean lbErrorBD = false;
            Int32 liResultado;

            #region Parametros
            SqlParameter[] loArrParametros = new SqlParameter[13];

            loArrParametros[0] = new SqlParameter("@DOC_TIPO", SqlDbType.Char, 1);
            loArrParametros[0].Value = poDocumento.idDocTipo;
            loArrParametros[1] = new SqlParameter("@MACC_CODIGO", SqlDbType.VarChar, 10);
            loArrParametros[1].Value = poDocumento.idDocTipoMotivo;
            loArrParametros[2] = new SqlParameter("@DOC_FECREGISTRO", SqlDbType.DateTime);
            loArrParametros[2].Value = poDocumento.fechaMovil;
            loArrParametros[3] = new SqlParameter("@PAR_CODUSUARIO", SqlDbType.VarChar, 20);
            loArrParametros[3].Value = poDocumento.idUsuario;
            loArrParametros[4] = new SqlParameter("@PAR_CODCLIENTE", SqlDbType.VarChar, 20);
            loArrParametros[4].Value = poDocumento.idCliente;
            loArrParametros[5] = new SqlParameter("@LATITUD ", SqlDbType.VarChar, 20);
            loArrParametros[5].Value = poDocumento.latitud;
            loArrParametros[6] = new SqlParameter("@LONGITUD", SqlDbType.VarChar, 20);
            loArrParametros[6].Value = poDocumento.longitud;
            loArrParametros[7] = new SqlParameter("@SERIE", SqlDbType.VarChar, 5);
            loArrParametros[7].Value = poDocumento.serie;
            loArrParametros[8] = new SqlParameter("@CORRELATIVO", SqlDbType.VarChar, 10);
            loArrParametros[8].Value = poDocumento.correlativo;
            loArrParametros[9] = new SqlParameter("@R_CORRELATIVO", SqlDbType.BigInt);
            loArrParametros[9].Value = poDocumento.rangocorrelativo;
            loArrParametros[10] = new SqlParameter("@PAG_CODIGO", SqlDbType.Int);
            loArrParametros[10].Value = string.IsNullOrWhiteSpace(poDocumento.idPago) ? "0" : poDocumento.idPago;
            loArrParametros[11] = new SqlParameter("@MONTOSOLES", SqlDbType.VarChar, 20);
            loArrParametros[11].Value = poDocumento.montosoles;
            loArrParametros[12] = new SqlParameter("@MONTODOLARES", SqlDbType.VarChar, 20);
            loArrParametros[12].Value = poDocumento.montodolares;
            #endregion

            try
            {
                liResultado = Convert.ToInt32(SqlHelper.ExecuteScalar(connectionString, "USPS_GUARDAR_DOCUMENTO_HISTORIAL", loArrParametros));

                #region DEPRECADO - si se considera esto aparece mensaje de error al enviar documento
                if (liResultado == -1)//repetido
                {
                    lbResultado = false;
                }

                #endregion
            }
            catch (Exception e)
            {
                ControlBase.registerLog("fnInsDocumento.Erro: " + e.Message + " : " + e.StackTrace);
                lbResultado = false;
                lbErrorBD = true;

            }

            lbLstResultadoInsPago.Add(lbResultado);
            lbLstResultadoInsPago.Add(lbErrorBD);

            return lbLstResultadoInsPago;
        }


        #endregion
    }

}
