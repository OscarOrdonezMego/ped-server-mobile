using System;
using System.Collections.Generic;
using System.Text;
using Java.Bean;

namespace Nextel.BE
{
    [Serializable()]
    public class BEEnvDetPedido
    {
        private String _empresa = "";
        private String _idArticulo = "";
        private String _codigoArticulo = "";
        private String _cantidad = "0";
        private String _bonificacion = "0";
        private String _monto = "0";
        private String _montoSoles = "0"; //@JBELVY
        private String _montoDolar = "0"; //@JBELVY
        private String _precioBase = "0";
        private String _precioBaseSoles = "0"; //@JBELVY
        private String _precioBaseDolares = "0"; //@JBELVY
        private String _tipoMoneda = "0"; //@JBELVY
        private String _stock = "0";
        private String _descuento = "0";
        private String _CodPedido = "";
        private String _nombreArticulo = "";
        private String _observacion = "";
        private String _tipoDescuento = "";
        private String _atendible = "";
        private String _codListaArticulo = "";
        private String _codAlmacen = "";
        private String _flete = "";
        private String _cantidadPre = "";
        private String _cantidadFrac = "";
        private String _precioFrac = "";
        private long _codBonificacion;
   		private String _montoSinDescuento = "0";
        private String _bonificacionFrac = "";

        public long codBonificacion
        {
            get { return _codBonificacion; }
            set { _codBonificacion = value; }
        }

     
        public String montoSinDescuento
        {
            get { return _montoSinDescuento; }
            set { _montoSinDescuento = value; }
        }

        public String tipoDescuento
        {
            get { return _tipoDescuento; }
            set { _tipoDescuento = value; }
        }

        public String empresa
        {
            get { return _empresa; }
            set { _empresa = value; }
        }
        public String idArticulo
        {
            get { return _idArticulo; }
            set { _idArticulo = value; }
        }        
        public String codigoArticulo
        {
            get { return _codigoArticulo; }
            set { _codigoArticulo = value; }
        }
        
        public String cantidad
        {
            get { return _cantidad; }
            set { _cantidad = value; }
        }
        
        public String monto
        {
            get { return _monto; }
            set { _monto = value; }
        }
        public String montoSoles
        {
            get { return _montoSoles; }
            set { _montoSoles = value; }
        } //@JBELVY
        public String montoDolar
        {
            get { return _montoDolar; }
            set { _montoDolar = value; }
        } //@JBELVY

        public String precioBase
        {
            get { return _precioBase; }
            set { _precioBase = value; }
        }
        public String precioBaseSoles
        {
            get { return _precioBaseSoles; }
            set { _precioBaseSoles = value; }
        } //@JBELVY

        public String precioBaseDolares
        {
            get { return _precioBaseDolares; }
            set { _precioBaseDolares = value; }
        } //@JBELVY

        public String tipoMoneda
        {
            get { return _tipoMoneda; }
            set { _tipoMoneda = value; }
        } //@JBELVY

        public String stock
        {
            get { return _stock; }
            set { _stock = value; }
        }
        
        public String descuento
        {
            get { return _descuento; }
            set { _descuento = value; }
        }
        
        public String codPedido
        {
            get { return _CodPedido; }
            set { _CodPedido = value; }
        }
        
        public String nombreArticulo
        {
            get { return _nombreArticulo; }
            set { _nombreArticulo = value; }
        }

        public String observacion
        {
            get { return _observacion; }
            set { _observacion = value; }
        }

        public String atendible
        {
            get { return _atendible; }
            set { _atendible = value; }
        }

        public String bonificacion
        {
            get { return _bonificacion; }
            set { _bonificacion = value; }
        }
        public String codListaArticulo
        {
            get { return _codListaArticulo; }
            set { _codListaArticulo = value; }
        }
        public String codAlmacen
        {
            get { return _codAlmacen; }
            set { _codAlmacen = value; }
        }
        public String flete
        {
            get { return _flete; }
            set { _flete = value; }
        }
        public String cantidadPre
        {
            get { return _cantidadPre; }
            set { _cantidadPre = value; }
        }
        public String cantidadFrac
        {
            get { return _cantidadFrac; }
            set { _cantidadFrac = value; }
        }
        public String precioFrac
        {
            get { return _precioFrac; }
            set { _precioFrac = value; }
        }
        public String bonificacionFrac
        {
            get { return _bonificacionFrac; }
            set { _bonificacionFrac = value; }
        }

        public BEEnvDetPedido()
		{
		}
        
        public BeanPedidoDet cambiabeanes()
        {
            BeanPedidoDet lobeanpedidodet = new BeanPedidoDet();
            lobeanpedidodet.codBonificacion = this.codBonificacion;
            lobeanpedidodet.empresa = this.empresa;
            lobeanpedidodet.codigoArticulo = this.codigoArticulo;
            lobeanpedidodet.cantidad = this.cantidad;
            lobeanpedidodet.monto = this.monto;
            lobeanpedidodet.montoSoles = this.montoSoles; //@JBELVY
            lobeanpedidodet.montoDolar = this.montoDolar; //@JBELVY
            lobeanpedidodet.precioBase = this.precioBase;
            lobeanpedidodet.precioBaseSoles = this.precioBaseSoles; //@JBELVY
            lobeanpedidodet.precioBaseDolares = this.precioBaseDolares; //@JBELVY
            lobeanpedidodet.tipoMoneda = this.tipoMoneda; //@JBELVY
            lobeanpedidodet.stock = this.stock;
            lobeanpedidodet.descuento = this.descuento;
            lobeanpedidodet.codigoPedido = this.codPedido;
            lobeanpedidodet.nombreArticulo = this.nombreArticulo;
            lobeanpedidodet.descripcion = this.observacion;
            lobeanpedidodet.tipoDescuento = this.tipoDescuento;
            lobeanpedidodet.atendible = this.atendible;
            lobeanpedidodet.bonificacion = this.bonificacion;
            lobeanpedidodet.codListaArticulo = this.codListaArticulo;
            try { 
                lobeanpedidodet.codAlmacen = Int16.Parse(codAlmacen);
            }
            catch (Exception e)
            {
                lobeanpedidodet.codAlmacen = 0;
            }
            lobeanpedidodet.flete = this.flete;
            lobeanpedidodet.cantidadPre = this._cantidadPre;
            lobeanpedidodet.cantidadFrac = this.cantidadFrac;
            lobeanpedidodet.precioFrac = this.precioFrac;
            lobeanpedidodet.montoSinDescuento = this.montoSinDescuento;
            lobeanpedidodet.bonificacionFrac = this.bonificacionFrac;
            return lobeanpedidodet;
        }
		
	}
}

