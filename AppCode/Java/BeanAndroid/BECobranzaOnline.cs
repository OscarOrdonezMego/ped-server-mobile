﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Java.Bean
{

    public class BECobranzaOnline
    {
        public String cobPk;
        public String cobCodigo;
        public String cobNumDocumento;
        public String cobTipoDocumento;
        public String cobMontoPagado;
        public String cobMontoTotal;
        public String cobFecVencimiento;
        public String cobSerie;
        public String cliCodigo;
        public String cobEmpresa;
        public String cobUsuario;
        public String cliPK;
        public String saldo;
    }
}