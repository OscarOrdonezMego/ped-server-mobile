using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Java.Configuracion;

namespace Java.Bean
{
    public class BEClienteOnline
    {
       #region Atributos
        public String clipk;
        public String clicod;
        public String clinom;
        public String clidireccion;
        public String tclicod;
        public String tclinombre;
        public String cligiro;
        public String clideuda;
        public String estado;
        public String online = "1";
        public String secuencia;

        public String campoAdicional1;
        public String campoAdicional2;
        public String campoAdicional3;
        public String campoAdicional4;
        public String campoAdicional5;
        public String saldoCredito;
        public String codUsuario;

       #endregion

        public void cargabean(BeanCliente bcli)
        {
            this.clipk = "" + bcli.idCliente;
            this.clicod = bcli.codigo;
            this.clinom = bcli.nombre;
            this.clidireccion = bcli.direccion;
            this.tclicod = bcli.codigoTipoCliente;
            this.tclinombre = bcli.nombreTipoCliente;
            this.cligiro = bcli.giro;
            this.clideuda = bcli.deuda;
            this.estado = bcli.estado;
            this.online = "1";
            
            this.campoAdicional1 = bcli.campoAdicional1;
            this.campoAdicional2 = bcli.campoAdicional2;
            this.campoAdicional3 = bcli.campoAdicional3;
            this.campoAdicional4 = bcli.campoAdicional4;
            this.campoAdicional5 = bcli.campoAdicional5;
            this.saldoCredito = bcli.saldoCredito;
        }
    }

   
}