﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BEListaEficienciaOnline
/// </summary>
/// 
namespace Java.Bean
{
    public class BEListaEficienciaOnline
    {
        public String resultado = "";
        public int idResultado = 1;
        public List<BEEficienciaOnline> listaEficiencia;
        public String usuario;
        public List<String> usuariosVendedores=new List<string>();
        public String perfilUsuario;
        public BEKpi kpi = new BEKpi();
    }
}