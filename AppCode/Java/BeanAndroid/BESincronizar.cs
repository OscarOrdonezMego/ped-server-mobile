using System;
using System.Collections.Generic;
using System.Text;

namespace Nextel.BE
{
    public class BESincronizar
    {
        #region CONSTUCTOR
        public BESincronizar()
        {
            _resScript = true;
        }
        public BESincronizar(Boolean pbResScript, List<BESinScript> poLista,EnumResGrabJava piResultado,String psRespuesta)
        {
            _resScript = pbResScript;
            _listaScript = poLista;
            _idRespuesta =(Int32) piResultado;
            _respuesta = psRespuesta;
        }
        public BESincronizar(Boolean pbResScript, String psScript, EnumResGrabJava piResultado, String psRespuesta)
        {
            _resScript = pbResScript;
            _stringScript = psScript;
            _idRespuesta = (Int32)piResultado;
            _respuesta = psRespuesta;
        }
        #endregion

        #region PROPIEDADES
        private Boolean _resScript;
        private String _stringScript;
        private String _respuesta;
        private Int32 _idRespuesta;
		private List<BESinScript> _listaScript;
		public String Respuesta
        {
            get { return _respuesta; }
            set { _respuesta = value; }
        }
        public Int32 idRespuesta
        {
            get { return _idRespuesta; }
            set { _idRespuesta = value; }
        }
        public List<BESinScript> listaScript
		{
            get { return _listaScript; }
            set { _listaScript = value; }
		}
        public String StringScript
        {
            get 
            {
                if (_resScript)
                {
                    return _stringScript + Util.fnResScript(_idRespuesta, _respuesta);
                }
                else
                {
                    return _stringScript; 
                }
                
            }
            set { _stringScript = value; }
        }
        public Boolean ResScript
        {
            get { return _resScript; }
            set { _resScript = value; }
        }
		#endregion

    }
}
