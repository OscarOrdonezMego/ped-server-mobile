﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BEProspecto
/// </summary>
namespace Java.Bean
{

public class BEProspecto
{
	public String codigo;
    public String nombre;
    public String direccion;
    public String latitud;
    public String longitud;
    public String codUsuario;
    public String tcli_cod;
    public String giro;
    public String campoAdicional1;
    public String campoAdicional2;
    public String campoAdicional3;
    public String campoAdicional4;
    public String campoAdicional5;
    public String observacion;
    public String fecha;
}
}