﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Nextel.BE
{
    [Serializable()]
    public class BEEnvCtrlPedido
    {
        private String _idPedido = "";
        private String _etiquetaControl = "";
        private String _idTipoControl = "";
        private String _valorControl = "";
        private String _idControl = "";
        private String _idFormulario = "";

        public String idFormulario
        {
            get { return _idFormulario; }
            set { _idFormulario = value; }
        }
        
        public String etiquetaControl
        {
            get { return _etiquetaControl; }
            set { _etiquetaControl = value; }
        }
        public String idTipoControl
        {
            get { return _idTipoControl; }
            set { _idTipoControl = value; }
        }
        public String valorControl
        {
            get { return _valorControl; }
            set { _valorControl = value; }
        }
        public String idControl
        {
            get { return _idControl; }
            set { _idControl = value; }
        }


        public String idPedido
        {
            get { return _idPedido; }
            set { _idPedido = value; }
        }

        public BEEnvCtrlPedido()
        {
          
        }        
    }
}