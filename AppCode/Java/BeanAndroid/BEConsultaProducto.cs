using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Collections.Generic;
using System.Collections;
using NextlibServer.RMS;
using NextlibServer.Utils;

namespace Java.Bean
{
    public class BEConsultaProducto
    {
        #region Atributos
        public String codigoUsuario = "";
        public String fechaInicio = "";
        public String codigoCliente = "";
        public String codigoArticulo = "";
        public String condicionVenta = "";
        public String cantidadTotal = "";
        public String direccion = "";
        public String direccionDespacho = "";
        public int codigoAlmacen = 0;
        public String bonificacion = "";
        public String cantidad = "";
        public String codigoListaPrecio = "";
        public String codigoPedido = "";
        public String descripcion = "";
        public String descuento = "";
        public String fechaUltimaVenta = "";
        public String flete = "";
        public String monto = "";
        public String precio = "";
        public String precioUltimaVenta = "";
        public int secuencia = 0;
        public String tipoDescuento = "";
        #endregion

    }
}