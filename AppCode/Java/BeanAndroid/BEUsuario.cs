using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Java.Configuracion;
using System.Collections.Generic;

namespace Java.Bean
{
    public class BEUsuario
    {
        #region Atributos
        public Int64 id = 0;
        public String codVendedor = String.Empty;
        public String nombre = String.Empty;
        public String usuario = String.Empty;
        public String clave = String.Empty;
        public String numero = String.Empty;
        public String perfil = String.Empty;
        public String serie = String.Empty;
        public String correlativo = String.Empty;
        //Version vigente
        public String currentVersion = "0.0.0";

        public String resultado = String.Empty;
        public int idResultado = 0;

        public List<String> configuracion = new List<String>();
        public List<String> vendedores = new List<String>();
        //public String flgPermisoPedido = String.Empty;
        //public String flgPermisoCanje = String.Empty;
        //public String flgPermisoCobranza = String.Empty;
        //public String flgPermisoDevolucion = String.Empty;
       // public String flgPermisoEficiencia = String.Empty;
        //public String flgPermisoGps = String.Empty;
        //public String flgPermisoStockLinea = String.Empty;
        //public String flgPermisoStockValidar = String.Empty;
        //public String flgPermisoPrecioValidar = String.Empty;

        //public String flgPermisoDescuento = String.Empty;
        //public String flgPermisoEditarPrecio = String.Empty;
        //public String flgPermisoListaPrecio = String.Empty;
        //public String moneda = String.Empty;
        //public String simboloDecimal = String.Empty;
        //public String numDecimales = String.Empty;
        //public String idioma = String.Empty;
        //public String codCompania = String.Empty;


        //public String flgPermisoPrecioTipoCli = String.Empty;
        //public String flgPermisoPrecioCondVta = String.Empty;
        //public String flgPermisoDescPorc = String.Empty;
        //public String flgPermisoDescMonto = String.Empty;
        //public String flgPermisoDescMin = String.Empty;
        //public String flgPermisoDescMax = String.Empty;

        //public String flgMuestraCondVenta = String.Empty;
        //public String flgDescuentaStock = String.Empty;
        
        //Integracion Ntrack
        //public String flgNServices = String.Empty; //Flag de instancia
        public String flgNServicesObligatorio = String.Empty; //Flag de usuario

       

        //public String flgConsultarStockLinea = String.Empty;
        //public String flgRestrictivo = String.Empty;

        //public String flgBonificacion = String.Empty;

        #endregion

       
    }
}