﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Java.Configuracion;

namespace Nextel.BE
{
    public class BERequestService
    {
        #region Atributos
        //
        public String telefono { get; set; }
        public String servicio { get; set; }
        public String instalacion { get; set; }
        public long fecha = 0;
        public String url { get; set; }
        #endregion
    }
}