using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Java.Configuracion;
using Nextel.BE;

namespace Java.Bean
{
    public class BEPendientes
    {
        #region Atributos
        
        public List<BEEnvCabPedido> listPedidos;
        public List<BeanPago> listPagos;
        public List<BeanCanjeCab> listCanjes;
        public List<BeanDevolucionCab> listDevoluciones;
        public List<BeanClienteDireccion> listDirecciones;
        public List<BEProspecto> listProspectos;

        public String errorConexion;
        public String errorPosicion;
        public String resultado = "";
        public int idResultado = 0;

        
        #endregion

        public void subActualizarDireccionPedido(List<BeanClienteDireccion> poListDirecciones) {

            List<BEEnvCabPedido> loListPedidos = new List<BEEnvCabPedido>();

                  foreach (BEEnvCabPedido loBE in listPedidos)
                  {
                      if(loBE.flgNuevoDireccion.Equals("T"))
                      {
                          foreach (BeanClienteDireccion loDir in poListDirecciones)
                          {
                                if(loDir.codDireccion.ToString().Equals(loBE.codDireccion))
                                {
                                    loBE.codDireccion = loDir.pk.ToString();

                                    break;
                                }
                          }
                      }
                      loListPedidos.Add(loBE);
                  }

                  listPedidos = loListPedidos;
             }

            
        
        }

       
    
}