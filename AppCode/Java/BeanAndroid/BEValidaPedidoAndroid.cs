﻿using System;
using System.Collections.Generic;
using System.Web;
using Java.Bean;

namespace Nextel.BE
{
    public class BEValidaPedidoAndroid
    {
        public String resultado = "";
        public int idResultado = 0;
        public String codPedidoServidor = "0";        
        public int numReservados = 0;
        public List<BeanPedidoDet> listDetPedido;

        public BEValidaPedidoAndroid(List<BeanPedidoDet> poLista,String psResultado,String piCodPedido, int piResultado, int piReservados)
        {
            this.idResultado = piResultado;
            this.resultado = psResultado;
            this.numReservados = piReservados;
            this.codPedidoServidor = piCodPedido;
            this.listDetPedido = poLista;
        }
        
        public BEValidaPedidoAndroid(String psResultado, int piResultado)
        {
            this.idResultado = piResultado;
            this.resultado = psResultado;
            this.numReservados = 0;
            this.codPedidoServidor = "0";
            this.listDetPedido = new List<BeanPedidoDet>();
        }
    }
}