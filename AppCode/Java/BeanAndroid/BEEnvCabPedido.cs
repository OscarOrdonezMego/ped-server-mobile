using System;
using System.Collections.Generic;
using System.Text;
using Java.Bean;

namespace Nextel.BE
{
    public class BEEnvCabPedido
	{
        private String _codigoPedido = "0";        
        private String _empresa = "";
        private String _codigoUsuario = "";
        private String _fechaInicio = "";
        private String _fechaFin = "";
        private String _condicionVenta = "";
        private String _codigoCliente = "";
        private String _codigoMotivo = "";
        private String _montoTotal = "0";
        private String _montoTotalSoles = "0"; //@JBELVY
        private String _montoTotalDolar = "0"; //@JBELVY
        private String _codDireccion = "";
        private String _codDireccionDespacho = "";
        private String _latitud = "";
        private String _longitud = "";
        private String _celda = "";
        private String _errorConexion = "";
        private String _errorPosicion = "";
        private String _observacion = "";
        private String _cantidadTotal = "0";
        private List<BEEnvDetPedido> _listaDetPedido;
        private List<BEEnvCtrlPedido> _listaCtrlPedido;
        private String _tipoCambioSoles = "0"; //@JBELVY
        private String _tipoCambioDolar = "0"; //@JBELVY

        private String _flgEnCobertura = "";

        private String _flgTerminado = "";

        private String _codigoPedidoServidor = "0";

        private String _flgEliminar = "";
        private String _bonificacionTotal = "";
        private string _flgTipo = "";
        private string _flgNuevoDireccion = "F";
        private String _codAlmacen = "0";
        private String _fleteTotal = "";
        private String _tipoArticulo = "";
        private String _creditoUtilizado = "0";

        private bool _enEdicion = false;

        private bool _procesado = false;

        public String empresa
        {
            get { return _empresa; }
            set { _empresa = value; }
        }
        
        public String codigoUsuario
        {
            get { return _codigoUsuario; }
            set { _codigoUsuario = value; }
        }
        
        public String fechaInicio
        {
            get { return _fechaInicio; }
            set { _fechaInicio = value; }
        }
        
        public String fechaFin
        {
            get { return _fechaFin; }
            set { _fechaFin = value; }
        }
        
        public String condicionVenta
        {
            get { return _condicionVenta; }
            set { _condicionVenta = value; }
        }
        
        public String codigoCliente
        {
            get { return _codigoCliente; }
            set { _codigoCliente = value; }
        }
        
        public String codigoMotivo
        {
            get { return _codigoMotivo; }
            set { _codigoMotivo = value; }
        }
        
        public String montoTotal
        {
            get { return _montoTotal; }
            set { _montoTotal = value; }
        }

        public String montoTotalSoles
        {
            get { return _montoTotalSoles; }
            set { _montoTotalSoles = value; }
        }

        public String montoTotalDolar
        {
            get { return _montoTotalDolar; }
            set { _montoTotalDolar = value; }
        }

        public String codDireccion
        {
            get { return _codDireccion; }
            set { _codDireccion = value; }
        }

        public String codDireccionDespacho
        {
            get { return _codDireccionDespacho; }
            set { _codDireccionDespacho = value; }
        }
        
        public String latitud
        {
            get { return _latitud; }
            set { _latitud = value; }
        }
        
        public String longitud
        {
            get { return _longitud; }
            set { _longitud = value; }
        }
        
        public String celda
        {
            get { return _celda; }
            set { _celda = value; }
        }
        
        public String errorConexion
        {
            get { return _errorConexion; }
            set { _errorConexion = value; }
        }
        
        public String errorPosicion
        {
            get { return _errorPosicion; }
            set { _errorPosicion = value; }
        }
        
        public String observacion
        {
            get { return _observacion; }
            set { _observacion = value; }
        }
        
        public String cantidadTotal
        {
            get { return _cantidadTotal; }
            set { _cantidadTotal = value; }
        }
        
        public List<BEEnvDetPedido> listaDetPedido
        {
            get { return _listaDetPedido; }
            set { _listaDetPedido = value; }
        }
        public String FlgEnCobertura
        {
            get { return _flgEnCobertura; }
            set { _flgEnCobertura = value; }
        }
        public String FlgTerminado
        {
            get { return _flgTerminado; }
            set { _flgTerminado = value; }
        }
        public String codigoPedido
        {
            get { return _codigoPedido; }
            set { _codigoPedido = value; }
        }

        public String CodigoPedidoServidor
        {
            get { return _codigoPedidoServidor; }
            set { _codigoPedidoServidor = value; }
        }
        public String FlgEliminar
        {
            get { return _flgEliminar; }
            set { _flgEliminar = value; }
        }
        public String bonificacionTotal
        {
            get { return _bonificacionTotal; }
            set { _bonificacionTotal = value; }
        }

        public String flgTipo
        {
            get { return _flgTipo; }
            set { _flgTipo = value; }
        }
        public String flgNuevoDireccion
        {
            get { return _flgNuevoDireccion; }
            set { _flgNuevoDireccion = value; }
        }
        public String fleteTotal
        {
            get { return _fleteTotal; }
            set { _fleteTotal = value; }
        }
        public String codAlmacen
        {
            get { return _codAlmacen; }
            set { _codAlmacen = value; }
        }
        public List<BEEnvCtrlPedido> listaCtrlPedido
        {
            get { return _listaCtrlPedido; }
            set { _listaCtrlPedido = value; }
        }


        public String tipoArticulo
        {
            get { return _tipoArticulo; }
            set { _tipoArticulo = value; }
        }
        public String creditoUtilizado
        {
            get { return _creditoUtilizado; }
            set { _creditoUtilizado = value; }
        }

        public bool enEdicion
        {
            get { return _enEdicion; }
            set { _enEdicion = value; }
        }

        public bool procesado
        {
            get { return _procesado; }
            set { _procesado = value; }
        }

        public String tipoCambioSoles
        {
            get { return _tipoCambioSoles; }
            set { _tipoCambioSoles = value; }
        }

        public String tipoCambioDolar
        {
            get { return _tipoCambioDolar; }
            set { _tipoCambioDolar = value; }
        }

        #region CONSTRUCTOR
        public BEEnvCabPedido()
		{

		}

		#endregion

        public BeanPedidoCab cambiabeanes()
        {
            BeanPedidoCab lobeanfinal = new BeanPedidoCab();
            lobeanfinal.lstPedidoDetalle = new List<BeanPedidoDet>();
            //List<BeanPedidoDet> lolistadetalle = new List<BeanPedidoDet>();
            BeanPedidoDet lobeandetalle = new BeanPedidoDet();
 
            lobeanfinal.codigoPedido = this.codigoPedido;
            lobeanfinal.empresa = this.empresa;
            lobeanfinal.codigoUsuario = this.codigoUsuario;
            lobeanfinal.fechaInicio = this.fechaInicio;
            lobeanfinal.fechaFin = this.fechaFin;
            lobeanfinal.condicionVenta = this.condicionVenta;
            lobeanfinal.codigoCliente = this.codigoCliente;
            lobeanfinal.codigoMotivo = this.codigoMotivo;
            lobeanfinal.montoTotal = this.montoTotal;
            lobeanfinal.montoTotalSoles = this.montoTotalSoles; //@JBELVY
            lobeanfinal.montoTotalDolar = this.montoTotalDolar; //@JBELVY
            lobeanfinal.tipoCambioSoles = this.tipoCambioSoles; //@JBELVY
            lobeanfinal.tipoCambioDolar = this.tipoCambioDolar; //@JBELVY

            try
            {
                int.Parse(this.codDireccion);
                lobeanfinal.direccion = this.codDireccion;
            
            }
            catch (Exception e)
            {
                lobeanfinal.direccion = "0";
            }

            try
            {
                int.Parse(this.codDireccionDespacho);
                lobeanfinal.direccionDespacho = this.codDireccionDespacho;

            }
            catch (Exception e)
            {
                lobeanfinal.direccionDespacho = "0";
            }
            lobeanfinal.doubleLatitud = Double.Parse(this.latitud.ToString());
            lobeanfinal.doubleLongitud = Double.Parse(this.longitud.ToString());
            lobeanfinal.celda = this.celda;
            lobeanfinal.errorConexion = Int32.Parse(this.errorConexion);
            lobeanfinal.errorPosicion = Int32.Parse(this.errorPosicion);
            lobeanfinal.observacion = this.observacion;
            lobeanfinal.cantidadTotal = this.cantidadTotal;

            lobeanfinal.FlgEnCobertura = this.FlgEnCobertura;
            lobeanfinal.FlgTerminado = this.FlgTerminado;
            lobeanfinal.CodigoPedidoServidor = this.CodigoPedidoServidor;
            lobeanfinal.bonificacionTotal = this.bonificacionTotal;
            lobeanfinal.flgTipo = this.flgTipo;
            lobeanfinal.codAlmacen = Int16.Parse(codAlmacen);
            lobeanfinal.fleteTotal = this.fleteTotal;
            lobeanfinal.tipoArticulo = this.tipoArticulo;
            lobeanfinal.creditoUtilizado = this.creditoUtilizado;
            lobeanfinal.enEdicion = this.enEdicion;
            foreach (BEEnvDetPedido loPedidodet in this.listaDetPedido)
            {
                lobeandetalle = loPedidodet.cambiabeanes();
                lobeanfinal.lstPedidoDetalle.Add(lobeandetalle);
            }

            lobeanfinal.listaCtrlPedido = this.listaCtrlPedido;

            return lobeanfinal;
        }
	}
}

