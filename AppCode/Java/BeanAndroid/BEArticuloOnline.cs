using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Collections.Generic;
using System.Collections;
using NextlibServer.RMS;
using NextlibServer.Utils;

namespace Java.Bean
{
    public class BEArticuloOnline
    {
        #region Atributos
        public String id="";
        public String codigo = "";
        public String nombre = "";
        public String stock = "";
        public String precioBase = "0";
        public String codAlmacen = "0";
        public String nombreAlmacen = "";
        public String tipoArticulo = "";
        public String tipoBusqueda = "";
        public String codigoProducto = "";
        public String nombreProducto = "";
        public String cantidadPresentacion = "";
        public String unidadDefecto = "";
        #endregion

    }
}