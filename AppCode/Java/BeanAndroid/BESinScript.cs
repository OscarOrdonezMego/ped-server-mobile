using System;
using System.Collections.Generic;
using System.Text;

namespace Nextel.BE
{
    public class BESinScript
    {
        #region PROPIEDADES
        private String _sql;

        public String sql
        {
            get { return _sql; }
            set { _sql = value; }
        }
        #endregion

        #region CONSTRUCTOR
        public BESinScript(String pssql)
        {
            _sql = pssql;
        }
        #endregion
        
    }
}
