﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Configuration;

namespace Pedidos_Mobile.App_Code.Tools
{
    public class LogHelper
    {
        // private static Logger logger = LogManager.GetCurrentClassLogger();
        private Logger logger;
        private string logActive;

        public LogHelper()
        {
            logActive = ConfigurationManager.AppSettings["LOG_ACTIVO"];
        }

        public void Error(string Tag, Exception e)
        {
            logger = LogManager.GetLogger("ERROR " + Tag + " : ");
            logger.Error(e);
        }

        public void Debug(string Tag, String message)
        {
            logger = LogManager.GetLogger("DEBUG " + Tag + " : ");
            if (logActive == "1")
                logger.Debug(message);
        }

        public void Debug(string Tag, Object objecto)
        {
            logger = LogManager.GetLogger("DEBUG " + Tag + " : ");
            if (logActive == "1")
                logger.Debug(JsonConvert.SerializeObject(objecto));
        }

        //public static 
    }
}