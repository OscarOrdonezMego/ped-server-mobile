using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Collections.Generic;
using System.Collections;

namespace NextlibServer.RMS
{
    /// <summary>
    /// eespinoza - Clase a implementarse para la administraci�n de acceso a los RMS
    /// </summary>
    public abstract class RMSManager
    {
        /// <summary>
        /// Nombre del store
        /// </summary>
        public String storeName;

        /// <summary>
        /// Number of times the store has been opened.
        /// private int storeOpened = 0;
        /// Instance of the persistent store
        /// </summary>
        protected DataOutputStream data;

        /// <summary>
        /// Ingresa el nombre del RecorStore
        /// </summary>
        /// <param name="name"></param>
        public RMSManager(String name)
        {
            storeName = name;
        }

        /**
         * Abstract method to create an empty record.
         * <P>
         * <b>Example: </b>
         * <pre>
         * <code>
         * public class MyStore extends GestorRMS
         * {
         *   ...
         *   protected OAbstractRecord createRecord()
         *   {
         *     return new MyRecord();
         *   }
         * </code>
         * </pre>
         * where MyRecord is the name of your record.
         * @return An empty OAbstractRecord.
         */

        /// <summary>
        /// M�todo abstracto para crear un record limpio
        /// </summary>
        /// <returns>Persistent</returns>
        abstract protected Persistent createObjectBean();

        /**
         * Es el metodo que se debe redefinir para guardar los datos de un objeto en el
         * Record Store
         * @param object el objeto que encapsula los datos a guardar
         * @throws Exception excepcion generada en el proceso de guardado
         */
        //protected void guardar(Persistente object1)
        //{
        //    byte[] arrByte = object1.persistir(null, null);

        //    data.write(arrByte);
        //}

        /// <summary>
        /// M�todo para guardar un persistent
        /// </summary>
        /// <param name="arrByte">Persistent a guardar</param>
        protected void save(byte[] arrByte)
        {
            data.write(arrByte);
        }

        /// <summary>
        /// M�todo para guardar una lista de persistent
        /// </summary>
        /// <param name="persistentList">Lista de persistent a guardar</param>
        protected void save(List<Persistent> persistentList)
        {
            DataOutputStream dataTemp = new DataOutputStream();

            createObjectBean().persist(dataTemp, persistentList);

            data.writeInt(dataTemp.byteList.Count);
            data.write(dataTemp.getArray());

            dataTemp.clear();
            dataTemp = null;
        }

        /// <summary>
        /// Crea un objeto para persistir la lista de persistent
        /// </summary>
        /// <param name="persistentList"></param>
        protected void temporarilyServerSave(List<Persistent> persistentList)
        {
            createObjectBean().persist(data, persistentList);
        }
        //protected List<Persistente> getAllList(DataInputStream inputStream)
        //{
        //    List<Persistente> listBean = null;
        //    listBean = createObjectBean().recuperar(inputStream);
        //    return listBean;
        //}

        /// <summary>
        /// Abre el RecordStore
        /// </summary>
        protected void openRS()
        {
            data = new DataOutputStream();
        }

        /// <summary>
        /// Cierra el RecordStore
        /// </summary>
        protected void closeRS()
        {
            data.clear( );
        }

        /// <summary>
        /// Obtiene registros o el registro en byte[]
        /// </summary>
        /// <returns>registros o el registro en byte[]</returns>
        public byte[] getDataByte()
        {
            return data.getArray();
        }
    }
}
