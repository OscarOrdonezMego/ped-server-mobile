#region referencias
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Collections.Generic;
using System.Collections;
#endregion

namespace NextlibServer.RMS
{
    /// <summary>
    /// eespinoza - 1.0 30/11/07 - Clase para administrar la creacion de indices
    /// </summary>
    public class RMSIndex : RMSManager
    {
        /// <summary>
        /// Constructor. El nombre ingresado es con el que se se hace match en la parte m�vil.
        /// </summary>
        /// <param name="name">Nombre ingresado</param>
        public RMSIndex(String name) : base(name) { }

        /// <summary>
        /// Abre el RecordStore
        /// </summary>
        public void openIndex()
        {
            openRS();
        }

        //public void guardarIndice(byte[] arrByte){
        //      guardar(arrByte);
        //}

        /// <summary>
        /// M�todo para guardar el �ndice tipo String
        /// </summary>
        /// <param name="object1">�ndice tipo String</param>
        public void saveIndex(String object1)
        {
            DataOutputStream dataTemp = new DataOutputStream();

            dataTemp.writeString(object1);
            //createObjectBean().persistir(dataTemp, lista);
            data.writeInt(dataTemp.byteList.Count);
            data.write(dataTemp.getArray());

            dataTemp.clear();
            dataTemp = null;
        }

        /// <summary>
        /// M�todo para guardar el �ndice tipo long
        /// </summary>
        /// <param name="object1">�ndice tipo long</param>
        public void saveIndex(long object1)
        {
            DataOutputStream dataTemp = new DataOutputStream();

            dataTemp.writeLong(object1);
            //createObjectBean().persistir(dataTemp, lista);

            data.writeInt(dataTemp.byteList.Count);
            data.write(dataTemp.getArray());

            dataTemp.clear();
            dataTemp = null;
            //data.writeString(object1);
        }

        /// <summary>
        /// Cierra el RecordStore
        /// </summary>
        public void closeIndex()
        {
            closeRS();
        }

        /// <summary>
        /// M�todo que crea Persistent nulo.
        /// </summary>
        /// <returns>Persistent nulo.</returns>
        protected override Persistent createObjectBean()
        {
            return null;
        }
    }
}
