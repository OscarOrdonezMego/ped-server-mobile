#region Referencias
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Collections.Generic;
using System.Collections;
#endregion

namespace NextlibServer.RMS
{
    /// <summary>
    /// Clase utilizada para leer el arreglo de datos recibidos del m�vil.
    /// </summary>
    public class DataInputStream
    {
        /// <summary>
        /// Arreglo de bytes que sera utilizado para almacenar los bytes recibidos del m�vil.
        /// </summary>
        private byte[] _byteArray;

        /// <summary>
        /// Cantidad total de bytes leido de la parte m�vil
        /// </summary>
        private int totalRead = -1;

        /// <summary>
        /// Constructor. Inicializa el _byteArray con el arreglo de bytes.
        /// </summary>
        /// <param name="byteArray">Arreglo de bytes con el que se inicializa el _byteArray</param>
        public DataInputStream(byte[] byteArray)
        {
            this._byteArray = byteArray;
        }

        /// <summary>
        /// M�todo que lee un atributo int del arreglo de bytes.
        /// </summary>
        /// <returns>Atributo int</returns>
        public int readInt()
        {
            byte ch1 = _byteArray[++totalRead];
            byte ch2 = _byteArray[++totalRead];
            byte ch3 = _byteArray[++totalRead];
            byte ch4 = _byteArray[++totalRead];

            return ((ch1 << 24) + (ch2 << 16) + (ch3 << 8) + (ch4 << 0));
        }

        /// <summary>
        /// M�todo que lee un atributo char del arreglo de bytes.
        /// </summary>
        /// <returns>Atributo char</returns>
        public char readChar()
        {
            byte ch1 = _byteArray[++totalRead];
            byte ch2 = _byteArray[++totalRead];

            return (char)((ch1 << 8) + (ch2 << 0));
        }

        /// <summary>
        /// M�todo que lee un atributo short del arreglo de bytes.
        /// </summary>
        /// <returns>Atributo short</returns>
        public short readShort()
        {
            byte ch1 = _byteArray[++totalRead];
            byte ch2 = _byteArray[++totalRead];

            return (short)((ch1 << 8) + (ch2 << 0));
        }

        /// <summary>
        /// M�todo que lee un atributo string del arreglo de bytes.
        /// </summary>
        /// <returns>Atributo string</returns>
        public string readString()
        {
            int strLen = readInt();

            char[] str = new char[strLen];

            for (int it = 0; it < strLen; it++)
            {
                str[it] = readChar();
            }

            return new String(str);
        }

        /// <summary>
        /// M�todo que lee un atributo long del arreglo de bytes.
        /// </summary>
        /// <returns>Atributo long</returns>
        public long readLong()
        {
            return ((long)(readInt()) << 32) + (readInt() & 0xFFFFFFFFL);
        }

        /// <summary>
        /// La cadena de Bytes que se lee incluye la cabecera que puede contener 1 byte o 2 bytes.
        /// El tama�o m�ximo de una cadena de bytes para una cabecera byte ser�a 255 + 1 = 256 [cabecera=1Byte][Data].
        /// El tama�o m�ximo de una cadena de bytes para una cabecera short ser�a 65535 + 2 = 65537 [cabecera=2Bytes][Data].
        /// </summary>
        /// <returns>Atributo String</returns>
        public string readStringBeta()
        {
            int longCadenaBytes = _byteArray.Length; //Lee el arreglo de Bytes
            int strLen = 0;

            if (longCadenaBytes <= 256)
            {
                strLen = readByte();
            }
            else
            {
                strLen = readShort();
            }

            char[] str = new char[strLen];

            for (int it = 0; it < strLen; it++)
            {
                str[it] = readChar();
            }

            return new String(str);
        }

        /// <summary>
        /// aMendiola 21/10/2010 - M�todo que lee un atributo short del arreglo de bytes.
        /// </summary>
        /// <returns>Atributo short</returns>
        public short readByte()
        {
            byte ch1 = _byteArray[++totalRead];
            return (byte)(ch1);
        }

        /// <summary>
        /// M�todo que lee un atributo byte[] del arreglo de bytes.
        /// </summary>
        /// <returns>Atributo byte[]</returns>
        public byte[] readBytes()
        {
            byte[] byteArr = new byte[_byteArray.Length - totalRead - 1];

            for (int i = 0; i < byteArr.Length; i++)
            {
                byteArr[i] = _byteArray[++totalRead];
            }
            return byteArr;
        }
    }
}
