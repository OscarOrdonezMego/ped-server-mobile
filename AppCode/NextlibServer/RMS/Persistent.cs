using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Collections.Generic;
using System.Collections;

namespace NextlibServer.RMS
{
    /// <summary>
    /// Clase abstracta que deben implementar las clase Bean que sus datos seran guardados
    /// en un Record Store
    /// Permite almacenar mas de un objeto por registro, para eso se utiliza
    /// el List.
    /// <author>eespinoza</author>
    /// <version>1.2, 30/11/2007</version>
    /// </summary>
    public abstract class Persistent
    {
        #region Attributes
        /// <summary>
        /// Id del recordStore
        /// </summary>
        private int idRecordset;

        /// <summary>
        /// Posici�n del recordStore. Solo usado en caso se guarde m�s de un psersistent en un Este solo es en una lista empieza de 1 en adelante
        /// </summary>
        private short idPosition;
        #endregion

        #region Getter and setter
        /// <summary>
        /// Obtiene el RecordId donde se almacena el objeto
        /// </summary>
        /// <returns>idRecordset</returns>
        public int getIdRecordset()
        {
            return idRecordset;
        }

        /// <summary>
        /// Ingresa el idRecodset donde se almacena el objeto
        /// </summary>
        /// <param name="idRecodset">idRecodset</param>
        public void setIdRecordset(int idRecordset)
        {
            this.idRecordset = idRecordset;
        }

        /// <summary>
        /// Obtiene la posici�n del recordstore donde se almacena el objeto
        /// </summary>
        /// <returns>IdPosition</returns>
        public short getIdPosition()
        {
            return idPosition;
        }

        /// <summary>
        /// Obtiene la posici�n del recordstore donde se almacena el objeto
        /// </summary>
        /// <returns>IdPosition</returns>
        public void setIdPosition(short idPosition)
        {
            this.idPosition = idPosition;
        }
        #endregion

        /// <summary>
        /// M�todo para persistir la lista de persistent
        /// </summary>
        /// <param name="dataOutput">Stream donde se escribe el arreglo de bytes</param>
        /// <param name="list">Lista de persistentes a persistir</param>
        public abstract void persist(DataOutputStream dataOutput, List<Persistent> list);

        //public abstract List<Persistente> recuperar(DataInputStream dataInput);
    }
}
