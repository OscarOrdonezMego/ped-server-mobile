#region Referencias
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Collections.Generic;
using System.Collections;
#endregion

namespace NextlibServer.RMS
{
    /// <summary>
    /// Clase utilizada para escribir el arreglo de datos a enviarse al m�vil.
    /// </summary>
    public class DataOutputStream
    {
        /// <summary>
        /// Lista de byte que sera utilizado para almacenar los bytes escritos a enviarse al m�vil.
        /// </summary>
        private List<byte> _byteList;

        /// <summary>
        /// Lista de byte que sera utilizado para almacenar los bytes escritos a enviarse al m�vil.
        /// </summary>
        public List<byte> byteList
        {
            get { return _byteList; }
            set { _byteList = value; }
        }

        /// <summary>
        /// Constructor. Inicializa el byteList.
        /// </summary>
        public DataOutputStream()
        {
            _byteList = new List<byte>();
        }

        /// <summary>
        /// Devuelve el arreglo de bytes de byteList.
        /// </summary>
        /// <returns>Arreglo de bytes de byteList.</returns>
        public byte[] getArray()
        {
            byte[] sendByte = new byte[_byteList.Count];

            for (int i = 0; i < _byteList.Count; i++)
            {
                sendByte[i] = _byteList[i];
            }

            return sendByte;
        }

        /// <summary>
        /// M�todo para a�adir un atributo int al arreglo de bytes.
        /// </summary>
        /// <param name="v">Atributo int</param>
        public void writeInt(int v)
        {
            _byteList.Add((byte)((v >> 24) & 0xFF));
            _byteList.Add((byte)((v >> 16) & 0xFF));
            _byteList.Add((byte)((v >> 8) & 0xFF));
            _byteList.Add((byte)((v >> 0) & 0xFF));
        }


        /// <summary>
        /// M�todo para a�adir un atributo String al arreglo de bytes.
        /// </summary>
        /// <param name="v">Atributo String</param>
        public void writeString(String s)
        {
            char[] arrChar = s.ToCharArray();

            writeInt(arrChar.Length);

            for (int i = 0; i < arrChar.Length; i++)
            {
                char v = arrChar[i];
                writeChar(v);
            }
        }

        /// <summary>
        /// M�todo para a�adir un atributo char al arreglo de bytes.
        /// </summary>
        /// <param name="v">Atributo char</param>
        public void writeChar(char v)
        {
            _byteList.Add((byte)((v >> 8) & 0xFF));
            _byteList.Add((byte)((v >> 0) & 0xFF));
        }

        /// <summary>
        /// M�todo para a�adir un atributo byte al arreglo de bytes.
        /// </summary>
        /// <param name="v">Atributo byte</param>
        public void writeByte(byte v) 
        {	  
	        _byteList.Add(v);
        }

        /// <summary>
        /// M�todo para a�adir un atributo byte[] al arreglo de bytes.
        /// </summary>
        /// <param name="v">Atributo byte[]</param>
        public void write(byte[] b)
        {
            for (int i = 0; i < b.Length; i++)
            {
                _byteList.Add(b[i]);
            }
        }

        /// <summary>
        /// M�todo para a�adir un atributo short al arreglo de bytes.
        /// </summary>
        /// <param name="v">Atributo short</param>
        public void writeShort(short v)
        {
            _byteList.Add((byte)((v >> 8) & 0xFF));
            _byteList.Add((byte)((v >> 0) & 0xFF));
        }

        /// <summary>
        /// M�todo para a�adir un atributo long al arreglo de bytes.
        /// </summary>
        /// <param name="v">Atributo long</param>
        public void writeLong(long v)
        {
            _byteList.Add((byte)((v >> 56) & 0xFF));
            _byteList.Add((byte)((v >> 48) & 0xFF));
            _byteList.Add((byte)((v >> 40) & 0xFF));
            _byteList.Add((byte)((v >> 32) & 0xFF));
            _byteList.Add((byte)((v >> 24) & 0xFF));
            _byteList.Add((byte)((v >> 16) & 0xFF));
            _byteList.Add((byte)((v >> 8) & 0xFF));
            _byteList.Add((byte)((v >> 0) & 0xFF));
        }

        /// <summary>
        /// M�todo que inicializa el arreglo de bytes.
        /// </summary>
        public void clear()
        {
            if (_byteList != null)
            {
                _byteList.Clear();
                _byteList = null;
            }
        }

        /// <summary>
        /// aMendiola 21/10/2010 - Escribe la cabecera del string como short.
        /// Usarlo s�lo para persistir beans
        /// </summary>
        /// <param name="s"></param>
        public void writeStringShort(String s)
        {
            char[] arrChar = s.ToCharArray();

            writeShort((short)arrChar.Length);

            for (int i = 0; i < arrChar.Length; i++)
            {
                char v = arrChar[i];
                writeChar(v);
            }
        }

        /// <summary>
        /// aMendiola 21/10/2010 - Escribe la cabecera del string como byte (1..255).
        /// Usarlo s�lo para persistir beans
        /// </summary>
        /// <param name="s"></param>
        public void writeStringByte(String s)
        {
            char[] arrChar = s.ToCharArray();

            writeByte((byte)arrChar.Length);

            for (int i = 0; i < arrChar.Length; i++)
            {
                char v = arrChar[i];
                writeChar(v);
            }
        }
    }
}
