using System;
using System.Configuration;

namespace NextlibServer.DAO
{
    /// <summary>
    /// Clase Base para la clase de conexi�n a base de datos.
    /// </summary>
    public class DALBase
    {
        #region Propiedades
        /// <summary>
        /// Atributo que accede a la cadena de conexi�n de base de datos.
        /// La cadena de conexi�n se encuentra en el web config como el atributo "SQLServerConnection"
        /// </summary>
        public static string connectionString
        {
            get
            {
                ConnectionStringSettings ConnectionStringSettings = ConfigurationManager.ConnectionStrings["SQLServerConnection"];
                return ConnectionStringSettings.ConnectionString;
            }
        }

        //Integracion Ntrack
        /// <summary>
        /// Atributo que accede a la cadena de conexi�n de base de datos del NServices.
        /// La cadena de conexi�n se encuentra en el web config como el atributo "SQLServerNServices"
        /// </summary>
        public static string connectionNServices
        {
            get
            {
                ConnectionStringSettings ConnectionStringSettings = ConfigurationManager.ConnectionStrings["SQLServerNServices"];
                return ConnectionStringSettings.ConnectionString;
            }
        }


        #endregion

        #region Manjejo de nulos
        /// <summary>
        /// Funci�n que verifica si el objeto ingresado es nulo.
        /// De ser nulo, devuelve el vac�o de String.
        /// De lo contrario, devuelve el mismo objeto
        /// </summary>
        /// <param name="objectParameter">Objeto a verificar si es nulo</param>
        /// <returns>Vac�o en caso de ser nulo, el objeto en caso contrario</returns>
        protected static String convertNullStringToBlank(object objectParameter)
        {
            if (objectParameter == DBNull.Value)
            {
                return "";
            }
            else
            {
                return (String)objectParameter;
            }
        }

        /// <summary>
        /// Funci�n que verifica si el objeto ingresado es nulo.
        /// De ser nulo, devuelve el vac�o (0) de Int32.
        /// De lo contrario, devuelve el mismo objeto
        /// </summary>
        /// <param name="parameterObject">Objeto a verificar si es nulo</param>
        /// <returns>Vac�o en caso de ser nulo, el objeto en caso contrario</returns>
        protected static Int32 convertNullIntToZero(object objectParameter)
        {
            if (objectParameter == DBNull.Value)
            {
                return 0;
            }
            else
            {
                return (Int32)objectParameter;
            }
        }

        /// <summary>
        /// Funci�n que verifica si el objeto ingresado es nulo.
        /// De ser nulo, devuelve el vac�o (0) de Decimal.
        /// De lo contrario, devuelve el mismo objeto
        /// </summary>
        /// <param name="parameterObject">Objeto a verificar si es nulo</param>
        /// <returns>Vac�o en caso de ser nulo, el objeto en caso contrario</returns>
        protected static Decimal convertNullDecimalToZero(object objectParameter)
        {
            if (objectParameter == DBNull.Value)
            {
                return 0;
            }
            else
            {
                return (Decimal)objectParameter;
            }
        }

        /// <summary>
        /// Funci�n que verifica si el objeto ingresado es nulo.
        /// De ser nulo, devuelve la fecha actual.
        /// De lo contrario, devuelve el mismo objeto
        /// </summary>
        /// <param name="parameterObject">Objeto a verificar si es nulo</param>
        /// <returns>Fecha actual en caso de ser nulo, el objeto en caso contrario</returns>
        protected static DateTime convertNullDateToDateNow(object objectParameter)
        {
            if (objectParameter == DBNull.Value)
            {
                return DateTime.Now;
            }
            else
            {
                return (DateTime)objectParameter;
            }
        }

        /// <summary>
        /// Funci�n que verifica si el objeto ingresado es nulo.
        /// De ser nulo, devuelve el vac�o (0) de Decimal.
        /// De lo contrario, devuelve el mismo objeto
        /// </summary>
        /// <param name="parameterObject">Objeto a verificar si es nulo</param>
        /// <returns>Vac�o en caso de ser nulo, el objeto en caso contrario</returns>
        protected static T convertToDefault<T>(T parameterObject)
        {
            if ((Object)parameterObject == DBNull.Value)
            {
                if (default(T) != null)
                {
                    return default(T);
                }
                else
                {
                    String ls = "";
                    return (T)Convert.ChangeType(ls, typeof(T));
                }
            }
            else
            {
                return parameterObject;
            }
        }
        #endregion
    }
}
