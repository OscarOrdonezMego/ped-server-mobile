using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;

namespace NextlibServer.DAO
{
    /// <summary>
    /// Clase auxiliar para la ejecuci�n de Querys
    /// </summary>
    public class DBUtilSQLServer
    {
        /// <summary>
        /// Ejecuci�n del Query. Devuelve el parametro indicado.
        /// </summary>
        /// <param name="storeProcedureName">Nombre del Store Procedure</param>
        /// <param name="parametersList">Lista de parametros</param>
        /// <param name="parameter">Indice del valor a devolver</param>
        /// <returns>Valor del parametro a devolver</returns>
        public static string executeQuery(string storeProcedureName, ArrayList parametersList, int parameter)
        {
            string value = "";

            SqlConnection sqlConnection = getConnection();
            SqlCommand sqlCommand = getCommand(sqlConnection, storeProcedureName, parametersList);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();
            dataAdapter.Fill(dataSet);

            value = sqlCommand.Parameters[parameter].Value + "";

            closeConnection(sqlConnection);

            return value;
        }

        /// <summary>
        /// Ejecuci�n del Query.
        /// </summary>
        /// <param name="storeProcedureName">Nombre del Store Procedure</param>
        /// <param name="parametersList">Lista de parametros</param>
        public static void executeNonQuery(string storeProcedureName, ArrayList parametersList)
        {
            SqlConnection sqlConnection = getConnection();
            SqlCommand sqlCommand = getCommand(sqlConnection, storeProcedureName, parametersList);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.ExecuteNonQuery();

            closeConnection(sqlConnection);
        }

        /// <summary>
        /// Funci�n que ejecuta el command y obtiene el dataset
        /// Soluciona el problema de timeout
        /// </summary>
        /// <param name="sqlCommand">Command a ejecutar</param>
        /// <returns>dataset resultante</returns>
        protected DataSet executeDataSet(SqlCommand sqlCommand)
        {
            // Create the DataAdapter & DataSet
            using (SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand))
            {
                DataSet dataSet = new DataSet();

                // Fill the DataSet using default values for DataTable names, etc
                dataAdapter.Fill(dataSet);

                // Detach the SqlParameters from the command object, so they can be used again
                sqlCommand.Parameters.Clear();

                // Return the dataset
                return dataSet;
            }
        }

        /// <summary>
        /// Ejecuci�n del Query. Devuelve el dataset generado
        /// </summary>
        /// <param name="storeProcedureName">Nombre del Store Procedure</param>
        /// <param name="parametersList">Lista de parametros</param>
        /// <param name="parameter">Indice del valor a devolver.</param>
        /// <returns>Dataset resultante del Store Procedure.</returns>
        public static DataSet getDataset(string storeProcedureName, ArrayList parametersList)
        {

            SqlConnection sqlConnection = getConnection();
            SqlCommand sqlCommand = getCommand(sqlConnection, storeProcedureName, parametersList);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
            DataSet dataSet = new DataSet();
            dataAdapter.Fill(dataSet);

            closeConnection(sqlConnection);
            return dataSet;
        }

        /// <summary>
        /// Obtiene la conexi�n abierta
        /// </summary>
        /// <returns>Conexi�n</returns>
        public static SqlConnection getConnection()
        {
            SqlConnection sqlConnection = new SqlConnection(DALBase.connectionString);
            sqlConnection.Open();
            return sqlConnection;
        }
        
        /// <summary>
        /// Funci�n que devuelve la conexi�n abierta
        /// </summary>
        /// <param name="sqlConnection">Conexi�n a abrir</param>
        /// <returns>Conexi�n abierta</returns>
        public static SqlConnection openConnection(SqlConnection sqlConnection)
        {
            sqlConnection.Open();
            return sqlConnection;
        }

        /// <summary>
        /// Funci�n que crea un objeto SqlCommand con los parametros enviados.
        /// </summary>
        /// <param name="sqlConnection">Conexi�n Sql</param>
        /// <param name="storeProcedureName">Nombre del Store Procedure</param>
        /// <param name="parametersList">Lista de parametros</param>
        /// <returns>Objeto SqlCommand</returns>
        private static SqlCommand getCommand(SqlConnection sqlConnection, string storeProcedureName, ArrayList parametersList)
        {
            SqlCommand sqlCommand = new SqlCommand(storeProcedureName, sqlConnection);

            IEnumerator parametersEnumerator = parametersList.GetEnumerator();
            while (parametersEnumerator.MoveNext())
            {
                sqlCommand.Parameters.Add((SqlParameter)parametersEnumerator.Current);
            }

            return sqlCommand;
        }

        /// <summary>
        /// Funci�n que crea un objeto SqlCommand con los parametros enviados.
        /// </summary>
        /// <param name="sqlConnection">Conexi�n Sql</param>
        /// <param name="query">Query a ejecutar</param>
        /// <returns>Objeto SqlCommand</returns>
        private static SqlCommand getCommand(SqlConnection sqlConnection, string query)
        {
            SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);

            return sqlCommand;
        }

        /// <summary>
        /// M�todo que cierra la conexi�n Sql.
        /// </summary>
        /// <param name="sqlConnection">Conexi�n Sql a cerrar.</param>
        private static void closeConnection(SqlConnection sqlConnection)
        {
            sqlConnection.Close();
        }

        /// <summary>
        /// Funci�n que obtiene los parametros de la conexi�n como Hashtable
        /// </summary>
        /// <returns>Hashtable con los parametros de la conexi�n</returns>
        public static Hashtable getConnectionStringTable()
        {
            string[] parameters = DALBase.connectionString.Split(';');
            string[] keyValue;
            Hashtable table = new Hashtable(4);

            foreach (string parameter in parameters)
            {
                keyValue = parameter.Trim().Split('=');
                table.Add(keyValue[0].Trim().ToUpper(), keyValue[1]);
            }

            return table;
        }

        /// <summary>
        /// Funci�n que crea una conexi�n Sql sin aperturarla
        /// </summary>
        /// <returns>Conexi�n Sql sin aperturarla</returns>
        public static SqlConnection createConnection()
        {
            SqlConnection sqlConnection = new SqlConnection(DALBase.connectionString);
            return sqlConnection;
        }
    }
}
