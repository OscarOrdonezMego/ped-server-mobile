using ICSharpCode.SharpZipLib.Zip;
using System;
using System.IO;
using System.IO.Compression;

namespace NextlibServer.Utils
{
    public class StringUtils
    {
        /// <summary>
        /// Convierte el parameterString de null a vacio si es nulo. 
        /// Caso contrario, devuelve el misma parameterString.
        /// </summary>
        /// <param name="parameterString">Cadena de texto</param>
        /// <returns>"" si el parametro es nulo, 
        /// sino el mismo valor del parametro</returns>
        public static String convertNullStringToBlank(String parameterString)
        {
            return parameterString != null ? parameterString : "";
        }

        /// <summary>
        /// Funci�n que cambia la primera letra de la palabra a may�scula.
        /// Si la palabra es nula o vac�a, devuelve vac�o.
        /// </summary>
        /// <param name="word">Palabra a modificar a primera letra.</param>
        /// <returns>Palabra con la primera letra en may�scula</returns>
        public static string changeFirstLetterToUppercase(string word)
        {
            if (string.IsNullOrEmpty(word))
            {
                return string.Empty;
            }
            // Return char and concat substring.
            return char.ToUpper(word[0]) + word.Substring(1).ToLower();
        }

        /// <summary>
        /// Transformaci�n para el encode de javascript.
        /// </summary>
        /// <param name="value">valor a transformar.</param>
        /// <returns>Valor transformado a javascript.</returns>
        public static String JSEncode(String value)
        {
            if (value == null) { return ""; }
            String s1 = value.Replace("\\", "\\\\");
            String s2 = s1.Replace("\n", "\\n");
            String s3 = s2.Replace("\r", "\\r");
            String s4 = s3.Replace("'", "\\'");

            return s4.Replace("\"", "\\\"");
        }

        #region Conversiones
        /// <summary>
        /// Funci�n que convierte el String ingresado a char
        /// </summary>
        /// <param name="stringParameter">string a convertir</param>
        /// <returns>String convertido a char. 
        /// En caso el String sea nulo, el char tendr� de valor '0'</returns>
        public static char convertStringToChar(String stringParameter)
        {
            char result = '0';
            if (stringParameter != null)
            {
                if (stringParameter.Length > 0)
                    result = stringParameter.ToCharArray()[0];
            }

            return result;
        }

        /// <summary>
        /// Funci�n que convierte el String ingresado a int
        /// </summary>
        /// <param name="stringParameter">string a convertir</param>
        /// <returns>String convertido a int. 
        /// En caso el String sea nulo, el int tendr� de valor 0</returns>
        public static int convertStringToInt(String stringParameter)
        {
            int result = 0;
            if (stringParameter != null)
            {
                if (MathUtils.isValidIntNumber(stringParameter))
                {
                    result = Convert.ToInt32(stringParameter);
                }
            }

            return result;
        }

        /// <summary>
        /// Funci�n que convierte el String ingresado a Int64
        /// </summary>
        /// <param name="stringParameter">string a convertir</param>
        /// <returns>String convertido a Int64. 
        /// En caso el String sea nulo, el Int64 tendr� de valor 0</returns>
        public static Int64 convertStringToInt64(String stringParameter)
        {
            Int64 result = 0;
            if (stringParameter != null)
            {
                if (MathUtils.isValidInt64Number(stringParameter))
                {
                    result = Convert.ToInt64(stringParameter);
                }
            }

            return result;
        }
        #endregion

        #region Compresi�n de byteArray
        /// <summary>
        /// Funci�n de compresi�n. 
        /// El algoritmo var�a dependiendo de la plataforma proveniente.
        /// </summary>
        /// <param name="data">Arreglo de bytes a comprimir</param>
        /// <param name="platform">Plataforma proveniente: 2G (iDEN) o 3G</param>
        /// <returns>Arreglo de bytes comprimido en formato *.zip</returns>
        public static byte[] compress(byte[] data, String platform)
        {
            MemoryStream stream = new MemoryStream();
            byte[] buffer;

            if (platform.Equals("2G"))
            {
                ZipOutputStream oZipStream = new ZipOutputStream(stream);

                ZipEntry entry = new ZipEntry("datos.bin");
                entry.Size = data.Length;
                oZipStream.PutNextEntry(entry);
                oZipStream.Write(data, 0, data.Length);
                oZipStream.CloseEntry();
                oZipStream.Finish();
                oZipStream.Close();
            }
            else if (platform.Equals("3G"))
            {
                using (GZipStream gz = new GZipStream(stream, CompressionMode.Compress, false))
                {
                    gz.Write(data, 0, data.Length);
                }
            }
            buffer = stream.ToArray();
            return buffer;
        }

        /// <summary>
        /// Funci�n de descompresi�n. 
        /// El algoritmo var�a dependiendo de la plataforma proveniente.
        /// HMONTERO 20120924 - El algoritmo de 3G est� de PRUEBA.
        /// </summary>
        /// <param name="data">Arreglo de bytes a comprimir</param>
        /// <param name="platform">Plataforma proveniente: 2G (iDEN) o 3G</param>
        /// <returns>Arreglo de bytes descomprimido.</returns>
        public static byte[] decompress(byte[] data, String platform)
        {
            MemoryStream stream;
            byte[] buffer = null;

            if (platform.Equals("2G"))
            {
                stream = new MemoryStream(data);
                ZipInputStream zin = new ZipInputStream(stream);
                ZipEntry zen = zin.GetNextEntry();
                buffer = new byte[zen.Size];
                zin.Read(buffer, 0, buffer.Length);
            }
            else if (platform.Equals("3G"))
            {
                stream = new MemoryStream();

                using (GZipStream gz = new GZipStream(stream, CompressionMode.Decompress, false))
                {
                    gz.Write(data, 0, data.Length);
                }

                buffer = stream.ToArray();
            }

            return buffer;
        }
        #endregion
    }
}
