using System;
using System.Data;
using System.Drawing;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace NextlibServer.GPS
{
    public class Point
    {
        #region Atributos
        private double _x;
        private double _y;
        #endregion

        #region Propiedades
        public double x 
        {
            get { return _x; }
            set { _x = value; }
        }

        public double y
        {
            get { return _y; }
            set { _y = value; }
        }
        #endregion

        #region Constructor
        public Point(String x, String y)
        {
            this._x = double.Parse(x);
            this._y = double.Parse(y);
        }

        public Point(double x, double y)
        {
            this._x = x;
            this._y = y;
        }
        #endregion
    }
}
