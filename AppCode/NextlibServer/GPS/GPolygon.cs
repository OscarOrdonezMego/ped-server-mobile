using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace NextlibServer.GPS
{
    /// <summary>
    /// Clase que se encarga del manejo de polígonos en el mapa
    /// </summary>
    public class GPolygon
    {
        #region Atributos
        /// <summary>
        /// Lista de puntos que representa los vertices
        /// </summary>
        private readonly List<Point> _vertex = new List<Point>();
        #endregion

        #region Constructores
        public GPolygon(){}

        public GPolygon(List<Point> vertex)
        {
            this._vertex = vertex;
        }
        #endregion

        /// <summary>
        /// Función que verifica si las coordenadas ingresadas 
        /// se encuentran dentro del polígono
        /// </summary>
        /// <param name="latitude">Latitud</param>
        /// <param name="longitud">longitud</param>
        /// <returns>Boolean de verificación</returns>
        public bool isPointInsidePolygon(string latitude, string longitud)
        {
            if (latitude != "" && longitud != "")
            {
                Point point = new Point(latitude, longitud);
                return isPointInsidePolygon(point);
            }
            return false;
        }

        /// <summary>
        /// Función que verifica si el punto ingresado 
        /// se encuentran dentro del polígono
        /// </summary>
        /// <param name="point">Punto con coordenadas</param>
        /// <returns>Boolean de verificación</returns>
        public bool isPointInsidePolygon(Point point)
        {
            int j = _vertex.Count - 1;
            bool oddNodes = false;

            for (int i = 0; i < _vertex.Count; i++)
            {
                if (_vertex[i].y < point.y && _vertex[j].y >= point.y ||
                    _vertex[j].y < point.y && _vertex[i].y >= point.y)
                {
                    if (_vertex[i].x +
                        (point.y - _vertex[i].y) / (_vertex[j].y - _vertex[i].y) * (_vertex[j].x - _vertex[i].x) < point.x)
                    {
                        oddNodes = !oddNodes;
                    }
                }
                j = i;
            }

            return oddNodes;
        }

        /// <summary>
        /// Función que transforma la cadena e ingresa las coordenadas a la 
        /// lista de puntos que representa los vertices
        /// </summary>
        /// <param name="parameterString">Cadena con muchas posiciones.
        /// Las posiciones son divididas por el caracter @, mientras 
        /// la latitud y la longitud son dividos por el caracter "|".</param>
        public void parseString(string parameterString)
        {
            Point point = null;
            string[] arraylatlon = parameterString.Split('@');
            foreach (string latlon in arraylatlon)
            {
                string[] arrayPoint = latlon.Split('|');
                if (arrayPoint.Length > 1 && arrayPoint[0] != "" && arrayPoint[1] != "")
                {
                    point = new Point(arrayPoint[0], arrayPoint[1]);
                    _vertex.Add(point);
                }
            }
        }
    }
}
