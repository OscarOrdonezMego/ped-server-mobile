using System;
using System.Xml;
using System.Configuration;
using Pedidos_Mobile.App_Code.Tools;

namespace NextlibServer.Main
{
    /// <summary>
    /// Clase Base para los controles.
    /// </summary>
    public class ControlBase
    {
        /// <summary>
        /// M�todo que permite registrar en el log el mensaje ingresado
        /// </summary>
        /// <param name="message">Mensaje a registrar</param>
        public static void registerLog(String message)
        {
            string logActive = ConfigurationManager.AppSettings["LOG_ACTIVO"];

            if (logActive == "1")
            {
                string path = ConfigurationManager.AppSettings["rutaLog"];
                //Log.Write(HttpContext.Current.Server.MapPath("~") + path, message);
                new LogHelper().Debug("ControlBase -> registerLog", message);
            }
        }

        /// <summary>
        /// Funci�n que anexa la cabecera "version='1.0' encoding='ISO-8859-1'" a la trama XML
        /// </summary>
        /// <param name="xmlOutput">XML a la que se le agrega la cabecera</param>
        public static void appendXmlHeader(XmlDocument xmlOutput)
        {
            xmlOutput.AppendChild(xmlOutput.CreateProcessingInstruction("xml", "version='1.0' encoding='ISO-8859-1'"));
        }

        /// <summary>
        /// Funcion que devuelve un XML de Error
        /// </summary>
        /// <param name="errorMessage">Mensaje de Error a Mostrar</param>
        /// <returns>XML con la cabecera "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>" y de cuerpo una etiqueta 'r' y descripci�n de mensaje de error</returns>
        public static string createErrorXml(String errorMessage)
        {
            return "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?> <r e=\"" + errorMessage + "\" /> ";
        }

        /// <summary>
        /// Funci�n que obtiene un String del arreglo de Byte ingrseado
        /// </summary>
        /// <param name="binaryValue">Arreglo de Byte a transformar</param>
        /// <returns>Strign obtenido del arreglo de bytes</returns>
        public static string convertBinaryToString(Byte[] binaryValue)
        {
            string stringValue = "";

            for (Int64 i = 0; i < binaryValue.Length; i++)
            {
                stringValue += (char)binaryValue[i];
            }

            return stringValue;
        }
    }
}
