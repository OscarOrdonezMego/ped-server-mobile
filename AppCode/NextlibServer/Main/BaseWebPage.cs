using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace NextlibServer.Main
{
    /// <summary>
    /// Clase base para las p�ginas *.aspx.cs.
    /// </summary>
    public class BaseWebPage : Page
    {
        #region Atributos
        private String _inputXml;
        private byte[] _inputByteArray;
        private String _platform = "2G";
        #endregion

        #region Propiedades
        public String inputXml
        {
            get { return _inputXml; }
            set { _inputXml = value; }
        }

        public byte[] inputByteArray
        {
            get { return _inputByteArray; }
            set { _inputByteArray = value; }
        }

        public String platform
        {
            get { return _platform; }
            set { _platform = value; }
        }
        #endregion

        /// <summary>
        /// M�todo que realiza el procesamiento inicial del paquete enviado y 
        /// verifica la plataforma de la que fue enviado (iDEN-2G o J2ME-3G)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void initializesPage()
        {
            _inputByteArray = Request.BinaryRead(Request.TotalBytes);
            _inputXml = ControlBase.convertBinaryToString(_inputByteArray);

            _platform = "2G";
            if (Request.Headers["Platform"] != null && Request.Headers["Platform"] == "J2ME-3G")
                _platform = "3G";
        }

        /// <summary>
        /// M�todo que realiza el procesamiento inicial del paquete enviado, 
        /// lo carga en un  arreglo de bytes, y verifica la plataforma de la 
        /// que fue enviado (iDEN-2G o J2ME-3G)
        /// </summary>
        protected void initializesPageByteArray()
        {
            _inputByteArray = Request.BinaryRead(Request.TotalBytes);

            _platform = "2G";
            if (Request.Headers["Platform"] != null && Request.Headers["Platform"] == "J2ME-3G")
                _platform = "3G";
        }

    }
}
