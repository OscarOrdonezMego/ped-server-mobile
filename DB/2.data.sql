GO
SET IDENTITY_INSERT [dbo].[CFServicio] ON 

GO
INSERT [dbo].[CFServicio] ([IdServicio], [CodServicio], [FchRegistro]) VALUES (1, N'LIT', CAST(N'2015-01-26 15:32:01.120' AS DateTime))
GO
INSERT [dbo].[CFServicio] ([IdServicio], [CodServicio], [FchRegistro]) VALUES (2, N'PRO', CAST(N'2015-01-26 15:32:05.097' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[CFServicio] OFF
GO
SET IDENTITY_INSERT [dbo].[CFModulo] ON 

GO
INSERT [dbo].[CFModulo] ([IdModulo], [CodModulo], [Descripcion], [FlgHabilitado], [FchRegistro]) VALUES (1, N'MPED', N'Pedido', N'T', CAST(N'2014-07-25 15:37:50.660' AS DateTime))
GO
INSERT [dbo].[CFModulo] ([IdModulo], [CodModulo], [Descripcion], [FlgHabilitado], [FchRegistro]) VALUES (2, N'MCOB', N'Cobranza', N'T', CAST(N'2014-07-25 15:37:50.660' AS DateTime))
GO
INSERT [dbo].[CFModulo] ([IdModulo], [CodModulo], [Descripcion], [FlgHabilitado], [FchRegistro]) VALUES (3, N'MDEV', N'Devolución', N'F', CAST(N'2014-07-30 19:43:07.240' AS DateTime))
GO
INSERT [dbo].[CFModulo] ([IdModulo], [CodModulo], [Descripcion], [FlgHabilitado], [FchRegistro]) VALUES (4, N'MCAN', N'Canje', N'F', CAST(N'2014-07-30 19:43:07.240' AS DateTime))
GO
INSERT [dbo].[CFModulo] ([IdModulo], [CodModulo], [Descripcion], [FlgHabilitado], [FchRegistro]) VALUES (5, N'MGEN', N'General', N'T', CAST(N'2015-02-06 10:01:42.103' AS DateTime))
GO
INSERT [dbo].[CFModulo] ([IdModulo], [CodModulo], [Descripcion], [FlgHabilitado], [FchRegistro]) VALUES (6, N'MCTI', N'Cotización', N'F', CAST(N'2015-04-07 11:03:44.277' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[CFModulo] OFF
GO
SET IDENTITY_INSERT [dbo].[CFFuncion] ON 

GO
INSERT [dbo].[CFFuncion] ([IdFuncion], [IdModulo], [CodFuncion], [Descripcion], [FlgHabilitado], [FchRegistro]) VALUES (1, 1, N'FPED', N'Toma Pedido', N'T', CAST(N'2014-07-25 16:13:02.707' AS DateTime))
GO
INSERT [dbo].[CFFuncion] ([IdFuncion], [IdModulo], [CodFuncion], [Descripcion], [FlgHabilitado], [FchRegistro]) VALUES (2, 1, N'FDES', N'Descuento', N'T', CAST(N'2014-07-25 16:15:44.753' AS DateTime))
GO
INSERT [dbo].[CFFuncion] ([IdFuncion], [IdModulo], [CodFuncion], [Descripcion], [FlgHabilitado], [FchRegistro]) VALUES (3, 1, N'FSTO', N'Stock', N'T', CAST(N'2014-07-25 16:19:32.780' AS DateTime))
GO
INSERT [dbo].[CFFuncion] ([IdFuncion], [IdModulo], [CodFuncion], [Descripcion], [FlgHabilitado], [FchRegistro]) VALUES (4, 1, N'FPRE', N'Precio', N'T', CAST(N'2014-07-25 16:29:20.800' AS DateTime))
GO
INSERT [dbo].[CFFuncion] ([IdFuncion], [IdModulo], [CodFuncion], [Descripcion], [FlgHabilitado], [FchRegistro]) VALUES (5, 1, N'FBON', N'Bonificación', N'F', CAST(N'2014-07-25 16:39:17.733' AS DateTime))
GO
INSERT [dbo].[CFFuncion] ([IdFuncion], [IdModulo], [CodFuncion], [Descripcion], [FlgHabilitado], [FchRegistro]) VALUES (6, 2, N'FCOB', N'Cobranza', N'T', CAST(N'2014-07-25 16:37:33.423' AS DateTime))
GO
INSERT [dbo].[CFFuncion] ([IdFuncion], [IdModulo], [CodFuncion], [Descripcion], [FlgHabilitado], [FchRegistro]) VALUES (7, 3, N'FDEV', N'Devolución', N'T', CAST(N'2014-07-30 19:43:46.890' AS DateTime))
GO
INSERT [dbo].[CFFuncion] ([IdFuncion], [IdModulo], [CodFuncion], [Descripcion], [FlgHabilitado], [FchRegistro]) VALUES (8, 4, N'FCAN', N'Canje', N'T', CAST(N'2014-07-30 19:43:46.890' AS DateTime))
GO
INSERT [dbo].[CFFuncion] ([IdFuncion], [IdModulo], [CodFuncion], [Descripcion], [FlgHabilitado], [FchRegistro]) VALUES (9, 5, N'FGEN', N'General', N'T', CAST(N'2014-07-30 19:43:46.890' AS DateTime))
GO
INSERT [dbo].[CFFuncion] ([IdFuncion], [IdModulo], [CodFuncion], [Descripcion], [FlgHabilitado], [FchRegistro]) VALUES (10, 1, N'FCLI', N'Cliente', N'F', CAST(N'2015-03-30 18:18:44.670' AS DateTime))
GO
INSERT [dbo].[CFFuncion] ([IdFuncion], [IdModulo], [CodFuncion], [Descripcion], [FlgHabilitado], [FchRegistro]) VALUES (11, 6, N'FCTI', N'Cotización', N'T', CAST(N'2015-04-07 11:04:23.787' AS DateTime))
GO
INSERT [dbo].[CFFuncion] ([IdFuncion], [IdModulo], [CodFuncion], [Descripcion], [FlgHabilitado], [FchRegistro]) VALUES (13, 1, N'FFRA', N'Fraccionamiento', N'F', CAST(N'2015-05-14 17:18:31.417' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[CFFuncion] OFF
GO
SET IDENTITY_INSERT [dbo].[CFConfiguracion] ON 

GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (1, N'Habilitar Descuento', N'F', 2, N'T', CAST(N'2014-07-30 00:00:00.000' AS DateTime), N'DEHD', N'CHK', 1)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (2, N'Descuento por Porcentaje', N'T', 2, N'T', CAST(N'2014-07-30 00:00:00.000' AS DateTime), N'DEDP', N'HOR', 4)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (3, N'Rango Descuento Minimo', N'10', 2, N'T', CAST(N'2014-07-30 00:00:00.000' AS DateTime), N'DERI', N'NUM', 5)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (5, N'Rango Descuento Maximo', N'90', 2, N'T', CAST(N'2014-07-30 00:00:00.000' AS DateTime), N'DERA', N'NUM', 6)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (10, N'Stock', N'T', 3, N'T', CAST(N'2014-07-30 00:00:00.000' AS DateTime), N'STST', N'HOR', 7)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (11, N'Validar Stock', N'F', 3, N'T', CAST(N'2014-07-30 00:00:00.000' AS DateTime), N'STVS', N'HOR', 8)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (13, N'Descontar Stock', N'T', 3, N'T', CAST(N'2014-07-30 00:00:00.000' AS DateTime), N'STDS', N'CHK', 9)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (15, N'Consultar Stock en Linea', N'T', 3, N'T', CAST(N'2014-08-28 17:02:52.720' AS DateTime), N'STCL', N'CHK', 10)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (16, N'Stock Restrictivo', N'F', 3, N'T', CAST(N'2014-09-04 13:11:36.823' AS DateTime), N'STSR', N'CHK', 11)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (17, N'Validar Precio', N'F', 4, N'T', CAST(N'2014-12-02 19:27:45.993' AS DateTime), N'PRVP', N'CHK', 12)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (19, N'Precio Editable', N'F', 4, N'T', CAST(N'2014-12-03 15:48:26.350' AS DateTime), N'PRPE', N'CHK', 13)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (20, N'Precio por Tipo Cliente', N'T', 4, N'T', CAST(N'2014-12-03 17:19:29.300' AS DateTime), N'PRTC', N'CHG', 14)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (21, N'Precio por Condición de Venta', N'T', 4, N'T', CAST(N'2014-12-04 19:24:55.470' AS DateTime), N'PRCV', N'NUM', 15)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (25, N'Bonificación Manual', N'F', 5, N'T', CAST(N'2014-12-05 16:28:06.710' AS DateTime), N'BOBM', N'NUM', 16)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (26, N'Mostrar Condición Venta', N'T', 1, N'T', CAST(N'2014-12-05 16:28:06.710' AS DateTime), N'TPMV', N'NUM', 17)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (33, N'Cobranza', N'T', 6, N'T', CAST(N'2015-02-06 11:16:05.070' AS DateTime), N'COCO', N'NUM', 24)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (34, N'Devolución', N'T', 7, N'T', CAST(N'2015-02-06 11:16:05.070' AS DateTime), N'DEDE', N'NUM', 25)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (35, N'Canje', N'T', 8, N'T', CAST(N'2015-02-06 11:16:05.070' AS DateTime), N'CACN', N'NUM', 26)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (37, N'Gps', N'T', 9, N'T', CAST(N'2015-02-06 11:17:27.393' AS DateTime), N'GEGP', N'NUM', 27)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (38, N'Eficiencia', N'T', 9, N'T', CAST(N'2015-02-06 11:17:27.393' AS DateTime), N'GEEF', N'NUM', 28)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (39, N'Seguimiento Ruta', N'F', 9, N'T', CAST(N'2015-02-06 11:17:27.393' AS DateTime), N'GESR', N'NUM', 29)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (40, N'Es Embebido', N'F', 9, N'T', CAST(N'2015-02-06 11:17:27.393' AS DateTime), N'GEEE', N'NUM', 30)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (41, N'Número Decimales Vista', N'2', 9, N'T', CAST(N'2015-02-06 11:17:27.393' AS DateTime), N'GEDV', N'NUM', 31)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (42, N'Numero Decimales Descarga', N'6', 9, N'T', CAST(N'2015-02-06 11:17:27.393' AS DateTime), N'GEDD', N'NUM', 32)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (44, N'Paginación', N'10', 9, N'T', CAST(N'2015-02-06 11:17:27.393' AS DateTime), N'GEPA', N'NUM', 33)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (45, N'Longitud Del Código De Producto', N'10', 9, N'T', CAST(N'2015-02-06 11:17:27.393' AS DateTime), N'GELC', N'NUM', 34)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (46, N'Descuento por Monto', N'F', 9, N'T', CAST(N'2015-02-06 11:17:27.393' AS DateTime), N'GEDM', N'NUM', 35)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (48, N'Simbolo de Moneda', N'S/.', 9, N'T', CAST(N'2015-02-06 12:36:47.150' AS DateTime), N'GESM', N'NUM', 36)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (49, N'Mostrar Icono', N'1', 9, N'T', CAST(N'2015-02-06 12:37:22.467' AS DateTime), N'GEMI', N'NUM', 37)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (50, N'Imagen Icono', N'Wikimedia-logo.png', 9, N'T', CAST(N'2015-02-06 12:38:05.660' AS DateTime), N'GEII', N'NUM', 38)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (51, N'Habilitar Descuento por Producto', N'T', 2, N'T', CAST(N'2015-03-26 12:24:32.153' AS DateTime), N'DEHP', N'NUM', 2)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (52, N'Habilitar Descuento General', N'F', 2, N'T', CAST(N'2015-03-26 12:28:08.840' AS DateTime), N'DEHG', N'NUM', 3)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (74, N'Campo10', N'F', 10, N'T', CAST(N'2015-04-06 10:46:07.753' AS DateTime), N'CLC1', N'NUM', 39)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (75, N'Campo2', N'F', 10, N'T', CAST(N'2015-04-06 10:46:16.883' AS DateTime), N'CLC2', N'NUM', 40)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (76, N'Campo3', N'F', 10, N'T', CAST(N'2015-04-06 10:46:27.783' AS DateTime), N'CLC3', N'NUM', 41)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (82, N'Campo4', N'F', 10, N'T', CAST(N'2015-04-06 17:00:26.947' AS DateTime), N'CLC4', N'NUM', 42)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (83, N'Campo5', N'F', 10, N'T', CAST(N'2015-04-06 17:00:49.990' AS DateTime), N'CLC5', N'NUM', 43)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (84, N'Cotización', N'T', 11, N'T', CAST(N'2015-04-07 11:05:01.600' AS DateTime), N'CTCT', N'NUM', 45)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (85, N'Mostrar Direcciones del Pedido', N'F', 1, N'T', CAST(N'2015-04-07 17:01:17.503' AS DateTime), N'TPDP', N'NUM', 18)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (87, N'Mostrar Direcciones del Despacho', N'F', 1, N'T', CAST(N'2015-04-07 17:01:48.280' AS DateTime), N'TPDD', N'NUM', 19)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (88, N'Mostrar Direcciones de No Visita', N'F', 1, N'T', CAST(N'2015-04-08 14:42:01.547' AS DateTime), N'TTDV', N'NUM', 20)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (89, N'Crear Dirección de Pedido', N'T', 1, N'T', CAST(N'2015-04-09 12:20:52.667' AS DateTime), N'TTCP', N'NUM', 21)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (90, N'Mostrar Almacen', N'F', 1, N'T', CAST(N'2015-04-16 12:36:34.327' AS DateTime), N'TPAL', NULL, 22)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (92, N'Fraccionamiento Simple', N'F', 13, N'T', CAST(N'2015-05-14 17:19:34.777' AS DateTime), N'TPFS', N'NUM', 46)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (93, N'Fraccionamiento Multiple', N'F', 13, N'T', CAST(N'2015-05-14 17:20:00.367' AS DateTime), N'TPFM', N'NUM', 47)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (94, N'Mostrar Formulario Inicio', N'F', 1, N'T', CAST(N'2015-06-04 17:12:24.050' AS DateTime), N'TPCP', N'NUM', 48)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (95, N'Mostrar Formulario Fin', N'F', 1, N'T', CAST(N'2015-06-04 17:12:48.003' AS DateTime), N'TPFP', N'NUM', 49)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (96, N'Habilitar Consulta Producto Web Service', N'F', 1, N'T', CAST(N'2015-06-10 11:21:50.713' AS DateTime), N'TPCW', N'NUM', 50)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (99, N'Bonificación Automática', N'F', 5, N'T', CAST(N'2015-06-22 15:30:02.787' AS DateTime), N'BONM', N'NUM', 51)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (100, N'Descuento por Volumen', N'F', 2, N'T', CAST(N'2015-06-30 11:08:33.403' AS DateTime), N'DEVL', N'CHK', 52)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (104, N'Fraccionamiento Decimal', N'T', 13, N'T', CAST(N'2015-07-22 00:00:00.000' AS DateTime), N'TPFD', N'NUM', 53)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (105, N'Maximo Numero Decimales', N'0', 13, N'T', CAST(N'2015-07-22 00:00:00.000' AS DateTime), N'TPDA', N'NUM', 54)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (106, N'Habilitar Seguimiento Pedido Web Service', N'F', 1, N'T', CAST(N'2015-08-03 17:58:16.913' AS DateTime), N'TPSP', N'NUM', 55)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (107, N'Teclado Alfanumerico', N'F', 9, N'T', CAST(N'2015-09-18 09:05:34.557' AS DateTime), N'GETA', N'CHK', 56)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (109, N'Habilitar Verificacion Pedidos Web Service', N'F', 1, N'T', CAST(N'2015-09-22 15:40:37.220' AS DateTime), N'PTVW', N'CHK', 57)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (110, N'Habilitar Restricción Por Deuda Vencida', N'F', 1, N'T', CAST(N'2015-10-20 14:31:59.740' AS DateTime), N'TPDV', N'CHK', 58)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (111, N'Habilitar Limite de Credito', N'F', 1, N'T', CAST(N'2015-10-26 17:27:23.780' AS DateTime), N'TPLC', N'CHK', 59)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (112, N'Habilitar Cobranza Cliente Fuera de Ruta', N'T', 1, N'T', CAST(N'2015-11-02 16:20:56.197' AS DateTime), N'TPCF', N'CHK', 60)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (113, N'Numero Maximo de Items por Pedido', N'50', 1, N'T', CAST(N'2016-03-22 10:25:29.007' AS DateTime), N'TPMI', N'NUM', 61)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (114, N'Monto Máximo por Pedido', N'99999', 1, N'T', CAST(N'2016-03-31 11:04:02.700' AS DateTime), N'TPMX', N'NUM', 63)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (115, N'Monto Mínimo por Pedido', N'0', 1, N'T', CAST(N'2016-03-31 11:04:02.917' AS DateTime), N'TPMN', N'NUM', 62)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (116, N'Buscar clientes solo con código numérico', N'F', 9, N'T', CAST(N'2016-07-18 14:26:09.693' AS DateTime), N'BNUC', N'CHK', 64)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (117, N'Buscar productos solo con código numérico', N'F', 9, N'T', CAST(N'2016-07-18 14:26:09.747' AS DateTime), N'BNUA', N'CHK', 65)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (118, N'Modo fuera de cobertura', N'F', 9, N'T', CAST(N'2016-11-30 18:33:01.797' AS DateTime), N'MFDC', N'CHK', 66)
GO
INSERT [dbo].[CFConfiguracion] ([IdConfiguracion], [Descripcion], [Valor], [IdFuncion], [FlgHabilitado], [FchRegistro], [CodConfiguracion], [TipoControl], [Orden]) VALUES (119, N'Reporte tracking', N'F', 9, N'T', CAST(N'2017-09-22 11:53:58.677' AS DateTime), N'RPTK', N'CHK', 67)
GO
SET IDENTITY_INSERT [dbo].[CFConfiguracion] OFF
GO
SET IDENTITY_INSERT [dbo].[CFServicioPermiso] ON 

GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (1, 1, N'T', NULL, NULL, 1, CAST(N'2014-07-25 12:19:54.523' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (2, 2, N'T', NULL, NULL, 1, CAST(N'2015-01-16 18:41:36.407' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (3, 1, N'T', NULL, NULL, 2, CAST(N'2015-01-16 18:41:55.837' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (4, 2, N'T', NULL, NULL, 2, CAST(N'2015-01-16 18:42:06.137' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (5, 1, N'T', NULL, NULL, 3, CAST(N'2015-01-16 18:42:10.290' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (6, 2, N'T', NULL, NULL, 3, CAST(N'2015-01-16 18:42:18.680' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (7, 1, N'T', NULL, NULL, 4, CAST(N'2015-01-16 18:44:35.510' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (8, 2, N'T', NULL, NULL, 4, CAST(N'2015-01-16 18:44:46.503' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (11, 1, N'T', NULL, 1, NULL, CAST(N'2015-01-16 18:45:07.950' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (12, 2, N'T', NULL, 1, NULL, CAST(N'2015-01-16 18:45:11.430' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (13, 1, N'T', NULL, 2, NULL, CAST(N'2015-01-16 18:45:33.230' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (14, 2, N'T', NULL, 2, NULL, CAST(N'2015-01-16 18:46:09.887' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (15, 1, N'T', NULL, 3, NULL, CAST(N'2015-01-16 18:46:15.057' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (16, 2, N'T', NULL, 3, NULL, CAST(N'2015-01-16 18:46:20.040' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (17, 1, N'T', NULL, 4, NULL, CAST(N'2015-01-16 18:46:23.743' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (18, 2, N'T', NULL, 4, NULL, CAST(N'2015-01-16 18:46:26.700' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (19, 1, N'T', NULL, 5, NULL, CAST(N'2015-01-16 18:46:29.153' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (20, 2, N'T', NULL, 5, NULL, CAST(N'2015-01-16 18:46:34.600' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (21, 1, N'T', NULL, 6, NULL, CAST(N'2015-01-16 18:46:44.113' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (22, 2, N'T', NULL, 6, NULL, CAST(N'2015-01-16 18:46:47.477' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (23, 1, N'T', NULL, 7, NULL, CAST(N'2015-01-16 18:48:11.610' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (24, 2, N'T', NULL, 7, NULL, CAST(N'2015-01-16 18:48:14.670' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (25, 1, N'T', NULL, 8, NULL, CAST(N'2015-01-16 18:48:17.773' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (26, 2, N'T', NULL, 8, NULL, CAST(N'2015-01-16 18:48:20.213' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (27, 1, N'T', 1, NULL, NULL, CAST(N'2015-01-16 18:48:25.107' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (28, 2, N'T', 1, NULL, NULL, CAST(N'2015-01-16 18:48:27.993' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (29, 1, N'T', 2, NULL, NULL, CAST(N'2015-01-16 18:48:45.183' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (30, 2, N'T', 2, NULL, NULL, CAST(N'2015-01-16 18:48:47.757' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (31, 1, N'T', 3, NULL, NULL, CAST(N'2015-01-16 18:48:54.260' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (32, 2, N'T', 3, NULL, NULL, CAST(N'2015-01-16 18:49:00.840' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (33, 1, N'T', 5, NULL, NULL, CAST(N'2015-01-16 18:49:02.750' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (34, 2, N'T', 5, NULL, NULL, CAST(N'2015-01-16 18:49:06.763' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (35, 1, N'T', 10, NULL, NULL, CAST(N'2015-01-16 18:49:09.630' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (36, 2, N'T', 10, NULL, NULL, CAST(N'2015-01-16 18:49:12.260' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (37, 1, N'T', 11, NULL, NULL, CAST(N'2015-01-16 18:49:25.440' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (38, 2, N'T', 11, NULL, NULL, CAST(N'2015-01-16 18:49:29.243' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (39, 1, N'T', 13, NULL, NULL, CAST(N'2015-01-16 18:49:33.070' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (40, 2, N'T', 13, NULL, NULL, CAST(N'2015-01-16 18:49:44.267' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (41, 1, N'T', 15, NULL, NULL, CAST(N'2015-01-16 18:49:47.223' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (42, 2, N'T', 15, NULL, NULL, CAST(N'2015-01-16 18:49:50.547' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (43, 1, N'T', 16, NULL, NULL, CAST(N'2015-01-16 18:50:06.097' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (44, 2, N'T', 16, NULL, NULL, CAST(N'2015-01-16 18:50:09.223' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (45, 1, N'T', 17, NULL, NULL, CAST(N'2015-01-16 18:50:16.290' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (46, 2, N'T', 17, NULL, NULL, CAST(N'2015-01-16 18:50:19.643' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (47, 1, N'T', 19, NULL, NULL, CAST(N'2015-01-16 18:50:22.260' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (48, 2, N'T', 19, NULL, NULL, CAST(N'2015-01-16 18:50:26.990' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (49, 1, N'T', 20, NULL, NULL, CAST(N'2015-01-16 18:50:26.990' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (50, 2, N'T', 20, NULL, NULL, CAST(N'2015-01-16 18:50:26.990' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (51, 1, N'T', 21, NULL, NULL, CAST(N'2015-01-16 18:50:26.990' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (52, 2, N'T', 21, NULL, NULL, CAST(N'2015-01-16 18:50:26.990' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (53, 1, N'T', 25, NULL, NULL, CAST(N'2015-01-16 18:50:26.990' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (54, 2, N'T', 25, NULL, NULL, CAST(N'2015-01-16 18:50:26.990' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (55, 1, N'T', 26, NULL, NULL, CAST(N'2015-01-16 18:50:26.990' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (56, 2, N'T', 26, NULL, NULL, CAST(N'2015-01-16 18:50:26.990' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (57, 1, N'T', 33, NULL, NULL, CAST(N'2015-01-16 18:50:26.990' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (58, 2, N'T', 33, NULL, NULL, CAST(N'2015-01-16 18:50:26.990' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (59, 1, N'T', 34, NULL, NULL, CAST(N'2015-01-16 18:50:26.990' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (60, 2, N'T', 34, NULL, NULL, CAST(N'2015-01-16 18:50:26.990' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (61, 1, N'T', 37, NULL, NULL, CAST(N'2015-01-16 18:50:26.990' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (62, 2, N'T', 37, NULL, NULL, CAST(N'2015-01-16 18:50:26.990' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (63, 1, N'T', 38, NULL, NULL, CAST(N'2015-01-16 18:50:26.990' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (64, 2, N'T', 38, NULL, NULL, CAST(N'2015-01-16 18:50:26.990' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (65, 1, N'T', 39, NULL, NULL, CAST(N'2015-01-16 18:50:26.990' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (66, 2, N'T', 39, NULL, NULL, CAST(N'2015-01-16 18:50:26.990' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (67, 1, N'T', 40, NULL, NULL, CAST(N'2015-01-16 18:50:26.990' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (68, 2, N'T', 40, NULL, NULL, CAST(N'2015-01-16 18:50:26.990' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (69, 1, N'T', 41, NULL, NULL, CAST(N'2015-01-16 18:50:26.990' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (70, 2, N'T', 41, NULL, NULL, CAST(N'2015-01-16 18:50:26.990' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (71, 1, N'T', 42, NULL, NULL, CAST(N'2015-01-16 18:50:26.990' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (72, 2, N'T', 42, NULL, NULL, CAST(N'2015-01-16 18:50:26.990' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (73, 1, N'T', 44, NULL, NULL, CAST(N'2015-01-16 18:50:26.990' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (74, 2, N'T', 44, NULL, NULL, CAST(N'2015-01-16 18:50:26.990' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (75, 1, N'T', 45, NULL, NULL, CAST(N'2015-01-16 18:50:26.990' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (76, 2, N'T', 45, NULL, NULL, CAST(N'2015-01-16 18:50:26.990' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (77, 1, N'T', 46, NULL, NULL, CAST(N'2015-01-16 18:50:26.990' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (78, 2, N'T', 46, NULL, NULL, CAST(N'2015-01-16 18:50:26.990' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (79, 1, N'T', 48, NULL, NULL, CAST(N'2015-01-16 18:50:26.990' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (80, 2, N'T', 48, NULL, NULL, CAST(N'2015-01-16 18:50:26.990' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (81, 1, N'T', 49, NULL, NULL, CAST(N'2015-01-16 18:50:26.990' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (82, 2, N'T', 49, NULL, NULL, CAST(N'2015-01-16 18:50:26.990' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (83, 1, N'T', 50, NULL, NULL, CAST(N'2015-01-16 18:50:26.990' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (84, 2, N'T', 50, NULL, NULL, CAST(N'2015-01-16 18:50:26.990' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (94, 1, N'T', 35, NULL, NULL, CAST(N'2015-01-16 18:50:26.990' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (95, 2, N'T', 35, NULL, NULL, CAST(N'2015-01-16 18:50:26.990' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (96, 1, N'T', 51, NULL, NULL, CAST(N'2015-03-26 17:25:22.960' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (97, 2, N'T', 51, NULL, NULL, CAST(N'2015-03-26 17:25:49.680' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (98, 2, N'T', 52, NULL, NULL, CAST(N'2015-03-26 17:25:56.253' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (99, 1, N'T', 52, NULL, NULL, CAST(N'2015-03-26 17:26:02.083' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (100, 1, N'T', NULL, 10, NULL, CAST(N'2015-03-30 18:21:21.037' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (101, 2, N'T', NULL, 10, NULL, CAST(N'2015-03-30 18:21:31.197' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (112, 1, N'T', 74, NULL, NULL, CAST(N'2015-04-06 17:02:07.573' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (113, 2, N'T', 74, NULL, NULL, CAST(N'2015-04-06 17:02:38.560' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (114, 1, N'T', 75, NULL, NULL, CAST(N'2015-04-06 17:02:45.743' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (115, 2, N'T', 75, NULL, NULL, CAST(N'2015-04-06 17:02:50.170' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (116, 1, N'T', 76, NULL, NULL, CAST(N'2015-04-06 17:02:54.933' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (117, 2, N'T', 76, NULL, NULL, CAST(N'2015-04-06 17:03:02.313' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (118, 1, N'T', 82, NULL, NULL, CAST(N'2015-04-06 17:03:24.947' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (119, 2, N'T', 82, NULL, NULL, CAST(N'2015-04-06 17:03:33.813' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (120, 1, N'T', 83, NULL, NULL, CAST(N'2015-04-06 17:03:37.740' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (121, 2, N'T', 83, NULL, NULL, CAST(N'2015-04-06 17:03:42.063' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (122, 1, N'T', NULL, NULL, 6, CAST(N'2015-04-07 11:10:17.217' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (123, 2, N'T', NULL, NULL, 6, CAST(N'2015-04-07 11:10:23.033' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (124, 1, N'T', NULL, 11, NULL, CAST(N'2015-04-07 11:10:52.320' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (125, 2, N'T', NULL, 11, NULL, CAST(N'2015-04-07 11:10:59.710' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (126, 1, N'T', 84, NULL, NULL, CAST(N'2015-04-07 11:11:06.913' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (127, 2, N'T', 84, NULL, NULL, CAST(N'2015-04-07 11:11:12.337' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (128, 1, N'T', 85, NULL, NULL, CAST(N'2015-04-07 17:02:54.063' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (129, 2, N'T', 85, NULL, NULL, CAST(N'2015-04-07 17:03:03.550' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (130, 1, N'T', 87, NULL, NULL, CAST(N'2015-04-07 17:03:09.213' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (131, 2, N'T', 87, NULL, NULL, CAST(N'2015-04-07 17:03:12.883' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (132, 1, N'T', 88, NULL, NULL, CAST(N'2015-04-08 14:42:45.043' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (133, 2, N'T', 88, NULL, NULL, CAST(N'2015-04-08 14:42:55.410' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (134, 1, N'T', 89, NULL, NULL, CAST(N'2015-04-09 14:52:49.597' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (135, 2, N'T', 89, NULL, NULL, CAST(N'2015-04-09 14:52:53.680' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (137, 1, N'T', 90, NULL, NULL, CAST(N'2015-04-16 12:38:51.143' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (138, 2, N'T', 90, NULL, NULL, CAST(N'2015-04-16 12:38:55.970' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (141, 1, N'T', NULL, 13, NULL, CAST(N'2015-05-14 17:21:28.690' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (142, 2, N'T', NULL, 13, NULL, CAST(N'2015-05-14 17:21:35.013' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (143, 1, N'T', 92, NULL, NULL, CAST(N'2015-05-14 17:21:50.097' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (144, 2, N'T', 92, NULL, NULL, CAST(N'2015-05-14 17:21:58.817' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (145, 1, N'T', 93, NULL, NULL, CAST(N'2015-05-14 17:22:03.017' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (146, 2, N'T', 93, NULL, NULL, CAST(N'2015-05-15 09:31:32.377' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (147, 1, N'T', 94, NULL, NULL, CAST(N'2015-06-04 17:14:18.287' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (148, 2, N'T', 94, NULL, NULL, CAST(N'2015-06-04 17:14:34.150' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (149, 1, N'T', 95, NULL, NULL, CAST(N'2015-06-04 17:14:39.657' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (150, 2, N'T', 95, NULL, NULL, CAST(N'2015-06-04 17:14:45.503' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (151, 1, N'T', 96, NULL, NULL, CAST(N'2015-06-10 11:22:57.540' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (152, 2, N'T', 96, NULL, NULL, CAST(N'2015-06-10 11:23:07.410' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (157, 1, N'T', 99, NULL, NULL, CAST(N'2015-06-22 15:39:26.320' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (158, 2, N'T', 99, NULL, NULL, CAST(N'2015-06-22 15:41:55.800' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (159, 1, N'T', 100, NULL, NULL, CAST(N'2015-07-01 10:33:09.577' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (160, 2, N'T', 100, NULL, NULL, CAST(N'2015-07-01 10:33:16.143' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (161, 1, N'T', 104, NULL, NULL, CAST(N'2015-07-22 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (162, 2, N'T', 104, NULL, NULL, CAST(N'2015-07-22 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (163, 1, N'T', 105, NULL, NULL, CAST(N'2015-07-22 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (164, 2, N'T', 105, NULL, NULL, CAST(N'2015-07-22 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (165, 1, N'T', 106, NULL, NULL, CAST(N'2015-08-03 18:00:27.793' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (166, 2, N'T', 106, NULL, NULL, CAST(N'2015-08-03 18:00:34.337' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (171, 1, N'T', 107, NULL, NULL, CAST(N'2015-09-18 09:19:14.380' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (172, 2, N'T', 107, NULL, NULL, CAST(N'2015-09-18 09:19:38.610' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (173, 1, N'T', 109, NULL, NULL, CAST(N'2015-09-22 15:43:01.687' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (174, 2, N'T', 109, NULL, NULL, CAST(N'2015-09-22 15:43:01.690' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (175, 1, N'T', 110, NULL, NULL, CAST(N'2015-10-26 16:55:41.677' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (176, 2, N'T', 110, NULL, NULL, CAST(N'2015-10-26 16:55:49.620' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (177, 1, N'T', 111, NULL, NULL, CAST(N'2015-10-26 17:27:54.480' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (178, 2, N'T', 111, NULL, NULL, CAST(N'2015-10-26 17:28:02.167' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (179, 1, N'T', 112, NULL, NULL, CAST(N'2015-11-02 16:21:26.297' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (180, 2, N'T', 112, NULL, NULL, CAST(N'2015-11-02 16:21:31.437' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (181, 1, N'T', 113, NULL, NULL, CAST(N'2016-03-28 12:36:45.133' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (182, 2, N'T', 113, NULL, NULL, CAST(N'2016-03-28 12:36:45.133' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (183, 1, N'T', 114, NULL, NULL, CAST(N'2016-03-31 11:04:02.963' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (184, 2, N'T', 114, NULL, NULL, CAST(N'2016-03-31 11:04:02.963' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (185, 1, N'T', 115, NULL, NULL, CAST(N'2016-03-31 11:04:02.963' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (186, 2, N'T', 115, NULL, NULL, CAST(N'2016-03-31 11:04:02.963' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (187, 1, N'T', 116, NULL, NULL, CAST(N'2016-07-18 14:40:32.003' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (188, 2, N'T', 116, NULL, NULL, CAST(N'2016-07-18 14:40:32.003' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (189, 1, N'T', 117, NULL, NULL, CAST(N'2016-07-18 14:40:32.003' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (190, 2, N'T', 117, NULL, NULL, CAST(N'2016-07-18 14:40:32.003' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (191, 1, N'T', 118, NULL, NULL, CAST(N'2016-11-30 18:33:01.873' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (192, 2, N'T', 118, NULL, NULL, CAST(N'2016-11-30 18:33:01.873' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (193, 1, N'T', 119, NULL, NULL, CAST(N'2017-09-22 11:53:58.677' AS DateTime))
GO
INSERT [dbo].[CFServicioPermiso] ([IdServicioPermiso], [IdServicio], [FlgHabilitado], [IdConfiguracion], [IdFuncion], [IdModulo], [FchRegistro]) VALUES (194, 2, N'T', 119, NULL, NULL, CAST(N'2017-09-22 11:53:58.677' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[CFServicioPermiso] OFF
GO
SET IDENTITY_INSERT [dbo].[CFTipoPerfil] ON 

GO
INSERT [dbo].[CFTipoPerfil] ([IdTipoPerfil], [CodTipoPerfil], [FchRegistro], [Nivel], [Descripcion]) VALUES (1, N'DBO', CAST(N'2015-01-16 18:26:42.143' AS DateTime), 0, N'DBO')
GO
INSERT [dbo].[CFTipoPerfil] ([IdTipoPerfil], [CodTipoPerfil], [FchRegistro], [Nivel], [Descripcion]) VALUES (2, N'ADM', CAST(N'2015-01-16 18:26:42.143' AS DateTime), 1, N'Administrador')
GO
INSERT [dbo].[CFTipoPerfil] ([IdTipoPerfil], [CodTipoPerfil], [FchRegistro], [Nivel], [Descripcion]) VALUES (3, N'VEN', CAST(N'2015-01-16 18:26:42.143' AS DateTime), 2, N'Vendedor')
GO
INSERT [dbo].[CFTipoPerfil] ([IdTipoPerfil], [CodTipoPerfil], [FchRegistro], [Nivel], [Descripcion]) VALUES (4, N'SUP', CAST(N'2015-07-23 09:48:15.543' AS DateTime), 3, N'SUPERVISOR')
GO
SET IDENTITY_INSERT [dbo].[CFTipoPerfil] OFF
GO
SET IDENTITY_INSERT [dbo].[CFManual] ON 

GO
INSERT [dbo].[CFManual] ([IdManual], [IdTipoPerfil], [Descripcion], [Url], [FchRegistro], [IdModulo]) VALUES (14, 1, N'Manual Administración', N'ManualAdministracion.pdf', CAST(N'2014-10-13 15:51:01.583' AS DateTime), 1)
GO
INSERT [dbo].[CFManual] ([IdManual], [IdTipoPerfil], [Descripcion], [Url], [FchRegistro], [IdModulo]) VALUES (16, 1, N'Manual Usuario Móvil', N'ManualUsuarioMovil.pdf', CAST(N'2014-10-13 15:51:11.813' AS DateTime), 1)
GO
INSERT [dbo].[CFManual] ([IdManual], [IdTipoPerfil], [Descripcion], [Url], [FchRegistro], [IdModulo]) VALUES (17, 1, N'Manual Usuario Web', N'ManualUsuarioWeb.pdf', CAST(N'2014-10-13 15:51:55.167' AS DateTime), 1)
GO
INSERT [dbo].[CFManual] ([IdManual], [IdTipoPerfil], [Descripcion], [Url], [FchRegistro], [IdModulo]) VALUES (18, 1, N'Carga Datos', N'carga_pedidos.xls', CAST(N'2014-10-13 15:52:03.250' AS DateTime), 1)
GO
INSERT [dbo].[CFManual] ([IdManual], [IdTipoPerfil], [Descripcion], [Url], [FchRegistro], [IdModulo]) VALUES (23, 1, N'Manual Rest Api', N'ManualRestApi.pdf', CAST(N'2015-08-11 14:56:40.890' AS DateTime), 1)
GO
INSERT [dbo].[CFManual] ([IdManual], [IdTipoPerfil], [Descripcion], [Url], [FchRegistro], [IdModulo]) VALUES (24, 1, N'Diccionario Datos', N'DiccionarioDatos.pdf', CAST(N'2015-08-11 14:57:13.550' AS DateTime), 1)
GO
INSERT [dbo].[CFManual] ([IdManual], [IdTipoPerfil], [Descripcion], [Url], [FchRegistro], [IdModulo]) VALUES (25, 1, N'Manual Configuración', N'ManualConfiguracion.pdf', CAST(N'2015-08-11 14:58:08.410' AS DateTime), 1)
GO
INSERT [dbo].[CFManual] ([IdManual], [IdTipoPerfil], [Descripcion], [Url], [FchRegistro], [IdModulo]) VALUES (26, 1, N'Manual Web Service', N'ManualWebService.pdf', CAST(N'2015-08-11 15:03:26.127' AS DateTime), 1)
GO
INSERT [dbo].[CFManual] ([IdManual], [IdTipoPerfil], [Descripcion], [Url], [FchRegistro], [IdModulo]) VALUES (27, 2, N'Manual Administración', N'ManualAdministracion.pdf', CAST(N'2014-10-13 15:51:01.000' AS DateTime), 1)
GO
INSERT [dbo].[CFManual] ([IdManual], [IdTipoPerfil], [Descripcion], [Url], [FchRegistro], [IdModulo]) VALUES (28, 2, N'Manual Usuario Móvil', N'ManualUsuarioMovil.pdf', CAST(N'2014-10-13 15:51:11.000' AS DateTime), 1)
GO
INSERT [dbo].[CFManual] ([IdManual], [IdTipoPerfil], [Descripcion], [Url], [FchRegistro], [IdModulo]) VALUES (29, 2, N'Manual Usuario Web', N'ManualUsuarioWeb.pdf', CAST(N'2014-10-13 15:51:55.000' AS DateTime), 1)
GO
INSERT [dbo].[CFManual] ([IdManual], [IdTipoPerfil], [Descripcion], [Url], [FchRegistro], [IdModulo]) VALUES (30, 2, N'Carga Datos', N'carga_pedidos.xls', CAST(N'2014-10-13 15:52:03.000' AS DateTime), 1)
GO
INSERT [dbo].[CFManual] ([IdManual], [IdTipoPerfil], [Descripcion], [Url], [FchRegistro], [IdModulo]) VALUES (32, 2, N'Manual Rest Api', N'ManualRestApi.pdf', CAST(N'2015-08-11 14:56:40.000' AS DateTime), 1)
GO
INSERT [dbo].[CFManual] ([IdManual], [IdTipoPerfil], [Descripcion], [Url], [FchRegistro], [IdModulo]) VALUES (33, 2, N'Diccionario Datos', N'DiccionarioDatos.pdf', CAST(N'2015-08-11 14:57:13.000' AS DateTime), 1)
GO
INSERT [dbo].[CFManual] ([IdManual], [IdTipoPerfil], [Descripcion], [Url], [FchRegistro], [IdModulo]) VALUES (34, 2, N'Manual Configuración', N'ManualConfiguracion.pdf', CAST(N'2015-08-11 14:58:08.000' AS DateTime), 1)
GO
INSERT [dbo].[CFManual] ([IdManual], [IdTipoPerfil], [Descripcion], [Url], [FchRegistro], [IdModulo]) VALUES (35, 2, N'Manual Web Service', N'ManualWebService.pdf', CAST(N'2015-08-11 15:03:26.000' AS DateTime), 1)
GO
INSERT [dbo].[CFManual] ([IdManual], [IdTipoPerfil], [Descripcion], [Url], [FchRegistro], [IdModulo]) VALUES (36, 4, N'Manual Administración', N'ManualAdministracion.pdf', CAST(N'2014-10-13 15:51:01.000' AS DateTime), 1)
GO
INSERT [dbo].[CFManual] ([IdManual], [IdTipoPerfil], [Descripcion], [Url], [FchRegistro], [IdModulo]) VALUES (37, 4, N'Manual Usuario Móvil', N'ManualUsuarioMovil.pdf', CAST(N'2014-10-13 15:51:11.000' AS DateTime), 1)
GO
INSERT [dbo].[CFManual] ([IdManual], [IdTipoPerfil], [Descripcion], [Url], [FchRegistro], [IdModulo]) VALUES (38, 4, N'Manual Usuario Web', N'ManualUsuarioWeb.pdf', CAST(N'2014-10-13 15:51:55.000' AS DateTime), 1)
GO
INSERT [dbo].[CFManual] ([IdManual], [IdTipoPerfil], [Descripcion], [Url], [FchRegistro], [IdModulo]) VALUES (39, 4, N'Carga Datos', N'carga_pedidos.xls', CAST(N'2014-10-13 15:52:03.000' AS DateTime), 1)
GO
INSERT [dbo].[CFManual] ([IdManual], [IdTipoPerfil], [Descripcion], [Url], [FchRegistro], [IdModulo]) VALUES (46, 3, N'Manual Usuario Móvil', N'ManualUsuarioMovil.pdf', CAST(N'2014-10-13 15:51:11.000' AS DateTime), 1)
GO
INSERT [dbo].[CFManual] ([IdManual], [IdTipoPerfil], [Descripcion], [Url], [FchRegistro], [IdModulo]) VALUES (47, 3, N'Manual Usuario Web', N'ManualUsuarioWeb.pdf', CAST(N'2014-10-13 15:51:55.000' AS DateTime), 1)
GO
SET IDENTITY_INSERT [dbo].[CFManual] OFF
GO
SET IDENTITY_INSERT [dbo].[GRPerfil] ON 

GO
INSERT [dbo].[GRPerfil] ([IdPerfil], [Descripcion], [FchRegistro], [IdTipoPerfil]) VALUES (1, N'Administrador', CAST(N'2014-08-04 11:14:14.960' AS DateTime), 2)
GO
INSERT [dbo].[GRPerfil] ([IdPerfil], [Descripcion], [FchRegistro], [IdTipoPerfil]) VALUES (2, N'Vendedor', CAST(N'2014-08-04 11:14:14.960' AS DateTime), 3)
GO
INSERT [dbo].[GRPerfil] ([IdPerfil], [Descripcion], [FchRegistro], [IdTipoPerfil]) VALUES (3, N'DBO', CAST(N'2014-08-04 11:18:21.363' AS DateTime), 1)
GO
INSERT [dbo].[GRPerfil] ([IdPerfil], [Descripcion], [FchRegistro], [IdTipoPerfil]) VALUES (4, N'Supervisor', CAST(N'2015-07-23 09:48:15.547' AS DateTime), 4)
GO
SET IDENTITY_INSERT [dbo].[GRPerfil] OFF
GO
SET IDENTITY_INSERT [dbo].[GRMaestroTabla] ON 

GO
INSERT [dbo].[GRMaestroTabla] ([id], [tabla], [descripcion], [tablaTemp], [spcargaTabla], [FlgHabilitado]) VALUES (1, N'Cliente', N'Tabla cliente', N'TEMP_CLIENTE', N'USPM_CARGA_CLIENTE_NEW', N'T')
GO
INSERT [dbo].[GRMaestroTabla] ([id], [tabla], [descripcion], [tablaTemp], [spcargaTabla], [FlgHabilitado]) VALUES (2, N'Producto', N'Tabla Producto', N'TEMP_PRODUCTO', N'USPM_CARGA_ARTICULO_NEW', N'T')
GO
INSERT [dbo].[GRMaestroTabla] ([id], [tabla], [descripcion], [tablaTemp], [spcargaTabla], [FlgHabilitado]) VALUES (3, N'GENERAL', N'Tabla GENERAL', N'TEMP_GENERAL', N'USPM_CARGA_GENERAL_NEW', N'T')
GO
INSERT [dbo].[GRMaestroTabla] ([id], [tabla], [descripcion], [tablaTemp], [spcargaTabla], [FlgHabilitado]) VALUES (4, N'VENDEDOR', N'Tabla VENDEDOR', N'TEMP_USUARIO', N'USPM_CARGA_USUARIO_NEW', N'T')
GO
INSERT [dbo].[GRMaestroTabla] ([id], [tabla], [descripcion], [tablaTemp], [spcargaTabla], [FlgHabilitado]) VALUES (5, N'RUTA', N'Tabla RUTA', N'TEMP_RUTA', N'USPM_CARGA_RUTA_NEW', N'T')
GO
INSERT [dbo].[GRMaestroTabla] ([id], [tabla], [descripcion], [tablaTemp], [spcargaTabla], [FlgHabilitado]) VALUES (6, N'FAMILIA', N'Tabla FAMILIA', N'TEMP_FAMILIA', N'USPM_CARGA_FAMILIA_NEW', N'T')
GO
INSERT [dbo].[GRMaestroTabla] ([id], [tabla], [descripcion], [tablaTemp], [spcargaTabla], [FlgHabilitado]) VALUES (7, N'FAMILIA_PRODUCTO', N'Tabla FAMILIA_PRODUCTO', N'TEMP_FAMILIA_PRODUCTO', N'USPM_CARGA_FAMILIA_PRODUCTO_NEW', N'T')
GO
INSERT [dbo].[GRMaestroTabla] ([id], [tabla], [descripcion], [tablaTemp], [spcargaTabla], [FlgHabilitado]) VALUES (8, N'MARCA', N'Tabla MARCA', N'TEMP_MARCA', N'USPM_CARGA_MARCA_NEW', N'T')
GO
INSERT [dbo].[GRMaestroTabla] ([id], [tabla], [descripcion], [tablaTemp], [spcargaTabla], [FlgHabilitado]) VALUES (9, N'MARCA_PRODUCTO', N'Tabla MARCA_PRODUCTO', N'TEMP_MARCA_PRODUCTO', N'USPM_CARGA_MARCA_PRODUCTO_NEW', N'T')
GO
SET IDENTITY_INSERT [dbo].[GRMaestroTabla] OFF
GO
SET IDENTITY_INSERT [dbo].[GRMaestroTablaColumnas] ON 

GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (1, 1, N'CODIGO', N'CLI_CODIGO', N'Codigo de cliente', N'TEXTO (500)', N'T', N'T')
GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (2, 1, N'NOMBRE', N'CLI_NOMBRE', N'Nombre de cliente', N'TEXTO (500)', N'T', N'T')
GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (3, 1, N'DIRECCION', N'CLI_DIRECCION', N'Direccion de cliente', N'TEXTO (500)', N'T', N'T')
GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (4, 1, N'TIPO DE CLIENTE', N'TCLI_COD', N'TipoCliente de cliente', N'TEXTO (500)', N'T', N'T')
GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (5, 1, N'GIRO', N'CLI_GIRO', N'GIRO de cliente', N'TEXTO (500)', N'F', N'T')
GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (6, 1, N'LATITUD', N'CLI_LATITUD', N'LATITUD de cliente', N'TEXTO (500)', N'F', N'T')
GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (7, 1, N'LONGITUD', N'CLI_LONGITUD', N'LONGITUD de cliente', N'TEXTO (500)', N'F', N'T')
GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (8, 1, N'ADICIONAL 1', N'CLI_ADICIONAL_1', N'ADICIONAL 1 de cliente', N'TEXTO (500)', N'F', N'T')
GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (9, 1, N'ADICIONAL 2', N'CLI_ADICIONAL_2', N'ADICIONAL 2 de cliente', N'TEXTO (500)', N'F', N'T')
GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (10, 1, N'ADICIONAL 3', N'CLI_ADICIONAL_3', N'ADICIONAL 3 de cliente', N'TEXTO (500)', N'F', N'T')
GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (11, 1, N'ADICIONAL 4', N'CLI_ADICIONAL_4', N'ADICIONAL 4 de cliente', N'TEXTO (500)', N'F', N'T')
GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (12, 1, N'ADICIONAL 5', N'CLI_ADICIONAL_5', N'ADICIONAL 5 de cliente', N'TEXTO (500)', N'F', N'T')
GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (13, 1, N'LIMITE CREDITO', N'LIMITE_CREDITO', N'LIMITE CREDITO de cliente', N'TEXTO (500)', N'F', N'T')
GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (14, 1, N'CREDITO UTILIZADO', N'CREDITO_UTILIZADO', N'CREDITO UTILIZADO de cliente', N'TEXTO (500)', N'F', N'T')
GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (15, 2, N'CODIGO PRO/PRE', N'PRO_CODIGO', N'CODIGO PRO/PRE de Producto', N'TEXTO (500)', N'T', N'T')
GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (16, 2, N'NOMBRE', N'PRO_NOMBRE', N'NOMBRE de Producto', N'TEXTO (500)', N'T', N'T')
GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (17, 2, N'PRECIO BASE', N'PRO_PRECIOBASE', N'PRECIO BASE de Producto', N'TEXTO (500)', N'T', N'T')
GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (18, 2, N'STOCK', N'PRO_STOCK', N'STOCK de Producto', N'TEXTO (500)', N'T', N'T')
GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (19, 2, N'DESCUENTO MINIMO', N'DESC_MIN', N'DESCUENTO MINIMO de Producto', N'TEXTO (500)', N'F', N'T')
GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (20, 2, N'DESCUENTO MAXIMO', N'DESC_MAX', N'DESCUENTO MAXIMO de Producto', N'TEXTO (500)', N'F', N'T')
GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (21, 2, N'UNIDAD FRACCIONAMIENTO', N'UNIDAD_FRACCIONAMIENTO', N'UNIDAD FRACCIONAMIENTO de Producto', N'TEXTO (500)', N'F', N'T')
GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (22, 2, N'CANTIDAD', N'CANTIDAD', N'CANTIDAD de Producto', N'TEXTO (500)', N'F', N'T')
GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (23, 3, N'TIPO CARGA', N'Tipo', N'TIPO CARGA de GENERAL', N'TEXTO (500)', N'T', N'T')
GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (24, 3, N'CODIGO CONCEPTO', N'Codigo', N'CODIGO CONCEPTO de GENERAL', N'TEXTO (500)', N'T', N'T')
GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (25, 3, N'DESCRIPCION CONCEPTO', N'Nombre', N'DESCRIPCION CONCEPTO de GENERAL', N'TEXTO (500)', N'T', N'T')
GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (26, 3, N'ABREVIATURA', N'Abreviatura', N'ABREVIATURA de GENERAL', N'TEXTO (500)', N'T', N'T')
GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (27, 4, N'CODIGO VENDEDOR', N'USR_CODIGO', N'CODIGO VENDEDOR de VENDEDOR', N'TEXTO (500)', N'T', N'T')
GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (28, 4, N'LOGIN', N'USR_LOGIN', N'LOGIN de VENDEDOR', N'TEXTO (500)', N'T', N'T')
GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (29, 4, N'NOMBRE VENDEDOR', N'USR_NOMBRE', N'NOMBRE VENDEDOR de VENDEDOR', N'TEXTO (500)', N'T', N'T')
GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (30, 4, N'CLAVE VENDEDOR', N'USR_CLAVE', N'CLAVE VENDEDOR de VENDEDOR', N'TEXTO (500)', N'T', N'T')
GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (31, 4, N'CODIGO GRUPO', N'USR_GRUPO', N'CODIGO GRUPO de VENDEDOR', N'TEXTO (500)', N'T', N'T')
GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (32, 4, N'CODIGO PERFIL', N'USR_PERFIL', N'CODIGO PERFIL de VENDEDOR', N'TEXTO (500)', N'T', N'T')
GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (33, 5, N'CODIGO CLIENTE', N'CLI_COD', N'CODIGO CLIENTE de RUTA', N'TEXTO (500)', N'T', N'T')
GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (34, 5, N'CODIGO VENDEDOR', N'VEN_COD', N'CODIGO VENDEDOR de RUTA', N'TEXTO (500)', N'T', N'T')
GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (35, 5, N'RUTA', N'VCLID_FECHA', N'RUTA de RUTA', N'TEXTO (500)', N'T', N'T')
GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (36, 6, N'CODIGO FAMILIA', N'FAM_CODIGO', N'CODIGO FAMILIA de FAMILIA', N'TEXTO (500)', N'T', N'T')
GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (37, 6, N'NOMBRE FAMILIA', N'FAM_NOMBRE', N'NOMBRE FAMILIA de FAMILIA', N'TEXTO (500)', N'T', N'T')
GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (38, 7, N'CODIGO FAMILIA', N'FAM_CODIGO', N'CODIGO FAMILIA de FAMILIA', N'TEXTO (500)', N'T', N'T')
GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (39, 7, N'CODIGO PRODUCTO', N'PRO_CODIGO', N'CODIGO PRODUCTO de FAMILIA', N'TEXTO (500)', N'T', N'T')
GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (40, 8, N'CODIGO MARCA', N'MAR_CODIGO', N'CODIGO MARCA de MARCA', N'TEXTO (500)', N'T', N'T')
GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (41, 8, N'NOMBRE MARCA', N'MAR_NOMBRE', N'NOMBRE MARCA de MARCA', N'TEXTO (500)', N'T', N'T')
GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (42, 9, N'CODIGO MARCA', N'MAR_CODIGO', N'CODIGO MARCA de MARCA_PRODUCTO', N'TEXTO (500)', N'T', N'T')
GO
INSERT [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (43, 9, N'CODIGO PRODUCTO', N'PRO_CODIGO', N'CODIGO PRODUCTO de MARCA_PRODUCTO', N'TEXTO (500)', N'T', N'T')
GO
SET IDENTITY_INSERT [dbo].[GRMaestroTablaColumnas] OFF
GO
SET IDENTITY_INSERT [dbo].[TBL_TIPO_CLIENTE] ON 

GO
INSERT [dbo].[TBL_TIPO_CLIENTE] ([TC_PK], [TC_CODIGO], [TC_NOMBRE], [flgEnable]) VALUES (40, N'TCL01', N'Tipo Cliente Prueba', N'F')
GO
INSERT [dbo].[TBL_TIPO_CLIENTE] ([TC_PK], [TC_CODIGO], [TC_NOMBRE], [flgEnable]) VALUES (41, N'01', N'COBERTURA', N'T')
GO
INSERT [dbo].[TBL_TIPO_CLIENTE] ([TC_PK], [TC_CODIGO], [TC_NOMBRE], [flgEnable]) VALUES (42, N'02', N'MERCADO ZO', N'T')
GO
INSERT [dbo].[TBL_TIPO_CLIENTE] ([TC_PK], [TC_CODIGO], [TC_NOMBRE], [flgEnable]) VALUES (43, N'03', N'MAYORISTA', N'T')
GO
INSERT [dbo].[TBL_TIPO_CLIENTE] ([TC_PK], [TC_CODIGO], [TC_NOMBRE], [flgEnable]) VALUES (44, N'04', N'AOML4', N'T')
GO
INSERT [dbo].[TBL_TIPO_CLIENTE] ([TC_PK], [TC_CODIGO], [TC_NOMBRE], [flgEnable]) VALUES (45, N'05', N'TUMBES COB', N'T')
GO
INSERT [dbo].[TBL_TIPO_CLIENTE] ([TC_PK], [TC_CODIGO], [TC_NOMBRE], [flgEnable]) VALUES (46, N'06', N'TUMBES MAY', N'T')
GO
INSERT [dbo].[TBL_TIPO_CLIENTE] ([TC_PK], [TC_CODIGO], [TC_NOMBRE], [flgEnable]) VALUES (47, N'07', N'TUMBES MER', N'T')
GO
INSERT [dbo].[TBL_TIPO_CLIENTE] ([TC_PK], [TC_CODIGO], [TC_NOMBRE], [flgEnable]) VALUES (48, N'08', N'TUMBES GOL', N'T')
GO
SET IDENTITY_INSERT [dbo].[TBL_TIPO_CLIENTE] OFF
GO
SET IDENTITY_INSERT [dbo].[TBL_USUARIO] ON 

GO
INSERT [dbo].[TBL_USUARIO] ([USR_PK], [USR_COMPANIA], [USR_CODIGO], [USR_LOGIN], [USR_NOMBRE], [USR_CLAVE], [VEN_ESTADO], [VEN_PERFIL], [flgEnable], [ULTIMAFECHA], [ULTIMALATITUD], [ULTIMALONGITUD], [MODELO], [SENAL], [BATERIA], [IP], [NEXTEL], [TELEFONO], [ERRORPOSICION], [ERRORCONEXION], [ASISTENCIA], [FlgNServicesObligatorio], [IdPerfil], [Sup_Asignado]) VALUES (1, N'99', N'ADMIN', N'ADMIN', N'ADMIN', N'DD84F0EE24CFD17BA4AC744AFAD8DE1687C2A400', N'A', N'ADM       ', N'T', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'T', 1, NULL)
GO
INSERT [dbo].[TBL_USUARIO] ([USR_PK], [USR_COMPANIA], [USR_CODIGO], [USR_LOGIN], [USR_NOMBRE], [USR_CLAVE], [VEN_ESTADO], [VEN_PERFIL], [flgEnable], [ULTIMAFECHA], [ULTIMALATITUD], [ULTIMALONGITUD], [MODELO], [SENAL], [BATERIA], [IP], [NEXTEL], [TELEFONO], [ERRORPOSICION], [ERRORCONEXION], [ASISTENCIA], [FlgNServicesObligatorio], [IdPerfil], [Sup_Asignado]) VALUES (2, N'99', N'DBO', N'DBO', N'DBO', N'BF9A45BC88DEBEAC31BDD11A4057F731714C96E0', N'A', N'DBO       ', N'T', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'T', 3, NULL)
GO
SET IDENTITY_INSERT [dbo].[TBL_USUARIO] OFF
GO
SET IDENTITY_INSERT [dbo].[GRArchivo] ON 

GO
INSERT [dbo].[GRArchivo] ([IdArchivo], [CodigoArchivo], [NombreArchivo], [FlgHabilitado]) VALUES (1, N'PDO ', N'PEDIDO', N'T')
GO
INSERT [dbo].[GRArchivo] ([IdArchivo], [CodigoArchivo], [NombreArchivo], [FlgHabilitado]) VALUES (2, N'CJE ', N'CANJE', N'T')
GO
INSERT [dbo].[GRArchivo] ([IdArchivo], [CodigoArchivo], [NombreArchivo], [FlgHabilitado]) VALUES (3, N'DVN ', N'DEVOLUCION', N'T')
GO
INSERT [dbo].[GRArchivo] ([IdArchivo], [CodigoArchivo], [NombreArchivo], [FlgHabilitado]) VALUES (4, N'NVTA', N'NO VENTA', N'T')
GO
INSERT [dbo].[GRArchivo] ([IdArchivo], [CodigoArchivo], [NombreArchivo], [FlgHabilitado]) VALUES (5, N'PGO ', N'PAGO', N'T')
GO
INSERT [dbo].[GRArchivo] ([IdArchivo], [CodigoArchivo], [NombreArchivo], [FlgHabilitado]) VALUES (6, N'CTN ', N'COTIZACION', N'T')
GO
INSERT [dbo].[GRArchivo] ([IdArchivo], [CodigoArchivo], [NombreArchivo], [FlgHabilitado]) VALUES (7, N'PRT ', N'PROSPECTO', N'T')
GO
SET IDENTITY_INSERT [dbo].[GRArchivo] OFF
GO
SET IDENTITY_INSERT [dbo].[GRColumnaArchivo] ON 

GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (1, 1, N'PED_SEC_INTERNO', N'PRO', N'C', 1, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (2, 1, N'VEN_COD', N'PRO', N'C', 2, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (3, 1, N'GRU_COD', N'PRO', N'C', 3, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (4, 1, N'CLI_COD', N'PRO', N'C', 4, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (5, 1, N'DIS_COD', N'PRO', N'C', 5, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (7, 1, N'PED_TOTAL', N'PRO', N'C', 6, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (8, 1, N'PED_FECREG_CEL', N'PRO', N'C', 7, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (9, 1, N'OBSERVACION', N'PRO', N'C', 8, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (10, 1, N'LATITUD', N'PRO', N'C', 9, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (11, 1, N'LONGITUD', N'PRO', N'C', 10, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (12, 1, N'ALM_CODIGO', N'PRO', N'C', 11, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (13, 1, N'PED_NRO', N'PRO', N'D', 2, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (14, 1, N'VEN_COD', N'PRO', N'D', 1, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (15, 1, N'GRU_COD', N'PRO', N'D', 5, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (16, 1, N'CLI_COD', N'PRO', N'D', 3, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (17, 1, N'DIS_COD', N'PRO', N'D', 4, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (18, 1, N'DPED_SEC', N'PRO', N'D', 6, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (19, 1, N'PROD_COD', N'PRO', N'D', 7, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (20, 1, N'DPED_CANTIDAD', N'PRO', N'D', 8, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (21, 1, N'DPED_PRECIOBASE', N'PRO', N'D', 9, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (22, 1, N'DPED_DESCUENTO', N'PRO', N'D', 10, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (23, 1, N'DPED_TOTAL', N'PRO', N'D', 11, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (24, 1, N'DET_DESCRIPCION', N'PRO', N'D', 12, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (25, 1, N'PED_SEC_INTERNO', N'PRE', N'C', 1, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (26, 1, N'VEN_COD', N'PRE', N'C', 2, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (27, 1, N'GRU_COD', N'PRE', N'C', 3, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (28, 1, N'CLI_COD', N'PRE', N'C', 4, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (29, 1, N'DIS_COD', N'PRE', N'C', 5, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (31, 1, N'PED_TOTAL', N'PRE', N'C', 6, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (32, 1, N'PED_FECREG_CEL', N'PRE', N'C', 7, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (33, 1, N'OBSERVACION', N'PRE', N'C', 8, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (34, 1, N'LATITUD', N'PRE', N'C', 9, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (35, 1, N'LONGITUD', N'PRE', N'C', 10, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (36, 1, N'ALM_CODIGO', N'PRE', N'C', 11, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (37, 1, N'PED_NRO', N'PRE', N'D', 2, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (38, 1, N'VEN_COD', N'PRE', N'D', 1, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (39, 1, N'GRU_COD', N'PRE', N'D', 4, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (40, 1, N'CLI_COD', N'PRE', N'D', 5, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (41, 1, N'DIS_COD', N'PRE', N'D', 3, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (42, 1, N'DPED_SEC', N'PRE', N'D', 9, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (43, 1, N'PROD_COD', N'PRE', N'D', 10, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (44, 1, N'DPED_CANTIDAD', N'PRE', N'D', 11, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (45, 1, N'DPED_PRECIOBASE', N'PRE', N'D', 12, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (46, 1, N'DPED_DESCUENTO', N'PRE', N'D', 13, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (47, 1, N'DPED_TOTAL', N'PRE', N'D', 14, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (48, 1, N'DPED_FLETE', N'PRE', N'D', 6, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (49, 1, N'DDET_PRECIO_FRAC', N'PRE', N'D', 7, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (50, 1, N'DDET_CANTIDAD_FRAC', N'PRE', N'D', 8, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (51, 1, N'DET_DESCRIPCION', N'PRE', N'D', 15, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (52, 2, N'CAN_SEC_INTERNO', N'PRO', N'C', 1, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (53, 2, N'CLI_COD', N'PRO', N'C', 2, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (54, 2, N'VEN_COD', N'PRO', N'C', 3, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (55, 2, N'GRU_COD', N'PRO', N'C', 4, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (56, 2, N'DIS_COD', N'PRO', N'C', 5, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (57, 2, N'CAN_FECREG_CEL', N'PRO', N'C', 6, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (58, 2, N'IDDENTIFICADOR', N'PRO', N'D', 1, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (59, 2, N'CLI_COD', N'PRO', N'D', 2, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (60, 2, N'DIS_COD', N'PRO', N'D', 3, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (61, 2, N'VEN_COD', N'PRO', N'D', 4, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (62, 2, N'GRU_COD', N'PRO', N'D', 5, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (63, 2, N'DCAN_SEC', N'PRO', N'D', 6, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (64, 2, N'PROD_COD', N'PRO', N'D', 7, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (65, 2, N'DCAN_CANTIDAD', N'PRO', N'D', 8, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (66, 2, N'MOT_CANJE', N'PRO', N'D', 9, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (67, 2, N'OBSERVACION', N'PRO', N'D', 10, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (68, 2, N'DCAN_FECHA_VENC', N'PRO', N'D', 11, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (69, 2, N'CAN_SEC_INTERNO', N'PRE', N'C', 1, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (70, 2, N'CLI_COD', N'PRE', N'C', 2, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (71, 2, N'VEN_COD', N'PRE', N'C', 3, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (72, 2, N'GRU_COD', N'PRE', N'C', 4, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (73, 2, N'DIS_COD', N'PRE', N'C', 5, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (74, 2, N'CAN_FECREG_CEL', N'PRE', N'C', 6, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (75, 2, N'IDDENTIFICADOR', N'PRE', N'D', 1, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (76, 2, N'CLI_COD', N'PRE', N'D', 2, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (77, 2, N'DIS_COD', N'PRE', N'D', 3, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (78, 2, N'VEN_COD', N'PRE', N'D', 4, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (79, 2, N'GRU_COD', N'PRE', N'D', 5, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (80, 2, N'DCAN_SEC', N'PRE', N'D', 6, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (81, 2, N'PRE_PK', N'PRE', N'D', 7, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (82, 2, N'CODPRESENTACION', N'PRE', N'D', 8, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (83, 2, N'DCAN_CANTIDAD', N'PRE', N'D', 9, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (84, 2, N'DETC_CANTIDAD_PRE', N'PRE', N'D', 12, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (85, 2, N'DETC_CANTIDAD_FRAC', N'PRE', N'D', 11, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (86, 2, N'MOT_CANJE', N'PRE', N'D', 10, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (87, 2, N'OBSERVACION', N'PRE', N'D', 13, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (88, 2, N'DCAN_FECHA_VENC', N'PRE', N'D', 14, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (89, 3, N'DEV_SEC_INTERNO', N'PRO', N'C', 1, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (90, 3, N'CLI_COD', N'PRO', N'C', 2, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (91, 3, N'VEN_COD', N'PRO', N'C', 3, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (92, 3, N'GRU_COD', N'PRO', N'C', 4, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (93, 3, N'DIS_COD', N'PRO', N'C', 5, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (94, 3, N'DEV_FECREG_CEL', N'PRO', N'C', 6, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (95, 3, N'IDENTIFICADOR', N'PRO', N'D', 1, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (96, 3, N'CLI_COD', N'PRO', N'D', 2, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (97, 3, N'VEN_COD', N'PRO', N'D', 3, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (98, 3, N'GRU_COD', N'PRO', N'D', 4, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (99, 3, N'DIS_COD', N'PRO', N'D', 5, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (100, 3, N'DDEV_SEC', N'PRO', N'D', 6, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (101, 3, N'PROD_COD', N'PRO', N'D', 7, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (102, 3, N'DDEV_CANTIDAD', N'PRO', N'D', 8, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (103, 3, N'DDEV_MOTIVO', N'PRO', N'D', 9, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (104, 3, N'DDEV_FECHA_VENC', N'PRO', N'D', 10, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (105, 3, N'DEV_SEC_INTERNO', N'PRE', N'C', 1, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (106, 3, N'CLI_COD', N'PRE', N'C', 2, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (107, 3, N'VEN_COD', N'PRE', N'C', 3, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (108, 3, N'GRU_COD', N'PRE', N'C', 4, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (109, 3, N'DIS_COD', N'PRE', N'C', 5, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (110, 3, N'DEV_FECREG_CEL', N'PRE', N'C', 6, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (111, 3, N'IDENTIFICADOR', N'PRE', N'D', 1, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (112, 3, N'CLI_COD', N'PRE', N'D', 2, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (113, 3, N'VEN_COD', N'PRE', N'D', 3, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (114, 3, N'GRU_COD', N'PRE', N'D', 4, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (115, 3, N'DIS_COD', N'PRE', N'D', 5, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (116, 3, N'DDEV_SEC', N'PRE', N'D', 6, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (117, 3, N'PRE_PK', N'PRE', N'D', 7, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (118, 3, N'DDEV_CODPRESENTACION', N'PRE', N'D', 8, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (119, 3, N'DDEV_CANTIDAD', N'PRE', N'D', 9, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (120, 3, N'DDEV_CANTIDAD_PRE', N'PRE', N'D', 10, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (121, 3, N'DDEV_CANTIDAD_FRAC', N'PRE', N'D', 11, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (122, 3, N'DDEV_MOTIVO', N'PRE', N'D', 12, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (123, 3, N'DDEV_FECHA_VENC', N'PRE', N'D', 13, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (124, 4, N'NVTA_SEC_INTERNO', N'PRO', N'C', 1, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (125, 4, N'CLI_COD', N'PRO', N'C', 2, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (126, 4, N'VEN_COD', N'PRO', N'C', 3, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (127, 4, N'GRU_COD', N'PRO', N'C', 4, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (128, 4, N'DIS_COD', N'PRO', N'C', 5, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (129, 4, N'NVTA_MOTIVO', N'PRO', N'C', 6, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (130, 4, N'NVTA_FECREG_CEL', N'PRO', N'C', 7, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (131, 4, N'NVTA_SEC_INTERNO', N'PRE', N'C', 1, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (132, 4, N'CLI_COD', N'PRE', N'C', 2, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (133, 4, N'VEN_COD', N'PRE', N'C', 3, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (134, 4, N'GRU_COD', N'PRE', N'C', 4, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (135, 4, N'DIS_COD', N'PRE', N'C', 5, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (136, 4, N'NVTA_MOTIVO', N'PRE', N'C', 6, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (137, 4, N'NVTA_FECREG_CEL', N'PRE', N'C', 7, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (138, 5, N'COB_CODIGO', N'PRO', N'C', 1, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (139, 5, N'USR_CODIGO', N'PRO', N'C', 2, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (140, 5, N'GRU_COD', N'PRO', N'C', 7, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (141, 5, N'PAG_MONTOPAGO', N'PRO', N'C', 8, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (142, 5, N'PAG_TIPOPAGO', N'PRO', N'C', 9, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (143, 5, N'PAG_CODIGOBANCO', N'PRO', N'C', 6, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (144, 5, N'PAG_VOUCHER', N'PRO', N'C', 5, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (145, 5, N'DIS_COD', N'PRO', N'C', 4, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (146, 5, N'CAN_FECREG_CEL', N'PRO', N'C', 3, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (147, 5, N'COB_CODIGO', N'PRE', N'C', 1, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (148, 5, N'USR_CODIGO', N'PRE', N'C', 2, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (149, 5, N'GRU_COD', N'PRE', N'C', 7, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (150, 5, N'PAG_MONTOPAGO', N'PRE', N'C', 8, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (151, 5, N'PAG_TIPOPAGO', N'PRE', N'C', 9, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (152, 5, N'PAG_CODIGOBANCO', N'PRE', N'C', 6, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (153, 5, N'PAG_VOUCHER', N'PRE', N'C', 5, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (154, 5, N'DIS_COD', N'PRE', N'C', 4, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (155, 5, N'CAN_FECREG_CEL', N'PRE', N'C', 3, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (156, 6, N'PED_SEC_INTERNO', N'PRO', N'C', 1, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (157, 6, N'VEN_COD', N'PRO', N'C', 2, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (158, 6, N'GRU_COD', N'PRO', N'C', 3, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (159, 6, N'CLI_COD', N'PRO', N'C', 4, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (160, 6, N'DIS_COD', N'PRO', N'C', 5, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (161, 6, N'PED_TIPO', N'PRO', N'C', 6, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (162, 6, N'PED_TOTAL', N'PRO', N'C', 7, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (163, 6, N'PED_FECREG_CEL', N'PRO', N'C', 8, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (164, 6, N'OBSERVACION', N'PRO', N'C', 9, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (165, 6, N'LATITUD', N'PRO', N'C', 10, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (166, 6, N'LONGITUD', N'PRO', N'C', 11, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (167, 6, N'ALM_CODIGO', N'PRO', N'C', 12, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (168, 6, N'PED_NRO', N'PRO', N'D', 1, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (169, 6, N'VEN_COD', N'PRO', N'D', 2, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (170, 6, N'GRU_COD', N'PRO', N'D', 3, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (171, 6, N'CLI_COD', N'PRO', N'D', 4, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (172, 6, N'DIS_COD', N'PRO', N'D', 5, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (173, 6, N'DPED_SEC', N'PRO', N'D', 6, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (174, 6, N'PROD_COD', N'PRO', N'D', 7, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (175, 6, N'DPED_CANTIDAD', N'PRO', N'D', 8, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (176, 6, N'DPED_PRECIOBASE', N'PRO', N'D', 9, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (177, 6, N'DPED_DESCUENTO', N'PRO', N'D', 10, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (178, 6, N'DPED_TOTAL', N'PRO', N'D', 11, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (179, 6, N'DET_DESCRIPCION', N'PRO', N'D', 12, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (180, 6, N'PED_SEC_INTERNO', N'PRE', N'C', 1, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (181, 6, N'VEN_COD', N'PRE', N'C', 2, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (182, 6, N'GRU_COD', N'PRE', N'C', 3, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (183, 6, N'CLI_COD', N'PRE', N'C', 4, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (184, 6, N'DIS_COD', N'PRE', N'C', 5, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (185, 6, N'PED_TIPO', N'PRE', N'C', 6, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (186, 6, N'PED_TOTAL', N'PRE', N'C', 7, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (187, 6, N'PED_FECREG_CEL', N'PRE', N'C', 8, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (188, 6, N'OBSERVACION', N'PRE', N'C', 9, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (189, 6, N'LATITUD', N'PRE', N'C', 10, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (190, 6, N'LONGITUD', N'PRE', N'C', 11, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (191, 6, N'ALM_CODIGO', N'PRE', N'C', 12, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (192, 6, N'PED_NRO', N'PRE', N'D', 1, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (193, 6, N'VEN_COD', N'PRE', N'D', 2, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (194, 6, N'GRU_COD', N'PRE', N'D', 3, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (195, 6, N'CLI_COD', N'PRE', N'D', 4, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (196, 6, N'DIS_COD', N'PRE', N'D', 5, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (197, 6, N'DPED_SEC', N'PRE', N'D', 6, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (198, 6, N'PROD_COD', N'PRE', N'D', 7, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (199, 6, N'DPED_CANTIDAD', N'PRE', N'D', 8, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (200, 6, N'DPED_PRECIOBASE', N'PRE', N'D', 9, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (201, 6, N'DPED_DESCUENTO', N'PRE', N'D', 10, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (202, 6, N'DPED_TOTAL', N'PRE', N'D', 11, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (203, 6, N'DPED_FLETE', N'PRE', N'D', 12, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (204, 6, N'DDET_PRECIO_FRAC', N'PRE', N'D', 13, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (205, 6, N'DDET_CANTIDAD_FRAC', N'PRE', N'D', 14, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (206, 6, N'DET_DESCRIPCION', N'PRE', N'D', 15, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (207, 1, N'COD_COND_VENTA', N'PRO', N'C', 13, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (208, 1, N'COD_DIRECCION', N'PRO', N'C', 12, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (209, 1, N'COD_COND_VENTA', N'PRE', N'C', 13, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (210, 1, N'COD_DIRECCION', N'PRE', N'C', 12, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (227, 2, N'LATITUD', N'PRE', N'C', 7, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (228, 2, N'LONGITUD', N'PRE', N'C', 8, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (229, 2, N'LATITUD', N'PRO', N'C', 7, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (230, 2, N'LONGITUD', N'PRO', N'C', 8, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (231, 3, N'LATITUD', N'PRE', N'C', 7, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (232, 3, N'LONGITUD', N'PRE', N'C', 8, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (233, 3, N'LATITUD', N'PRO', N'C', 7, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (234, 3, N'LONGITUD', N'PRO', N'C', 8, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (235, 4, N'LATITUD', N'PRE', N'C', 8, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (236, 4, N'LONGITUD', N'PRE', N'C', 9, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (237, 4, N'LATITUD', N'PRO', N'C', 8, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (238, 4, N'LONGITUD', N'PRO', N'C', 9, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (239, 5, N'LATITUD', N'PRE', N'C', 10, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (240, 5, N'LONGITUD', N'PRE', N'C', 11, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (241, 5, N'LATITUD', N'PRO', N'C', 10, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (242, 5, N'LONGITUD', N'PRO', N'C', 11, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (547, 1, N'DDET_BONIF', N'PRO', N'D', 13, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (548, 1, N'DDET_BONIF', N'PRE', N'D', 16, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (581, 7, N'PRCTO_COD', N'PRO', N'C', 1, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (582, 7, N'PRCTO_NOM', N'PRO', N'C', 2, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (583, 7, N'PRCTO_DIR', N'PRO', N'C', 3, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (584, 7, N'PRCTO_CODVEND', N'PRO', N'C', 4, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (585, 7, N'PRCTO_NOMVEND', N'PRO', N'C', 5, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (586, 7, N'PRCTO_COD_TCLIENTE', N'PRO', N'C', 6, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (587, 7, N'PRCTO_NOM_TCLIENTE', N'PRO', N'C', 7, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (588, 7, N'PRCTO_GIRO', N'PRO', N'C', 8, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (589, 7, N'PRCTO_LATITUD', N'PRO', N'C', 9, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (590, 7, N'PRCTO_LONGITUD', N'PRO', N'C', 10, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (591, 7, N'PRCTO_CAMPOADC1', N'PRO', N'C', 11, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (592, 7, N'PRCTO_CAMPOADC2', N'PRO', N'C', 12, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (593, 7, N'PRCTO_CAMPOADC3', N'PRO', N'C', 13, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (594, 7, N'PRCTO_CAMPOADC4', N'PRO', N'C', 14, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (595, 7, N'PRCTO_CAMPOADC5', N'PRO', N'C', 15, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (596, 7, N'PRCTO_COD', N'PRE', N'C', 1, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (597, 7, N'PRCTO_NOM', N'PRE', N'C', 2, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (598, 7, N'PRCTO_DIR', N'PRE', N'C', 3, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (599, 7, N'PRCTO_CODVEND', N'PRE', N'C', 4, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (600, 7, N'PRCTO_NOMVEND', N'PRE', N'C', 5, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (601, 7, N'PRCTO_COD_TCLIENTE', N'PRE', N'C', 6, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (602, 7, N'PRCTO_NOM_TCLIENTE', N'PRE', N'C', 7, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (603, 7, N'PRCTO_GIRO', N'PRE', N'C', 8, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (604, 7, N'PRCTO_LATITUD', N'PRE', N'C', 9, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (605, 7, N'PRCTO_LONGITUD', N'PRE', N'C', 10, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (606, 7, N'PRCTO_CAMPOADC1', N'PRE', N'C', 11, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (607, 7, N'PRCTO_CAMPOADC2', N'PRE', N'C', 12, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (608, 7, N'PRCTO_CAMPOADC3', N'PRE', N'C', 13, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (609, 7, N'PRCTO_CAMPOADC4', N'PRE', N'C', 14, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (610, 7, N'PRCTO_CAMPOADC5', N'PRE', N'C', 15, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (611, 7, N'PRCTO_FECHAREGISTRO', N'PRE', N'C', 16, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (612, 7, N'PRCTO_FECHAREGISTRO', N'PRO', N'C', 16, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (613, 7, N'PRCTO_FECHAMOVIL', N'PRO', N'C', 17, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (614, 7, N'PRCTO_OBSERVACION', N'PRO', N'C', 18, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (615, 7, N'PRCTO_FECHAMOVIL', N'PRE', N'C', 17, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (616, 7, N'PRCTO_OBSERVACION', N'PRE', N'C', 18, N'T')
GO
INSERT [dbo].[GRColumnaArchivo] ([IDColArchivo], [IdArchivo], [NombreColumna], [TipoArticulo], [TipoColumna], [Orden], [FlgHabilitado]) VALUES (649, 1, N'DDET_CANTIDAD_PRE', N'PRE', N'D', 9, N'T')
GO
SET IDENTITY_INSERT [dbo].[GRColumnaArchivo] OFF
GO
SET IDENTITY_INSERT [dbo].[CFEtiqueta] ON 

GO
INSERT [dbo].[CFEtiqueta] ([IdEtiqueta], [CodEtiqueta], [Descripcion], [FlgSincronizar], [FchRegistro]) VALUES (1, N'ETI_MEN_MAN', N'Mantenimiento', N'F', CAST(N'2014-07-30 17:25:07.950' AS DateTime))
GO
INSERT [dbo].[CFEtiqueta] ([IdEtiqueta], [CodEtiqueta], [Descripcion], [FlgSincronizar], [FchRegistro]) VALUES (2, N'ETI_MEN_REP', N'Reportes', N'F', CAST(N'2014-07-30 17:25:35.537' AS DateTime))
GO
INSERT [dbo].[CFEtiqueta] ([IdEtiqueta], [CodEtiqueta], [Descripcion], [FlgSincronizar], [FchRegistro]) VALUES (3, N'ETI_MEN_MCL', N'Cargas', N'F', CAST(N'2014-07-30 17:25:53.960' AS DateTime))
GO
INSERT [dbo].[CFEtiqueta] ([IdEtiqueta], [CodEtiqueta], [Descripcion], [FlgSincronizar], [FchRegistro]) VALUES (4, N'ETI_MEN_MRU', N'Mantenimiento de clientes', N'F', CAST(N'2014-07-30 17:27:56.160' AS DateTime))
GO
INSERT [dbo].[CFEtiqueta] ([IdEtiqueta], [CodEtiqueta], [Descripcion], [FlgSincronizar], [FchRegistro]) VALUES (5, N'ETI_MEN_MUS', N'Mantenimiento de rutas', N'F', CAST(N'2014-07-30 17:28:06.500' AS DateTime))
GO
INSERT [dbo].[CFEtiqueta] ([IdEtiqueta], [CodEtiqueta], [Descripcion], [FlgSincronizar], [FchRegistro]) VALUES (7, N'ETI_MEN_MPR', N'Mantenimiento de usuarios', N'F', CAST(N'2014-07-30 17:28:35.040' AS DateTime))
GO
INSERT [dbo].[CFEtiqueta] ([IdEtiqueta], [CodEtiqueta], [Descripcion], [FlgSincronizar], [FchRegistro]) VALUES (8, N'ETI_MEN_MLP', N'Mantenimiento de productos', N'F', CAST(N'2014-07-30 17:28:38.503' AS DateTime))
GO
INSERT [dbo].[CFEtiqueta] ([IdEtiqueta], [CodEtiqueta], [Descripcion], [FlgSincronizar], [FchRegistro]) VALUES (10, N'ETI_MEN_MGE', N'Mantenimiento de lista de precio', N'F', CAST(N'2014-07-30 17:29:49.697' AS DateTime))
GO
INSERT [dbo].[CFEtiqueta] ([IdEtiqueta], [CodEtiqueta], [Descripcion], [FlgSincronizar], [FchRegistro]) VALUES (11, N'ETI_MEN_MCO', N'Mantenimiento general', N'F', CAST(N'2014-07-30 17:29:51.990' AS DateTime))
GO
INSERT [dbo].[CFEtiqueta] ([IdEtiqueta], [CodEtiqueta], [Descripcion], [FlgSincronizar], [FchRegistro]) VALUES (13, N'ETI_MEN_MPE', N'Mantenimiento de cobranza', N'F', CAST(N'2014-07-30 17:30:16.443' AS DateTime))
GO
INSERT [dbo].[CFEtiqueta] ([IdEtiqueta], [CodEtiqueta], [Descripcion], [FlgSincronizar], [FchRegistro]) VALUES (14, N'ETI_MEN_MPE', N'Mantenimiento de pedido', N'F', CAST(N'2014-07-30 17:30:18.677' AS DateTime))
GO
INSERT [dbo].[CFEtiqueta] ([IdEtiqueta], [CodEtiqueta], [Descripcion], [FlgSincronizar], [FchRegistro]) VALUES (16, N'ETI_MEN_MCF', N'Mantenimiento de configuración', N'F', CAST(N'2014-07-30 17:31:31.363' AS DateTime))
GO
INSERT [dbo].[CFEtiqueta] ([IdEtiqueta], [CodEtiqueta], [Descripcion], [FlgSincronizar], [FchRegistro]) VALUES (17, N'ETI_MEN_MIC', N'Mantenimiento de icono', N'F', CAST(N'2014-07-30 17:31:33.917' AS DateTime))
GO
INSERT [dbo].[CFEtiqueta] ([IdEtiqueta], [CodEtiqueta], [Descripcion], [FlgSincronizar], [FchRegistro]) VALUES (19, N'ETI_MEN_RPE', N'Reporte de pedido', N'F', CAST(N'2014-07-30 17:31:56.260' AS DateTime))
GO
INSERT [dbo].[CFEtiqueta] ([IdEtiqueta], [CodEtiqueta], [Descripcion], [FlgSincronizar], [FchRegistro]) VALUES (20, N'ETI_MEN_RNP', N'Reporte de no pedido', N'F', CAST(N'2014-07-30 17:31:58.190' AS DateTime))
GO
INSERT [dbo].[CFEtiqueta] ([IdEtiqueta], [CodEtiqueta], [Descripcion], [FlgSincronizar], [FchRegistro]) VALUES (22, N'ETI_MEN_RDE', N'Reporte de devolucion', N'F', CAST(N'2014-07-30 17:32:22.247' AS DateTime))
GO
INSERT [dbo].[CFEtiqueta] ([IdEtiqueta], [CodEtiqueta], [Descripcion], [FlgSincronizar], [FchRegistro]) VALUES (23, N'ETI_MEN_RCA', N'Reporte de canje', N'F', CAST(N'2014-07-30 17:32:24.197' AS DateTime))
GO
INSERT [dbo].[CFEtiqueta] ([IdEtiqueta], [CodEtiqueta], [Descripcion], [FlgSincronizar], [FchRegistro]) VALUES (24, N'ETI_MEN_RCO', N'Reporte de cobranza', N'F', CAST(N'2014-07-30 17:33:01.097' AS DateTime))
GO
INSERT [dbo].[CFEtiqueta] ([IdEtiqueta], [CodEtiqueta], [Descripcion], [FlgSincronizar], [FchRegistro]) VALUES (25, N'ETI_MEN_RPT', N'Reporte de pedido total', N'F', CAST(N'2014-07-30 17:33:06.560' AS DateTime))
GO
INSERT [dbo].[CFEtiqueta] ([IdEtiqueta], [CodEtiqueta], [Descripcion], [FlgSincronizar], [FchRegistro]) VALUES (27, N'ETI_MEN_RRU', N'Reporte de ruta', N'F', CAST(N'2014-07-30 17:33:37.930' AS DateTime))
GO
INSERT [dbo].[CFEtiqueta] ([IdEtiqueta], [CodEtiqueta], [Descripcion], [FlgSincronizar], [FchRegistro]) VALUES (28, N'ETI_MEN_RGR', N'Reporte de gráficos', N'F', CAST(N'2014-07-30 17:33:42.107' AS DateTime))
GO
INSERT [dbo].[CFEtiqueta] ([IdEtiqueta], [CodEtiqueta], [Descripcion], [FlgSincronizar], [FchRegistro]) VALUES (29, N'ETI_MEN_RSQ', N'Reporte de stock quiebres', N'F', CAST(N'2014-07-30 17:34:14.667' AS DateTime))
GO
INSERT [dbo].[CFEtiqueta] ([IdEtiqueta], [CodEtiqueta], [Descripcion], [FlgSincronizar], [FchRegistro]) VALUES (30, N'ETI_MEN_RRE', N'Reporte de reserva', N'F', CAST(N'2014-07-30 17:34:19.077' AS DateTime))
GO
INSERT [dbo].[CFEtiqueta] ([IdEtiqueta], [CodEtiqueta], [Descripcion], [FlgSincronizar], [FchRegistro]) VALUES (32, N'ETI_MEN_CCA', N'Carga', N'F', CAST(N'2014-07-30 17:34:56.960' AS DateTime))
GO
INSERT [dbo].[CFEtiqueta] ([IdEtiqueta], [CodEtiqueta], [Descripcion], [FlgSincronizar], [FchRegistro]) VALUES (33, N'ETI_MEN_CDE', N'Descarga', N'F', CAST(N'2014-07-30 17:35:03.460' AS DateTime))
GO
INSERT [dbo].[CFEtiqueta] ([IdEtiqueta], [CodEtiqueta], [Descripcion], [FlgSincronizar], [FchRegistro]) VALUES (55, N'ETI_MEN_MAL', N'Mantenimiento de Almacén', N'F', CAST(N'2015-05-11 09:35:13.620' AS DateTime))
GO
INSERT [dbo].[CFEtiqueta] ([IdEtiqueta], [CodEtiqueta], [Descripcion], [FlgSincronizar], [FchRegistro]) VALUES (56, N'ETI_MEN_MPAL', N'Mantenimiento de Producto por Almacén', N'F', CAST(N'2015-05-11 09:35:13.647' AS DateTime))
GO
INSERT [dbo].[CFEtiqueta] ([IdEtiqueta], [CodEtiqueta], [Descripcion], [FlgSincronizar], [FchRegistro]) VALUES (57, N'ETI_MEN_MPP', N'Mantenimiento de Producto Paquete', N'F', CAST(N'2015-05-11 17:04:04.590' AS DateTime))
GO
INSERT [dbo].[CFEtiqueta] ([IdEtiqueta], [CodEtiqueta], [Descripcion], [FlgSincronizar], [FchRegistro]) VALUES (58, N'ETI_MEN_RDC', N'Reporte dinámico consolidado', N'F', CAST(N'2015-05-28 00:51:42.020' AS DateTime))
GO
INSERT [dbo].[CFEtiqueta] ([IdEtiqueta], [CodEtiqueta], [Descripcion], [FlgSincronizar], [FchRegistro]) VALUES (59, N'ETI_MEN_MFO', N'Mantenimiento Formulario', N'F', CAST(N'2015-06-12 17:26:51.277' AS DateTime))
GO
INSERT [dbo].[CFEtiqueta] ([IdEtiqueta], [CodEtiqueta], [Descripcion], [FlgSincronizar], [FchRegistro]) VALUES (60, N'ETI_MEN_BOA', N'Mantenimiento Bonificación', N'F', CAST(N'2015-06-15 11:57:55.160' AS DateTime))
GO
INSERT [dbo].[CFEtiqueta] ([IdEtiqueta], [CodEtiqueta], [Descripcion], [FlgSincronizar], [FchRegistro]) VALUES (62, N'ETI_MANT_PER_GRU', N'Grupos', N'F', CAST(N'2015-07-23 09:44:41.110' AS DateTime))
GO
INSERT [dbo].[CFEtiqueta] ([IdEtiqueta], [CodEtiqueta], [Descripcion], [FlgSincronizar], [FchRegistro]) VALUES (63, N'ETI_MEN_RPR', N'Reporte de Prospectos', N'F', CAST(N'2016-11-30 12:18:25.390' AS DateTime))
GO
INSERT [dbo].[CFEtiqueta] ([IdEtiqueta], [CodEtiqueta], [Descripcion], [FlgSincronizar], [FchRegistro]) VALUES (64, N'ETI_MEN_RTR', N'Reporte de Tracking', N'F', CAST(N'2017-08-22 10:04:59.290' AS DateTime))
GO
INSERT [dbo].[CFEtiqueta] ([IdEtiqueta], [CodEtiqueta], [Descripcion], [FlgSincronizar], [FchRegistro]) VALUES (65, N'ETI_MEN_RD', N'Dashboard', N'F', CAST(N'2017-08-22 10:04:59.290' AS DateTime))
GO
INSERT [dbo].[CFEtiqueta] ([IdEtiqueta], [CodEtiqueta], [Descripcion], [FlgSincronizar], [FchRegistro]) VALUES (66, N'ETI_MEN_RMP', N'Reporte Marca de Productos', N'F', CAST(N'2017-11-15 09:22:24.037' AS DateTime))
GO
INSERT [dbo].[CFEtiqueta] ([IdEtiqueta], [CodEtiqueta], [Descripcion], [FlgSincronizar], [FchRegistro]) VALUES (67, N'ETI_MEN_RFP', N'Reporte Familia de Productos', N'F', CAST(N'2017-08-22 10:04:59.290' AS DateTime))
GO
INSERT [dbo].[CFEtiqueta] ([IdEtiqueta], [CodEtiqueta], [Descripcion], [FlgSincronizar], [FchRegistro]) VALUES (68, N'JAVA_LINKAUTOAYUDA', N'Link Autoayuda', N'T', CAST(N'2019-05-17 16:30:55.087' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[CFEtiqueta] OFF
GO
SET IDENTITY_INSERT [dbo].[CFMenu] ON 

GO
INSERT [dbo].[CFMenu] ([IdMenu], [IdMenuPadre], [IdEtiqueta], [IdFuncion], [Descripcion], [IdEtiquetaDetalle], [Url], [UrlImagen], [Posicion], [FchRegistro], [CodMenu]) VALUES (1, NULL, 1, 1, N'Mantenimiento', NULL, NULL, N'WEB_MANTENIMIENTO', 1, CAST(N'2014-07-30 18:17:44.400' AS DateTime), N'MAN')
GO
INSERT [dbo].[CFMenu] ([IdMenu], [IdMenuPadre], [IdEtiqueta], [IdFuncion], [Descripcion], [IdEtiquetaDetalle], [Url], [UrlImagen], [Posicion], [FchRegistro], [CodMenu]) VALUES (2, NULL, 2, 1, N'Reportes', NULL, NULL, N'WEB_REPORTE', 2, CAST(N'2014-07-30 18:18:20.330' AS DateTime), N'REP')
GO
INSERT [dbo].[CFMenu] ([IdMenu], [IdMenuPadre], [IdEtiqueta], [IdFuncion], [Descripcion], [IdEtiquetaDetalle], [Url], [UrlImagen], [Posicion], [FchRegistro], [CodMenu]) VALUES (3, NULL, 3, 1, N'Cargas', NULL, NULL, N'WEB_MANTENIMIENTO', 3, CAST(N'2014-07-30 18:18:26.527' AS DateTime), N'CAR')
GO
INSERT [dbo].[CFMenu] ([IdMenu], [IdMenuPadre], [IdEtiqueta], [IdFuncion], [Descripcion], [IdEtiquetaDetalle], [Url], [UrlImagen], [Posicion], [FchRegistro], [CodMenu]) VALUES (4, 1, 4, 1, N'Mantenimiento de clientes', NULL, N'Mantenimiento/cliente/cliente.aspx', NULL, 2, CAST(N'2014-07-30 18:19:21.320' AS DateTime), N'MCL')
GO
INSERT [dbo].[CFMenu] ([IdMenu], [IdMenuPadre], [IdEtiqueta], [IdFuncion], [Descripcion], [IdEtiquetaDetalle], [Url], [UrlImagen], [Posicion], [FchRegistro], [CodMenu]) VALUES (5, 1, 5, 1, N'Mantenimiento de rutas', NULL, N'Mantenimiento/ruta/ruta.aspx', NULL, 3, CAST(N'2014-07-30 18:19:23.273' AS DateTime), N'MRU')
GO
INSERT [dbo].[CFMenu] ([IdMenu], [IdMenuPadre], [IdEtiqueta], [IdFuncion], [Descripcion], [IdEtiquetaDetalle], [Url], [UrlImagen], [Posicion], [FchRegistro], [CodMenu]) VALUES (6, 1, 7, 1, N'Mantenimiento de usuarios', NULL, N'Mantenimiento/usuario/usuario.aspx', NULL, 6, CAST(N'2014-07-30 18:19:28.247' AS DateTime), N'MUS')
GO
INSERT [dbo].[CFMenu] ([IdMenu], [IdMenuPadre], [IdEtiqueta], [IdFuncion], [Descripcion], [IdEtiquetaDetalle], [Url], [UrlImagen], [Posicion], [FchRegistro], [CodMenu]) VALUES (7, 1, 8, 1, N'Mantenimiento de productos', NULL, N'Mantenimiento/productos/producto.aspx', NULL, 7, CAST(N'2014-07-30 18:19:32.477' AS DateTime), N'MPR')
GO
INSERT [dbo].[CFMenu] ([IdMenu], [IdMenuPadre], [IdEtiqueta], [IdFuncion], [Descripcion], [IdEtiquetaDetalle], [Url], [UrlImagen], [Posicion], [FchRegistro], [CodMenu]) VALUES (8, 1, 10, 1, N'Mantenimiento de lista de precio', NULL, N'Mantenimiento/listaprecio/listaprecio.aspx', NULL, 11, CAST(N'2014-07-30 18:19:36.987' AS DateTime), N'MLP')
GO
INSERT [dbo].[CFMenu] ([IdMenu], [IdMenuPadre], [IdEtiqueta], [IdFuncion], [Descripcion], [IdEtiquetaDetalle], [Url], [UrlImagen], [Posicion], [FchRegistro], [CodMenu]) VALUES (9, 1, 11, 1, N'Mantenimiento general', NULL, N'Mantenimiento/general/general.aspx', NULL, 12, CAST(N'2014-07-30 18:19:40.877' AS DateTime), N'MGE')
GO
INSERT [dbo].[CFMenu] ([IdMenu], [IdMenuPadre], [IdEtiqueta], [IdFuncion], [Descripcion], [IdEtiquetaDetalle], [Url], [UrlImagen], [Posicion], [FchRegistro], [CodMenu]) VALUES (11, 1, 13, 6, N'Mantenimiento de cobranza', NULL, N'Mantenimiento/cobranza/cobranza.aspx', NULL, 1, CAST(N'2014-07-30 18:21:01.470' AS DateTime), N'MCO')
GO
INSERT [dbo].[CFMenu] ([IdMenu], [IdMenuPadre], [IdEtiqueta], [IdFuncion], [Descripcion], [IdEtiquetaDetalle], [Url], [UrlImagen], [Posicion], [FchRegistro], [CodMenu]) VALUES (12, 1, 14, 1, N'Mantenimiento de pedido', NULL, N'Mantenimiento/pedido/pedido.aspx', NULL, 4, CAST(N'2014-07-30 18:21:13.900' AS DateTime), N'MPE')
GO
INSERT [dbo].[CFMenu] ([IdMenu], [IdMenuPadre], [IdEtiqueta], [IdFuncion], [Descripcion], [IdEtiquetaDetalle], [Url], [UrlImagen], [Posicion], [FchRegistro], [CodMenu]) VALUES (14, 1, 16, 1, N'Mantenimiento de configuración', NULL, N'Mantenimiento/config/config.aspx', NULL, 5, CAST(N'2014-07-30 18:26:03.483' AS DateTime), N'MCF')
GO
INSERT [dbo].[CFMenu] ([IdMenu], [IdMenuPadre], [IdEtiqueta], [IdFuncion], [Descripcion], [IdEtiquetaDetalle], [Url], [UrlImagen], [Posicion], [FchRegistro], [CodMenu]) VALUES (17, 1, 17, 1, N'Mantenimiento de icono', NULL, N'Mantenimiento/icono/icono.aspx', NULL, 10, CAST(N'2014-07-31 09:56:04.950' AS DateTime), N'MIC')
GO
INSERT [dbo].[CFMenu] ([IdMenu], [IdMenuPadre], [IdEtiqueta], [IdFuncion], [Descripcion], [IdEtiquetaDetalle], [Url], [UrlImagen], [Posicion], [FchRegistro], [CodMenu]) VALUES (19, 2, 19, 1, N'Reporte de pedido', NULL, N'Reporte/pedido/pedido.aspx', NULL, 5, CAST(N'2014-08-11 16:03:34.603' AS DateTime), N'RPE')
GO
INSERT [dbo].[CFMenu] ([IdMenu], [IdMenuPadre], [IdEtiqueta], [IdFuncion], [Descripcion], [IdEtiquetaDetalle], [Url], [UrlImagen], [Posicion], [FchRegistro], [CodMenu]) VALUES (21, 2, 20, 1, N'Reporte de no pedido', NULL, N'Reporte/nopedido/nopedido.aspx', NULL, 6, CAST(N'2014-09-01 14:59:15.653' AS DateTime), N'RNP')
GO
INSERT [dbo].[CFMenu] ([IdMenu], [IdMenuPadre], [IdEtiqueta], [IdFuncion], [Descripcion], [IdEtiquetaDetalle], [Url], [UrlImagen], [Posicion], [FchRegistro], [CodMenu]) VALUES (22, 2, 22, 3, N'Reporte de devolucion', NULL, N'Reporte/devolucion/devolucion.aspx', NULL, 7, CAST(N'2014-09-01 14:59:15.653' AS DateTime), N'RDE')
GO
INSERT [dbo].[CFMenu] ([IdMenu], [IdMenuPadre], [IdEtiqueta], [IdFuncion], [Descripcion], [IdEtiquetaDetalle], [Url], [UrlImagen], [Posicion], [FchRegistro], [CodMenu]) VALUES (24, 2, 23, 4, N'Reporte de canje', NULL, N'Reporte/canje/canje.aspx', NULL, 9, CAST(N'2014-09-01 14:59:15.653' AS DateTime), N'RCA')
GO
INSERT [dbo].[CFMenu] ([IdMenu], [IdMenuPadre], [IdEtiqueta], [IdFuncion], [Descripcion], [IdEtiquetaDetalle], [Url], [UrlImagen], [Posicion], [FchRegistro], [CodMenu]) VALUES (26, 2, 24, 6, N'Reporte de cobranza', NULL, N'Reporte/cobranza/cobranza.aspx', NULL, 10, CAST(N'2014-09-01 14:59:15.653' AS DateTime), N'RCO')
GO
INSERT [dbo].[CFMenu] ([IdMenu], [IdMenuPadre], [IdEtiqueta], [IdFuncion], [Descripcion], [IdEtiquetaDetalle], [Url], [UrlImagen], [Posicion], [FchRegistro], [CodMenu]) VALUES (27, 2, 25, 1, N'Reporte de pedido total', NULL, N'Reporte/pedidototal/pedidototal.aspx', NULL, 11, CAST(N'2014-09-01 14:59:15.653' AS DateTime), N'RPT')
GO
INSERT [dbo].[CFMenu] ([IdMenu], [IdMenuPadre], [IdEtiqueta], [IdFuncion], [Descripcion], [IdEtiquetaDetalle], [Url], [UrlImagen], [Posicion], [FchRegistro], [CodMenu]) VALUES (30, 2, 27, 1, N'Reporte de ruta', NULL, N'Reporte/ruta/ruta.aspx', NULL, 11, CAST(N'2014-09-01 14:59:15.653' AS DateTime), N'RRU')
GO
INSERT [dbo].[CFMenu] ([IdMenu], [IdMenuPadre], [IdEtiqueta], [IdFuncion], [Descripcion], [IdEtiquetaDetalle], [Url], [UrlImagen], [Posicion], [FchRegistro], [CodMenu]) VALUES (32, 2, 28, 1, N'Reporte de gráficos', NULL, N'Reporte/graficos/graficos.aspx', NULL, 11, CAST(N'2014-09-01 14:59:15.653' AS DateTime), N'RGR')
GO
INSERT [dbo].[CFMenu] ([IdMenu], [IdMenuPadre], [IdEtiqueta], [IdFuncion], [Descripcion], [IdEtiquetaDetalle], [Url], [UrlImagen], [Posicion], [FchRegistro], [CodMenu]) VALUES (33, 2, 29, 1, N'Reporte de stock quiebres', NULL, N'Reporte/quiebrestock/quiebrestock.aspx', NULL, 11, CAST(N'2014-09-01 14:59:15.653' AS DateTime), N'RSQ')
GO
INSERT [dbo].[CFMenu] ([IdMenu], [IdMenuPadre], [IdEtiqueta], [IdFuncion], [Descripcion], [IdEtiquetaDetalle], [Url], [UrlImagen], [Posicion], [FchRegistro], [CodMenu]) VALUES (34, 2, 30, 1, N'Reporte de reserva', NULL, N'Reporte/reserva/reserva.aspx', NULL, 11, CAST(N'2014-09-01 14:59:15.653' AS DateTime), N'RRE')
GO
INSERT [dbo].[CFMenu] ([IdMenu], [IdMenuPadre], [IdEtiqueta], [IdFuncion], [Descripcion], [IdEtiquetaDetalle], [Url], [UrlImagen], [Posicion], [FchRegistro], [CodMenu]) VALUES (36, 3, 32, 1, N'Carga', NULL, N'Carga/Carga.aspx', NULL, 11, CAST(N'2014-09-01 14:59:15.653' AS DateTime), N'CCA')
GO
INSERT [dbo].[CFMenu] ([IdMenu], [IdMenuPadre], [IdEtiqueta], [IdFuncion], [Descripcion], [IdEtiquetaDetalle], [Url], [UrlImagen], [Posicion], [FchRegistro], [CodMenu]) VALUES (37, 3, 33, 1, N'Descarga', NULL, N'Carga/Descarga.aspx', NULL, 11, CAST(N'2014-09-01 14:59:15.653' AS DateTime), N'CDE')
GO
INSERT [dbo].[CFMenu] ([IdMenu], [IdMenuPadre], [IdEtiqueta], [IdFuncion], [Descripcion], [IdEtiquetaDetalle], [Url], [UrlImagen], [Posicion], [FchRegistro], [CodMenu]) VALUES (38, 1, 55, 1, N'Mantenimiento de Almacén', NULL, N'Mantenimiento/almacen/almacen.aspx', NULL, 8, CAST(N'2015-05-11 09:35:13.623' AS DateTime), N'MAL')
GO
INSERT [dbo].[CFMenu] ([IdMenu], [IdMenuPadre], [IdEtiqueta], [IdFuncion], [Descripcion], [IdEtiquetaDetalle], [Url], [UrlImagen], [Posicion], [FchRegistro], [CodMenu]) VALUES (39, 1, 56, 1, N'Mantenimiento de Producto por Almacén', NULL, N'Mantenimiento/productoalm/productoalm.aspx', NULL, 9, CAST(N'2015-05-11 09:35:13.647' AS DateTime), N'MPA')
GO
INSERT [dbo].[CFMenu] ([IdMenu], [IdMenuPadre], [IdEtiqueta], [IdFuncion], [Descripcion], [IdEtiquetaDetalle], [Url], [UrlImagen], [Posicion], [FchRegistro], [CodMenu]) VALUES (52, 2, 58, 1, N'Reporte dinámico consolidado', NULL, N'Reporte/consolidado/consolidado.aspx', NULL, 13, CAST(N'2015-05-28 00:49:52.497' AS DateTime), N'RDC')
GO
INSERT [dbo].[CFMenu] ([IdMenu], [IdMenuPadre], [IdEtiqueta], [IdFuncion], [Descripcion], [IdEtiquetaDetalle], [Url], [UrlImagen], [Posicion], [FchRegistro], [CodMenu]) VALUES (53, 1, 59, 1, N'Mantenimiento de formulario', NULL, N'Mantenimiento/formulario/formulario.aspx', NULL, 6, CAST(N'2015-06-12 17:27:14.890' AS DateTime), N'MFO')
GO
INSERT [dbo].[CFMenu] ([IdMenu], [IdMenuPadre], [IdEtiqueta], [IdFuncion], [Descripcion], [IdEtiquetaDetalle], [Url], [UrlImagen], [Posicion], [FchRegistro], [CodMenu]) VALUES (55, 1, 60, 1, N'Bonificación Automatica', NULL, N'Mantenimiento/bonificacion/bonificacion.aspx', NULL, 14, CAST(N'2015-06-24 10:44:08.783' AS DateTime), N'MBA')
GO
INSERT [dbo].[CFMenu] ([IdMenu], [IdMenuPadre], [IdEtiqueta], [IdFuncion], [Descripcion], [IdEtiquetaDetalle], [Url], [UrlImagen], [Posicion], [FchRegistro], [CodMenu]) VALUES (58, 1, 62, 1, N'Grupos', NULL, N'Mantenimiento/perfgrupos/perfgrupos.aspx', NULL, 14, CAST(N'2015-07-23 12:05:06.340' AS DateTime), N'MPG')
GO
INSERT [dbo].[CFMenu] ([IdMenu], [IdMenuPadre], [IdEtiqueta], [IdFuncion], [Descripcion], [IdEtiquetaDetalle], [Url], [UrlImagen], [Posicion], [FchRegistro], [CodMenu]) VALUES (59, 2, 63, 10, N'Reporte de prospectos', NULL, N'Reporte/prospeccion/Clientes.aspx', NULL, 15, CAST(N'2016-08-31 19:00:00.000' AS DateTime), N'RPR')
GO
INSERT [dbo].[CFMenu] ([IdMenu], [IdMenuPadre], [IdEtiqueta], [IdFuncion], [Descripcion], [IdEtiquetaDetalle], [Url], [UrlImagen], [Posicion], [FchRegistro], [CodMenu]) VALUES (60, 2, 64, 1, N'Reporte de Tracking', NULL, N'Reporte/tracking/tracking.aspx', NULL, 11, CAST(N'2017-09-22 11:53:58.677' AS DateTime), N'RRT')
GO
INSERT [dbo].[CFMenu] ([IdMenu], [IdMenuPadre], [IdEtiqueta], [IdFuncion], [Descripcion], [IdEtiquetaDetalle], [Url], [UrlImagen], [Posicion], [FchRegistro], [CodMenu]) VALUES (61, 2, 65, 1, N'Dashboard', NULL, N'', NULL, 11, CAST(N'2017-09-22 11:53:58.677' AS DateTime), N'RRD')
GO
INSERT [dbo].[CFMenu] ([IdMenu], [IdMenuPadre], [IdEtiqueta], [IdFuncion], [Descripcion], [IdEtiquetaDetalle], [Url], [UrlImagen], [Posicion], [FchRegistro], [CodMenu]) VALUES (62, 2, 66, 1, N'Familia de Productos', NULL, N'Reporte/FamiliaProducto/familiaproducto.aspx', NULL, 11, CAST(N'2017-09-22 11:53:58.677' AS DateTime), N'RFP')
GO
INSERT [dbo].[CFMenu] ([IdMenu], [IdMenuPadre], [IdEtiqueta], [IdFuncion], [Descripcion], [IdEtiquetaDetalle], [Url], [UrlImagen], [Posicion], [FchRegistro], [CodMenu]) VALUES (63, 2, 67, 1, N'Reporte Marca de Productos', NULL, N'Reporte/MarcaProducto/marcaproducto.aspx', NULL, 12, CAST(N'2017-11-15 09:22:24.037' AS DateTime), N'RMP')
GO
SET IDENTITY_INSERT [dbo].[CFMenu] OFF
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (1, 1, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (1, 2, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (1, 3, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (1, 4, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (1, 5, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (1, 6, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (1, 7, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (1, 8, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (1, 9, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (1, 11, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (1, 12, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (1, 19, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (1, 21, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (1, 22, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (1, 24, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (1, 26, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (1, 27, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (1, 30, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (1, 32, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (1, 33, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (1, 34, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (1, 36, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (1, 37, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (1, 38, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (1, 39, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (1, 52, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (1, 53, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (1, 55, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (1, 58, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (1, 59, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (1, 60, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (1, 61, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (1, 62, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (1, 63, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (2, 1, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (2, 2, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (2, 4, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (2, 5, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (2, 6, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (2, 11, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (2, 12, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (2, 19, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (2, 21, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (2, 22, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (2, 24, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (2, 26, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (2, 27, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (2, 30, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (2, 32, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (2, 33, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (2, 34, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (2, 52, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (2, 59, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (2, 60, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (3, 1, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (3, 2, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (3, 3, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (3, 4, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (3, 5, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (3, 6, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (3, 7, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (3, 8, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (3, 9, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (3, 11, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (3, 12, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (3, 14, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (3, 19, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (3, 21, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (3, 22, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (3, 24, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (3, 26, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (3, 27, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (3, 30, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (3, 32, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (3, 33, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (3, 34, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (3, 36, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (3, 37, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (3, 38, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (3, 39, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (3, 52, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (3, 53, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (3, 55, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (3, 58, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (3, 59, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (3, 60, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (3, 61, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (3, 62, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (3, 63, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (4, 1, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (4, 2, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (4, 3, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (4, 4, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (4, 5, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (4, 6, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (4, 11, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (4, 12, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (4, 19, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (4, 21, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (4, 22, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (4, 24, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (4, 26, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (4, 27, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (4, 30, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (4, 32, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (4, 33, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (4, 34, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (4, 37, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (4, 52, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (4, 58, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (4, 59, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (4, 60, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (4, 61, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (4, 62, N'T', N'T', N'T', N'T', N'T')
GO
INSERT [dbo].[GRPerfilMenu] ([IdPerfil], [IdMenu], [FlgCrear], [FlgVer], [FlgEditar], [FlgEliminar], [FlgExportar]) VALUES (4, 63, N'T', N'T', N'T', N'T', N'T')
GO
SET IDENTITY_INSERT [dbo].[CFPais] ON 

GO
INSERT [dbo].[CFPais] ([IdPais], [CodPais], [Descripcion], [FchRegistro]) VALUES (1, N'PE', N'Peru', CAST(N'2014-07-25 16:48:47.997' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[CFPais] OFF
GO
INSERT [dbo].[CFValorEtiqueta] ([IdPais], [IdEtiqueta], [Descripcion], [FchRegistro]) VALUES (1, 1, N'Mantenimiento', CAST(N'2014-07-30 17:39:45.117' AS DateTime))
GO
INSERT [dbo].[CFValorEtiqueta] ([IdPais], [IdEtiqueta], [Descripcion], [FchRegistro]) VALUES (1, 2, N'Reportes', CAST(N'2014-07-30 17:39:49.913' AS DateTime))
GO
INSERT [dbo].[CFValorEtiqueta] ([IdPais], [IdEtiqueta], [Descripcion], [FchRegistro]) VALUES (1, 3, N'Cargas', CAST(N'2014-07-30 17:39:52.487' AS DateTime))
GO
INSERT [dbo].[CFValorEtiqueta] ([IdPais], [IdEtiqueta], [Descripcion], [FchRegistro]) VALUES (1, 4, N'clientes', CAST(N'2014-07-30 17:39:55.607' AS DateTime))
GO
INSERT [dbo].[CFValorEtiqueta] ([IdPais], [IdEtiqueta], [Descripcion], [FchRegistro]) VALUES (1, 5, N'rutas', CAST(N'2014-07-30 17:39:58.693' AS DateTime))
GO
INSERT [dbo].[CFValorEtiqueta] ([IdPais], [IdEtiqueta], [Descripcion], [FchRegistro]) VALUES (1, 7, N'usuarios', CAST(N'2014-07-30 17:40:08.987' AS DateTime))
GO
INSERT [dbo].[CFValorEtiqueta] ([IdPais], [IdEtiqueta], [Descripcion], [FchRegistro]) VALUES (1, 8, N'productos', CAST(N'2014-07-30 17:40:12.830' AS DateTime))
GO
INSERT [dbo].[CFValorEtiqueta] ([IdPais], [IdEtiqueta], [Descripcion], [FchRegistro]) VALUES (1, 10, N'lista de precio', CAST(N'2014-07-30 17:40:26.043' AS DateTime))
GO
INSERT [dbo].[CFValorEtiqueta] ([IdPais], [IdEtiqueta], [Descripcion], [FchRegistro]) VALUES (1, 11, N'general', CAST(N'2014-07-30 17:40:31.520' AS DateTime))
GO
INSERT [dbo].[CFValorEtiqueta] ([IdPais], [IdEtiqueta], [Descripcion], [FchRegistro]) VALUES (1, 13, N'cobranza', CAST(N'2014-07-30 17:40:44.750' AS DateTime))
GO
INSERT [dbo].[CFValorEtiqueta] ([IdPais], [IdEtiqueta], [Descripcion], [FchRegistro]) VALUES (1, 14, N'pedido', CAST(N'2014-07-30 17:40:48.247' AS DateTime))
GO
INSERT [dbo].[CFValorEtiqueta] ([IdPais], [IdEtiqueta], [Descripcion], [FchRegistro]) VALUES (1, 16, N'configuración', CAST(N'2014-07-30 17:41:01.383' AS DateTime))
GO
INSERT [dbo].[CFValorEtiqueta] ([IdPais], [IdEtiqueta], [Descripcion], [FchRegistro]) VALUES (1, 17, N' de icono', CAST(N'2014-07-30 17:41:04.430' AS DateTime))
GO
INSERT [dbo].[CFValorEtiqueta] ([IdPais], [IdEtiqueta], [Descripcion], [FchRegistro]) VALUES (1, 19, N'pedido', CAST(N'2014-07-30 17:41:15.420' AS DateTime))
GO
INSERT [dbo].[CFValorEtiqueta] ([IdPais], [IdEtiqueta], [Descripcion], [FchRegistro]) VALUES (1, 20, N'no pedido', CAST(N'2014-07-30 17:41:16.760' AS DateTime))
GO
INSERT [dbo].[CFValorEtiqueta] ([IdPais], [IdEtiqueta], [Descripcion], [FchRegistro]) VALUES (1, 22, N'devolucion', CAST(N'2014-07-30 17:41:31.317' AS DateTime))
GO
INSERT [dbo].[CFValorEtiqueta] ([IdPais], [IdEtiqueta], [Descripcion], [FchRegistro]) VALUES (1, 23, N'canje', CAST(N'2014-07-30 17:41:35.340' AS DateTime))
GO
INSERT [dbo].[CFValorEtiqueta] ([IdPais], [IdEtiqueta], [Descripcion], [FchRegistro]) VALUES (1, 24, N'Pagos', CAST(N'2014-07-30 17:41:38.470' AS DateTime))
GO
INSERT [dbo].[CFValorEtiqueta] ([IdPais], [IdEtiqueta], [Descripcion], [FchRegistro]) VALUES (1, 25, N'pedido total', CAST(N'2014-07-30 17:41:41.073' AS DateTime))
GO
INSERT [dbo].[CFValorEtiqueta] ([IdPais], [IdEtiqueta], [Descripcion], [FchRegistro]) VALUES (1, 27, N'ruta', CAST(N'2014-07-30 17:42:06.653' AS DateTime))
GO
INSERT [dbo].[CFValorEtiqueta] ([IdPais], [IdEtiqueta], [Descripcion], [FchRegistro]) VALUES (1, 28, N'gráficos', CAST(N'2014-07-30 17:42:09.913' AS DateTime))
GO
INSERT [dbo].[CFValorEtiqueta] ([IdPais], [IdEtiqueta], [Descripcion], [FchRegistro]) VALUES (1, 29, N'stock quiebres', CAST(N'2014-07-30 17:42:15.560' AS DateTime))
GO
INSERT [dbo].[CFValorEtiqueta] ([IdPais], [IdEtiqueta], [Descripcion], [FchRegistro]) VALUES (1, 30, N'reserva', CAST(N'2014-07-30 17:42:22.883' AS DateTime))
GO
INSERT [dbo].[CFValorEtiqueta] ([IdPais], [IdEtiqueta], [Descripcion], [FchRegistro]) VALUES (1, 32, N'Carga', CAST(N'2014-07-30 17:42:38.913' AS DateTime))
GO
INSERT [dbo].[CFValorEtiqueta] ([IdPais], [IdEtiqueta], [Descripcion], [FchRegistro]) VALUES (1, 33, N'Descarga', CAST(N'2014-07-30 17:42:41.867' AS DateTime))
GO
INSERT [dbo].[CFValorEtiqueta] ([IdPais], [IdEtiqueta], [Descripcion], [FchRegistro]) VALUES (1, 55, N'almacén', CAST(N'2015-05-11 09:35:13.633' AS DateTime))
GO
INSERT [dbo].[CFValorEtiqueta] ([IdPais], [IdEtiqueta], [Descripcion], [FchRegistro]) VALUES (1, 56, N'producto por almacén', CAST(N'2015-05-11 09:35:13.650' AS DateTime))
GO
INSERT [dbo].[CFValorEtiqueta] ([IdPais], [IdEtiqueta], [Descripcion], [FchRegistro]) VALUES (1, 57, N'producto presentación', CAST(N'2015-05-11 17:04:49.327' AS DateTime))
GO
INSERT [dbo].[CFValorEtiqueta] ([IdPais], [IdEtiqueta], [Descripcion], [FchRegistro]) VALUES (1, 58, N'reporte dinámico', CAST(N'2015-05-28 00:53:10.087' AS DateTime))
GO
INSERT [dbo].[CFValorEtiqueta] ([IdPais], [IdEtiqueta], [Descripcion], [FchRegistro]) VALUES (1, 59, N'formulario', CAST(N'2015-06-12 17:27:52.313' AS DateTime))
GO
INSERT [dbo].[CFValorEtiqueta] ([IdPais], [IdEtiqueta], [Descripcion], [FchRegistro]) VALUES (1, 60, N'Bonificación Automática', CAST(N'2015-06-24 10:40:29.843' AS DateTime))
GO
INSERT [dbo].[CFValorEtiqueta] ([IdPais], [IdEtiqueta], [Descripcion], [FchRegistro]) VALUES (1, 62, N'Grupos', CAST(N'2015-07-23 12:04:49.320' AS DateTime))
GO
INSERT [dbo].[CFValorEtiqueta] ([IdPais], [IdEtiqueta], [Descripcion], [FchRegistro]) VALUES (1, 63, N'Prospección de Clientes', CAST(N'2016-11-30 12:44:14.520' AS DateTime))
GO
INSERT [dbo].[CFValorEtiqueta] ([IdPais], [IdEtiqueta], [Descripcion], [FchRegistro]) VALUES (1, 64, N'Tracking', CAST(N'2017-09-22 11:53:58.677' AS DateTime))
GO
INSERT [dbo].[CFValorEtiqueta] ([IdPais], [IdEtiqueta], [Descripcion], [FchRegistro]) VALUES (1, 65, N'Dashboard', CAST(N'2017-09-22 11:53:58.677' AS DateTime))
GO
INSERT [dbo].[CFValorEtiqueta] ([IdPais], [IdEtiqueta], [Descripcion], [FchRegistro]) VALUES (1, 66, N'Familia de Productos', CAST(N'2017-09-22 11:53:58.677' AS DateTime))
GO
INSERT [dbo].[CFValorEtiqueta] ([IdPais], [IdEtiqueta], [Descripcion], [FchRegistro]) VALUES (1, 67, N'Marca de Productos', CAST(N'2017-11-15 09:22:24.037' AS DateTime))
GO
INSERT [dbo].[CFValorEtiqueta] ([IdPais], [IdEtiqueta], [Descripcion], [FchRegistro]) VALUES (1, 68, N'', CAST(N'2019-05-17 16:30:55.090' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[CFFormulario] ON 

GO
INSERT [dbo].[CFFormulario] ([IdFormulario], [Descripcion]) VALUES (1, N'Inicio')
GO
INSERT [dbo].[CFFormulario] ([IdFormulario], [Descripcion]) VALUES (2, N'Fin')
GO
SET IDENTITY_INSERT [dbo].[CFFormulario] OFF
GO
SET IDENTITY_INSERT [dbo].[CFGrupo] ON 

GO
INSERT [dbo].[CFGrupo] ([IdGrupo], [Nombre], [FlgHabilitado]) VALUES (1, N'Tipo documento', N'T')
GO
INSERT [dbo].[CFGrupo] ([IdGrupo], [Nombre], [FlgHabilitado]) VALUES (2, N'GR01', N'T')
GO
SET IDENTITY_INSERT [dbo].[CFGrupo] OFF
GO
SET IDENTITY_INSERT [dbo].[CFGrupoDetalle] ON 

GO
INSERT [dbo].[CFGrupoDetalle] ([IdGrupoDetalle], [IdGrupo], [CodGrupoDetalle], [Nombre], [FlgHabilitado]) VALUES (1, 1, N'1', N'Boleta', N'T')
GO
INSERT [dbo].[CFGrupoDetalle] ([IdGrupoDetalle], [IdGrupo], [CodGrupoDetalle], [Nombre], [FlgHabilitado]) VALUES (2, 1, N'2', N'Factura', N'T')
GO
INSERT [dbo].[CFGrupoDetalle] ([IdGrupoDetalle], [IdGrupo], [CodGrupoDetalle], [Nombre], [FlgHabilitado]) VALUES (3, 2, N'1', N'boleta', N'T')
GO
INSERT [dbo].[CFGrupoDetalle] ([IdGrupoDetalle], [IdGrupo], [CodGrupoDetalle], [Nombre], [FlgHabilitado]) VALUES (4, 2, N'2', N'Factura', N'T')
GO
SET IDENTITY_INSERT [dbo].[CFGrupoDetalle] OFF
GO
SET IDENTITY_INSERT [dbo].[CFSeguridad] ON 

GO
INSERT [dbo].[CFSeguridad] ([IdSeguridad], [Valor], [FchRegistro]) VALUES (1, N'0x0bdbb2c325525e986d55ef50ca66d056', CAST(N'2015-02-10 10:07:31.807' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[CFSeguridad] OFF
GO
SET IDENTITY_INSERT [dbo].[CFTipoControl] ON 

GO
INSERT [dbo].[CFTipoControl] ([IdTipoControl], [Nombre], [FlgHabilitado]) VALUES (1, N'ALFANUMÉRICO', N'T')
GO
INSERT [dbo].[CFTipoControl] ([IdTipoControl], [Nombre], [FlgHabilitado]) VALUES (2, N'CHECKBOX', N'T')
GO
INSERT [dbo].[CFTipoControl] ([IdTipoControl], [Nombre], [FlgHabilitado]) VALUES (3, N'COMBOBOX', N'T')
GO
INSERT [dbo].[CFTipoControl] ([IdTipoControl], [Nombre], [FlgHabilitado]) VALUES (4, N'NUMÉRICO', N'T')
GO
INSERT [dbo].[CFTipoControl] ([IdTipoControl], [Nombre], [FlgHabilitado]) VALUES (5, N'FECHA', N'T')
GO
INSERT [dbo].[CFTipoControl] ([IdTipoControl], [Nombre], [FlgHabilitado]) VALUES (6, N'DECIMAL', N'T')
GO
INSERT [dbo].[CFTipoControl] ([IdTipoControl], [Nombre], [FlgHabilitado]) VALUES (7, N'HORA', N'T')
GO
INSERT [dbo].[CFTipoControl] ([IdTipoControl], [Nombre], [FlgHabilitado]) VALUES (8, N'RADIOBUTTON', N'T')
GO
INSERT [dbo].[CFTipoControl] ([IdTipoControl], [Nombre], [FlgHabilitado]) VALUES (9, N'POSICION', N'T')
GO
SET IDENTITY_INSERT [dbo].[CFTipoControl] OFF
GO
SET IDENTITY_INSERT [dbo].[TBL_CONDVTA] ON 

GO
INSERT [dbo].[TBL_CONDVTA] ([CONDVTA_PK], [CONDVTA_CODIGO], [CONDVTA_NOMBRE], [FLGENABLE]) VALUES (1, N'CT', N'CONTADO', N'F')
GO
INSERT [dbo].[TBL_CONDVTA] ([CONDVTA_PK], [CONDVTA_CODIGO], [CONDVTA_NOMBRE], [FLGENABLE]) VALUES (2, N'002', N'CREDITO', N'F')
GO
INSERT [dbo].[TBL_CONDVTA] ([CONDVTA_PK], [CONDVTA_CODIGO], [CONDVTA_NOMBRE], [FLGENABLE]) VALUES (3, N'02', N'CREDITO 07 DIAS', N'T')
GO
INSERT [dbo].[TBL_CONDVTA] ([CONDVTA_PK], [CONDVTA_CODIGO], [CONDVTA_NOMBRE], [FLGENABLE]) VALUES (4, N'01', N'CONTADO', N'T')
GO
INSERT [dbo].[TBL_CONDVTA] ([CONDVTA_PK], [CONDVTA_CODIGO], [CONDVTA_NOMBRE], [FLGENABLE]) VALUES (5, N'06', N'CREDITO 06 DIAS', N'T')
GO
INSERT [dbo].[TBL_CONDVTA] ([CONDVTA_PK], [CONDVTA_CODIGO], [CONDVTA_NOMBRE], [FLGENABLE]) VALUES (6, N'07', N'CREDITO 07 DIAS', N'T')
GO
INSERT [dbo].[TBL_CONDVTA] ([CONDVTA_PK], [CONDVTA_CODIGO], [CONDVTA_NOMBRE], [FLGENABLE]) VALUES (7, N'10', N'CREDITO 10 DIAS', N'T')
GO
INSERT [dbo].[TBL_CONDVTA] ([CONDVTA_PK], [CONDVTA_CODIGO], [CONDVTA_NOMBRE], [FLGENABLE]) VALUES (8, N'14', N'CREDITO 14 DIAS', N'T')
GO
INSERT [dbo].[TBL_CONDVTA] ([CONDVTA_PK], [CONDVTA_CODIGO], [CONDVTA_NOMBRE], [FLGENABLE]) VALUES (9, N'15', N'CREDITO 15 DIAS', N'T')
GO
INSERT [dbo].[TBL_CONDVTA] ([CONDVTA_PK], [CONDVTA_CODIGO], [CONDVTA_NOMBRE], [FLGENABLE]) VALUES (10, N'20', N'CREDITO 20 DIAS', N'T')
GO
INSERT [dbo].[TBL_CONDVTA] ([CONDVTA_PK], [CONDVTA_CODIGO], [CONDVTA_NOMBRE], [FLGENABLE]) VALUES (11, N'21', N'CREDITO 28 DIAS', N'T')
GO
INSERT [dbo].[TBL_CONDVTA] ([CONDVTA_PK], [CONDVTA_CODIGO], [CONDVTA_NOMBRE], [FLGENABLE]) VALUES (12, N'30', N'CREDITO 30 DIAS', N'T')
GO
INSERT [dbo].[TBL_CONDVTA] ([CONDVTA_PK], [CONDVTA_CODIGO], [CONDVTA_NOMBRE], [FLGENABLE]) VALUES (13, N'45', N'CREDITO 45 DIAS', N'T')
GO
INSERT [dbo].[TBL_CONDVTA] ([CONDVTA_PK], [CONDVTA_CODIGO], [CONDVTA_NOMBRE], [FLGENABLE]) VALUES (14, N'46', N'CREDITO 16 DIAS', N'T')
GO
INSERT [dbo].[TBL_CONDVTA] ([CONDVTA_PK], [CONDVTA_CODIGO], [CONDVTA_NOMBRE], [FLGENABLE]) VALUES (15, N'47', N'CREDITO 8 DIAS', N'T')
GO
INSERT [dbo].[TBL_CONDVTA] ([CONDVTA_PK], [CONDVTA_CODIGO], [CONDVTA_NOMBRE], [FLGENABLE]) VALUES (16, N'60', N'CREDITO 60 DIAS', N'T')
GO
INSERT [dbo].[TBL_CONDVTA] ([CONDVTA_PK], [CONDVTA_CODIGO], [CONDVTA_NOMBRE], [FLGENABLE]) VALUES (17, N'61', N'CREDITO 4 DIAS', N'T')
GO
SET IDENTITY_INSERT [dbo].[TBL_CONDVTA] OFF
GO
SET IDENTITY_INSERT [dbo].[TBL_GRAFICO] ON 

GO
INSERT [dbo].[TBL_GRAFICO] ([CodGrafico], [Grafico]) VALUES (1, N'GR_AVANCE')
GO
INSERT [dbo].[TBL_GRAFICO] ([CodGrafico], [Grafico]) VALUES (2, N'GR_TOP_VENDEDORES')
GO
INSERT [dbo].[TBL_GRAFICO] ([CodGrafico], [Grafico]) VALUES (3, N'GR_TOP_PRODUCTOS')
GO
INSERT [dbo].[TBL_GRAFICO] ([CodGrafico], [Grafico]) VALUES (4, N'GR_TOP_CLIENTES')
GO
INSERT [dbo].[TBL_GRAFICO] ([CodGrafico], [Grafico]) VALUES (5, N'GR_INDICADOR')
GO
INSERT [dbo].[TBL_GRAFICO] ([CodGrafico], [Grafico]) VALUES (6, N'GR_INDICADOR_PRODUCTO')
GO
SET IDENTITY_INSERT [dbo].[TBL_GRAFICO] OFF
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'AR', N'JAVA_CONTENIDO_AYUDA', N'Se debe sincronizar los datos antes de usar la aplicacion. \n\nEl usuario puede realizar: pedido, no pedido, canjes, devoluciones, cobranzas \n(si existe cobranzas pendientes al cliente  seleccionado). \n\nEl usuario tiene otras opciones como son: \n * Envíos: enviar pedidos pendientes al servidor \n * Consolidado: visualizar  acciones realizada por usuario en el día \n * Eficiencia: visualizar indicadores de gestión diaria del usuario \n * Stock: visualizar el stock de producto seleccionado \n \nMientras la aplicación utiliza el canal de datos otros servicios están siendo bloqueados. \n\nIngreso Rápido Productos:Ubicarse columna CODIGO e ingresar código producto,cantidad y descuento,automáticamente se generará precio de acuerdo  a la cantidad y descuento solicitado. \n (OK) funciona para agregar fila \n (#) funciona para eliminar fila \n\nEl Equipo debe esperar un promedio de 90 Seg para capturar primera posición GPS valida \n El Tiempo de espera para la toma de GPS es 30 Seg \n\nEmail: solucionesdenegocio@nextel.com.ar', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'AR', N'JAVA_GIRO', N'Rubro', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'AR', N'JAVA_TITULO', N'i-order', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'AR', N'WEB_BIENVENIDO_P', N'Para acceder al servicio i-order ingrese su login y contraseña.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'AR', N'WEB_GIRO', N'RUBRO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'AR', N'WEB_HOME_ASPX_MENSAJE', N'Consulte nuestras guías de usuario móvil y web para entender el uso del sistema i-order.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'AR', N'WEB_LOGOEMPRESA', N'SOLUCIONES DE NEGOCIO NEXTEL ARGENTINA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'AR', N'WEB_LOGOEMPRESA_PIE_ALT', N'NEXTEL DE ARGENTINA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'AR', N'WEB_PIE_BR', N'Por favor,sírvase leer nuestros Términos y Condiciones en www.nextel.com.ar/legales/tyc.php.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'AR', N'WEB_PIE_P', N'PRODUCTO / i-order Nextel de Argentina Derechos reservados. ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'AR', N'WEB_PIE2_BR', N'Para mayor información o asistencia podrá comunicarse con nuestro centro de atención al cliente llamando desde su equipos Nextel al *611 o bien al 5359-0000 o al 0800-199-3983.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'AR', N'WEB_RUTA_MANUAL_TECNICO', N'estandares/v2_10r/ar/order/ManualTecnico.zip', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'AR', N'WEB_RUTA_MANUAL_USUARIO', N'estandares/v2_1r/ar/order/ManualUsuario.zip', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'AR', N'WEB_TITULO', N'PEDIDOS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'CL', N'WEB_LOGOEMPRESA', N'SOLUCIONES DE NEGOCIO NEXTEL CHILE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'CL', N'WEB_LOGOEMPRESA_PIE_ALT', N'NEXTEL DE CHILE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'CL', N'WEB_PIE_P', N'PRODUCTO / n-order Nextel de Chile Derechos reservados.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_ADDYUPDSTOCK', N'Add/Stock', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_AGREGAR', N'Add', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_ARTICULO', N'Article', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_ARTICULO_EXISTE_CANJE', N'The article already exists in the Exchange', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_ARTICULO_EXISTE_DEVOLUCION', N'The article already exists in the Return', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_ARTICULO_EXISTE_PEDIDO', N'The article already exists in the Order', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_ARTICULO_NO_PRECIO', N'Article Priceless', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_ARTICULO_NO_PRECIO_ASIGNADO', N'The article has no price given', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_ATENCION_BORRAR_CLIENTES', N'WARNING: This will delete all customers. ¿Do you want to continue?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_ATENCION_BORRAR_INFORMACION', N'WARNING: This will delete all information stored on the mobile phone. ¿Do you want to continue?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_ATENCION_BORRAR_PRODUCTOS', N'WARNING: This will delete all products. ¿Do you want to continue?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_ATENCION_ENVIARA_CANJES', N'ATTENTION: Exchanges will be send. Do you want to continue?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_ATENCION_ENVIARA_DEVOLUCIONES', N'ATTENTION: Returns will be send. Do you want to continue?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_ATENCION_ENVIARA_PAGOS', N'ATTENTION: Payments will be send. Do you want to continue?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_ATENCION_ENVIARA_PEDIDOS', N'ATTENTION: Orders/Not Order will be send. Do you want to continue?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_ATENCION_ENVIARA_REGISTRO', N'WARNING: Records are sent back-ordered. Do you want to continue? ', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_ATENCION_ENVIARA_TODOS', N'ATTENTION: Pending info will be send. Do you want to continue?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_ATENCION_NOENVIARA_CANJES', N'WARNING: There are not Exchanges to be sent', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_ATENCION_NOENVIARA_DEVOLUCION', N'WARNING: There are not Return to be sent', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_ATENCION_NOENVIARA_PAGOS', N'WARNING: There are not Payments to be sent', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_ATENCION_NOENVIARA_PEDIDOS', N'WARNING: There are not Order/Not Order to be sent', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_ATENCION_NOENVIARA_TODOS', N'WARNING: There are not data to be sent', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_ATENDIDO', N'Attended', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_ATRAS', N'Back', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_AYUDA', N'Help', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_BANCOS', N'Banks', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_BIENVENIDO', N'Welcome', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_BUSCAR', N'Search', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_BUSCAR_CLIENTES', N'Search Customers', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_BUSCAR_X_NOMBRE', N'Search by Name', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_CANCELAR', N'Cancel', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_CANJE', N'Exchange', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_CANJE_REGISTRADO', N'Registered Exchange', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_CANJES', N'Exchange', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_CANT', N'Amt.', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_CANT_TTL', N'Tot Amt', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_CANTIDAD_INCORRECTA', N'Wrong Quantity', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_CLAVE', N'Password', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_CLIENTE', N'Customer', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_CLIENTE_NOEXISTE', N'Customer does not exist', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_CLIENTES', N'Customers', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_CLIENTES_ONLINE', N'Customers - Online', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_COBRANZA', N'Collection', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_COBRANZAS', N'Collections', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_COBRAR', N'Charge', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_COD_TCLIE', N'Cod.TCust', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_CODIGO', N'Code', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_COMPLETAR_DATOS', N'Complete Data', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_CON_PEDIDO', N'WITH ORDER', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_CONDICION', N'Condition', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_CONFIRMAR_CANCELAR_CANJE', N'Are you sure you want to cancel the exchange?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_CONFIRMAR_CANCELAR_DEVOLUCION', N'Are you sure you want to cancel the return?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_CONFIRMAR_CANCELAR_PEDIDO', N'Are you sure you want to cancel the order?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_CONFIRMAR_FINALIZAR_CANJE', N'Are you sure you want to finalize the exchange?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_CONFIRMAR_FINALIZAR_DEVOLUCION', N'Are you sure you want to finalize the returm?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_CONFIRMAR_FINALIZAR_NOPEDIDO', N'ATTENTION: Are you sure you want to finalize the Not Order?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_CONFIRMAR_FINALIZAR_NOPEDIDO2', N'Are you sure you want to finalize the Not Order?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_CONFIRMAR_FINALIZAR_PEDIDO', N'Are you sure you want to finalize the order?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_CONSOLIDADO', N'Consolidated', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_CONTENIDO_AYUDA', N'Data must be synchronized before use application.\n\nThe user can perform: order, not order, exchanges, returns, collection\n(if there are outstanding collection for the selected customer).\n\nThe user has other options such as: *Shipments:  to send pending actions to the server.\n *Consolidated: to visualize user actions on the day\n *Efficiency: to visualize indicators for daily managment of the user. \n *Stock: to visualize the stock of the selected product.\n\nWhile the application are using the data channel,others services are blocked.\n\nFast Income Products:Placed in the CODE column and enter product code,quantity discount,automatically the price will be generated according to the quantity and discount applied. \n (OK) it works to add row\n (#) it works to delete row.\n\nEquipment must wait an average of 90 Sec to capture the first right position.\nThe waiting time for making GPS is 30 Sec. \n \nEmail: Data.soporte@nextel.com.pe', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_DEBE_SINCRONIZAR', N'First you must synchronize', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_DES', N'Dis', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_DESC_INCORRECTA', N'WRONG DISCOUNT MUST BE BETWEEN 0 to 100', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_DESCRIPCION', N'DESC.', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_DESCUENTO_PORCENTAJE', N'DISCOUNT UP TO 100', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_DETALLE_COBRANZA', N'Collection Detail', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_DETALLE_PEDIDO', N'Order Detail', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_DEUDA', N'Debt', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_DEVOLUCION', N'Return', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_DEVOLUCION_REGISTRADA', N'Return Registered', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_DEVOLUCIONES', N'Return', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_DIRECCION', N'Address', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_DOC', N'DOC', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_DOC_COBR', N'Doc. Cobr.,', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_DOCUMENTO', N'Document', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_DOCUMENTO_TITULO', N'Document', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_EFEC', N'Efec', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_EFICIENCIA', N'Efficiency', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_ELIMINAR_FILA', N'it works to delete row', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_EMPRESA', N'Enterprise', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_ENVIO_REGISTRADO', N'Send registered', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_ENVIOS', N'Sendings', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_ESTADO', N'State', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_FEC_VCMTO', N'Exp. Date(DD/MM/YYYY)', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_FECHA', N'Date', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_FECHA_INCORRECTA', N'Incorrect Date', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_FINALIZAR', N'Finalize', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_FINALIZAR_COBRANZA', N'Are you sure you want to record?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_FUERARUTA', N'Out of route', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_GENERAL', N'General', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_GIRO', N'Line', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_INDICADOR', N'Indicator', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_INGRESAR_MAYOR_CERO', N'You must enter a number greater than zero', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_INGRESAR_MONTO_MAYOR_CERO', N'You must enter Amount greater than zero', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_INGRESE_CAMPOS', N'Please Enter all fields', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_INGRESE_CANTIDAD', N'Enter Quantity', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_INGRESE_CODIGO', N'You must enter the code', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_INGRESE_DATOS', N'Input data', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_INGRESE_FECHA', N'Enter date', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_INGRESE_MONTO', N'Enter Amount', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_INGRESO', N'Sign in', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_INICIAR', N'Start', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_ITEMS_CANJE', N'Items Exchange', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_ITEMS_DEVOL', N'Items Return', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_ITEMS_PEDIDO', N'Items Order', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_LOGIN', N'LOGIN', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_MANTENIMIENTO', N'Maintenance', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_MENSAJE', N'Message', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_MENU', N'Menu', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_MENU_PRIN', N'Main Menu', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_MNT_COBR', N'Amt Charg', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_MNT_TTL', N'Tot Amt', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_MONTO_MAYOR_SALDO', N'Amount greater than the Balance', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_MONTO_PEDIDO', N'Order Amount', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_MOTIVO', N'Reason', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_MOTIVO_CANJE', N'Exchange Reason', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_MOTIVO_DEVOLUCION', N'Return Reason', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_MOVIL', N'Mobile', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_MXPAG', N'MXPAID', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_NO', N'Not', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_NO_COBRANZAS', N'There is no collection', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_NO_CONEXION_SERVIDOR', N'No server connection but offline login successful,you can work offline', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_NO_ENCONTRARON_RESULTADOS', N'No results were found', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_NO_PEDIDO', N'Not Ord.', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_NO_PEDIDO2', N'Not Order', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_NO_PRECIO', N'Priceless', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_NOATENDIO_CANJE', N'Could not attend the Exchange', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_NOATENDIO_DEVOLUCION', N'Could not attend the Return', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_NOATENDIO_PEDIDO', N'Could not attend the Order', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_NOCOBERTURA_ENVIO', N'Not coverage Area,process the sending through the (main - menu) option Sendings', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_NOCOBERTURA_SINCRONIZAR', N'Area without coverage,can not synchronize', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_NOEXISTE', N'Does not exist', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_NOEXISTE_TABELA', N'Type does not exists', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_NOMBRE', N'Name', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_NOMBRECLIENTE', N'', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_NOPEDIDO_REGISTRADO', N'Not Order Registered', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_NUMERO', N'Number', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_OBS', N'Obs.', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_PAGAD', N'PAID', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_PAGO', N'Payment', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_PAGO_COBR', N'Cobr', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_PAGO_REGISTRADO', N'Registered payment', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_PAGOS', N'Payments', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_PED', N'Ord', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_PEDI_NOPEDI', N'Order/Not Order', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_PEDIDO', N'Order', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_PEDIDO_NO_DETALLE', N'The order has no detail', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_PEDIDO_REGISTRADO', N'Order registered', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_PEDIDOS', N'Orders', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_PEDIDOS_DIA', N'Orders Day', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_POR_VISITAR', N'TO VISIT', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_PRECIO', N'Price', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_PROD_CANJE', N'Prod. Exchange', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_PROD_DEVOLUCION', N'Prod. Return', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_PROD_PEDIDO', N'Prod. Order', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_PROD_STOCK', N'Prod. Stock', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_PRODUCTO', N'Product', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_PRODUCTO_EXISTE', N'The product entered already exists in the current order', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_PRODUCTO_EXISTE_CANJE', N'The product entered already exists in the current exchange', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_PRODUCTO_MOTIVO_EXISTEN', N'The product and reason already exist', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_PRODUCTOS', N'Products', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_REGISTRADO', N'Registered', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_SALDO', N'SALDO', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_SALIR', N'Exit', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_SEGUIR', N'Next', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_SEGURO_GRABAR', N'Are you sure you want to record?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_SELECCION', N'Select', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_SELECCION_MOTIVO_CANJE', N'You must select the reason for exchange', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_SELECCION_MOTIVO_DEVOLUCION', N'You must select the reason for return', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_SI', N'Yes', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_SIN_COBERTURA', N'Area without coverage,try to synchronize later', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_SIN_COBERTURA_CONSLTA', N'Area without coverage,try consulting later', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_SINCRONIZAR', N'Synchronize', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_SINCRONIZAR_CLIENTES', N'Customers must be synchronized', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_STATUS_CONNECTED', N'Connected', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_STATUS_CONNECTING', N'Connecting', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_STATUS_ERROR', N'Error', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_STATUS_PROCESSING', N'Processing', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_STATUS_RECEIVING', N'Receiving', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_STATUS_SAVING', N'Saving', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_STATUS_SENDING', N'Sending', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_STATUS_WAIT', N'WAIT', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_STOCK', N'Stock', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_STOCK_ABREV', N'S', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_STOCK_LINEA', N'Stock Online', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_TAB', N'Type', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_TITULO', N'n-order', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_TODO', N'All', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_TOT', N'Tot', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_TOTAL', N'Total', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_TOTAL_CLIENTES', N'Total Customers', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_TPV', N'AVT(s)', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_USUARIO', N'User', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_USUARIO_PASS_INVALIDO', N'Invalid User or password', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_USUARIO_PASS_INVALIDO_CODIGO', N'Invalid User or password without code', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_VALOR', N'Value', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_VERSION', N'VERSION', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_VISITA_NO_PEDIDO', N'Visit Not. Ord.', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_VISITA_PEDIDO', N'Visit Ord.', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'JAVA_VISITADO', N'VISITED', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ABREVIATURA', N'ABBREVIATION', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ABRIRBARRA', N'Open bar', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ABRIRBUSCADOR', N'To Open Searcher', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ACTUALIZADOS', N'#Updates', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ADMINISTRADOR', N'Administrator', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_AGREGAR', N'Add', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_AGREGAR_ARCHIVO_MIN', N'You must add at least one file', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ARCHIVOS', N'FILE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ARCHIVOS_EJECUTADO', N'Files executed. Some files with errors', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ARCHIVOS_EJECUTADO_CORRECT', N'Files executed successfully', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_AUMENTO_ARCHIVO', N'A file was added to the list', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_AYUDA', N'Help', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_BANCO', N'Bank', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_BATERIA', N'battery ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_BIENVENIDO_H2', N'Welcome - Start Session', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_BIENVENIDO_P', N'To access the service n-order enter your login and password.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_BONIFICACIONMANUAL', N'Bonus Manual', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_BONIFICACIONREP', N'Bonus', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_BUSCADOR', N'SEARCHER', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_BUSCAR', N'SEARCH', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_BUSCAR_LUPA', N'', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_BUSQUEDAGEORUTA', N'GEOROUTE SEARCH', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CAMPOREQUERIDO', N'Required field', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CANCELAR', N'CANCEL', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CANJE_DETCANJE', N'Exchange |exchange detail', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CANJESULT5D', N'Eschanges in the last 5 days', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CANTIDAD', N'QUANTITY', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CARGA_DETCARGA', N'Load |download detail', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CARGA_EXITO', N' file(s) was(were)  loaded successfully: ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CARGADETALLE', N'DETAIL LOAD', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CARGAR', N'Upload', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CARGAR_ARCHIVOS', N'Upload Files', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CERRAR', N'Close', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CLAVE', N'Password', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CLAVE_ALERTA', N'Enter password', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CLAVE_NUMERICA', N'Mobile users using numerical password', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CLI_DESHAB', N'Disable customers', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CLI_PEDIDOS', N'Customers with more Orders', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CLI_PRO_MAS_VENDIDOS', N'Cust. with more products sold', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CLIENTE', N'Customer', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CLIENTES_MAS_CANJES', N'Customers with more Exchange', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CLIENTES_MAS_DEVOLUCIONES', N'Customers with more Devolution', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CLIENTES_MAS_VENTAS', N'Customers with more Sales', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CLIENTESCONPEDIDO', N'Customer with Orders', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CLIENTESMASCANJES30D', N'Customers who register more Exchanges(30 days)', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CLIENTESMASDEV30D', N'Customers who register more Returns(30 days)', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CLIENTESMASNOPED30D', N'Customers who register more Not Orders(30 days)', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CLIENTESMASPAGOS30D', N'Customers who register more Payments (30 days)', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CLIENTESMASVIS30D', N'Customers more visited in 30 days', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_COBRANZA_DETCOBRANZA', N'Collection |collection detail', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_COBRANZAS_TOTALES', N'Total Collections', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_COBRANZASHOY', N'Today´s Collections', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CODIGO', N'Code', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CODIGO_ALMACEN', N'STORE CODE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CODIGO_CANAL', N'CHANNEL CODE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CODIGO_CLIENTE', N'CUSTOMER CODE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CODIGO_FAMILIA', N'CODE FAMILY', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CODIGO_GRUPO', N'GROUP_CODE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CODIGO_MARCA', N'CODE MARK', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CODIGO_PEDIDO_EDITADO', N'Code Edition', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CODIGO_PRODUCTO', N'PRODUCT CODE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CODIGO_VENDEDOR', N'SELLER CODE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CODIGOPRODUCTO', N'Product Code', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CONBONIFICACION', N'With bonus', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_COND_VTA', N'COND. OF SALE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CONDICION', N'CONDITION', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CONDVENTAUPPERCASE', N'SALES CONDITION', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CONDVTA', N'SALES CONDITION', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CONF_ICONO_GUARDADA', N'Configuración de icono guardada exitosamente', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CONFIG_CABECERA_DESCARGA', N'Download incluces header.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CONFIG_CANTIDAD_DECIMALES_CARGA', N'Decimal places download', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CONFIG_CANTIDAD_DECIMALES_VISTA', N'Decimal places vista', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CONFIG_COMA', N'Semicolon', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CONFIG_CONDVTA', N'Condition of Sale', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CONFIG_DECIMAL', N'Decimal Setting', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CONFIG_DESCUENTO', N'Discount ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CONFIG_DESCUENTO_GENERAL', N'Discount General', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CONFIG_DESCUENTO_PRODUCTO', N'Discount Product', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CONFIG_EFICIENCIA', N'Efficiency', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CONFIG_GENERAL', N'General setting', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CONFIG_GPSRUTA', N'GPS - Route', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CONFIG_IDIOMA', N'Language', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CONFIG_MANT_DESCUENTO', N'Discount Settings', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CONFIG_MONEDA', N'Money', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CONFIG_MONEDAIDIOMA', N'Money and Language Setting', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CONFIG_MONTO', N'Amount', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CONFIG_MOVIL', N'Mobile setting', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CONFIG_PORCENTAJE', N'Percentage', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CONFIG_PRECIO', N'Price Setting', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CONFIG_PUNTO', N'Point', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CONFIG_RANGO_DESC', N'Discount Range (%)', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CONFIG_RANGO_MAX', N'Max', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CONFIG_RANGO_MIN', N'Min', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CONFIG_SIMBOLO', N'Decimal symbol setting', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CONFIG_TIPOCLI', N'Type of Client', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CONFIGURACIONGUARDADO', N'Setting saved correctly', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_CONFIGURAR', N'Set', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DATOS_INVALIDOS', N'Invalid data in', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DEBE_TENER', N' AND MUST HAVE ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESC', N'DISCCOUNT', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_CANJE', N'EXCHANGE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_CANJE_COL01', N'COD_EXCHANGE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_CANJE_COL02', N'COD_COSTUMER', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_CANJE_COL03', N'COD_SELLER', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_CANJE_COL04', N'COD_COMPANY', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_CANJE_COL05', N'DAT_REGISTER', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_DETALLE_CANJE', N'EXCHANGE_DETAIL', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_DETALLE_CANJE_COL01', N'COD_EXCHANGE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_DETALLE_CANJE_COL02', N'COD_COSTUMER', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_DETALLE_CANJE_COL03', N'COD_COMPANY', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_DETALLE_CANJE_COL04', N'COD_SELLER', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_DETALLE_CANJE_COL05', N'COD_EXCHANGEDET', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_DETALLE_CANJE_COL06', N'COD_PRODUCT', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_DETALLE_CANJE_COL07', N'QUANTITY', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_DETALLE_CANJE_COL08', N'COD_REASON', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_DETALLE_CANJE_COL09', N'OBSERVATION', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_DETALLE_CANJE_COL10', N'DAT_EXPIRATION', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_DETALLE_DEVOLUCION', N'RETURN_DETAIL', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_DETALLE_DEVOLUCION_COL01', N'COD_RETURN', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_DETALLE_DEVOLUCION_COL02', N'COD_COSTUMER', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_DETALLE_DEVOLUCION_COL03', N'COD_SELLER', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_DETALLE_DEVOLUCION_COL04', N'COD_COMPANY', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_DETALLE_DEVOLUCION_COL05', N'COD_DETRETURN', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_DETALLE_DEVOLUCION_COL06', N'COD_PRODUCT', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_DETALLE_DEVOLUCION_COL07', N'QUANTITY', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_DETALLE_DEVOLUCION_COL08', N'COD_REASON', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_DETALLE_DEVOLUCION_COL09', N'OBSERVATION', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_DETALLE_DEVOLUCION_COL10', N'FEC_EXPIRATION', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_DETALLE_PEDIDO', N'ORDER_DETAIL', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_DETALLE_PEDIDO_COL01', N'COD_ORDER', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_DETALLE_PEDIDO_COL02', N'COD_SELLER', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_DETALLE_PEDIDO_COL03', N'COD_COSTUMER', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_DETALLE_PEDIDO_COL04', N'COD_COMPANY', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_DETALLE_PEDIDO_COL05', N'COD_ORDERDET', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_DETALLE_PEDIDO_COL06', N'COD_PRODUCT', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_DETALLE_PEDIDO_COL07', N'QUANTITY', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_DETALLE_PEDIDO_COL08', N'PRICE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_DETALLE_PEDIDO_COL09', N'DISCCOUNT', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_DETALLE_PEDIDO_COL10', N'TOTAL', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_DETALLE_PEDIDO_COL11', N'DESCRIPCTION', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_DETDESCARGA', N'Download |download detail', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_DEVOLUCION', N'RETURN', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_DEVOLUCION_COL01', N'COD_RETURN', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_DEVOLUCION_COL02', N'COD_COSTUMER', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_DEVOLUCION_COL03', N'COD_SELLER', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_DEVOLUCION_COL04', N'COD_COMPANY', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_DEVOLUCION_COL05', N'DAT_REGISTER', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_NOVENTA', N'NOSALES', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_NOVENTA_COL01', N'COD_NOSALE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_NOVENTA_COL02', N'COD_COSTUMER', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_NOVENTA_COL03', N'COD_SELLER', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_NOVENTA_COL04', N'COD_COMPANY', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_NOVENTA_COL05', N'COD_REASON', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_NOVENTA_COL06', N'DAT_REGISTRO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_PAGO', N'PAYMENT', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_PAGO_COL01', N'COD_PAYMENT', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_PAGO_COL02', N'COD_SELLER', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_PAGO_COL03', N'AMOUNT', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_PAGO_COL04', N'TYPE_PAYMENT', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_PAGO_COL05', N'COD_BANK', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_PAGO_COL06', N'VOUCHER', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_PAGO_COL07', N'COD_COMPANY', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_PAGO_COL08', N'DAT_REGISTRO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_PEDIDO', N'ORDER', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_PEDIDO_COL01', N'COD_ORDER', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_PEDIDO_COL02', N'COD_SELLER', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_PEDIDO_COL03', N'COD_COSTUMER', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_PEDIDO_COL04', N'COD_COMPANY', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_PEDIDO_COL05', N'COD_CONDSALE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_PEDIDO_COL06', N'TOTAL', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_PEDIDO_COL07', N'DAT_REGISTER', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_PEDIDO_COL08', N'OBSERVATION', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_PEDIDO_COL09', N'LATITUDE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGA_PEDIDO_COL10', N'LONGITUDE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGADETALLE', N'DETAIL DOWNLOAD', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGAEXITOSA', N'Download successful', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGAR', N'Download', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCARGAR_INFORMACION', N'Information Download', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCPORCENTAJE', N'DISCCOUNT %', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCRIPCION', N'DESCRIPCTION', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCRIPCION_PRODUCTO', N'PRODUCT DESCRIPCION', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCUENTOMAX', N'Discount Maximum', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESCUENTOMIN', N'Discount Minimum', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DESDE', N'From', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DETALLE', N'DETAIL', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DETALLE_COBRANZA', N'collection detail', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DETALLE_ERROR', N'Details of error:', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DETALLE_GENERAL', N'general detail', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DETALLE_PEDIDOS_EDITADOS', N'Detail Editions', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DETALLE_PRODUCTOS', N'Detail Products', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DETALLECLIENTE', N'customer detail', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DETALLECOBRANZA', N'Has not run any files', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DETALLECONFIGURACION', N'setting detail', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DETALLEICONO', N'icon detail', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DETALLELISTAPRECIO', N'price list detail', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DETALLEPEDIDO', N'Pedido detail', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DETALLEPRODUCTO', N'product detail', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DETALLERUTA', N'route detail', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DETALLEUSUARIO', N'user detail', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DEVOLUCI_DETDEVOLUCI', N'Return |return detail', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DEVOLUCIONESULT5D', N'Returns in the last 5 days', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DIAS', N'Days', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DIASEMANA_DO', N'Su', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DIASEMANA_JU', N'Th', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DIASEMANA_LU', N'Mo', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DIASEMANA_MA', N'Tu', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DIASEMANA_MI', N'We', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DIASEMANA_SA', N'Sa', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DIASEMANA_VI', N'Fr', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DIRECCION', N'Address', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DISTANCIA', N'DISTANCE (m)', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_DISTRIBUIDORA', N'Distribuitor', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_EDITAR', N'Edit', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_EDITARACTIVIDAD', N'Edit Activity', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_EDITARALMACEN', N'Edit storage', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_EDITARCLIENTE', N'Edit customer', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_EDITARCOBRANZA', N'Edit collection', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_EDITARDETALLE', N'Edit Detail', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_EDITARGENERAL', N'Edit Register', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_EDITARLISTAPRECIO', N'Edit price list', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_EDITARPEDIDO', N'Edit Order', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_EDITARPRECIO', N'Edit Price', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_EDITARPRODUCTO', N'Edit product', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_EDITARRUTA', N'Edit route', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_EDITARUSUARIO', N'Edit user', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_EJECUCION_EXITOSA', N'Successful execution', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_EJECUCIONEXITO', N'Execution was successful.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_EJECUTAR', N'Run', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_EJECUTAR_ARCHIVOS', N'Run Files', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_EJEMPLO_XLS', N'Excel Example', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ELIJAVALOR', N'Choose a value.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ELIMINAR', N'Delete', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ELIMINAR_REGISTRO', N'Delete Record(s)', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ELIMINAR_TODO', N'Delete All', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ELIMINAR_TODO_DATOS', N'Delete all Data', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_EMPRESA', N'Enterprise', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ENCOBERTURA', N'In Coverage', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_EQUIPO', N'Equipment', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ERR', N'ERROR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ERR_DATA', N'ERR_DATA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ERR_DUPLICADO', N'ERR_DUPLICATED', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ERR_INVALIDO', N'ERR_INVALID', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ERR_NO_DATA', N'ERR_FILE_WITHOUT_DATA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ERR_NUMERIC', N'ERR_NUMERIC', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ERROR', N'The data entered were incorrect.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ERROR_ARCHIVO', N'ERR_FILE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ERROR_CARACTERES_ESPECIALES', N'ERROR: SPECIAL_CHARACTERS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ERROR_CARGA', N'Loading Error', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ERROR_CARGANDO_ARCHIVO', N'Error loading the file ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ERROR_DESCARGA_DATOS', N'Data input error: Check the date and time', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ERROR_EJECUTAR_DTS', N'Error running DTS: <BR> Global Variable ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ERROR_EN_ARCHIVO', N'Error on file', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ERROR_EN_DATOS', N'Data error', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ERROR_ENCRIPTACION', N'Encryption error', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ERROR_ESTRUCTURA', N'ERROR_STRUCTURE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ERROR_EXTENSION_ICONO', N'Solo archivos con extension jpg, jpeg, gif, png o bmp son permitidos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ERROR_INSERTANDO', N'Error inserting', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ERROR_NOSELECCIONADO', N'Error: You must be at least a selected file ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ERROR_NUM_COLUMNAS', N'ERR_COLUMNS_NUMBER:', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ERROR_SUBIENDO_ARCHI', N'Error uploading file', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ERROR_TAMANIO_ICONO', N'Tamaño máximo del archivo debe ser 400KB', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ERROR_VACIO', N'ERR_EMPTY', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ERRORDTS_VARIABLESGLOBALES', N'Error running DTS: The number of global variables must match the number of input values ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ERRORGRABAR', N'ERROR: SAVING DATA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ESTADO', N'STATE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_EUSUARIO_RESERVADO', N'RESERVED USER', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_FAMILIA', N'Family', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_FAMILIA_PRODUCTO', N'New Family product', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_FECHA', N'Date', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_FECHA_REGISTRO', N'RECORD DATE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_FECHA_VENCIMIENTO', N'EXPIRATION DATE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_FECHA_VENCIMIENTO_CANJE', N'DATE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_FECHAEDICION', N'Date edition', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_FECHAFIN', N'End Date', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_FECHAINICIO', N'Start Date', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_FECHAREGISTRO', N'Date register', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_FECHAVISITA', N'VISIT DATE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_FILA', N'ROW', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_FILENAME_ALMACEN', N'STORE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_FILENAME_ALMACENPRODUCTO', N'STOREPRODUCT', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_FILENAME_CLIENTE', N'CLIENTE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_FILENAME_COBRANZA', N'COBRANZA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_FILENAME_FAMILIA', N'FAMILY', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_FILENAME_FAMILIA_PRODUCTO', N'FAMILIA_PRODUCTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_FILENAME_GENERAL', N'GENERAL', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_FILENAME_LISTAPRECIOS', N'LISTAPRECIOS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_FILENAME_MARCA', N'MARK', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_FILENAME_MARCA_PRODUCTO', N'MARCA_PRODUCTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_FILENAME_PRODUCTO', N'PRODUCTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_FILENAME_RUTA', N'RUTA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_FILENAME_VENDEDOR', N'VENDEDOR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_FIN', N'End', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_FLAG_GENERADOR', N'0', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_FLAG_MOSTRARLOGOTOP', N'0', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_FORMATO', N'ERR_FORMAT', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_FORMATO_INVAL_ARCHIV', N'Invalid File Format', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_FUERACOBERTURA', N'Coverage outside', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GEN_ARCHIVOS', N'File generator', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GENERARREPORTE', N'Generate Report', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GEO_GRAFNPANT', N'GRAPH ALREADY EXISTS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GEOANALISIS_DETGEOANALISIS', N'Geo Analysis |Geo Analysis report', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GEOCERCA_NO_ENCONTRO_DIR', N'Address not Found', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GEORUTA', N'GEOROUTE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GIRO', N'LINE OF BUSINESS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GPS', N'GPS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GRABAR', N'SAVE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GRAFICO', N'GRAPHIC', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GRAFICO_BARRAS', N'BAR GRAPHS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GRAFICO_DONA', N'GRAPH DONUT', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GRAFICO_LINEA', N'LINE GRAPHS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GRAFICO_PIE', N'PIE CHARTS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GRAFICOCANJE', N'Exchange graphic', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GRAFICOCOBRANZA', N'Collection graphic', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GRAFICODEVOLUCION', N'Return graphic', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GRAFICONOPEDIDO', N'Not Order Graphic', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GRAFICOPEDIDO', N'Order Graphic', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GRAFICOS_DETGRAFICOS', N'Graphics|Graphics', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GRCLIENTE_CANJE_VS_CANTIDAD', N'CANJE VS CANTIDAD', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GRCLIENTE_COBRANZA_VS_MONTO', N'COBRANZA VS MONTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GRCLIENTE_DEVOLUCION_VS_CANTIDAD', N'DEVOLUCIÓN VS CANTIDAD', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GRCLIENTE_PEDIDO_VS_CANTIDAD', N'PEDIDO VS CANTIDAD', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GRCLIENTE_PEDIDO_VS_MONTO', N'PEDIDO VS MONTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GRMAPA_CLIENTE_VS_CANTIDAD', N'CLIENTE VS CANTIDAD', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GRMAPA_CLIENTE_VS_MONTO', N'CLIENTE VS MONTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GRMAPA_PRODUCTO_VS_CANTIDAD', N'PRODUCTO VS CANTIDAD', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GRMAPA_PRODUCTO_VS_MONTO', N'PRODUCTO VS MONTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GRP_AVANCE', N'ADVANCE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GRP_FILTROOPERACION', N'Operation', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GRP_FILTROTIPO', N'Type', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GRP_INDICADOR', N'INDICATOR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GRP_INDICADORPRODUCTO', N'PRODUCT INDICATOR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GRP_OPERACIONCANTIDAD', N'Quantity', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GRP_OPERACIONMONTO', N'Amount', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GRP_TOP_CLIENTES', N'TOP CLIENTS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GRP_TOP_PRODUCTOS', N'TOP PRODUCTS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GRP_TOP_VENDEDORES', N'TOP VENDEDORES ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GRPRODUCTO_CANJE_VS_CANTIDAD', N'CANJE VS CANTIDAD', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GRPRODUCTO_DEVOLUCION_VS_CANTIDAD', N'DEVOLUCIÓN VS CANTIDAD', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GRPRODUCTO_PEDIDO_VS_CANTIDAD', N'PEDIDO VS CANTIDAD', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GRPRODUCTO_PEDIDO_VS_MONTO', N'PEDIDO VS MONTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GRVENDEDOR_CANJE_VS_CANTIDAD', N'CANJE VS CANTIDAD', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GRVENDEDOR_COBRANZA_VS_MONTO', N'COBRANZA VS MONTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GRVENDEDOR_DEVOLUCION_VS_CANTIDAD', N'DEVOLUCIÓN VS CANTIDAD', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GRVENDEDOR_PEDIDO_VS_CANTIDAD', N'PEDIDO VS CANTIDAD', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_GRVENDEDOR_PEDIDO_VS_MONTO', N'PEDIDO VS CANTIDAD', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_HABILITADO', N'Enabled', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_HASTA', N'To', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_HISTORIAL_EDICION_PEDIDOS', N'edited orders', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_HOME_ASPX_BIENVENIDO', N'Welcome', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_HOME_ASPX_IR', N'Go to:', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_HOME_ASPX_MENSAJE', N'Read our user guides to understand n-order system.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ICONO', N'ICONO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ICONO_ACTUALIZADO_EXITOSAMENTE', N'Icono actualizado exitosamente', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_INDICADOR_CIRCULAR', N'CIRCULAR INDICATOR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_INDICADOR_SEMICIRCULAR', N'SEMICIRCULAR INDICATOR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_INDICADOR_TERMOMETRO', N'THERMOMETER INDICATOR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_INFORMACION', N'INFORMATION', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_INGRESAR', N'SIGN IN', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_INGRESECONCEPTO', N'Please enter at least one search term', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_INGRESEVALOR', N'Enter a value.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_INICIO', N'Home', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_INSERTADOS', N'#INSERTS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_IZQUIERDA', N'Left', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_LATITUD', N'Latitude ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_LIMITE_INFERIOR', N'Lower Limit', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_LIMITE_SUPERIOR', N'Upper Limit', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_LISTAPRECIO', N'Price list', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_LOGIN', N'Login', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_LOGIN_ERROR', N'INCORRECT DATA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_LOGINREPETIDO', N'The login {0} already exists.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_LOGOEMPRESA', N'BUSINESS SOLUTIONS NEXTEL PERU', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_LOGOEMPRESA_PIE_ALT', N'Nextel Peru.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_LONGITUD', N'Longitude', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MANTENIMIENTO_CONFIGURACION', N'Setting Maintenance', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MANTENIMIENTO_ICONO', N'Mantenimiento icono', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MANTENIMIENTOALMACEN', N'Store Maintenance', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MANTENIMIENTOCLIENTES', N'Customers Maintenance', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MANTENIMIENTOFAMILIAPRODUCTOS', N'Family products', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MANTENIMIENTOPRODUCTOS', N'Products Maintenance', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MANTENIMIENTORUTAS', N'Route Maintenance ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MANTENIMIENTOUSUARIO', N'Users Maintenance', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MANUAL_IT', N'Technical User guide', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MANUAL_PROCEDURES', N'Procedures Manual', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MAPA_GEOANALISIS', N'Geo Analysis', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MAPA_GEOANALISIS_DATO', N'Data', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MAPA_GRAFICOS', N'Graphics', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MARCA', N'MARK', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MENSAJE_ACCION', N'Are you sure you want to', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MENSAJE_ACCIONREGISTRO', N'delete the selected record?', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MENSAJE_ACCIONREGISTROS', N'all selected items?', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MENSAJE_COMBINACION_LISTAPRECIOEXISTENTE', N'ERROR : EXISTING PRICE LIST', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MENSAJE_EXITO', N'It was recorded correctly.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MENSAJE_GUIA', N'See our guide to use {0}', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MENSAJE_SELECCION', N'You have not selected any records.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MENSAJECODIGOYAREGISTRADO', N'ERROR: CODE {0} IS REGISTERED IN DATABASE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MENSAJEDATOOBLIGATORIO', N'ERROR: DATA {0} IS REQUIRED', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MENSAJEDATOOBLIGATORIOSEL', N'ERROR: DATA {0} IS REQUIRED SELECTED AN OPTION.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MENSAJEDATOSGRABADOS', N'DATA SAVE CORRECTLY', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MENSAJEDATOSINCOMPLETOS', N'ERROR: INCOMPLETE DATA: ENTER AT LEAST ONE TYPE OF CUSTOMER OR A CONDITION OF SALE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MENSAJEDATOSINCORRECTOS', N'ERROR: INCORRECT DATA: CHECK DATA ENTERED', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MENSAJEDATOYAREGISTRADO', N'ERROR: DATA {0} FOR {1} SELECTED IS ALREADY REGISTERED IN DATABASE.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MENSAJEERRORBD', N'ERROR: IN DATABASE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MENSAJEFECHASINCORRECTOS', N'CHECK DATE ENTERED.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MENSAJELOGINYAREGISTRADO', N'ERROR: USER LOGIN {0} IS ALREADY REGISTERED IN DATABASE.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MENSAJENOEXISTENDATOS', N'No data', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MENSAJEOBLIGATORIO', N'Fields with an asterisk (*) are required.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MENSAJEREPITEASIG', N'ERROR: ROUTE ASSIGNMENT IS REPEATED', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MESES', N'Month', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_META', N'Goal', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MIDDLEWARE', N'Middleware', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MODELO', N'Model ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MODULOCARGADESCARGA', N'LOAD/DOWNLOAD MODULE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MODULODE', N'Module of', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MONT_CLIENTE', N'Amount per Customers', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MONT_PRODUCTO', N'Amount per Products', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MONT_VENDEDOR', N'Amount per Seller', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MONTO', N'AMOUNT', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MONTO_PAGADO', N'AMOUNT PAID', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MONTO_TOTAL', N'AMOUNT TOTAL', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MOSTRAR', N'Show', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MOSTRAR_ICONO', N'Mostrar icono', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MOTIVO', N'REASON', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MOTIVO_CANJE', N'Exchange Reason', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MOTIVO_NOPEDIDO', N'Not Order Reason', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MOTIVO_VALOR', N'REASON', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MOTIVODEVOLUCION', N'RETURN REASON', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_MSJ_YAEXISTEZONADESCRIPCION', N'ERROR: ALREADY EXISTS AN AREA WITH THE DESCRIPTION ENTERED', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NAV_CANJE', N'Exchange', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NAV_CARGA', N'Loads', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NAV_CARGA_TAB', N'Loads', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NAV_CARGADESCARGA', N'Loads / Downloads', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NAV_CLIENTES', N'Customers', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NAV_COBRANZA', N'Collection', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NAV_CONFIGURACION', N'Settings', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NAV_DESCARGA', N'Downloads', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NAV_DEVOLUCION', N'Return', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NAV_GENERAL', N'General', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NAV_GEOANALISIS', N'Geo Analysis', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NAV_GPSRUTA', N'Route', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NAV_GRAFICOS', N' Graphics', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NAV_LISTAPRECIOS', N'Price list', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NAV_MANTENIMIENTO', N'Maintenance', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NAV_NOPEDIDO', N'Not Order', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NAV_PEDIDO', N'Order', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NAV_PEDIDOTOTAL', N'Total Order', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NAV_PRODUCTOS', N'Products', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NAV_REPORTES', N'Reports', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NAV_RUTAS', N'Routes', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NAV_USUARIOS', N'Users', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NEXTEL', N'NEXTEL', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NIN_ARCHIVO_EJECUTADO', N'No file executed', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NINGUN_ARCHIVO', N'You have not selected any files', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NO', N'NOT', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NOCLIENTESVISITA', N'There is not customeres visited today', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NOCOBRANZASHOY', N'You have not made collections today', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NODATOS_DESCARGAR', N'There are no outstanding data to download', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NOEXISTE', N' does not exist', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NOLATLONREGISTRADA', N'RECORD DOES NOT HAVE THE LATITUDE AND LONGITUDE REGISTERED', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NOMBRE', N'Name', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NOMBRE_CLIENTE', N'CUSTOMER NAME', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NOMBRE_CORTO', N'Short name', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NOPEDIDO_DETNOPEDIDO', N'Not Order |not order detail', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NOPEDIDOSULT5D', N'Not Orders in the last 5 days', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NOPOSICIONESFILTRO', N'There are not positions for the indicated filter', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NOPOSICIONESREGISTRADAS', N'REGISTERED POSITIONS NOT FOUND', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NOTIENEGPS', N'Does not have GPS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NUEVA_CARGA', N'New upload', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NUEVARUTA', N'New route', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NUEVARUTAGUARDADO', N'New route saved correctly', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NUEVO', N'New', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NUEVOACTIVIDAD', N'New Activity', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NUEVOALMACEN', N'New store', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NUEVOCLIENTE', N'New customer', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NUEVOCLIENTEGUARDADO', N'New customer saved correctly', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NUEVOCOBRANZA', N'New collection', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NUEVODETALLE', N'New Detail', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NUEVOGENERAL', N'New general', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NUEVOGENERALGUARDADO', N'New general saved correctly', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NUEVOLISTAPRECIOGUARDADO', N'New price list', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NUEVOPEDIDO', N'New Order', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NUEVOPRECIO', N'New Price', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NUEVOPRODUCTO', N'New product', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NUEVOPRODUCTOGUARDADO', N'New product saved correctly', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NUEVOREGGENERAL', N'New Registration', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NUEVOSUARIO', N'New user', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NUEVOUSUARIO', N'New user', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NUEVOUSUARIOGUARDADO', N'New user saved correctly', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NUMCANJEUPPERCASE', N'NUM EXCHANGE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NUMDEVOLUCIONUPPERCASE', N'NUM. RETURN', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NUMERO', N'Number', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NUMNOPEDIDOUPPERCASE', N'NUM. NOT ORDER', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_NUMPEDIDOUPPERCASE', N'NUM. ORDER', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_OBSERVACION', N'OBSERVATION', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_OBTENER', N'Get', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_OK_ARCHIVO', N'[OK] File:', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_OPERACION', N'Operation', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_PAGADO', N'Paid', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_PAGINACION', N'Pagination', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_PAGOSDECOBRANZA', N'Payments of collection', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_PEDIDO_DETPEDIDO', N'Order |order detail', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_PEDIDOS_EDITADOS', N'Editions', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_PEDIDOSDELDIAMES', N'Orders of the day by the seller during the mont', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_PEDIDOSHOY', N'Today´s Orders', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_PEDTOTAL_DETPEDTOTAL', N'Total Order |total order detail', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_PERFIL', N'Profile', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_PIE_BR', N'Please,please read our agreement and privacy for use of the site.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_PIE_P', N'PRODUCTO / n-order Nextel Perú All rights reserved.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_PIE2_BR', N'', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_PORFAVOR_REVISARLO', N'Please,review', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_PRECIO', N'Price', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_PRECIO_EDITABLE', N'Editable Price', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_PRECIO_POR', N'Price by', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_PRECIO_VALIDAR', N'Validate Price', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_PRECIOBASE', N'Base price', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_PRO_MAS_VENDIDOS', N'Products best Selling', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_PRODUCTO', N'Product', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_PRODUCTOS_DESHA', N'Disable products', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_PUNTOS', N'Points', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_REGISTROS', N'records', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_REGISTROS_ACTUALIZADOS', N' updated records', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_REGISTROS_INSERTADOS', N' inserted records', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_REMOVIO_ARCHIVO', N'A file was removed from the list', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_REPORTE_EQUIPO', N'Equipment Report', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_REPORTECANJE', N'EXCHANGE REPORT', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_REPORTECOBRANZA', N'Collection Report', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_REPORTEDECANJE', N'Exchange Report', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_REPORTEDECOBRANZA', N'Collection Report', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_REPORTEDEDEVOLUCION', N'Return Report', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_REPORTEDENOPEDIDOS', N'Not order Report', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_REPORTEDEPEDIDOS', N'Order Report', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_REPORTEDEPEDIDOTOTAL', N'Total order Report ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_REPORTEDEVOLUCION', N'RETURN REPORT', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_REPORTEFAMILIA', N'Family Report products', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_REPORTEMARCA', N'Product brand report', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_REPORTENOPEDIDOS', N'NOT ORDER REPORT', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_REPORTEPEDIDOS', N'ORDER REPORT', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_REPORTEPEDIDOTOTAL', N'TOTAL ORDER REPORT', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_REPORTERESERVA', N'Reporte de Reserva', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_RESTAURAR', N'Restore', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_RESTAURAR_TODO', N'Restore All', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_RESTAURAR_TODO_DATOS', N'Restore all Data', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_RUTA', N'Route', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_RUTA_DETRUTA', N'Route |route detail', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_RUTA_GENERADOR', N'estandares/Iorder/GeneradorArchivosIOrder.zip', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_RUTA_ICONO_LOGO', N'nextel-logo.jpg', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_RUTA_MANUAL_PROCEDURE', N'estandares/V2/Iorder/Manual_procedures_ES.zip', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_RUTA_MANUAL_TECNICO', N'estandares/v2_10r/es/order/ManualTecnico.zip', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_RUTA_MANUAL_USUARIO', N'estandares/v2_1r/es/order/ManualUsuario.zip', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_RUTA_TERMINOS_Y_CONDICIONES', N'', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_RUTA_WIN_SERVICE', N'estandares/Iorder/ServicioWindowsIOrderV25.zip', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_RUTAVENDEDOR', N'Seller Route', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_SALDO', N'Balance', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_SALIR', N'Exit', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_SATELITE', N'Satélite', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_SEGMENTO_DEFAULT', N'BODEGA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_SELE_ICONO_G', N'Select the icon to save', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_SELECCIONAR', N'Select', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_SELECCIONAR_RUTA', N'Select at least one day in the route', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_SELECCIONE', N'Select', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_SELECCIONE_ARCHIVO', N'select the file and then click on run', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_SELECCIONECONFIGURACION', N'Select setting to save', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_SENAL', N'Signal ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_SERIE', N'SERIE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_SERVICIO_WINDOWS', N'Windows Service', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_SESSIONFINALIZADO', N'Session finished', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_SHOW', N'Show', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_SI', N'YES', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_SIN_DATOS', N'No data', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_SINBONIFICACION', N'No bonus', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_SKU_DEMANDADOS', N'SKU most Demanded', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_SKU_MAS_ROTACION', N'SKU with the highest Rotation', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_SKU_MAS_VENDIDOS', N'SKU best Selling', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_SOLO_BANCO', N'Only for banks', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_STOCK', N'Stock', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_STOCK_ALL', N'Validate Stock', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_SUBIDO', N'UPLOADED', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_SUBIDOS', N'#Uploads', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_SUBIR_ICONO', N'Subir icono', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_SUPERVISOR', N'Supervisor', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_TABLA', N'Type', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_TELEFONO', N'Telephone ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_TERMINOS_Y_CONDICIONES', N'Terminos y condiciones', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_TIENE', N' HAVE ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_TIPO', N'type', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_TIPO_CANAL', N'type', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_TIPO_CARGA', N'LOAD TYPE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_TIPO_CLIENTE', N'CUSTOMER TYPE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_TIPO_DOCUMENTO', N'FORMA PAGO - COBRANZA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_TIPO_GRUPO', N'GROUP TYPE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_TITULO', N'PEDIDOS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_TODOS', N'ALL', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_TOPPRODUCTOSMES', N'Top-selling products Month', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_TOTAL', N'Total', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_TOTAL_CARGADOS', N'#LOADED TOTAL', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_TOTAL_NO_CARGADOS', N'#UNLOADED TOTAL', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_TOTAL_PRODUCTOS', N'Total Products', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_TOTAL_VENTA', N'Total', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_TRACKINGCANJE', N'TRACKING OF EXCHANGE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_TRACKINGCOBRANZA', N'TRACKING OF COLLECTIONS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_TRACKINGDEVOLUCION', N'TRACKING OF RETURN', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_TRACKINGNOPEDIDO', N'TRACKING OF NOT ORDER', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_TRACKINGPEDIDO', N'TRACKING OF ORDER', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_UBIQUE_ARCHIVO', N'LOCATE THE FILE (allowed formats TXT,ZIP max. size 1 mb)', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ULT_FEC_REGISTRO', N'Last Date Record', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_ULT_POS_EQUIPO', N'Last Position of Equipment', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_UNIDADDEFECTO', N'Default unit', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_UNIDADES', N'Unit', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_UNIDADES_VENDIDAS', N'UNITS SOLD', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_UNIDADMEDIDA', N'Measurement unit', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_USUARIO', N'User', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_USUARIO_ALERTA', N'Enter the user', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_USUARIOS_DESHA', N'Disable users', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_VALIDARDESC', N'ERROR: MUST ENTER A DISCOUNT OF NO MORE THAN 100%', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_VEND_MAS_VENTAS', N'Sellers with higher Sales', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_VENDEDOR', N'Seller', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_VENTAS_TOTALES', N'Total Sales', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_VENTAS_TOTALES_PRODUCTO', N'TOTAL SALES BY PRODUCT', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_VENTASULT30D', N'Sales in the last 30 days', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_VERGRAFICO', N'See graphic', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_VOLVER', N'BACK', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'EN', N'WEB_VOUCHER', N'Voucher', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_ADDYUPDSTOCK', N'Agre./Stock', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_AGREGAR', N'Agregar', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_ARTICULO', N'Artículo', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_ARTICULO_EXISTE_CANJE', N'El articulo ya existe en el Canje', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_ARTICULO_EXISTE_DEVOLUCION', N'El articulo ya existe en la Devolución', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_ARTICULO_EXISTE_PEDIDO', N'El articulo ya existe en el Pedido', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_ARTICULO_NO_PRECIO', N'No tiene precio el Artículo', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_ARTICULO_NO_PRECIO_ASIGNADO', N'El artículo no tiene precio asignado', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_ATENCION_BORRAR_CLIENTES', N'ATENCION: Se borrarán todos los clientes. ¿Desea continuar?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_ATENCION_BORRAR_INFORMACION', N'ATENCION: Antes de sincronizar  información,  revisar los pedidos pendientes', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_ATENCION_BORRAR_PRODUCTOS', N'ATENCION: Se borrarán todos los productos. ¿Desea continuar?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_ATENCION_ENVIARA_CANJES', N'WARNING: Se enviaran los canjes pendientes de envío. Desea continuar?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_ATENCION_ENVIARA_DEVOLUCIONES', N'WARNING: Se enviaran las devoluciones pendientes de envío. Desea continuar?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_ATENCION_ENVIARA_PAGOS', N'WARNING: Se enviaran los pagos pendientes de envío. Desea continuar?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_ATENCION_ENVIARA_PEDIDOS', N'WARNING: Se enviaran los pedidos/no pedidos pendientes de envío. Desea continuar?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_ATENCION_ENVIARA_REGISTRO', N'ATENCION: Se enviaran los Registros pendientes de envio. Desea continuar?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_ATENCION_ENVIARA_TODOS', N'WARNING: Se enviaran los datos pendientes de envío. Desea continuar?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_ATENCION_NOENVIARA_CANJES', N'ATENCION: No hay Canjes pendientes de envío', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_ATENCION_NOENVIARA_DEVOLUCION', N'ATENCION: No hay Devoluciones pendientes de envío', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_ATENCION_NOENVIARA_PAGOS', N'ATENCION: No hay Pagos pendientes de envío', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_ATENCION_NOENVIARA_PEDIDOS', N'ATENCION: No hay Pedidos/No Pedidos pendientes de envío', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_ATENCION_NOENVIARA_TODOS', N'ATENCION: No hay datos pendientes de envío', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_ATENDIDO', N'Atendido', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_ATRAS', N'Atrás', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_AYUDA', N'Ayuda', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_BANCOS', N'Bancos', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_BIENVENIDO', N'Bienvenido', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_BUSCAR', N'Buscar', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_BUSCAR_CLIENTES', N'Buscar Clientes', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_BUSCAR_X_NOMBRE', N'Buscar por Nombre', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_CANCELAR', N'Cancel', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_CANJE', N'Canje', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_CANJE_REGISTRADO', N'Canje Registrado', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_CANJES', N'Canje', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_CANT', N'Cant.', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_CANT_TTL', N'Cant Ttl', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_CANTIDAD_INCORRECTA', N'Cantidad Incorrecta', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_CLAVE', N'Contraseña', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_CLIENTE', N'Cliente', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_CLIENTE_NOEXISTE', N'El cliente no existe', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_CLIENTES', N'Clientes', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_CLIENTES_ONLINE', N'Clientes - En Linea', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_COBRANZA', N'Cobranza', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_COBRANZAS', N'Cobranzas', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_COBRAR', N'Cobrar', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_COD_TCLIE', N'Cod.TClie', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_CODIGO', N'Código', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_COMPLETAR_DATOS', N'Completar Datos', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_CON_PEDIDO', N'CON PEDIDO', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_CONDICION', N'Condición', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_CONFIRMAR_CANCELAR_CANJE', N'Esta seguro que desea cancelar el canje?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_CONFIRMAR_CANCELAR_DEVOLUCION', N'Esta seguro que desea cancelar la devolucion?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_CONFIRMAR_CANCELAR_PEDIDO', N'Esta seguro que desea cancelar el pedido?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_CONFIRMAR_FINALIZAR_CANJE', N'Esta seguro de finalizar el canje?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_CONFIRMAR_FINALIZAR_DEVOLUCION', N'Esta seguro de finalizar la devolución?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_CONFIRMAR_FINALIZAR_NOPEDIDO', N'Atención: Esta seguro que desea finalizar el No Pedido?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_CONFIRMAR_FINALIZAR_NOPEDIDO2', N'Esta seguro que desea finalizar el No Pedido?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_CONFIRMAR_FINALIZAR_PEDIDO', N'Esta seguro que desea finalizar el pedido?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_CONSOLIDADO', N'Consolidado', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_CONTENIDO_AYUDA', N'Se debe sincronizar los datos antes de usar la aplicacion. \n\nEl usuario puede realizar: pedido, no pedido, canjes, devoluciones, cobranzas \n(si existe cobranzas pendientes al cliente  seleccionado). \n\nEl usuario tiene otras opciones como son: \n * Envíos: enviar pedidos pendientes al servidor \n * Consolidado: visualizar  acciones realizada por usuario en el día \n * Eficiencia: visualizar indicadores de gestión diaria del usuario \n * Stock: visualizar el stock de producto seleccionado \n \nMientras la aplicación utiliza el canal de datos otros servicios están siendo bloqueados. \n\nIngreso Rápido Productos:Ubicarse columna CODIGO e ingresar código producto,cantidad y descuento,automáticamente se generará precio de acuerdo  a la cantidad y descuento solicitado. \n (OK) funciona para agregar fila \n (#) funciona para eliminar fila \n\nEl Equipo debe esperar un promedio de 90 Seg para capturar primera posición GPS valida \n El Tiempo de espera para la toma de GPS es 30 Seg \n\nEmail: Data.soporte@nextel.com.pe', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_DEBE_SINCRONIZAR', N'Primero debe sincronizar', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_DES', N'Des', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_DESC_INCORRECTA', N'DESCUENTO INCORRECTO DEBE ESTAR ENTRE 0 - 100', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_DESCRIPCION', N'DESC.', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_DESCUENTO_PORCENTAJE', N'DESCUENTO HASTA 100', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_DETALLE_COBRANZA', N'Detalle Cobranza', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_DETALLE_PEDIDO', N'Detalle Pedido', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_DEUDA', N'Deuda', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_DEVOLUCION', N'Devolución', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_DEVOLUCION_REGISTRADA', N'Devolución Registrada', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_DEVOLUCIONES', N'Devolución', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_DIRECCION', N'Dirección', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_DOC', N'DOC', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_DOC_COBR', N'Doc. Cobr.', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_DOCUMENTO', N'Documento', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_DOCUMENTO_TITULO', N'Documento', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_EFEC', N'% Eficiencia', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_EFICIENCIA', N'Eficiencia', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_ELIMINAR_FILA', N'funciona para eliminar fila', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_EMPRESA', N'Empresa', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_ENVIO_REGISTRADO', N'Envio registrado', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_ENVIOS', N'Envíos', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_ESTADO', N'Estado', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_FEC_VCMTO', N'Fec.Vcmto(DD/MM/AAAA)', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_FECHA', N'Fecha', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_FECHA_INCORRECTA', N'Fecha Incorrecta', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_FINALIZAR', N'Finalizar', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_FINALIZAR_COBRANZA', N'Está seguro de Grabar?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_FUERARUTA', N'Fuera de ruta', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_GENERAL', N'General', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_GIRO', N'Giro', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_INDICADOR', N'Indicador', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_INGRESAR_MAYOR_CERO', N'Debe ingresar una cantidad mayor a cero', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_INGRESAR_MONTO_MAYOR_CERO', N'Ingrese Monto mayor a 0', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_INGRESE_CAMPOS', N'Por favor Ingrese todos los Campos', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_INGRESE_CANTIDAD', N'Ingrese la Cantidad', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_INGRESE_CODIGO', N'Debe ingresar el código', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_INGRESE_DATOS', N'Ingrese los datos', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_INGRESE_FECHA', N'Ingrese la fecha', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_INGRESE_MONTO', N'Ingrese Monto', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_INGRESE_TABELA', N'Ingrese Tabla', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_INGRESO', N'Ingresar', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_INICIAR', N'Iniciar', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_ITEMS_CANJE', N'Items Canje', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_ITEMS_DEVOL', N'Items Devol', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_ITEMS_PEDIDO', N'Items Pedido', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_LOGIN', N'LOGIN', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_MANTENIMIENTO', N'Mantenimiento', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_MENSAJE', N'Mensaje', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_MENU', N'Menu', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_MENU_PRIN', N'Menu Prin', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_MNT_COBR', N'Mnt Cobr', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_MNT_TTL', N'Mnt Ttl', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_MONTO_MAYOR_SALDO', N'Monto mayor al Saldo', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_MONTO_PEDIDO', N'Monto Pedido', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_MOTIVO', N'Motivo', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_MOTIVO_CANJE', N'Motivo Canje', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_MOTIVO_DEVOLUCION', N'Motivo Devolución', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_MOVIL', N'Movil', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_MXPAG', N'MXPAG', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_NO', N'No', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_NO_COBRANZAS', N'No se tiene cobranza', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_NO_CONEXION_SERVIDOR', N'No hay conexión con el servidor pero login fuera de línea exitoso,puede trabajar sin conexión', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_NO_ENCONTRARON_RESULTADOS', N'No se encontraron resultados', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_NO_PEDIDO', N'No Ped.', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_NO_PEDIDO2', N'No Pedido', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_NO_PRECIO', N'No tiene precio', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_NOATENDIO_CANJE', N'No se pudo atender el Canje', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_NOATENDIO_DEVOLUCION', N'No se pudo atender la devolución', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_NOATENDIO_PEDIDO', N'No se pudo atender el Pedido', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_NOCOBERTURA_ENVIO', N'Zona sin cobertura,procesar el envío mediante la opcion ENVIOS del menú principal', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_NOCOBERTURA_SINCRONIZAR', N'Zona sin cobertura,No se puede sincronizar', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_NOEXISTE', N'No existe', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_NOEXISTE_TABELA', N'No existe tipo', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_NOMBRE', N'Nombre', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_NOMBRECLIENTE', N'', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_NOPEDIDO_REGISTRADO', N'No Pedido Registrado', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_NUMERO', N'Número', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_OBS', N'Obs.', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_PAGAD', N'PAGAD', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_PAGO', N'Pago', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_PAGO_COBR', N'Cobr', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_PAGO_REGISTRADO', N'Pago registrado', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_PAGOS', N'Pagos', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_PED', N'Ped', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_PEDI_NOPEDI', N'Pedi/No Pedi', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_PEDIDO', N'Pedido', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_PEDIDO_NO_DETALLE', N'El pedido no posee detalle', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_PEDIDO_REGISTRADO', N'Pedido registrado', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_PEDIDOS', N'Pedidos', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_PEDIDOS_DIA', N'Pedidos Día', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_POR_VISITAR', N'POR VISITAR', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_PRECIO', N'Precio', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_PROD_CANJE', N'Prod. Canje', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_PROD_DEVOLUCION', N'Prod. Devolución', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_PROD_PEDIDO', N'Prod. Pedido', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_PROD_STOCK', N'Prod. Stock', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_PRODUCTO', N'Producto', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_PRODUCTO_EXISTE', N'El producto ingresado ya existe en el pedido actual', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_PRODUCTO_EXISTE_CANJE', N'El producto ingresado ya existe en el canje actual', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_PRODUCTO_MOTIVO_EXISTEN', N'El producto y motivo ingresados ya existen', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_PRODUCTOS', N'Productos', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_REGISTRADO', N'Registrado', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_SALDO', N'SALDO', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_SALIR', N'Salir', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_SEGUIR', N'Seguir', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_SEGURO_GRABAR', N'Está seguro de Grabar?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_SELECCION', N'Selecc', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_SELECCION_MOTIVO_CANJE', N'Debe seleccionar el motivo de canje', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_SELECCION_MOTIVO_DEVOLUCION', N'Debe seleccionar el motivo de devolución', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_SI', N'Si', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_SIN_COBERTURA', N'Zona sin cobertura,intente sincronizar mas tarde', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_SIN_COBERTURA_CONSLTA', N'Zona sin cobertura,intente realizar la consulta mas tarde', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_SINCRONIZAR', N'Sincronizar', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_SINCRONIZAR_CLIENTES', N'Se debe sincronizar los clientes', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_STATUS_CONNECTED', N'Conectado', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_STATUS_CONNECTING', N'Conectando', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_STATUS_ERROR', N'Error', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_STATUS_PROCESSING', N'Procesando', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_STATUS_RECEIVING', N'Recibiendo', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_STATUS_SAVING', N'Guardando', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_STATUS_SENDING', N'Enviando', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_STATUS_WAIT', N'ESPERE', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_STOCK', N'Stock', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_STOCK_ABREV', N'S', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_STOCK_LINEA', N'Stock En Linea', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_TAB', N'Tipo', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_TABELA', N'Tabla', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_TITULO', N'n-order', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_TODO', N'Todo', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_TOT', N'Tot', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_TOTAL', N'Total', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_TOTAL_CLIENTES', N'Total Clientes', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_TPV', N'TPV(s)', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_USUARIO', N'Usuario', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_USUARIO_PASS_INVALIDO', N'Usuario y/o password inválido', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_USUARIO_PASS_INVALIDO_CODIGO', N'Usuario y/o password inválido sin código', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_VALOR', N'Valor', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_VERSION', N'VERSIÓN', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_VISITA_NO_PEDIDO', N'Visita No Ped.', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_VISITA_PEDIDO', N'Visita Ped.', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'JAVA_VISITADO', N'VISITADO', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ABREVIATURA', N'ABREVIATURA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ABRIRBARRA', N'Abrir barra', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ABRIRBUSCADOR', N'Abrir Buscador', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ACTUALIZADOS', N'#Actualizados', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ADMINISTRADOR', N'Administrador', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_AGREGAR', N'Agregar', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_AGREGAR_ARCHIVO_MIN', N'Debe agregar un archivo como mínimo', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ALMACEN', N'Almacén', N'w')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ALMACEN_STOCK', N'Almacen * Stock', N'w')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_APP_VERSION', N'n-order v2.6.2 09/11/2011', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ARCHIVOS', N'ARCHIVO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ARCHIVOS_EJECUTADO', N'Archivos ejecutados. Existen errores en algunos archivos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ARCHIVOS_EJECUTADO_CORRECT', N'Archivos ejecutados correctamente', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_AUMENTO_ARCHIVO', N'Se añadió un archivo a la lista', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_AYUDA', N'Ayuda', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_BANCO', N'Banco', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_BATERIA', N'Bateria', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_BIENVENIDO_H2', N'Bienvenido - Inicie Sesión', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_BIENVENIDO_P', N'Para acceder al servicio n-order ingrese su login y contraseña.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_BOLETA', N'BOLETA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_BONIFICACIONMANUAL', N'Bonificación Manual', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_BONIFICACIONREP', N'Bonificación', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_BUSCADOR', N'BUSCADOR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_BUSCAR', N'BUSCAR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_BUSCAR_LUPA', N'', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_BUSQUEDAGEORUTA', N'BUSQUEDA GEORUTA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CAMPOREQUERIDO', N'Este campo es requerido', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CAMPOS_CABECERA', N'Campos de Cabecera', N'w')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CAMPOS_DETALLE', N'Campos Detalle', N'w')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CANAL', N'Canal', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CANCELAR', N'CANCELAR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CANJE_DETCANJE', N'Canje |detalle del canje', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CANJESULT5D', N'Trocas en los ultimos 5 dias', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CANT_FRACCIONADA', N'Cantidad Fraccionada', N'w')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CANTIDAD', N'CANTIDAD', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CANTIDAD_PRESENTACION', N'Cantidad Presentación', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CARGA_DETCARGA', N'Carga |detalle de carga', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CARGA_EXITO', N' archivo(s) fue(ron) cargado(s) exitosamente:  ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CARGADETALLE', N'CARGA DETALLE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CARGAR', N'Cargar', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CARGAR_ARCHIVOS', N'Cargar Archivos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CERRAR', N'Cerrar', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CLAVE', N'Contraseña', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CLAVE_ALERTA', N'Ingrese el password', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CLAVE_NUMERICA', N'Usuarios de moviles usan clave numerica', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CLI_DESHAB', N'Clientes deshabilitados', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CLI_PEDIDOS', N'Clientes con mas Pedidos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CLI_PRO_MAS_VENDIDOS', N'Clie. con mas Producto Vendidos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CLIENTE', N'Cliente', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CLIENTES_MAS_CANJES', N'Clientes con más Canjes', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CLIENTES_MAS_DEVOLUCIONES', N'Clientes con más Devoluciones', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CLIENTES_MAS_VENTAS', N'Clientes con más Ventas', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CLIENTESCONPEDIDO', N'Clientes con Pedidos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CLIENTESMASCANJES30D', N'clientes que registran mas Canjes(30 dias)', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CLIENTESMASDEV30D', N'Clientes que registran mas Devoluciones(30 dias)', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CLIENTESMASNOPED30D', N'clientes que registran mas No Pedidos(30 dias)', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CLIENTESMASPAGOS30D', N'Clientes que registran mas Pagos(30 dias)', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CLIENTESMASVIS30D', N'Clientes mas visitados en 30 dias', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_COBERTURA', N'Cobertura', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_COBRANZA_DETCOBRANZA', N'Cobranza |detalle de la cobranza', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_COBRANZAS_TOTALES', N'Cobranzas Totales', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_COBRANZASHOY', N'Cobranzas de hoy', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_COD_PRODUCTO', N'Código Producto', N'w')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CODIGO', N'Código', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CODIGO_ALMACEN', N'CODIGO ALMACEN', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CODIGO_CANAL', N'CODIGO CANAL', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CODIGO_CLIENTE', N'CÓDIGO CLIENTE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CODIGO_COBRANZA', N'Código Cobranza', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CODIGO_DETALLE', N'Código Detalle', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CODIGO_DIRECCION', N'Código dirección', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CODIGO_FAMILIA', N'CODIGO FAMILIA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CODIGO_GRUPO', N'CODIGO_GRUPO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CODIGO_MARCA', N'CODIGO MARCA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CODIGO_PEDIDO', N'Código Pedido', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CODIGO_PEDIDO_EDITADO', N'Código Edición', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CODIGO_PRODUCTO', N'CODIGO PRODUCTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CODIGO_VENDEDOR', N'CÓDIGO VENDEDOR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CODIGOPRODUCTO', N'Codigo de Producto', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CONBONIFICACION', N'Con Bonificacion', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_COND_VTA', N'COND. VENTA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CONDICION', N'CONDICIÓN', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CONDVENTAUPPERCASE', N'COND. VENTA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CONDVTA', N'COND. VENTA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CONF_ICONO_GUARDADA', N'Configuración de icono guardada exitosamente', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CONFIG_CABECERA_DESCARGA', N'Descarga incluye cabecera', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CONFIG_CANTIDAD_DECIMALES_CARGA', N'Numero decimales descarga', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CONFIG_CANTIDAD_DECIMALES_VISTA', N'Numero decimales vista', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CONFIG_COMA', N'Coma', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CONFIG_CONDVTA', N'Condición de Venta', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CONFIG_DECIMAL', N'Configuracion decimal', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CONFIG_DESCUENTO', N'Descuento', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CONFIG_DESCUENTO_GENERAL', N'Descuento General', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CONFIG_DESCUENTO_PRODUCTO', N'Descuento por Producto', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CONFIG_EFICIENCIA', N'Eficiencia', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CONFIG_GENERAL', N'Configuración General', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CONFIG_GPSRUTA', N'GPS - Ruta', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CONFIG_IDIOMA', N'Idioma', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CONFIG_LONGCODPROD', N'Longitud del Código de Producto', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CONFIG_MANT_DESCUENTO', N'Configuración descuento', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CONFIG_MONEDA', N'Moneda', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CONFIG_MONEDAIDIOMA', N'Configuración Moneda e Idioma', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CONFIG_MONTO', N'Por Monto', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CONFIG_MOVIL', N'Configuración movil', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CONFIG_PORCENTAJE', N'Por porcentaje', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CONFIG_PRECIO', N'Configuración precio', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CONFIG_PUNTO', N'Punto', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CONFIG_RANGO_DESC', N'Rango Descuento (%)', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CONFIG_RANGO_MAX', N'Max', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CONFIG_RANGO_MIN', N'Min', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CONFIG_SIMBOLO', N'Configuración símbolo decimal', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CONFIG_TIPOCLI', N'Tipo de Cliente', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CONFIGURACIONGUARDADO', N'Configuración guardada correctamente', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CONFIGURAR', N'Configurar', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CONTROL_STOCK', N'Control de Stock', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_CREDITO_UTILIZADO', N'CREDITO UTILIZADO', N'w')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DATOS_INVALIDOS', N'Existen datos invalidos en', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DEBE_TENER', N' Y DEBE TENER ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESC', N'DESCUENTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESC_VOL', N'DSCTO X VOL', N'w')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_CANJE', N'CANJE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_CANJE_COL01', N'COD_CANJE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_CANJE_COL02', N'COD_CLIENTE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_CANJE_COL03', N'COD_VENDEDOR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_CANJE_COL04', N'COD_EMPRESA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_CANJE_COL05', N'FEC_REGISTRO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_DETALLE_CANJE', N'DETALLE_CANJE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_DETALLE_CANJE_COL01', N'COD_CANJE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_DETALLE_CANJE_COL02', N'COD_CLIENTE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_DETALLE_CANJE_COL03', N'COD_EMPRESA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_DETALLE_CANJE_COL04', N'COD_VENDEDOR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_DETALLE_CANJE_COL05', N'COD_DETCANJE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_DETALLE_CANJE_COL06', N'COD_PRODUCTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_DETALLE_CANJE_COL07', N'CANTIDAD', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_DETALLE_CANJE_COL08', N'COD_MOTIVO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_DETALLE_CANJE_COL09', N'OBSERVACION', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_DETALLE_CANJE_COL10', N'FEC_VENCIMIENTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_DETALLE_DEVOLUCION', N'DETALLE_DEVOLUCION', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_DETALLE_DEVOLUCION_COL01', N'COD_DEVOLUCION', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_DETALLE_DEVOLUCION_COL02', N'COD_CLIENTE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_DETALLE_DEVOLUCION_COL03', N'COD_VENDEDOR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_DETALLE_DEVOLUCION_COL04', N'COD_EMPRESA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_DETALLE_DEVOLUCION_COL05', N'COD_DETDEVOLUCION', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_DETALLE_DEVOLUCION_COL06', N'COD_PRODUCTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_DETALLE_DEVOLUCION_COL07', N'CANTIDAD', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_DETALLE_DEVOLUCION_COL08', N'COD_MOTIVO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_DETALLE_DEVOLUCION_COL09', N'OBSERVACION', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_DETALLE_DEVOLUCION_COL10', N'FEC_VENCIMIENTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_DETALLE_PEDIDO', N'DETALLE_PEDIDO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_DETALLE_PEDIDO_COL01', N'COD_PEDIDO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_DETALLE_PEDIDO_COL02', N'COD_VENDEDOR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_DETALLE_PEDIDO_COL03', N'COD_CLIENTE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_DETALLE_PEDIDO_COL04', N'COD_EMPRESA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_DETALLE_PEDIDO_COL05', N'COD_DETPEDIDO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_DETALLE_PEDIDO_COL06', N'COD_PRODUCTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_DETALLE_PEDIDO_COL07', N'CANTIDAD', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_DETALLE_PEDIDO_COL08', N'PRECIO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_DETALLE_PEDIDO_COL09', N'DESCUENTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_DETALLE_PEDIDO_COL10', N'TOTAL', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_DETALLE_PEDIDO_COL11', N'DESCRIPCION', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_DETDESCARGA', N'Descarga |detalle de descarga', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_DEVOLUCION', N'DEVOLUCION', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_DEVOLUCION_COL01', N'COD_DEVOLUCION', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_DEVOLUCION_COL02', N'COD_CLIENTE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_DEVOLUCION_COL03', N'COD_VENDEDOR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_DEVOLUCION_COL04', N'COD_EMPRESA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_DEVOLUCION_COL05', N'FEC_REGISTRO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_NOVENTA', N'NOVENTA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_NOVENTA_COL01', N'COD_NOVENTA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_NOVENTA_COL02', N'COD_CLIENTE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_NOVENTA_COL03', N'COD_VENDEDOR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_NOVENTA_COL04', N'COD_EMPRESA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_NOVENTA_COL05', N'COD_MOTIVO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_NOVENTA_COL06', N'FEC_REGISTRO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_PAGO', N'PAGO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_PAGO_COL01', N'COD_PAGO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_PAGO_COL02', N'COD_VENDEDOR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_PAGO_COL03', N'MONTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_PAGO_COL04', N'TIPO_PAGO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_PAGO_COL05', N'COD_BANCO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_PAGO_COL06', N'VOUCHER', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_PAGO_COL07', N'COD_EMPRESA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_PAGO_COL08', N'FEC_REGISTRO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_PEDIDO', N'PEDIDO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_PEDIDO_COL01', N'COD_PEDIDO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_PEDIDO_COL02', N'COD_VENDEDOR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_PEDIDO_COL03', N'COD_CLIENTE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_PEDIDO_COL04', N'COD_EMPRESA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_PEDIDO_COL05', N'COD_CONDVENTA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_PEDIDO_COL06', N'TOTAL', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_PEDIDO_COL07', N'FEC_REGISTRO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_PEDIDO_COL08', N'OBSERVACION', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_PEDIDO_COL09', N'LATITUD', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGA_PEDIDO_COL10', N'LONGITUD', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGADETALLE', N'DESCARGA DETALLE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGAEXITOSA', N'Descarga exitosa', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGAR', N'Descargar', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCARGAR_INFORMACION', N'Descargar Información', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCPORCENTAJE', N'DESCUENTO %', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCRIPCION', N'Descripción', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCRIPCION_PRODUCTO', N'DESCRIPCION PRODUCTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCUENTO_MAX', N'Descuento Maximo', N'w')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCUENTO_MIN', N'Descuento Minimo', N'w')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCUENTOMAX', N'Desc Max', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESCUENTOMIN', N'Desc Min', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DESDE', N'Desde', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DETALLE', N'DETALLE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DETALLE_COBRANZA', N'detalle cobranza', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DETALLE_ERROR', N'Detalle del error:', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DETALLE_GENERAL', N'detalle general', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DETALLE_PEDIDOS_EDITADOS', N'Detalle Ediciones', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DETALLE_PRODUCTOS', N'Detalle de Productos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DETALLECLIENTE', N'detalle cliente', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DETALLECOBRANZA', N'No se ha ejecutado ningún archivo', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DETALLECONFIGURACION', N'detalle configuracion', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DETALLEICONO', N'detalle icono', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DETALLELISTAPRECIO', N'detalle lista precio', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DETALLEPEDIDO', N'Detalle Pedido', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DETALLEPRODUCTO', N'detalle producto', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DETALLERUTA', N'detalle ruta', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DETALLEUSUARIO', N'detalle usuario', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DEVOLUCI_DETDEVOLUCI', N'Devolución |detalle de la devolución', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DEVOLUCIONESULT5D', N'Devoluciones en los ultimos 5 dias', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DIAS', N'Días', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DIASEMANA_DO', N'Do', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DIASEMANA_JU', N'Ju', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DIASEMANA_LU', N'Lu', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DIASEMANA_MA', N'Ma', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DIASEMANA_MI', N'Mi', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DIASEMANA_SA', N'Sa', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DIASEMANA_VI', N'Vi', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DIRECCION', N'Dirección', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DISTANCIA', N'DISTANCIA (m)', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_DISTRIBUIDORA', N'Distribuidora', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_EDITAR', N'Editar', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_EDITARACTIVIDAD', N'Editar Actividad', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_EDITARALMACEN', N'Editar almacén', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_EDITARCLIENTE', N'Editar cliente', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_EDITARCOBRANZA', N'Editar Cobranza', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_EDITARDETALLE', N'Editar Detalle', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_EDITARDIRECCION', N'Editar dirección', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_EDITARGENERAL', N'Editar Registro', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_EDITARLISTAPRECIO', N'Editar lista precio', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_EDITARPEDIDO', N'Editar Pedido', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_EDITARPRECIO', N'Editar Precio', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_EDITARPRODUCTO', N'Editar producto', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_EDITARRUTA', N'Editar ruta', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_EDITARUSUARIO', N'Editar usuario', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_EJECUCION_EXITOSA', N'Ejecución exitosa', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_EJECUCIONEXITO', N'Ejecución realizada con éxito.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_EJECUTAR', N'Ejecutar', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_EJECUTAR_ARCHIVOS', N'Ejecutar Archivos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_EJEMPLO_XLS', N'Excel Ejemplo', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ELIJAVALOR', N'Elija un valor.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ELIMINAR', N'Eliminar', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ELIMINAR_REGISTRO', N'Eliminar Registro(s)', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ELIMINAR_TODO', N'Eliminar Todo', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ELIMINAR_TODO_DATOS', N'Eliminar todo los Datos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ELIMINO_EXITO', N'Se eliminó correctamente.', N'w')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_EMPRESA', N'Empresa', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ENCOBERTURA', N'En Cobertura', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_EQUIPO', N'Equipo', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ERR', N'ERROR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ERR_DATA', N'ERR_DATA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ERR_DUPLICADO', N'ERR_DUPLICADO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ERR_INVALIDO', N'ERR_INVALIDO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ERR_NO_DATA', N'ERR_ARCHIVO_SIN_DATOS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ERR_NUMERIC', N'ERR_NUMERICO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ERROR', N'Los datos ingresados no son correctos.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ERROR_ARCHIVO', N'ERROR EN ARCHIVO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ERROR_CARACTERES_ESPECIALES', N'ERROR: CARACTERES ESPECIALES', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ERROR_CARGA', N'Error de Carga', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ERROR_CARGANDO_ARCHIVO', N'Error cargando archivo ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ERROR_DESCARGA_DATOS', N'ERRO DE DADOS DE ENTRADA: verificar a data ea TIME.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ERROR_EJECUTAR_DTS', N'Error ejecutando DTS:<BR>La Variable Global ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ERROR_EN_ARCHIVO', N'Error en archivo', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ERROR_EN_DATOS', N'Error en datos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ERROR_ENCRIPTACION', N'Error de encriptacion', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ERROR_ESTRUCTURA', N'ERROR_ESTRUCTURA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ERROR_EXTENSION_ICONO', N'Solo archivos con extension jpg, jpeg, gif, png o bmp son permitidos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ERROR_INSERTANDO', N'Error insertando', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ERROR_NOSELECCIONADO', N'Error: debe haber por lo menos un archivo seleccionado ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ERROR_NUM_COLUMNAS', N'ERR_NUMERO_COLUMNAS:', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ERROR_SUBIENDO_ARCHI', N'Error subiendo archivo', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ERROR_TAMANIO_ICONO', N'Tamaño máximo del archivo debe ser 400KB', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ERROR_VACIO', N'ERR_VACIO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ERRORDTS_VARIABLESGLOBALES', N'Error ejecutando DTS:  El número de Variables Globales debe coincidir con el número de Valores ingresado ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ERRORGRABAR', N'ERROR: AL GRABAR LOS DATOS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ESLOGAN_01', N'Conéctate A {0}', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ESLOGAN_02', N'Conéctate A Tu Mundo.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ESTADO', N'ESTADO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ETIQUETA_NO_PROCESADO', N'No Procesado', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ETIQUETA_PROCESADO', N'Procesado', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_EUSUARIO_RESERVADO', N'USUARIO RESERVADO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_EXISTE', N'Existe', N'w')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_EXISTE_GRUPO', N'El código ya existe.', N'w')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_FACTURA', N'FACTURA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_FAMILIA', N'Familia', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_FAMILIA_PRODUCTO', N'Nueva familia de producto', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_FECHA', N'Fecha', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_FECHA_DIFERIDA', N'Fecha Diferida', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_FECHA_REGISTRO', N'FECHA REGISTRO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_FECHA_VENCIMIENTO', N'FECHA VENCIMIENTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_FECHA_VENCIMIENTO_CANJE', N'FECHA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_FECHAEDICION', N'Fecha edición', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_FECHAFIN', N'Fecha Fin', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_FECHAINICIO', N'Fecha Inicio', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_FECHAREGISTRO', N'Fecha registro', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_FECHAVISITA', N'FECHA VISITA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_FILA', N'FILA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_FILENAME_ALMACEN', N'ALMACEN', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_FILENAME_ALMACENPRODUCTO', N'ALMACEN_PRODUCTO_PRESENTACION', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_FILENAME_CLIENTE', N'CLIENTE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_FILENAME_COBRANZA', N'COBRANZA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_FILENAME_DIRECCION', N'DIRECCION', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_FILENAME_FAMILIA', N'FAMILIA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_FILENAME_FAMILIA_PRODUCTO', N'FAMILIA_PRODUCTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_FILENAME_GENERAL', N'GENERAL', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_FILENAME_LISTAPRECIOS', N'LISTAPRECIOS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_FILENAME_MARCA', N'MARCA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_FILENAME_MARCA_PRODUCTO', N'MARCA_PRODUCTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_FILENAME_PRESENTACION', N'PRESENTACION', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_FILENAME_PRODUCTO', N'PRODUCTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_FILENAME_RUTA', N'RUTA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_FILENAME_VENDEDOR', N'VENDEDOR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_FIN', N'Fin', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_FLAG_GENERADOR', N'0', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_FLAG_MOSTRARLOGOTOP', N'0', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_FLETE', N'Flete', N'w')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_FORMATO', N'ERR_FORMATO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_FORMATO_INVAL_ARCHIV', N'Formato inválido del archivo', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_FRACCION', N'Fracción', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_FRACCIONAMIENTO', N'Fraccionamiento', N'w')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_FUERACOBERTURA', N'Fuera de Cobertura', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GEN_ARCHIVOS', N'Generador de Archivos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GENERARREPORTE', N'Generar Reporte', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GEO_GRAFNPANT', N'GRÁFICO YA EXISTE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GEOANALISIS_DETGEOANALISIS', N'Geo Análisis |Reporte de geo análisis', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GEOCERCA_NO_ENCONTRO_DIR', N'No se Encontro Dirección', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GEORUTA', N'GEORUTA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GIRO', N'GIRO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GPS', N'GPS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GRABAR', N'GUARDAR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GRAFICO', N'GRÁFICO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GRAFICO_BARRAS', N'GRAFICO DE BARRAS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GRAFICO_DONA', N'GRAFICO DONUT', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GRAFICO_LINEA', N'GRAFICO DE LINEA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GRAFICO_PIE', N'GRAFICO PIE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GRAFICOCANJE', N'Gráfico Canje', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GRAFICOCOBRANZA', N'Gráfico Cobranza', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GRAFICODEVOLUCION', N'Gráfico Devolución', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GRAFICONOPEDIDO', N'Grafico No Pedido', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GRAFICOPEDIDO', N'Gráfico Pedido', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GRAFICOS_DETGRAFICOS', N'Gráficos|Gráficos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GRCLIENTE_CANJE_VS_CANTIDAD', N'CANJE VS CANTIDAD', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GRCLIENTE_COBRANZA_VS_MONTO', N'COBRANZA VS MONTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GRCLIENTE_DEVOLUCION_VS_CANTIDAD', N'DEVOLUCIÓN VS CANTIDAD', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GRCLIENTE_PEDIDO_VS_CANTIDAD', N'PEDIDO VS CANTIDAD', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GRCLIENTE_PEDIDO_VS_MONTO', N'PEDIDO VS MONTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GRMAPA_CLIENTE_VS_CANTIDAD', N'CLIENTE VS CANTIDAD', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GRMAPA_CLIENTE_VS_MONTO', N'CLIENTE VS MONTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GRMAPA_PRODUCTO_VS_CANTIDAD', N'PRODUCTO VS CANTIDAD', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GRMAPA_PRODUCTO_VS_MONTO', N'PRODUCTO VS MONTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GRP_AVANCE', N'AVANCE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GRP_FILTROOPERACION', N'Operación', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GRP_FILTROTIPO', N'Tipo', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GRP_INDICADOR', N'INDICADOR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GRP_INDICADORPRODUCTO', N'INDICADOR PRODUCTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GRP_OPERACIONCANTIDAD', N'Cantidad', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GRP_OPERACIONMONTO', N'Monto', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GRP_TOP_CLIENTES', N'TOP CLIENTES', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GRP_TOP_PRODUCTOS', N'TOP PRODUCTOS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GRP_TOP_VENDEDORES', N'TOP VENDEDORES', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GRPRODUCTO_CANJE_VS_CANTIDAD', N'CANJE VS CANTIDAD', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GRPRODUCTO_DEVOLUCION_VS_CANTIDAD', N'DEVOLUCIÓN VS CANTIDAD', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GRPRODUCTO_PEDIDO_VS_CANTIDAD', N'PEDIDO VS CANTIDAD', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GRPRODUCTO_PEDIDO_VS_MONTO', N'PEDIDO VS MONTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GRUPO', N'Grupo.', N'w')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GRVENDEDOR_CANJE_VS_CANTIDAD', N'CANJE VS CANTIDAD', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GRVENDEDOR_COBRANZA_VS_MONTO', N'COBRANZA VS MONTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GRVENDEDOR_DEVOLUCION_VS_CANTIDAD', N'DEVOLUCIÓN VS CANTIDAD', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GRVENDEDOR_PEDIDO_VS_CANTIDAD', N'PEDIDO VS CANTIDAD', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_GRVENDEDOR_PEDIDO_VS_MONTO', N'PEDIDO VS CANTIDAD', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_HABILITADO', N'Habilitado', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_HASTA', N'Hasta', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_HISTORIAL_EDICION_PEDIDOS', N'Pedidos editados', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_HOME_ASPX_BIENVENIDO', N'Bienvenido', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_HOME_ASPX_IR', N'Directo a:', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_HOME_ASPX_MENSAJE', N'Consulte nuestras guías de usuario móvil y web para entender el uso del sistema n-order.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ICONO', N'ICONO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ICONO_ACTUALIZADO_EXITOSAMENTE', N'Icono actualizado exitosamente', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_INDICADOR_CIRCULAR', N'INDICADOR CIRCULAR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_INDICADOR_SEMICIRCULAR', N'INDICADOR SEMICIRCULAR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_INDICADOR_TERMOMETRO', N'INDICADOR TERMOMETRO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_INFORMACION', N'INFORMACIÓN', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_INGRESAR', N'INGRESAR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_INGRESECONCEPTO', N'Ingrese por lo menos un concepto de búsqueda', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_INGRESEVALOR', N'Escriba un valor.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_INICIO', N'Inicio', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_INSERTADOS', N'#INSERTADOS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_IZQUIERDA', N'Izquierda', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_LATITUD', N'Latitud', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_LIMITE_CREDITO', N'LIMITE DE CREDITO', N'w')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_LIMITE_INFERIOR', N'Limite Inferior', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_LIMITE_SUPERIOR', N'Limite Superior', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_LISTA_PRECIO', N'Lista Precio', N'w')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_LISTAPRECIO', N'Lista precio', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_LOGIN', N'Login', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_LOGIN_ERROR', N'LOS DATOS NO SON CORRECTOS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_LOGINREPETIDO', N'El login {0} ya existe.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_LOGOEMPRESA', N'SOLUCIONES DE NEGOCIO NEXTEL PERÚ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_LOGOEMPRESA_PIE_ALT', N'NEXTEL DEL PERU', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_LONGITUD', N'Longitud', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MANT_AGREGAR_CONDICION', N'Agregar Condición', N'w')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MANT_ARCHIVO_DESCARGA', N'Archivo de Descarga', N'w')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MANT_BONIFICACIONES', N'Mantenimiento de Bonificaciones', N'w')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MANT_BONOMAX', N'Bono Max.', N'w')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MANT_CONFIG_DESCARGA', N'Configuración  Descarga', N'w')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MANT_EDITABLE', N'Editable', N'w')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MANT_GRUARDA_CONFIGURACION', N'Guardar Configuración', N'w')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MANT_NUEVO_GRUPO', N'Crear Nuevo Grupo', N'w')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MANT_PERFIL_GRUPO', N'Mantenimiento Grupos', N'w')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MANT_PRES_A_BON', N'Presentación a Bonificar', N'w')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MANT_PRESENTACION', N'Presentación', N'w')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MANT_PROD_A_BON', N'Producto a Bonificar', N'w')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MANTENIMIENTO_CONFIGURACION', N'Mantenimiento de Configuración', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MANTENIMIENTO_ICONO', N'Mantenimiento icono', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MANTENIMIENTOALMACEN', N'Mantenimiento de almacén', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MANTENIMIENTOCLIENTES', N'Mantenimiento Clientes', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MANTENIMIENTOCLIENTESDIRECCIONES', N'Mantenimiento de Direcciones', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MANTENIMIENTOFAMILIAPRODUCTOS', N'familia de Productos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MANTENIMIENTOPEDIDO', N'Mantenimiento de Pedido', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MANTENIMIENTOPRODUCTOS', N'Mantenimiento Productos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MANTENIMIENTORUTAS', N'Mantenimiento Ruta', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MANTENIMIENTOUSUARIO', N'Mantenimiento Usuario', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MANUAL_IT', N'Manual Técnico', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MANUAL_PROCEDURES', N'Manual de Procedimientos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MAPA_GEOANALISIS', N'Geo Análisis', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MAPA_GEOANALISIS_DATO', N'Dato', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MAPA_GRAFICOS', N'Gráficos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MARCA', N'MARCA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MENSAJE_ACCION', N'¿Está seguro que desea', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MENSAJE_ACCIONREGISTRO', N'el registro seleccionado?', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MENSAJE_ACCIONREGISTROS', N'todos los items seleccionados?', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MENSAJE_COMBINACION_LISTAPRECIOEXISTENTE', N'ERROR : LISTA DE PRECIO EXISTENTE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MENSAJE_DESCUENTO', N'Descuento No valido', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MENSAJE_EXITO', N'Se grabo correctamente.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MENSAJE_GUIA', N'Consulte nuestra guía para el uso de {0}', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MENSAJE_SELECCION', N'No ha seleccionado ningún registro.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MENSAJECODIGOYAREGISTRADO', N'ERROR: EL CÓDIGO DE {0} YA ESTÁ REGISTRADO EN LA BASE DE DATOS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MENSAJEDATOOBLIGATORIO', N'ERROR: EL DATO {0} ES OBLIGATORIO.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MENSAJEDATOOBLIGATORIOSEL', N'ERROR: EL DATO {0} ES OBLIGATORIO SELECCIONAR UNA OPCION.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MENSAJEDATOSGRABADOS', N'DATOS GRABADOS CORRECTAMENTE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MENSAJEDATOSINCOMPLETOS', N'ERROR: DATOS INCOMPLETOS: INGRESE AL MENOS UN TIPO DE CLIENTE O UNA CONDICION DE VENTA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MENSAJEDATOSINCORRECTOS', N'ERROR: DATOS INCORRECTOS: VERIFIQUE LOS DATOS INGRESADOS.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MENSAJEDATOYAREGISTRADO', N'ERROR: EL DATO {0} PARA EL {1} SELECCIONADO YA SE ENCUENTRA REGISTRADO EN LA BASE DE DATOS.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MENSAJEERRORBD', N'ERROR: EN LA BASE DE DATOS.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MENSAJEFECHASINCORRECTOS', N'VERIFIQUE FECHAS INGRESADAS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MENSAJELOGINYAREGISTRADO', N'ERROR: LOGIN DE {0} YA SE ENCUENTRA REGISTRADO EN LA BASE DE DATOS.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MENSAJENOEXISTENDATOS', N'No existen datos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MENSAJEOBLIGATORIO', N'Los campos con asterísco (*) son obligatorios.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MENSAJEREPITEASIG', N'ERROR: SE REPITE LA ASIGNACION DE RUTA.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MESES', N'Meses', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_META', N'Meta', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MIDDLEWARE', N'Middleware', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MODELO', N'Modelo', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MODULOCARGADESCARGA', N'MODULO CARGA/DESCARGA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MODULODE', N'Módulo de', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MONITOREO', N'Monitoreo', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MONT_CLIENTE', N'Monto por Cliente', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MONT_PRODUCTO', N'Monto por Producto', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MONT_VENDEDOR', N'Monto por Vendedor', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MONTO', N'MONTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MONTO_PAGADO', N'MONTO PAGADO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MONTO_TOTAL', N'MONTO TOTAL', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MOSTRAR', N'Mostrar', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MOSTRAR_ICONO', N'Mostrar icono', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MOTIVO', N'MOTIVO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MOTIVO_CANJE', N'Motivo Canje', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MOTIVO_NOPEDIDO', N'Motivo No Pedido', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MOTIVO_VALOR', N'MOTIVO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MOTIVODEVOLUCION', N'MOTIVO DEVOLUCION', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_MSJ_YAEXISTEZONADESCRIPCION', N'ERROR: YA EXISTE UNA ZONA CON LA DESCRIPCION INGRESADA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NAV_CANJE', N'Canje', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NAV_CARGA', N'Cargas', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NAV_CARGA_TAB', N'Cargas', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NAV_CARGADESCARGA', N'Cargas / Descargas', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NAV_CLIENTES', N'Clientes', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NAV_COBRANZA', N'Cobranza', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NAV_CONFIGURACION', N'Configuración', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NAV_DESCARGA', N'Descargas', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NAV_DEVOLUCION', N'Devolución', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NAV_GENERAL', N'General', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NAV_GEOANALISIS', N'Geo Análisis', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NAV_GPSRUTA', N'Ruta', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NAV_GRAFICOS', N'Gráficos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NAV_LISTAPRECIOS', N'Lista precio', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NAV_MANTENIMIENTO', N'Mantenimiento', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NAV_NOPEDIDO', N'No pedido', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NAV_PEDIDO', N'Pedido', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NAV_PEDIDOTOTAL', N'Pedido total', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NAV_PRODUCTOS', N'Productos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NAV_QUIEBRESTOCK', N'STOCK DE QUIEBRES', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NAV_REPORTES', N'Reportes', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NAV_RESERVA', N'Reserva', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NAV_RUTAS', N'Rutas', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NAV_USUARIOS', N'Usuarios', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NEXTEL', N'NEXTEL', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NIN_ARCHIVO_EJECUTADO', N'Ningún archivo ejecutado', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NINGUN_ARCHIVO', N'No ha seleccionado ningún archivo', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NO', N'NO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NO_INFORMACION', N'NO SE ENCONTRO INFORMACIÓN PARA LOS DATOS INGRESADOS.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NO_RESTRICTIVO', N'No Restrictivo', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NO_UBICO', N'No se ubico dirección', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NOCLIENTESVISITA', N'No hay Clientes Visitados en el dia', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NOCOBRANZASHOY', N'No ha realizado cobranzas hoy', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NODATOS_DESCARGAR', N'No existen datos pendientes por descargar', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NOEXISTE', N' no existe', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NOLATLONREGISTRADA', N'EL REGISTRO NO TIENE LATITUD Y LONGITUD REGISTRADA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NOM_PRODUCTO', N'Nombre Producto', N'w')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NOMBRE', N'Nombre', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NOMBRE_CLIENTE', N'NOMBRE CLIENTE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NOMBRE_CORTO', N'Nombre Corto', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NOMBRE_GRUPO', N'Nombre Grupo', N'w')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NOMBRE_PRESENTACION', N'Nombre Presentación', N'w')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NOMBREGRUPO', N'Nombre Grupo', N'w')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NOPEDIDO_DETNOPEDIDO', N'No pedido |detalle del no pedido', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NOPEDIDOSULT5D', N'No pedidos en los ultimos 5 dias', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NOPOSICIONESFILTRO', N'No se encuentran posiciones para el filtro indicado', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NOPOSICIONESREGISTRADAS', N'NO SE ENCONTRARON POSICIONES REGISTRADAS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NOTIENEGPS', N'No tiene GPS ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NRO_DOCUMENTO', N'NRO DOCUMENTO', N'w')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NSERVICES', N'Seguimiento de Ruta', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NSERVICES_DESACTIVADO', N'Falta configurar Seguimiento de Ruta. Por favor contáctese con su consultor de datos de {0} del Perú', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NSERVICES_EMBEBIDO', N'Es embebido', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NSERVICES_EMBEBIDO_REFRESH', N'Debe refrescar la página luego de guardar para habilitar esta opción', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NSERVICES_OBLIGA', N'{0} obligatorio', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NUEVA_CARGA', N'Nueva Carga', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NUEVADIRECCION', N'Nueva dirección', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NUEVARUTA', N'Nueva ruta', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NUEVARUTAGUARDADO', N'Nueva ruta guardado correctamente', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NUEVO', N'Nuevo', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NUEVOACTIVIDAD', N'Nueva Actividad', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NUEVOALMACEN', N'Nuevo almacén', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NUEVOCLIENTE', N'Nuevo cliente', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NUEVOCLIENTEGUARDADO', N'Nuevo cliente guardado correctamente', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NUEVOCOBRANZA', N'Nueva Cobranza', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NUEVODETALLE', N'Nuevo Detalle', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NUEVOGENERAL', N'Nuevo general', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NUEVOGENERALGUARDADO', N'Nuevo general guardado correctamente', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NUEVOLISTAPRECIOGUARDADO', N'Nueva lista precio', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NUEVOPEDIDO', N'Nuevo Pedido', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NUEVOPRECIO', N'Nuevo Precio', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NUEVOPRODUCTO', N'Nuevo producto', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NUEVOPRODUCTOGUARDADO', N'Nuevo producto guardado correctamente', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NUEVOREGGENERAL', N'Nuevo Registro', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NUEVOUSUARIO', N'Nuevo usuario', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NUEVOUSUARIOGUARDADO', N'Nuevo usuario guardado correctamente', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NUMCANJEUPPERCASE', N'NUM. CANJE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NUMDEVOLUCIONUPPERCASE', N'NUM. DEVOLUCION', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NUMERO', N'Número', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NUMNOPEDIDOUPPERCASE', N'NUM. NO PEDIDO ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_NUMPEDIDOUPPERCASE', N'NUM. PEDIDO ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_OBSERVACION', N'OBSERVACIÓN', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_OBTENER', N'Obtener', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_OK_ARCHIVO', N'[OK] Archivo:', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_OPERACION', N'Operacion', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_OPERADOR0', N'Entel', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_OPERADOR1', N'Nextel', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_PAGADO', N'Pagado', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_PAGINACION', N'Paginación', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_PAGOSDECOBRANZA', N'Pagos de la cobranza', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_PEDIDO_DETPEDIDO', N'Pedido |detalle del pedido', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_PEDIDOS_EDITADOS', N'Ediciones', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_PEDIDOSDELDIAMES', N'Pedidos del dia por vendedor durante el mes', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_PEDIDOSHOY', N'Pedidos de hoy', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_PEDTOTAL_DETPEDTOTAL', N'Pedido Total |detalle del pedido total', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_PERFIL', N'Perfil', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_PIE_BR', N'Por favor,sirvase leer nuestro convenio para el uso y privacidad del sitio.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_PIE_P', N'PRODUCTO / Pedidos {0} Perú. Derechos reservados.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_PIE2_BR', N'', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_PORFAVOR_REVISARLO', N'Por favor,revisarlo', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_PRECIO', N'Precio', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_PRECIO_EDITABLE', N'Precio Editable', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_PRECIO_POR', N'Precio por', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_PRECIO_PRESENTACION', N'Precio Presentación', N'w')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_PRECIO_UNIDAD', N'Precio Unidad', N'w')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_PRECIO_VALIDAR', N'Validar Precio', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_PRECIOBASE', N'Precio base', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_PRESENTACION', N'Presentación', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_PRO_MAS_VENDIDOS', N'Productos más Vendidos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_PRODUCTO', N'Producto', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_PRODUCTOS_DESHA', N'Productos deshabilitados', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_PUNTOS', N'Puntos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_REGISTROS', N'registros', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_REGISTROS_ACTUALIZADOS', N' registos actualizados', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_REGISTROS_INSERTADOS', N' registros insertados', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_REMOVIO_ARCHIVO', N'Se removió un archivo de la lista', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_REPORTE_EQUIPO', N'Reporte Equipo', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_REPORTECANJE', N'REPORTE CANJE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_REPORTECOBRANZA', N'Reporte Pagos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_REPORTECONSOLIDADO', N'Reporte Consolidado', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_REPORTEDECANJE', N'Reporte de Canje', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_REPORTEDECOBRANZA', N'Reporte de Pagos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_REPORTEDEDEVOLUCION', N'Reporte de devolución', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_REPORTEDENOPEDIDOS', N'Reporte de no pedidos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_REPORTEDEPEDIDOS', N'Reporte de Pedidos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_REPORTEDEPEDIDOTOTAL', N'Reporte de pedido total', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_REPORTEDEVOLUCION', N'REPORTE DEVOLUCIÓN', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_REPORTEFAMILIA', N'Reporte de Familia Productos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_REPORTEMARCA', N'Reporte de Marca Productos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_REPORTENOPEDIDOS', N'REPORTE NO PEDIDOS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_REPORTEPEDIDOS', N'REPORTE PEDIDOS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_REPORTEPEDIDOTOTAL', N'REPORTE PEDIDO TOTAL', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_REPORTEQUIEBRESTOCK', N'Reporte De Stock de Quiebres', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_REPORTERESERVA', N'Reporte de Reserva', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_RESTAURA', N'Restaurar', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_RESTAURAR', N'Restaurar Registro(s)', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_RESTAURAR_TODO', N'Restaurar Todo', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_RESTAURAR_TODO_DATOS', N'Restaurar todo los Datos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_RESTRICTIVO', N'Restrictivo', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_RUTA', N'Ruta', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_RUTA_DETRUTA', N'Ruta |detalle de la ruta', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_RUTA_EXCEL_EJEMPLO', N'estandares/v2_10r/es/order/excel_ejemplo.zip', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_RUTA_GENERADOR', N'estandares/Iorder/GeneradorArchivosIOrder.zip', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_RUTA_ICONO_LOGO', N'nextel-logo.jpg', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_RUTA_MANUAL', N'Manual_Usuario_ES.zip', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_RUTA_MANUAL_IT', N'Manual_IT_ES.zip', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_RUTA_MANUAL_PROCEDURE', N'estandares/V2/Iorder/Manual_procedures_ES.zip', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_RUTA_MANUAL_TECNICO', N'estandares/v2_10r/es/order/ManualTecnico.zip', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_RUTA_MANUAL_USUARIO', N'estandares/v2_1r/es/order/ManualUsuario.zip', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_RUTA_TERMINOS_Y_CONDICIONES', N'', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_RUTA_WIN_SERVICE', N'estandares/Iorder/ServicioWindowsIOrderV25.zip', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_RUTAVENDEDOR', N'Ruta del Vendedor', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_SALDO', N'Saldo', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_SALIR', N'Salir', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_SATELITE', N'Satélite', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_SEGMENTO_DEFAULT', N'BODEGA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_SELE_ICONO_G', N'Seleccione el icono a guardar', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_SELECCIONAR', N'Seleccionar', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_SELECCIONAR_RUTA', N'Seleccione al menos un día de ruta', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_SELECCIONE', N'Seleccione', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_SELECCIONE_ARCHIVO', N'seleccione el archivo y luego haga click en ejecutar', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_SELECCIONECONFIGURACION', N'Seleccione la configuración a guardar', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_SENAL', N'Señal', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_SERIE', N'SERIE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_SERVICIO_WINDOWS', N'Servicio de Windows', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_SESSIONFINALIZADO', N'La sessión ha finalizado', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_SHOW', N'Mostrar', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_SI', N'SI', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_SIN_DATOS', N'Sin datos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_SINBONIFICACION', N'Sin Bonificacion', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_SKU_DEMANDADOS', N'SKU más Demandados', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_SKU_MAS_ROTACION', N'SKU con mayor Rotación', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_SKU_MAS_VENDIDOS', N'SKU más Vendidos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_SOLO_BANCO', N'Solo para bancos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_STOCK', N'Stock', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_STOCK_ALL', N'Validar Stock', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_SUBIDO', N'SUBIDO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_SUBIDOS', N'#Subidos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_SUBIR_ICONO', N'Subir icono', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_SUPERVISOR', N'Supervisor', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_TABLA', N'Tipo', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_TELEFONO', N'Telefono', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_TERMINOS_Y_CONDICIONES', N'Terminos y condiciones', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_TIENE', N' TIENE ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_TIPO', N'tipo', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_TIPO_CANAL', N'tipo', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_TIPO_CARGA', N'TIPO CARGA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_TIPO_CLIENTE', N'TIPO CLIENTE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_TIPO_DOCUMENTO', N'FORMA PAGO - COBRANZA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_TIPO_GRUPO', N'TIPO GRUPO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_TIPOCLIENTE', N'Tipo Cliente', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_TITULO', N'PEDIDOS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_TITULO_DESCUENTO_VOL', N'Descuentos Por Volumen', N'w')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_TODOS', N'TODOS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_TOPPRODUCTOSMES', N'Top de productos vendidos del Mes', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_TOTAL', N'Total', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_TOTAL_CARGADOS', N'#TOT. CARGADOS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_TOTAL_NO_CARGADOS', N'#TOT. NO CARGADOS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_TOTAL_PRODUCTOS', N'Total de Productos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_TOTAL_VENTA', N'Total', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_TOTALBONIFICACION', N'Total Bonificacion', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_TRACKINGCANJE', N'TRACKING DE CANJE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_TRACKINGCOBRANZA', N'TRACKING DE COBRANZA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_TRACKINGDEVOLUCION', N'TRACKING DE DEVOLUCIÓN', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_TRACKINGNOPEDIDO', N'TRACKING DE NO PEDIDO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_TRACKINGPEDIDO', N'TRACKING DE PEDIDO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_UBIQUE_ARCHIVO', N'UBIQUE EL ARCHIVO (formatos permitidos TXT,ZIP peso max. 1 mb)', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ULT_FEC_REGISTRO', N'Ultima Fecha de Registro', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_ULT_POS_EQUIPO', N'Ultima Posición del Equipo', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_UNID_FRACIONAMIENTO', N'Unid Fraccionamiento', N'w')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_UNIDAD_FRACCIONAMIENTO', N'Unidad Fraccionamiento', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_UNIDADDEFECTO', N'Unidad defecto', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_UNIDADES', N'Unidades', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_UNIDADES_VENDIDAS', N'UNIDADES VENDIDAS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_UNIDADMEDIDA', N'Unidad medida', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_USUARIO', N'Usuario', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_USUARIO_ALERTA', N'Ingrese el usuario', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_USUARIOS_DESHA', N'Usuarios deshabilitados', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_VALIDARDESC', N'ERROR: DEBE INGRESAR UN DESCUENTO NO MAYOR A 100%', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_VALIDARSTOCK', N'Consultar Stock en línea', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_VEND_MAS_VENTAS', N'Vendedores con mayores Ventas', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_VENDEDOR', N'Vendedor', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_VENTAS_TOTALES', N'Ventas Totales', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_VENTAS_TOTALES_PRODUCTO', N'VENTAS TOTALES POR PRODUCTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_VENTASULT30D', N'Ventas los ultimos 30 dias', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_VERGRAFICO', N'Ver gráfico', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_VOLVER', N'ATRAS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'ES', N'WEB_VOUCHER', N'Voucher', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'MX', N'JAVA_ARTICULO_EXISTE_CANJE', N'El articulo ya existe en el Cambio', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'MX', N'JAVA_ATENCION_ENVIARA_CANJES', N'WARNING: Se enviaran los cambios pendientes de envío. Desea continuar?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'MX', N'JAVA_ATENCION_NOENVIARA_CANJES', N'ATENCION: No hay cambios pendientes de envío', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'MX', N'JAVA_CANJE', N'Cambio', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'MX', N'JAVA_CANJE_REGISTRADO', N'Cambio Registrado', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'MX', N'JAVA_CANJES', N'Cambio', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'MX', N'JAVA_CONFIRMAR_CANCELAR_CANJE', N'Esta seguro que desea cancelar el Cambio?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'MX', N'JAVA_CONFIRMAR_FINALIZAR_CANJE', N'Esta seguro de finalizar el Cambio?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'MX', N'JAVA_CONTENIDO_AYUDA', N'Se debe sincronizar los datos antes de usar la aplicacion. \n\nEl usuario puede realizar: pedido, no pedido, cambios, devoluciones, cobranzas \n(si existe cobranzas pendientes al cliente  seleccionado). \n\nEl usuario tiene otras opciones como son: \n * Envíos: enviar pedidos pendientes al servidor \n * Consolidado: visualizar  acciones realizada por usuario en el día \n * Eficiencia: visualizar indicadores de gestión diaria del usuario \n * Stock: visualizar el stock de producto seleccionado \n \nMientras la aplicación utiliza el canal de datos otros servicios están siendo bloqueados. \n\nIngreso Rápido Productos: Ubicarse columna CODIGO e ingresar código producto,cantidad y descuento,automáticamente se generará precio de acuerdo  a la cantidad y descuento solicitado. \n (OK) funciona para agregar fila \n (#) funciona para eliminar fila \n\nEl Equipo debe esperar un promedio de 90 Seg para capturar primera posición GPS valida \n El Tiempo de espera para la toma de GPS es 30 Seg \n\nEmail: dsg@nextel.com.mx', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'MX', N'JAVA_ITEMS_CANJE', N'Items Cambio', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'MX', N'JAVA_MOTIVO_CANJE', N'Motivo Cambio', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'MX', N'JAVA_NOATENDIO_CANJE', N'No se pudo atender el Cambio', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'MX', N'JAVA_PROD_CANJE', N'Prod. Cambio', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'MX', N'JAVA_PRODUCTO_EXISTE_CANJE', N'El producto ingresado ya existe en el Cambio actual', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'MX', N'JAVA_SELECCION_MOTIVO_CANJE', N'Debe seleccionar el motivo de Cambio', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'MX', N'WEB_CANJE_DETCANJE', N'Cambio |detalle del Cambio', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'MX', N'WEB_CLIENTES_MAS_CANJES', N'Clientes con más Cambios', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'MX', N'WEB_CLIENTESMASCANJES30D', N'clientes que registran mas Cambios(30 dias)', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'MX', N'WEB_DESCARGA_CANJE_COL01', N'COD_CAMBIO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'MX', N'WEB_DESCARGA_DETALLE_CANJE_COL01', N'COD_CAMBIO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'MX', N'WEB_GRAFICOCANJE', N'Gráfico Cambio', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'MX', N'WEB_LOGOEMPRESA', N'nSolutions Nextel de México', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'MX', N'WEB_LOGOEMPRESA_PIE_ALT', N'NEXTEL DE MÉXICO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'MX', N'WEB_NAV_CANJE', N'Cambio', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'MX', N'WEB_NUMCANJEUPPERCASE', N'NUM. CAMBIO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'MX', N'WEB_PIE_BR', N'Paseo de los Tamarindos No. 90. Col. Bosques de las Lomas. Del. Cuajimalpa. C.P. 05120. México, D.F. México-. 1998.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'MX', N'WEB_PIE_P', N'Derechos Reservados ©. Comunicaciones Nextel de México, S.A. de C.V.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'MX', N'WEB_REPORTECANJE', N'REPORTE CAMBIO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'MX', N'WEB_REPORTEDECANJE', N'Reporte de Cambio', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'MX', N'WEB_RUTA_GENERADOR', N'estandares/V2/NOrder/GeneradorArchivos_MX.zip', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'MX', N'WEB_RUTA_MANUAL', N'Manual_Usuario_MX.zip', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'MX', N'WEB_RUTA_MANUAL_IT', N'Manual_IT_MX.zip', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'MX', N'WEB_RUTA_MANUAL_PROCEDURE', N'estandares/V2/NOrder/Manual_procedures_MX.zip', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'MX', N'WEB_RUTA_MANUAL_TECNICO', N'estandares/v2_10r/mx/order/ManualTecnico.zip', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'MX', N'WEB_RUTA_MANUAL_USUARIO', N'estandares/v2_1r/mx/order/ManualUsuario.zip', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'MX', N'WEB_RUTA_WIN_SERVICE', N'estandares/V2/NOrder/ServicioWindows_MX.zip', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'MX', N'WEB_TITULO', N'PEDIDOS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'MX', N'WEB_TRACKINGCANJE', N'TRACKING DE CAMBIO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PE', N'WEB_ESLOGAN_01', N'Conéctate A {0}', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PE', N'WEB_ESLOGAN_02', N'Conéctate A Tu Mundo.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PE', N'WEB_NSERVICES', N'Seguimiento de Ruta', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PE', N'WEB_NSERVICES_DESACTIVADO', N'Falta configurar Seguimiento de Ruta. Por favor contáctese con su consultor de datos de {0} del Perú', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PE', N'WEB_NSERVICES_EMBEBIDO', N'Es embebido', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PE', N'WEB_NSERVICES_EMBEBIDO_REFRESH', N'Debe refrescar la página luego de guardar para habilitar esta opción', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PE', N'WEB_NSERVICES_OBLIGA', N'{0} obligatorio', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PE', N'WEB_OPERADOR0', N'Entel', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PE', N'WEB_OPERADOR1', N'Nextel', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PE', N'WEB_RUTA_ICONO_LOGO', N'nextel-logo-footer.jpg', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_ADDYUPDSTOCK', N'Adic./Estoque', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_AGREGAR', N'Adicionar', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_ARTICULO', N'Produto', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_ARTICULO_EXISTE_CANJE', N'O produto já existe na Troca', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_ARTICULO_EXISTE_DEVOLUCION', N'O produto já existe na Devolução', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_ARTICULO_EXISTE_PEDIDO', N'O produto já existe na Ordem', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_ARTICULO_NO_PRECIO', N'Produto sem preço', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_ARTICULO_NO_PRECIO_ASIGNADO', N'O produto não tem preço determinado', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_ATENCION_BORRAR_CLIENTES', N'Aviso: Esta ação apagará todos os clientes anteriores. Você quer continuar?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_ATENCION_BORRAR_INFORMACION', N'Aviso: antes de sincronizar, envie os itens pendentes. Continuar sem enviar?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_ATENCION_BORRAR_PRODUCTOS', N'Aviso: Esta ação apagará todos os produtos anteriores. Você quer continuar?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_ATENCION_ENVIARA_CANJES', N'NOTA: Trocas pendentes serão enviadas. Você quer continuar?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_ATENCION_ENVIARA_DEVOLUCIONES', N'NOTA: Devoluções pendentes serão enviadas. Você quer continuar?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_ATENCION_ENVIARA_PAGOS', N'NOTA: Pagamentos pendentes serão enviados. Você quer continuar?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_ATENCION_ENVIARA_PEDIDOS', N'NOTA: Pedidos / sem pedidos serão enviados. Você quer continuar?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_ATENCION_ENVIARA_REGISTRO', N'NOTA: Os registros pendentes de envio serão enviados. Deseja continuar?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_ATENCION_ENVIARA_TODOS', N'NOTA: Dados pendentes serão enviados. Você quer continuar?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_ATENCION_NOENVIARA_CANJES', N'NOTA: Sem trocas pendentes de envio', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_ATENCION_NOENVIARA_DEVOLUCION', N'NOTA: Sem Devoluções pendentes de envio', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_ATENCION_NOENVIARA_PAGOS', N'NOTA: Sem Pagamentos pendentes de envio', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_ATENCION_NOENVIARA_PEDIDOS', N'NOTA: Sem pedidos pendentes de envio', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_ATENCION_NOENVIARA_TODOS', N'Nota: Sem dados pendentes de envio', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_ATENDIDO', N'Atendidos', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_ATRAS', N'Voltar', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_AYUDA', N'Ajuda', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_BANCOS', N'Bancos', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_BIENVENIDO', N'Bem vindo', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_BUSCAR', N'Buscar', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_BUSCAR_CLIENTES', N'Buscar Clientes', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_BUSCAR_X_NOMBRE', N'Buscar pelo Nome', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_CANCELAR', N'Cancel', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_CANJE', N'Troca', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_CANJE_REGISTRADO', N'Troca Registrada', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_CANJES', N'Trocas', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_CANT', N'Quant.', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_CANT_TTL', N'Quant. total', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_CANTIDAD_INCORRECTA', N'Quantidade Errada', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_CLAVE', N'Senha', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_CLIENTE', N'Cliente', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_CLIENTE_NOEXISTE', N'Cliente não existe', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_CLIENTES', N'Clientes', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_CLIENTES_ONLINE', N'Clientes - Em Linea', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_COBRANZA', N'Cobrança', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_COBRANZAS', N'Cobranças', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_COBRAR', N'Cobrar', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_COD_TCLIE', N'Canal', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_CODIGO', N'Código', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_COMPLETAR_DATOS', N'Completar Dados', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_CON_PEDIDO', N'COM ORDEM', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_CONDICION', N'Condição', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_CONFIRMAR_CANCELAR_CANJE', N'Tem certeza que deseja cancelar a troca?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_CONFIRMAR_CANCELAR_DEVOLUCION', N'Tem certeza que deseja cancelar a Devolução?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_CONFIRMAR_CANCELAR_PEDIDO', N'Tem certeza que deseja cancelar a ordem?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_CONFIRMAR_FINALIZAR_CANJE', N'Confirmar troca?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_CONFIRMAR_FINALIZAR_DEVOLUCION', N'Confirmar devolução', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_CONFIRMAR_FINALIZAR_NOPEDIDO', N'Nota: Tem certeza de que deseja concluir sem pedido?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_CONFIRMAR_FINALIZAR_NOPEDIDO2', N'Confirma finalização da sem visita?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_CONFIRMAR_FINALIZAR_PEDIDO', N'Confirma finalização da ordem?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_CONSOLIDADO', N'Consolidado', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_CONTENIDO_AYUDA', N'Antes de usar a aplicação, os dados devem ser importados através da opção sincronizar.\nO usuário pode executar: pedido, sem pedido, trocas, devoluções e cobranças (caso existam cobranças pendentes ao cliente selecionado).\nAs demais opções são: *enviar: para enviar os registros pendentes para o servidor; *consolidados: para visualizar as ações realizadas pelo usuário no dia; *eficiência: para visualizar indicadores da gestão diária do usuário; *estoque: para visualizar o estoque do produto selecionado.\nEnquanto o aplicativo está ativo, outros serviços são bloqueados.\nIntrodução rápida de produtos: na coluna código, digite o código do produto, quantidade e desconto, automaticamente o preço será calculado de acordo com a quantidade e descontos introduzidos.\n(ok) botão utilizado para adicionar itens;\n(#) botão utilizado para eliminar um item;\nPosição GPS: o aparelho deve esperar uma média de 90s para capturar a primeira posição GPS válida ao iniciar o aplicativo. O tempo de espera para a segunda captura é de 30s.\nMais dúvidas ligue 1050.', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_DEBE_SINCRONIZAR', N'Primeiro você tem que sincronizar', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_DES', N'Desc.', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_DESC_INCORRECTA', N'DESCONTO ERRADO TEM QUE SER ENTRE 0 a 100', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_DESCRIPCION', N'DESC.', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_DESCUENTO_PORCENTAJE', N'DESCONTO ATÉ 100', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_DETALLE_COBRANZA', N'Detalhe de Cobrança', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_DETALLE_PEDIDO', N'Detalhe Ordem', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_DEUDA', N'Dívida', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_DEVOLUCION', N'Devolução', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_DEVOLUCION_REGISTRADA', N'Devolução Registrada', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_DEVOLUCIONES', N'Devoluções', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_DIRECCION', N'Endereço', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_DOC', N'DOC', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_DOC_COBR', N'Doc. Cobr.', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_DOCUMENTO', N'Documento', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_DOCUMENTO_TITULO', N'Documento Responsável', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_EFEC', N'Eficiência', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_EFICIENCIA', N'Eficiência', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_ELIMINAR_FILA', N'utilizado para eliminar a fila', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_EMPRESA', N'Empresa', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_ENVIO_REGISTRADO', N'Envio registrado', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_ENVIOS', N'Enviar', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_ESTADO', N'Estado', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_FEC_VCMTO', N'Data Venc.(DD/MM/AAAA)', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_FECHA', N'Venc.', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_FECHA_INCORRECTA', N'Data incorreta', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_FINALIZAR', N'Finalizar', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_FINALIZAR_COBRANZA', N'Confirmar Cobranza?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_FUERARUTA', N'Fora da rota', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_GENERAL', N'Geral', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_GIRO', N'Segmento', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_INDICADOR', N'Indicador', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_INGRESAR_MAYOR_CERO', N'Você deve digitar um número maior do que zero', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_INGRESAR_MONTO_MAYOR_CERO', N'Você deve digitar Quantidade maior do que zero', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_INGRESE_CAMPOS', N'Digite todos os campos', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_INGRESE_CANTIDAD', N'Digite Quantidade', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_INGRESE_CODIGO', N'Você deve digitar o código', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_INGRESE_DATOS', N'Os dados de entrada', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_INGRESE_FECHA', N'Digite a data', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_INGRESE_MONTO', N'Digite Quantidade', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_INGRESE_TABELA', N'Digite Tabela', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_INGRESO', N'Entrar', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_INICIAR', N'Novo', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_ITEMS_CANJE', N'Itens Troca', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_ITEMS_DEVOL', N'Itens Dev.', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_ITEMS_PEDIDO', N'Itens Pedido', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_LOGIN', N'ENTRAR', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_MANTENIMIENTO', N'Manutenção', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_MENSAJE', N'Mensagem', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_MENU', N'Menu', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_MENU_PRIN', N'Menu Prin', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_MNT_COBR', N'Valor cobr.', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_MNT_TTL', N'Valor total', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_MONTO_MAYOR_SALDO', N'Montante superior ao Saldo', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_MONTO_PEDIDO', N'Valor pedido', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_MOTIVO', N'Motivo', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_MOTIVO_CANJE', N'Motivo Troca', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_MOTIVO_DEVOLUCION', N'Motivo Devolução', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_MOVIL', N'Móvel', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_MXPAG', N'VALOR TOTAL', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_NO', N'Não', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_NO_COBRANZAS', N'Sem cobrança', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_NO_CONEXION_SERVIDOR', N'Sem conexão com o servidor,Mas login sem cobertura bem sucedido,você pode trabalhar offline', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_NO_ENCONTRARON_RESULTADOS', N'Não foram encontrados resultados', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_NO_PEDIDO', N'Sem Ord.', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_NO_PEDIDO2', N'Sem Pedido', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_NO_PRECIO', N'Não tem preço', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_NOATENDIO_CANJE', N'Não foi possível realizar a Troca', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_NOATENDIO_DEVOLUCION', N'Não foi possível realizar a Devolução', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_NOATENDIO_PEDIDO', N'Não foi possível realizar a Ordem', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_NOCOBERTURA_ENVIO', N'Area sem cobertura,processar o envio através da opção de menu principal Enviar', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_NOCOBERTURA_SINCRONIZAR', N'Area sem cobertura,Não é possível sincronizar', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_NOEXISTE', N'Não existe', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_NOEXISTE_TABELA', N'Não existe tabela', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_NOMBRE', N'Nome', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_NOMBRECLIENTE', N'CLIENTE', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_NOPEDIDO_REGISTRADO', N'Visita sem ordem Registrada', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_NUMERO', N'Número', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_OBS', N'Obs.', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_PAGAD', N'PAGO', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_PAGO', N'Pagamento', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_PAGO_COBR', N'Pago', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_PAGO_REGISTRADO', N'Pagamento registrado', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_PAGOS', N'Pagamentos', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_PED', N'Ped', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_PEDI_NOPEDI', N'Ord/Sem Ord', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_PEDIDO', N'Pedido', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_PEDIDO_NO_DETALLE', N'A ordem não tem nenhum detalhe', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_PEDIDO_REGISTRADO', N'Ordem registrada', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_PEDIDOS', N'Pedidos', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_PEDIDOS_DIA', N'Pedidos Dia', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_POR_VISITAR', N'POR VISITAR', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_PRECIO', N'Preço', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_PROD_CANJE', N'Prod. Troca', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_PROD_DEVOLUCION', N'Prod. Devolução', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_PROD_PEDIDO', N'Produto', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_PROD_STOCK', N'Estoque', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_PRODUCTO', N'Produto', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_PRODUCTO_EXISTE', N'O produto já existe na atual ordem', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_PRODUCTO_EXISTE_CANJE', N'O produto já existe na atual troca', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_PRODUCTO_MOTIVO_EXISTEN', N'O produto e razão já existem', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_PRODUCTOS', N'Produtos', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_REGISTRADO', N'Registrado', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_SALDO', N'SALDO', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_SALIR', N'Sair', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_SEGUIR', N'Continuar', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_SEGURO_GRABAR', N'Tem certeza que quer gravar?', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_SELECCION', N'Selecio', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_SELECCION_MOTIVO_CANJE', N'Você deve selecionar o motivo para o troca', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_SELECCION_MOTIVO_DEVOLUCION', N'Você deve selecionar o motivo da devolução', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_SI', N'Sim', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_SIN_COBERTURA', N'área sem cobertura,tente sincronizar mais tarde', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_SIN_COBERTURA_CONSLTA', N'área sem cobertura,tente mais tarde', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_SINCRONIZAR', N'Sincronizar', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_SINCRONIZAR_CLIENTES', N'Os clientes devem ser sincronizados', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_STATUS_CONNECTED', N'Conectado', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_STATUS_CONNECTING', N'Conectando', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_STATUS_ERROR', N'Erro', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_STATUS_PROCESSING', N'Processando', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_STATUS_RECEIVING', N'Recebendo', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_STATUS_SAVING', N'Salvando', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_STATUS_SENDING', N'Enviando', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_STATUS_WAIT', N'ESPERE', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_STOCK', N'Estoque', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_STOCK_ABREV', N'E', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_STOCK_LINEA', N'Estoque em Linha', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_TAB', N'Tab.', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_TABELA', N'Tabela', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_TITULO', N'VENDA ONLINE', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_TODO', N'Tudo', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_TOT', N'Tot', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_TOTAL', N'Total', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_TOTAL_CLIENTES', N'Clientes', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_TPV', N'TMV (seg)', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_USUARIO', N'Usuário', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_USUARIO_PASS_INVALIDO', N'Usuário e/ou senha inválida', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_USUARIO_PASS_INVALIDO_CODIGO', N'Usuário e/ou senha inválida sem código', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_VALOR', N'Valor', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_VERSION', N'VERSÃO', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_VISITA_NO_PEDIDO', N'Visita Sem', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_VISITA_PEDIDO', N'Visita Ped.', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'JAVA_VISITADO', N'VISITADO', N'J')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ABREVIATURA', N'ABREVIATURA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ABRIRBARRA', N'Abrir barra', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ABRIRBUSCADOR', N'Abrir o Buscador', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ACTUALIZADOS', N'Atualizados', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ADMINISTRADOR', N'Administrador', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_AGREGAR', N'Adicionar', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_AGREGAR_ARCHIVO_MIN', N'Você deve adicionar pelo menos um arquivo', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ARCHIVOS', N'ARQUIVO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ARCHIVOS_EJECUTADO', N'Arquivos executados. Alguns arquivos com erros', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ARCHIVOS_EJECUTADO_CORRECT', N'Arquivos executados com êxito', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_AUMENTO_ARCHIVO', N'Um arquivo foi adicionado à lista', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_AYUDA', N'Ajuda', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_BANCO', N'Banco', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_BATERIA', N'Bateria', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_BIENVENIDO_H2', N'Bem vindo / Sessão', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_BIENVENIDO_P', N'Para acessar ao serviço Venda Online digite seu login de usuário e senha.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_BOLETA', N'BOLETA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_BONIFICACIONMANUAL', N'Manual Bônus', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_BONIFICACIONREP', N'Bônus', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_BUSCADOR', N'BUSCADOR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_BUSCAR', N'BUSCAR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_BUSCAR_LUPA', N'Buscar', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_BUSQUEDAGEORUTA', N'PESQUISA DE ROTA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CAMPOREQUERIDO', N'Este campo é requerido', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CANAL', N'Canal', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CANCELAR', N'CANCELAR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CANJE_DETCANJE', N'Troca |detalhe da troca', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CANJESULT5D', N'Canjes nos últimos 5 dias', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CANTIDAD', N'QUANTIDADE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CARGA_DETCARGA', N'Upload |detalhe da importação de arquivos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CARGA_EXITO', N' arquivos foi(ram) cargado(s) exitosamente:  ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CARGADETALLE', N'DETALHE CARGA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CARGAR', N'Cargar', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CARGAR_ARCHIVOS', N'Carregar arquivos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CERRAR', N'Fechar', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CLAVE', N'Senha', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CLAVE_ALERTA', N'Digite a senha', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CLAVE_NUMERICA', N'Os usuários móveis utilizam senha numérica', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CLI_DESHAB', N'Clientes desabilitados', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CLI_PEDIDOS', N'Clientes com mais Pedidos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CLI_PRO_MAS_VENDIDOS', N'Clie. com mais produtos vendidos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CLIENTE', N'Cliente', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CLIENTES_MAS_CANJES', N'Clientes com mais Trocas', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CLIENTES_MAS_DEVOLUCIONES', N'Clientes com mais Devoluções ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CLIENTES_MAS_VENTAS', N'Clientes com mais Vendas', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CLIENTESCONPEDIDO', N'Clientes com Ordems', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CLIENTESMASCANJES30D', N'Os clientes que registram mais Trocas(30 dias)', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CLIENTESMASDEV30D', N'Os clientes que registram mais Devoluções(30 dias)', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CLIENTESMASNOPED30D', N'Os clientes que registram mais Sem Ordens(30 dias)', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CLIENTESMASPAGOS30D', N'Os clientes que registram mais Pagamentos(30 dias)', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CLIENTESMASVIS30D', N'Clientes mais visitados em 30 dias', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_COBRANZA_DETCOBRANZA', N'Cobrança |detalle da cobrança', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_COBRANZAS_TOTALES', N'Cobranças Totais', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_COBRANZASHOY', N'Cobranças de hoje', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CODIGO', N'Código', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CODIGO_ALMACEN', N'CODIGO ARMAZEN', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CODIGO_CANAL', N'CODIGO CANAL', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CODIGO_CLIENTE', N'CÓDIGO CLIENTE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CODIGO_FAMILIA', N'CODIGO FAMILIA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CODIGO_GRUPO', N'CODIGO_GRUPO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CODIGO_MARCA', N'CODIGO BRAND', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CODIGO_PEDIDO_EDITADO', N'código de Edição', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CODIGO_PRODUCTO', N'CODIGO PRODUTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CODIGO_VENDEDOR', N'CÓDIGO VENDEDOR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CODIGOPRODUCTO', N'Código do Produto', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CONBONIFICACION', N'Com bônus', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_COND_VTA', N'CONDIÇÃO DE VENDA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CONDICION', N'CONDIÇÃO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CONDVENTAUPPERCASE', N'CONDIÇÃO DE VENDAS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CONDVTA', N'CONDIÇÃO DE VENDA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CONF_ICONO_GUARDADA', N'Configuración de icono guardada exitosamente', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CONFIG_CABECERA_DESCARGA', N'Download inclui cabecalho.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CONFIG_CANTIDAD_DECIMALES_CARGA', N'Casas decimais download', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CONFIG_CANTIDAD_DECIMALES_VISTA', N'Casas decimais vista', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CONFIG_COMA', N'Vírgula.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CONFIG_CONDVTA', N'Condição de Venda', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CONFIG_DECIMAL', N'Configuração decimal', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CONFIG_DESCUENTO', N'Desconto ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CONFIG_DESCUENTO_GENERAL', N'Discount Geral.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CONFIG_DESCUENTO_PRODUCTO', N'Discount Produto.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CONFIG_EFICIENCIA', N'Eficiência', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CONFIG_GENERAL', N'Configuração Geral', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CONFIG_GPSRUTA', N'GPS - Rota', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CONFIG_IDIOMA', N'Idioma', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CONFIG_MANT_DESCUENTO', N'Configuración descuento', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CONFIG_MONEDA', N'Moeda', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CONFIG_MONEDAIDIOMA', N'Configuração Moeda e Idioma.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CONFIG_MONTO', N'Monto', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CONFIG_MOVIL', N'Configuração movel', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CONFIG_PORCENTAJE', N'Percentagem', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CONFIG_PRECIO', N'Configuração preço', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CONFIG_PUNTO', N'Ponto', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CONFIG_RANGO_DESC', N'Faixa de Desconto (%)', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CONFIG_RANGO_MAX', N'Max', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CONFIG_RANGO_MIN', N'Min', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CONFIG_SIMBOLO', N'Configuração símbolo decimal', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CONFIG_TIPOCLI', N'Tipo de Cliente', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CONFIGURACIONGUARDADO', N'Configuração salvada corretamente', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_CONFIGURAR', N'Configurar', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DATOS_INVALIDOS', N'Existem dados inválidos em', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DEBE_TENER', N'  E DEVE TER ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESC', N'DESCONTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_CANJE', N'TROCA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_CANJE_COL01', N'COD_TROCA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_CANJE_COL02', N'COD_CLIENTE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_CANJE_COL03', N'COD_VENDEDOR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_CANJE_COL04', N'COD_EMPRESA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_CANJE_COL05', N'DAT_REGISTRO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_DETALLE_CANJE', N'DETALHE_TROCA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_DETALLE_CANJE_COL01', N'COD_TROCA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_DETALLE_CANJE_COL02', N'COD_CLIENTE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_DETALLE_CANJE_COL03', N'COD_EMPRESA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_DETALLE_CANJE_COL04', N'COD_VENDEDOR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_DETALLE_CANJE_COL05', N'COD_DETTROCA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_DETALLE_CANJE_COL06', N'COD_PRODUTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_DETALLE_CANJE_COL07', N'QUANTIDADE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_DETALLE_CANJE_COL08', N'COD_MOTIVO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_DETALLE_CANJE_COL09', N'OBSERVACAO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_DETALLE_CANJE_COL10', N'DAT_VENCIMENTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_DETALLE_DEVOLUCION', N'DETALHE_DEVOLUCAO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_DETALLE_DEVOLUCION_COL01', N'COD_DEVOLUCAO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_DETALLE_DEVOLUCION_COL02', N'COD_CLIENTE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_DETALLE_DEVOLUCION_COL03', N'COD_VENDEDOR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_DETALLE_DEVOLUCION_COL04', N'COD_EMPRESA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_DETALLE_DEVOLUCION_COL05', N'COD_DETDEVOLUCAO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_DETALLE_DEVOLUCION_COL06', N'COD_PRODUTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_DETALLE_DEVOLUCION_COL07', N'QUANTIDADE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_DETALLE_DEVOLUCION_COL08', N'COD_MOTIVO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_DETALLE_DEVOLUCION_COL09', N'OBSERVACAO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_DETALLE_DEVOLUCION_COL10', N'DAT_VENCIMENTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_DETALLE_PEDIDO', N'DETALHE_PEDIDO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_DETALLE_PEDIDO_COL01', N'COD_PEDIDO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_DETALLE_PEDIDO_COL02', N'COD_VENDEDOR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_DETALLE_PEDIDO_COL03', N'COD_CLIENTE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_DETALLE_PEDIDO_COL04', N'COD_EMPRESA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_DETALLE_PEDIDO_COL05', N'COD_DETPEDIDO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_DETALLE_PEDIDO_COL06', N'COD_PRODUTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_DETALLE_PEDIDO_COL07', N'QUANTIDADE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_DETALLE_PEDIDO_COL08', N'PRECO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_DETALLE_PEDIDO_COL09', N'DESCONTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_DETALLE_PEDIDO_COL10', N'TOTAL', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_DETALLE_PEDIDO_COL11', N'DESCRIÇÃO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_DETDESCARGA', N'Download |detalhe da exportação de arquivos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_DEVOLUCION', N'DEVOLUCAO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_DEVOLUCION_COL01', N'COD_DEVOLUCAO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_DEVOLUCION_COL02', N'COD_CLIENTE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_DEVOLUCION_COL03', N'COD_VENDEDOR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_DEVOLUCION_COL04', N'COD_EMPRESA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_DEVOLUCION_COL05', N'DAT_REGISTRO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_NOVENTA', N'SEMPEDIDO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_NOVENTA_COL01', N'COD_SEMPEDIDO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_NOVENTA_COL02', N'COD_CLIENTE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_NOVENTA_COL03', N'COD_VENDEDOR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_NOVENTA_COL04', N'COD_EMPRESA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_NOVENTA_COL05', N'COD_MOTIVO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_NOVENTA_COL06', N'DAT_REGISTRO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_PAGO', N'PAGO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_PAGO_COL01', N'COD_PAGAMENTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_PAGO_COL02', N'COD_VENDEDOR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_PAGO_COL03', N'MONTANTE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_PAGO_COL04', N'TIPO_PAGO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_PAGO_COL05', N'COD_BANCO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_PAGO_COL06', N'VOUCHER', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_PAGO_COL07', N'COD_EMPRESA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_PAGO_COL08', N'DAT_REGISTRO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_PEDIDO', N'PEDIDO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_PEDIDO_COL01', N'COD_PEDIDO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_PEDIDO_COL02', N'COD_VENDEDOR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_PEDIDO_COL03', N'COD_CLIENTE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_PEDIDO_COL04', N'COD_EMPRESA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_PEDIDO_COL05', N'COD_CONDVENDA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_PEDIDO_COL06', N'TOTAL', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_PEDIDO_COL07', N'DAT_REGISTRO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_PEDIDO_COL08', N'OBSERVAÇÃO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_PEDIDO_COL09', N'LATITUDE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGA_PEDIDO_COL10', N'LONGITUDE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGADETALLE', N'DETALHE DOWNLOAD', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGAEXITOSA', N'Download bem-sucedido', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGAR', N'Exportar', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCARGAR_INFORMACION', N'Exportar informação', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCPORCENTAJE', N'DESCONTO %', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCRIPCION', N'DESCRIÇÃO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCRIPCION_PRODUCTO', N'DESCRIÇÃO PRODUTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCUENTOMAX', N'Desconto Máximo.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESCUENTOMIN', N'Desconto Mínimo.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DESDE', N'De', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DETALLE', N'DETALHE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DETALLE_COBRANZA', N'detalle da cobrança', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DETALLE_ERROR', N'Detalhes do erro:', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DETALLE_GENERAL', N'detalhe da geral', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DETALLE_PEDIDOS_EDITADOS', N'Detalhe Editions', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DETALLE_PRODUCTOS', N'detalhe do produto', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DETALLECLIENTE', N'detalhe cliente', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DETALLECOBRANZA', N'Não foi executado nenhum arquivo', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DETALLECONFIGURACION', N'detalhe configuração', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DETALLEICONO', N'detalhe icono', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DETALLELISTAPRECIO', N'detalhe lista preços', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DETALLEPEDIDO', N'Detalhe ordem', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DETALLEPRODUCTO', N'detalhe produto', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DETALLERUTA', N'detalhe rota', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DETALLEUSUARIO', N'detalhe usuário', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DEVOLUCI_DETDEVOLUCI', N'Devolução |detalhe da devolução', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DEVOLUCIONESULT5D', N'Devoluções nos últimos 5 dias', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DIAS', N'Dia', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DIASEMANA_DO', N'Dom', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DIASEMANA_JU', N'Qui', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DIASEMANA_LU', N'Seg', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DIASEMANA_MA', N'Ter', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DIASEMANA_MI', N'Qua', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DIASEMANA_SA', N'Sab', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DIASEMANA_VI', N'Sex', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DIRECCION', N'Endereço', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DISTANCIA', N'DISTÂNCIA (m)', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_DISTRIBUIDORA', N'Distribuidor', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_EDITAR', N'Editar', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_EDITARACTIVIDAD', N'Editar atividade', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_EDITARALMACEN', N'Editar loja', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_EDITARCLIENTE', N'Editar cliente', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_EDITARCOBRANZA', N'Editar coleção', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_EDITARDETALLE', N'Editar Detalhes', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_EDITARGENERAL', N'Editar Register', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_EDITARLISTAPRECIO', N'Editar lista preços', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_EDITARPEDIDO', N'Editar a Ordem', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_EDITARPRECIO', N'Editar preço', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_EDITARPRODUCTO', N'Editar produto', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_EDITARRUTA', N'Editar rota', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_EDITARUSUARIO', N'Editar usuário', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_EJECUCION_EXITOSA', N'Execução bem-sucedida', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_EJECUCIONEXITO', N'Execução com êxito', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_EJECUTAR', N'Executar', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_EJECUTAR_ARCHIVOS', N'Executar arquivos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_EJEMPLO_XLS', N'Excel Exemplo', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ELIJAVALOR', N'Escolher um valor', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ELIMINAR', N'Excluir', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ELIMINAR_REGISTRO', N'Excluir Registro(s)', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ELIMINAR_TODO', N'Excluir Todo', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ELIMINAR_TODO_DATOS', N'Excluir todos os Dados', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_EMPRESA', N'Empresa', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ENCOBERTURA', N'Em Cobertura', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_EQUIPO', N'Aparelho', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ERR', N'ERRO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ERR_DATA', N'ERR_DATA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ERR_DUPLICADO', N'ERR_DUPLICADO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ERR_INVALIDO', N'ERR_INVALIDO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ERR_NO_DATA', N'ERR_ARQUIVO_SEM_DADOS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ERR_NUMERIC', N'ERR_NUMERICO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ERROR', N'Os dados inseridos são incorretos.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ERROR_ARCHIVO', N'ERRO _ARQUIVO ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ERROR_CARACTERES_ESPECIALES', N'ERRO: CARACTERES ESPECIAIS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ERROR_CARGA', N'Erro de carga', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ERROR_CARGANDO_ARCHIVO', N'Erro Carregando arquivo ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ERROR_DESCARGA_DATOS', N'Erro de dados de entrada: Verifique a data e hora.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ERROR_EJECUTAR_DTS', N'Erro em execução de DTS: <BR> A Variable Global', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ERROR_EN_ARCHIVO', N'Error no arquivo', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ERROR_EN_DATOS', N'Erro nos dados', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ERROR_ENCRIPTACION', N'Erro de Encriptação', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ERROR_ESTRUCTURA', N'ERRO_ESTRUTURA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ERROR_EXTENSION_ICONO', N'Solo archivos con extension jpg, jpeg, gif, png o bmp son permitidos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ERROR_INSERTANDO', N'Erro inserindo', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ERROR_NOSELECCIONADO', N'Erro: Você deve ter pelo menos um arquivo selecionado ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ERROR_NUM_COLUMNAS', N'ERR_NUMERO_COLUNAS:', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ERROR_SUBIENDO_ARCHI', N'Erro subiendo arquivos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ERROR_TAMANIO_ICONO', N'Tamaño máximo del archivo debe ser 400KB', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ERROR_VACIO', N'ERR_VAZIO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ERRORDTS_VARIABLESGLOBALES', N'Erro ao executar o DTS: O número de variáveis globais deve corresponder ao número de valores de entrada ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ERRORGRABAR', N'ERRO: AO GRAVAR INFORMAÇÃO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ESTADO', N'STATUS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_EUSUARIO_RESERVADO', N'USUÁRIO RESERVADOS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_FACTURA', N'FATURA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_FAMILIA', N'Família', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_FAMILIA_PRODUCTO', N'Família de novos produtos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_FECHA', N'Data', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_FECHA_REGISTRO', N'DATA REGISTRO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_FECHA_VENCIMIENTO', N'DATA VENCIMENTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_FECHA_VENCIMIENTO_CANJE', N'DATA VENCIMENTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_FECHAEDICION', N'Data de edição', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_FECHAFIN', N'Data final', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_FECHAINICIO', N'Data inicial', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_FECHAREGISTRO', N'Data de registo', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_FECHAVISITA', N'DATA VISITA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_FILA', N'LINHA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_FILENAME_ALMACEN', N'ARMAZEN', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_FILENAME_ALMACENPRODUCTO', N'ARMAZENPRODUTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_FILENAME_CLIENTE', N'CLIENTE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_FILENAME_COBRANZA', N'COBRANCA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_FILENAME_FAMILIA', N'FAMILIA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_FILENAME_FAMILIA_PRODUCTO', N'FAMILIA_PRODUCTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_FILENAME_GENERAL', N'GERAL', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_FILENAME_LISTAPRECIOS', N'LISTAPRECOS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_FILENAME_MARCA', N'BRAND', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_FILENAME_MARCA_PRODUCTO', N'BRAND_PRODUCTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_FILENAME_PRODUCTO', N'PRODUTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_FILENAME_RUTA', N'ROTA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_FILENAME_VENDEDOR', N'VENDEDOR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_FIN', N'Fim', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_FLAG_GENERADOR', N'0', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_FLAG_MOSTRARLOGOTOP', N'1', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_FORMATO', N'ERR_FORMATO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_FORMATO_INVAL_ARCHIV', N'Formato inválido do arquivo', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_FUERACOBERTURA', N'Cobertura fora', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GEN_ARCHIVOS', N'Gerador de arquivos ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GENERARREPORTE', N'Gerar Relatório', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GEO_GRAFNPANT', N'GRÁFICO JÁ EXISTE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GEOANALISIS_DETGEOANALISIS', N'Mapa  |Relatório de mapa', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GEOCERCA_NO_ENCONTRO_DIR', N'Não se Encontrou Endereço', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GEORUTA', N'GEOROTA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GIRO', N'SEGMENTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GPS', N'GPS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GRABAR', N'SALVAR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GRAFICO', N'GRÁFICO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GRAFICO_BARRAS', N'GRAFICO DE BARRAS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GRAFICO_DONA', N'GRAFICO ROSCA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GRAFICO_LINEA', N'GRAFICO DE LINHAS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GRAFICO_PIE', N'GRÁFICO PIZZA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GRAFICOCANJE', N'Gráfico Troca', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GRAFICOCOBRANZA', N'Gráfico Cobrança', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GRAFICODEVOLUCION', N'Gráfico Devolução', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GRAFICONOPEDIDO', N'Grafico Sem Ordem', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GRAFICOPEDIDO', N'Gráfico Ordem', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GRAFICOS_DETGRAFICOS', N'Gráficos|Gráficos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GRCLIENTE_CANJE_VS_CANTIDAD', N'TROCA VS QUANTIDADE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GRCLIENTE_COBRANZA_VS_MONTO', N'COBRANÇA VS VALOR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GRCLIENTE_DEVOLUCION_VS_CANTIDAD', N'DEVOLUÇÃO VS QUANTIDADE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GRCLIENTE_PEDIDO_VS_CANTIDAD', N'PEDIDO VS QUANTIDADE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GRCLIENTE_PEDIDO_VS_MONTO', N'PEDIDO VS VALOR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GRMAPA_CLIENTE_VS_CANTIDAD', N'CLIENTE VS QUANTIDADE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GRMAPA_CLIENTE_VS_MONTO', N'CLIENTE VS VALOR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GRMAPA_PRODUCTO_VS_CANTIDAD', N'PRODUTO VS QUANTIDADE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GRMAPA_PRODUCTO_VS_MONTO', N'PRODUTO VS VALOR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GRP_AVANCE', N'AVANÇO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GRP_FILTROOPERACION', N'Operação', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GRP_FILTROTIPO', N'Tipo', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GRP_INDICADOR', N'INDICADOR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GRP_INDICADORPRODUCTO', N'INDICADOR PRODUTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GRP_OPERACIONCANTIDAD', N'Quantidade', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GRP_OPERACIONMONTO', N'Valor', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GRP_TOP_CLIENTES', N'TOP CLIENTES', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GRP_TOP_PRODUCTOS', N'TOP PRODUTOS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GRP_TOP_VENDEDORES', N'TOP VENDEDORES', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GRPRODUCTO_CANJE_VS_CANTIDAD', N'TROCA VS QUANTIDADE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GRPRODUCTO_DEVOLUCION_VS_CANTIDAD', N'DEVOLUÇÃO VS QUANTIDADE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GRPRODUCTO_PEDIDO_VS_CANTIDAD', N'PEDIDO VS QUANTIDADE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GRPRODUCTO_PEDIDO_VS_MONTO', N'PEDIDO VS VALOR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GRVENDEDOR_CANJE_VS_CANTIDAD', N'TROCA VS QUANTIDADE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GRVENDEDOR_COBRANZA_VS_MONTO', N'COBRANÇA VS VALOR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GRVENDEDOR_DEVOLUCION_VS_CANTIDAD', N'DEVOLUÇÃO VS QUANTIDADE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GRVENDEDOR_PEDIDO_VS_CANTIDAD', N'PEDIDO VS QUANTIDADE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_GRVENDEDOR_PEDIDO_VS_MONTO', N'PEDIDO VS VALOR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_HABILITADO', N'Habilitado', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_HASTA', N'Até', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_HISTORIAL_EDICION_PEDIDOS', N'ordens editados', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_HOME_ASPX_BIENVENIDO', N'Bem vindo', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_HOME_ASPX_IR', N'Ir para:', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_HOME_ASPX_MENSAJE', N'Leia os manuais do usuário e de arquivos para compreender o funcionamento do sistema Venda Online.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ICONO', N'ICONO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ICONO_ACTUALIZADO_EXITOSAMENTE', N'Icono actualizado exitosamente', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_INDICADOR_CIRCULAR', N'INDICADOR CIRCULAR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_INDICADOR_SEMICIRCULAR', N'INDICADOR SEMICIRCULAR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_INDICADOR_TERMOMETRO', N'INDICADOR TERMÔMETRO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_INFORMACION', N'INFORMAÇÃO  ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_INGRESAR', N'ENTRAR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_INGRESECONCEPTO', N'Por favor,insira pelo menos um termo de pesquisa', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_INGRESEVALOR', N'Digite um valor.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_INICIO', N'Início', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_INSERTADOS', N'NOVOS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_IZQUIERDA', N'Esquerda', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_LATITUD', N'Latitude ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_LIMITE_INFERIOR', N'Limite Inferior', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_LIMITE_SUPERIOR', N'Limite Superior', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_LISTAPRECIO', N'Lista preços', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_LOGIN', N'Login', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_LOGIN_ERROR', N'OS DADOS SÃO INCORRETOS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_LOGINREPETIDO', N'O login {0} já existe.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_LOGOEMPRESA', N'SOLUÇÕES NEGÓCIO NEXTEL BRASIL', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_LOGOEMPRESA_PIE_ALT', N'NEXTEL BRASIL', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_LONGITUD', N'Longitude', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MANTENIMIENTO_CONFIGURACION', N'Manutenção de Configuração', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MANTENIMIENTO_ICONO', N'Mantenimiento icono', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MANTENIMIENTOALMACEN', N'Manutenção da loja', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MANTENIMIENTOCLIENTES', N'Manutenção Clientes', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MANTENIMIENTOFAMILIAPRODUCTOS', N'Família de produtos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MANTENIMIENTOPRODUCTOS', N'Manutenção productos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MANTENIMIENTORUTAS', N'Manutenção Rota', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MANTENIMIENTOUSUARIO', N'Manutenção Usuário', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MANUAL_IT', N'Manual Arquivos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MANUAL_PROCEDURES', N'Manual de Procedimentos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MAPA_GEOANALISIS', N'MAPA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MAPA_GEOANALISIS_DATO', N'Dado', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MAPA_GRAFICOS', N'Gráficos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MARCA', N'BRAND', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MENSAJE_ACCION', N'Tem certeza de que deseja', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MENSAJE_ACCIONREGISTRO', N'o registro selecionado?', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MENSAJE_ACCIONREGISTROS', N'todos os itens selecionados?', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MENSAJE_COMBINACION_LISTAPRECIOEXISTENTE', N'ERROR : PREçO DE LISTA DE EXISTENTE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MENSAJE_EXITO', N'Ela foi gravada corretamente.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MENSAJE_GUIA', N'Veja a nossa guia para o uso de {0} em Ajuda', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MENSAJE_SELECCION', N'Você não selecionou nenhum registro.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MENSAJECODIGOYAREGISTRADO', N'ERRO: O CODIGO {0} JÁ ESTÁ REGISTRADO NO BANCO DE DADOS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MENSAJEDATOOBLIGATORIO', N'ERROR: O DADO {0} É OBRIGATÓRIO.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MENSAJEDATOOBLIGATORIOSEL', N'ERRO: O DADO {0} É OBRIGATÓRIO SELECIONAR UMA OPÇÃO.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MENSAJEDATOSGRABADOS', N'INFORMAÇÃO CARREGADA CORRETAMENTE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MENSAJEDATOSINCOMPLETOS', N'ERROR: DADOS INCOMPLETOS: PELO MENOS UM TIPO DE CLIENTE OU UMA CONDIÇÃO DE VENDA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MENSAJEDATOSINCORRECTOS', N'ERRO: DADOS INCORRETOS: VERIFIQUE OS DADOS INSERIDOS.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MENSAJEDATOYAREGISTRADO', N'ERRO: O DADO {0} PARA O {1} ESCOLHIDO JÁ ESTÁ REGISTRADO NO BANCO DE DADOS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MENSAJEERRORBD', N'ERRO: NO BANCO DE DADOS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MENSAJEFECHASINCORRECTOS', N'VERIFIQUE A DATA INSERIDA.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MENSAJELOGINYAREGISTRADO', N'ERRO: O LOGIN DE {0} JÁ ESTÁ REGISTRADO NO BANCO DE DADOS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MENSAJENOEXISTENDATOS', N'Nenhuma informação foi encontrada', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MENSAJEOBLIGATORIO', N'Os Campos com asterisco (*) são obrigatórios', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MENSAJEREPITEASIG', N'ERRO: SE REPETE A ATRIBUIÇÃO DA ROTA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MESES', N'Mês', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_META', N'Meta', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MIDDLEWARE', N'Middleware', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MODELO', N'Modelo ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MODULOCARGADESCARGA', N'MODULO CARGA/DESCARGA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MODULODE', N'Módulo de', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MONITOREO', N'RASTREAMENTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MONT_CLIENTE', N'Quantidade por Cliente', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MONT_PRODUCTO', N'Quantidade por Produto', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MONT_VENDEDOR', N'Quantidade por Vendedor', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MONTO', N'VALOR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MONTO_PAGADO', N'MONTANTE PAGO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MONTO_TOTAL', N'MONTANTE TOTAL', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MOSTRAR', N'Mostrar', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MOSTRAR_ICONO', N'Mostrar icono', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MOTIVO', N'MOTIVO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MOTIVO_CANJE', N'Motivo Troca', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MOTIVO_NOPEDIDO', N'Motivo Sem pedido', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MOTIVO_VALOR', N'VALOR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MOTIVODEVOLUCION', N'MOTIVO DEVOLUÇÃO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_MSJ_YAEXISTEZONADESCRIPCION', N'ERRO: JÁ EXISTE UMA ZONA COM A DESCRIÇÃO ESCOLHIDA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NAV_CANJE', N'Troca', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NAV_CARGA', N'Upload', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NAV_CARGA_TAB', N'Arquivos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NAV_CARGADESCARGA', N'Arquivos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NAV_CLIENTES', N'Clientes', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NAV_COBRANZA', N'Cobrança', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NAV_CONFIGURACION', N'Manutenção', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NAV_DESCARGA', N'Download', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NAV_DEVOLUCION', N'Devolução', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NAV_GENERAL', N'Geral', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NAV_GEOANALISIS', N'Mapa', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NAV_GPSRUTA', N'Rota', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NAV_GRAFICOS', N'Gráficos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NAV_LISTAPRECIOS', N'Lista preços', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NAV_MANTENIMIENTO', N'Configuração', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NAV_NOPEDIDO', N'Sem pedido', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NAV_PEDIDO', N'Pedido', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NAV_PEDIDOTOTAL', N'Pedido total', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NAV_PRODUCTOS', N'Produtos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NAV_REPORTES', N'Relatórios', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NAV_RUTAS', N'Rotas', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NAV_USUARIOS', N'Usuários', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NEXTEL', N'NEXTEL', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NIN_ARCHIVO_EJECUTADO', N'Nenhum arquivo executado', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NINGUN_ARCHIVO', N'Você não selecionou nenhum arquivo', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NO', N'NÃO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NO_INFORMACION', N'INFORMAÇÃO SOLICITADA NÃO FOI ENCONTRADA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NO_UBICO', N'O endereço não foi encontrado', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NOCLIENTESVISITA', N'Não há clientes visitados no dia', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NOCOBRANZASHOY', N'Voce Ainda não fez cobranças hoje', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NODATOS_DESCARGAR', N'Não há nenhuma informação para este período.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NOEXISTE', N' não existe', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NOLATLONREGISTRADA', N'O REGISTRO NÃO TEM LATITUDE E LONGITUD REGISTRADA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NOMBRE', N'Nome', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NOMBRE_CLIENTE', N'NOME CLIENTE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NOMBRE_CORTO', N'Nome Curto', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NOPEDIDO_DETNOPEDIDO', N'Sem pedido |detalhe do sem pedido', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NOPEDIDOSULT5D', N'Sem Ordens nos últimos 5 dias', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NOPOSICIONESFILTRO', N'Nenhuna posição para o filtro especificado', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NOPOSICIONESREGISTRADAS', N'NÃO FORAM ENCONTRADAS POSIÇÕES REGISTADAS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NOTIENEGPS', N'Não tem GPS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NUEVA_CARGA', N'Nova carga', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NUEVARUTA', N'Nova rota', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NUEVARUTAGUARDADO', N'Nova rota salvada corretamente', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NUEVO', N'Novo', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NUEVOACTIVIDAD', N'Nova atividade', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NUEVOALMACEN', N'Nova loja', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NUEVOCLIENTE', N'Novo cliente', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NUEVOCLIENTEGUARDADO', N'Novo cliente salvado corretamente', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NUEVOCOBRANZA', N'Nova coleção', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NUEVODETALLE', N'New Detalhe', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NUEVOGENERAL', N'Novo geral', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NUEVOGENERALGUARDADO', N'Novo geral salvado corretamente', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NUEVOLISTAPRECIOGUARDADO', N'Nova lista preços', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NUEVOPEDIDO', N'Novo Ordem', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NUEVOPRECIO', N'Novo preço', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NUEVOPRODUCTO', N'Novo produto', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NUEVOPRODUCTOGUARDADO', N'Novo produto salvado corretamente', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NUEVOREGGENERAL', N'Novo Registo', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NUEVOSUARIO', N'Novo usuário', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NUEVOUSUARIO', N'Novo usuário', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NUEVOUSUARIOGUARDADO', N'Novo usuário salvado corretamente', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NUMCANJEUPPERCASE', N'NUM. TROCA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NUMDEVOLUCIONUPPERCASE', N'NUM. DEVOLUÇÃO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NUMERO', N'Número', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NUMNOPEDIDOUPPERCASE', N'NUM. SEM PEDIDO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_NUMPEDIDOUPPERCASE', N'NUM. PEDIDO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_OBSERVACION', N'OBSERVAÇÃO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_OBTENER', N'Obter', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_OK_ARCHIVO', N'[OK] Arquivo:', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_OPERACION', N'Operação', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_PAGADO', N'Pago', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_PAGINACION', N'Paginação', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_PAGOSDECOBRANZA', N'Pagamentos da cobrança', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_PEDIDO_DETPEDIDO', N'Pedido |detalhe do pedido', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_PEDIDOS_EDITADOS', N'edições', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_PEDIDOSDELDIAMES', N'Ordens do dia pelo vendedor durante o mês', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_PEDIDOSHOY', N'Ordens de hoje', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_PEDTOTAL_DETPEDTOTAL', N'Pedido Total |detalhe do pedido total', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_PERFIL', N'Perfil', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_PIE_BR', N'Por favor, leia o nosso acordo de privacidade para o uso do site.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_PIE_P', N'PRODUCTO / Venda Online Nextel Brasil Todos os direitos reservados.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_PIE2_BR', N'', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_PORFAVOR_REVISARLO', N'Por favor,verifique', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_PRECIO', N'Preço', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_PRECIO_EDITABLE', N'Editável Preço', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_PRECIO_POR', N'Preço per', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_PRECIO_VALIDAR', N'Preço', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_PRECIOBASE', N'Preço base', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_PRO_MAS_VENDIDOS', N'Produtos mais Vendidos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_PRODUCTO', N'Produto', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_PRODUCTOS_DESHA', N'Produtos desabilitados', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_PUNTOS', N'Pontos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_REGISTROS', N'registros', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_REGISTROS_ACTUALIZADOS', N' registros atualizados', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_REGISTROS_INSERTADOS', N' registros inseridos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_REMOVIO_ARCHIVO', N'Um arquivo foi removido da lista', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_REPORTE_EQUIPO', N'Relatório do Aparelho', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_REPORTECANJE', N'RELATÓRIO TROCA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_REPORTECOBRANZA', N'Relatório Cobrança', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_REPORTEDECANJE', N'Relatório de Troca', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_REPORTEDECOBRANZA', N'Relatório de Cobrança', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_REPORTEDEDEVOLUCION', N'Relatório de devolução', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_REPORTEDENOPEDIDOS', N'Relatório de sem ordens', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_REPORTEDEPEDIDOS', N'Relatório de ordens', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_REPORTEDEPEDIDOTOTAL', N'Relatório de ordem total', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_REPORTEDEVOLUCION', N'RELATÓRIO DEVOLUÇÃO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_REPORTEFAMILIA', N'Relatório de familia productos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_REPORTEMARCA', N'Relatório da marca do produto', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_REPORTENOPEDIDOS', N'RELATÓRIO SEM ORDENS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_REPORTEPEDIDOS', N'RELATÓRIO ORDENS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_REPORTEPEDIDOTOTAL', N'RELATÓRIO ORDEM TOTAL', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_RESTAURAR', N'Restaurar', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_RESTAURAR_TODO', N'Restaurar Todo', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_RESTAURAR_TODO_DATOS', N'Restaurar todos os Dados', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_RUTA', N'Rota', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_RUTA_DETRUTA', N'Rota |detalhe da rota', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_RUTA_EXCEL_EJEMPLO', N'estandares/v2_10r/pt/venda_online/ManualTecnico.zip', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_RUTA_GENERADOR', N'estandares/V2/Venda_Online/GeradorArquivos.zip', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_RUTA_ICONO_LOGO', N'nextel-logo.jpg', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_RUTA_MANUAL', N'Manual_Usuario_PT.zip', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_RUTA_MANUAL_IT', N'Manual_IT_PT.zip', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_RUTA_MANUAL_PROCEDURE', N'estandares/V2/Venda_Online/Manual_procedures.zip', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_RUTA_MANUAL_TECNICO', N'estandares/v2_10r/pt/venda_online/ManualTecnico.zip', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_RUTA_MANUAL_USUARIO', N'estandares/v2_1r/pt/venda_online/ManualUsuario.zip', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_RUTA_TERMINOS_Y_CONDICIONES', N'estandares/V2_1/Venda_Online/TermosCondicoesV2.zip', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_RUTA_WIN_SERVICE', N'estandares/V2/Venda_Online/Servico_Windows.zip', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_RUTAVENDEDOR', N'Rota do vendedor', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_SALDO', N'Saldo', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_SALIR', N'Sair', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_SATELITE', N'Satélite', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_SEGMENTO_DEFAULT', N'PADRAO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_SELE_ICONO_G', N'Selecione o ícone para salvar', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_SELECCIONAR', N'Selecionar', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_SELECCIONAR_RUTA', N'Selecione pelo menos um dia de rota', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_SELECCIONE', N'Selecione', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_SELECCIONE_ARCHIVO', N'Selecione o arquivo e clique em executar', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_SELECCIONECONFIGURACION', N'Selecione a configuração a ser salva', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_SENAL', N'Sinal ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_SERIE', N'SERIE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_SERVICIO_WINDOWS', N'Serviço de Windows', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_SESSIONFINALIZADO', N'Sessão finalizada', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_SHOW', N'Apresentar', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_SI', N'SIM', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_SIN_DATOS', N'Sem dados', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_SINBONIFICACION', N'Nenhum bônus', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_SKU_DEMANDADOS', N'SKU com maior Demanda', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_SKU_MAS_ROTACION', N'SKU com maior Rotação', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_SKU_MAS_VENDIDOS', N'SKU de maior valor', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_SOLO_BANCO', N'Único banco', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_STOCK', N'Stock', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_STOCK_ALL', N'Estoque', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_SUBIDO', N'SUBIDO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_SUBIDOS', N'Itens', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_SUBIR_ICONO', N'Subir icono', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_SUPERVISOR', N'Supervisor', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_TABLA', N'Tabela', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_TELEFONO', N'Telefone ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_TERMINOS_Y_CONDICIONES', N'Termos e Condições', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_TIENE', N' TEM ', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_TIPO', N'tipo', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_TIPO_CANAL', N'canal', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_TIPO_CARGA', N'TIPO CARGA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_TIPO_CLIENTE', N'TIPO CLIENTE', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_TIPO_DOCUMENTO', N'FORMA PAGO - COBRANZA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_TIPO_GRUPO', N'TIPO GRUPO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_TITULO', N'PEDIDOS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_TODOS', N'TODOS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_TOPPRODUCTOSMES', N'Top dos produtos vendidos do mês', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_TOTAL', N'Total', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_TOTAL_CARGADOS', N'TOTAL CARREGADOS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_TOTAL_NO_CARGADOS', N'TOTAL NÃO CARREGADOS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_TOTAL_PRODUCTOS', N'Total de produtos', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_TOTAL_VENTA', N'Total venda', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_TRACKINGCANJE', N'LOCALIZAÇÃO DE TROCA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_TRACKINGCOBRANZA', N'LOCALIZAÇÃO DO COBRANÇA', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_TRACKINGDEVOLUCION', N'LOCALIZAÇÃO DE DEVOLUÇÃO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_TRACKINGNOPEDIDO', N'LOCALIZAÇÃO DE SEM PEDIDO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_TRACKINGPEDIDO', N'LOCALIZAÇÃO DO PEDIDO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_UBIQUE_ARCHIVO', N'LOCALIZE O ARQUIVO (formatos permitidos TXT,ZIP peso max. 1 mb)', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ULT_FEC_REGISTRO', N'Última Data de Registo', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_ULT_POS_EQUIPO', N'Última Posição do Aparelho', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_UNIDADDEFECTO', N'Unidade padrão', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_UNIDADES', N'Unidades', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_UNIDADES_VENDIDAS', N'UNIDADES VENDIDAS', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_UNIDADMEDIDA', N'Unidade medida.', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_USUARIO', N'Usuário', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_USUARIO_ALERTA', N'Introduza o usuário', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_USUARIOS_DESHA', N'Usuários desabilitados', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_VALIDARDESC', N'ERROR: TEM DE INTRODUZIR UM DESCONTO DE NÃO MAIS DO QUE 100%', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_VEND_MAS_VENTAS', N'Vendedores com maiores Vendas', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_VENDEDOR', N'Vendedor', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_VENTAS_TOTALES', N'Vendas Totais', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_VENTAS_TOTALES_PRODUCTO', N'TOTAL DE VENDAS POR PRODUTO', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_VENTASULT30D', N'Vendas os ultimos 30 dias', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_VERGRAFICO', N'Visualizar gráfico', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_VOLVER', N'VOLTAR', N'W')
GO
INSERT [dbo].[TBL_IDIOMA] ([CULTURE], [CODIGO], [DESCRIPCION], [TIPO]) VALUES (N'PT', N'WEB_VOUCHER', N'Recibo', N'W')
GO
SET IDENTITY_INSERT [dbo].[TBL_IDIOMAS] ON 

GO
INSERT [dbo].[TBL_IDIOMAS] ([IDIDIOMA], [IDIOMA], [ABREV]) VALUES (1, N'ENGLISH', N'EN')
GO
INSERT [dbo].[TBL_IDIOMAS] ([IDIDIOMA], [IDIOMA], [ABREV]) VALUES (2, N'PORTUGUES - BRASIL', N'PT')
GO
INSERT [dbo].[TBL_IDIOMAS] ([IDIDIOMA], [IDIOMA], [ABREV]) VALUES (3, N'ESPAÑOL - PERU', N'PE')
GO
INSERT [dbo].[TBL_IDIOMAS] ([IDIDIOMA], [IDIOMA], [ABREV]) VALUES (4, N'ESPAÑOL - MEXICO', N'MX')
GO
INSERT [dbo].[TBL_IDIOMAS] ([IDIDIOMA], [IDIOMA], [ABREV]) VALUES (5, N'ESPAÑOL - ARGENTINA', N'AR')
GO
INSERT [dbo].[TBL_IDIOMAS] ([IDIDIOMA], [IDIOMA], [ABREV]) VALUES (6, N'ESPAÑOL - CHILE', N'CL')
GO
SET IDENTITY_INSERT [dbo].[TBL_IDIOMAS] OFF
GO
SET IDENTITY_INSERT [dbo].[TBL_MONEDA] ON 

GO
INSERT [dbo].[TBL_MONEDA] ([IDMONEDA], [MONEDA]) VALUES (1, N'S/.')
GO
INSERT [dbo].[TBL_MONEDA] ([IDMONEDA], [MONEDA]) VALUES (2, N'$')
GO
INSERT [dbo].[TBL_MONEDA] ([IDMONEDA], [MONEDA]) VALUES (3, N'R$')
GO
SET IDENTITY_INSERT [dbo].[TBL_MONEDA] OFF
GO
SET IDENTITY_INSERT [dbo].[TBL_MOTIVO] ON 

GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (2, N'02', N'ERROR EN PEDIDO', N'N', N'T')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (3, N'03', N'PRODUCTO DETERIORADO', N'N', N'T')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (4, N'04', N'SIN EFECTIVO', N'N', N'T')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (5, N'05', N'DIRECCION INCORRECTA', N'N', N'T')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (6, N'06', N'CONDICION DE PAGO INCORRECTA', N'N', N'T')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (7, N'07', N'PEDIDO INCOMPLETO', N'N', N'T')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (8, N'08', N'FALLA DE FABRICA', N'N', N'T')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (9, N'13', N'SIN STOCK', N'N', N'T')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (10, N'16', N'ERROR DE PROMOCION', N'N', N'T')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (11, N'09', N'NO QUISO RECIBIR LA MERCADERIA', N'N', N'T')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (12, N'10', N'NO HIZO PEDIDO', N'N', N'T')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (13, N'11', N'POR DEUDA', N'N', N'T')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (14, N'12', N'FUERA DE RUTA', N'N', N'T')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (15, N'14', N'POR CAMBIO DE DOCUMENTO', N'N', N'T')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (16, N'15', N'PRODUCTO VENCIDO', N'N', N'T')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (17, N'001', N'CLIENTE SIN DINERO', N'N', N'F')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (18, N'002', N'CLIENTE CERRADO', N'N', N'F')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (19, N'003', N'NO QUIZO RECIBIR', N'N', N'F')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (20, N'004', N'NO HIZO PEDIDO', N'N', N'F')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (21, N'005', N'X CAMBIO FAC', N'N', N'F')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (22, N'006', N'POR CREDITO PENDIENTE', N'N', N'F')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (23, N'007', N'X CAMBIOS DE MERCADERIA', N'N', N'F')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (24, N'008', N'DIRECCION INCORRECTA', N'N', N'F')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (25, N'009', N'CLIENTE MOROSO', N'N', N'F')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (26, N'010', N'CLIENTE AUSENTE-NO DEJO ENCARGADO', N'N', N'F')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (27, N'011', N'NO SE UBICO DIRECCION', N'N', N'F')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (28, N'012', N'PEDIDO INCORRECTO AUMENTADO', N'N', N'F')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (29, N'013', N'FALTA TIEMPO TRANSPORTE', N'N', N'F')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (30, N'014', N'SOLICITO CREDITO', N'N', N'F')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (31, N'015', N'NO CONFORME CON PRECIOS', N'N', N'F')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (32, N'016', N'PEDIDO DUPLICADO', N'N', N'F')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (33, N'017', N'FUERA DE ZONA', N'N', N'F')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (34, N'018', N'PEDIDO INCORRECTO', N'N', N'F')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (35, N'020', N'PEDIDO INCOMPLETO', N'N', N'F')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (36, N'021', N'CLIENTE NO EXISTE', N'N', N'F')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (37, N'022', N'RECIBIO PEDIDO DE DISTRIBUIDOR ANTERIOR', N'N', N'F')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (38, N'023', N'VENDEDOR OFRECIO DSCTOS,BONIF.EXHIB.OBS.', N'N', N'F')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (39, N'050', N'ROBO', N'N', N'F')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (41, N'D001', N'MERCADERIA GOLPEADA', N'D', N'F')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (42, N'C001', N'MERCADERIA VENCIDA', N'C', N'F')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (43, N'0001', N'Tiene stock', N'N', N'F')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (44, N'0002', N'Sin Dinero', N'N', N'F')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (46, N'N002', N'CLIENTE AUSENTE', N'N', N'F')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (51, N'DUDEV1', N'DUDEV1', N'D', N'F')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (52, N'dpcanje', N'Dpcanje', N'C', N'F')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (53, N'01', N'LOCAL CERRADO', N'N', N'T')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (54, N'17', N'CHOFER NO PUSO MOTIVO', N'N', N'T')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (55, N'18', N'PEDIDO DUPLICADO', N'N', N'T')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (56, N'19', N'CLIENTE AUSENTE', N'N', N'T')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (57, N'20', N'FALTA DE TIEMPO', N'N', N'T')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (58, N'', N'', N'N', N'T')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (59, N'21', N'NUNCA RECIBE', N'N', N'T')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (60, N'22', N'ERROR IMPRESION', N'N', N'T')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (61, N'23', N'SIN REPARTO', N'N', N'T')
GO
INSERT [dbo].[TBL_MOTIVO] ([MOT_PK], [MOT_CODIGO], [MOT_NOMBRE], [MOT_TIPO], [FLGENABLE]) VALUES (62, N'ACEITES', N'ACEITES', N'N', N'F')
GO
SET IDENTITY_INSERT [dbo].[TBL_MOTIVO] OFF
GO
SET IDENTITY_INSERT [dbo].[TBL_PAIS] ON 

GO
INSERT [dbo].[TBL_PAIS] ([ID], [CODIGO], [NOMBRE], [LATITUD], [LONGITUD], [CULTUREMADRE]) VALUES (1, N'PE', N'PERÚ', N'-12.044425642104926', N'-77.04471575634764', N'ES')
GO
INSERT [dbo].[TBL_PAIS] ([ID], [CODIGO], [NOMBRE], [LATITUD], [LONGITUD], [CULTUREMADRE]) VALUES (2, N'MX', N'MÉXICO', N'19.42865014628925', N'-99.12841311645509', N'ES')
GO
INSERT [dbo].[TBL_PAIS] ([ID], [CODIGO], [NOMBRE], [LATITUD], [LONGITUD], [CULTUREMADRE]) VALUES (3, N'AR', N'ARGENTINA', N'-34.64258718224346', N'-58.4706114472656', N'ES')
GO
INSERT [dbo].[TBL_PAIS] ([ID], [CODIGO], [NOMBRE], [LATITUD], [LONGITUD], [CULTUREMADRE]) VALUES (4, N'CL', N'CHILE', N'-33.470743973917436', N'-70.64895617382811', N'ES')
GO
INSERT [dbo].[TBL_PAIS] ([ID], [CODIGO], [NOMBRE], [LATITUD], [LONGITUD], [CULTUREMADRE]) VALUES (5, N'EN', N'EEUU', N'40.61415984306802', N'-103.40057360546', N'EN')
GO
INSERT [dbo].[TBL_PAIS] ([ID], [CODIGO], [NOMBRE], [LATITUD], [LONGITUD], [CULTUREMADRE]) VALUES (6, N'PT', N'BRASIL', N'-15.844841533018792', N'-47.974550078124985', N'PT')
GO
SET IDENTITY_INSERT [dbo].[TBL_PAIS] OFF
GO
SET IDENTITY_INSERT [dbo].[TBL_PARAM_INDICADOR] ON 

GO
INSERT [dbo].[TBL_PARAM_INDICADOR] ([CodIndicador], [Indicador], [Limite_Inferior], [Limite_Superior], [Meta]) VALUES (1, N'Circular', N'20', N'70', N'8000')
GO
INSERT [dbo].[TBL_PARAM_INDICADOR] ([CodIndicador], [Indicador], [Limite_Inferior], [Limite_Superior], [Meta]) VALUES (2, N'Semi Circular', N'20', N'70', N'0')
GO
INSERT [dbo].[TBL_PARAM_INDICADOR] ([CodIndicador], [Indicador], [Limite_Inferior], [Limite_Superior], [Meta]) VALUES (3, N'Termometro', N'10', N'500', N'200')
GO
SET IDENTITY_INSERT [dbo].[TBL_PARAM_INDICADOR] OFF
GO
SET IDENTITY_INSERT [dbo].[TBL_POSICION_GRAFICOS] ON 

GO
INSERT [dbo].[TBL_POSICION_GRAFICOS] ([CodGrafiPos], [CodGraf], [Estado], [posicion], [CodUsuario]) VALUES (1, N'1', N'1', N'4', N'ADMIN')
GO
INSERT [dbo].[TBL_POSICION_GRAFICOS] ([CodGrafiPos], [CodGraf], [Estado], [posicion], [CodUsuario]) VALUES (2, N'2', N'1', N'3', N'ADMIN')
GO
INSERT [dbo].[TBL_POSICION_GRAFICOS] ([CodGrafiPos], [CodGraf], [Estado], [posicion], [CodUsuario]) VALUES (3, N'3', N'1', N'6', N'ADMIN')
GO
INSERT [dbo].[TBL_POSICION_GRAFICOS] ([CodGrafiPos], [CodGraf], [Estado], [posicion], [CodUsuario]) VALUES (4, N'4', N'1', N'2', N'ADMIN')
GO
INSERT [dbo].[TBL_POSICION_GRAFICOS] ([CodGrafiPos], [CodGraf], [Estado], [posicion], [CodUsuario]) VALUES (5, N'5', N'1', N'5', N'ADMIN')
GO
INSERT [dbo].[TBL_POSICION_GRAFICOS] ([CodGrafiPos], [CodGraf], [Estado], [posicion], [CodUsuario]) VALUES (6, N'6', N'1', N'1', N'ADMIN')
GO
INSERT [dbo].[TBL_POSICION_GRAFICOS] ([CodGrafiPos], [CodGraf], [Estado], [posicion], [CodUsuario]) VALUES (7, N'1', N'1', N'1', N'IORDER')
GO
INSERT [dbo].[TBL_POSICION_GRAFICOS] ([CodGrafiPos], [CodGraf], [Estado], [posicion], [CodUsuario]) VALUES (8, N'2', N'1', N'2', N'IORDER')
GO
INSERT [dbo].[TBL_POSICION_GRAFICOS] ([CodGrafiPos], [CodGraf], [Estado], [posicion], [CodUsuario]) VALUES (9, N'3', N'1', N'3', N'IORDER')
GO
INSERT [dbo].[TBL_POSICION_GRAFICOS] ([CodGrafiPos], [CodGraf], [Estado], [posicion], [CodUsuario]) VALUES (10, N'4', N'1', N'4', N'IORDER')
GO
INSERT [dbo].[TBL_POSICION_GRAFICOS] ([CodGrafiPos], [CodGraf], [Estado], [posicion], [CodUsuario]) VALUES (11, N'5', N'1', N'5', N'IORDER')
GO
INSERT [dbo].[TBL_POSICION_GRAFICOS] ([CodGrafiPos], [CodGraf], [Estado], [posicion], [CodUsuario]) VALUES (12, N'6', N'1', N'6', N'IORDER')
GO
SET IDENTITY_INSERT [dbo].[TBL_POSICION_GRAFICOS] OFF
GO
