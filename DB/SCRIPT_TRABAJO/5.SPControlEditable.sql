--select * from CFControl
IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'FlgEditable'
          AND Object_ID = Object_ID(N'dbo.CFControl'))
BEGIN
    alter table CFControl add FlgEditable char(1);
END
ELSE PRINT 'YA SE AGREGO LA COLUMNA FlgEditable A LA TABLA CFControl';

GO
/****** Object:  StoredProcedure [dbo].[spS_ManSelFormulario]    Script Date: 17/04/2019 04:27:21 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[spS_ManSelFormulario]   
   
AS    
BEGIN    
    
SET NOCOUNT ON    
 
 
 SELECT distinct IdFormulario FROM CFControl
where FlgHabilitado='T'
order by IDFORMULARIO

SELECT EtiquetaControl,IdTipoControl,MaxCaracteres,CFControl.IdGrupo,FlgObligatorio,Orden,IdFormulario,IdControl, CFGrupo.Nombre,ISNULL(FlgEditable,'') AS FlgEditable
FROM CFControl left join CFGrupo on CFControl.IdGrupo=CFGrupo.IdGrupo
where CFControl.FlgHabilitado='T'
order by IDFORMULARIO,orden

END
GO

--select * from cfcontrol
--exec spS_ManSelFormulario

GO
/****** Object:  StoredProcedure [dbo].[sps_ManInsControl]    Script Date: 22/04/2019 08:55:04 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[sps_ManInsControl]  
	@ESTADO VARCHAR(100),  
	@CONTROL VARCHAR(100),  
	@MAX VARCHAR(100),  
	@ETIQUETA VARCHAR(100),  
	@OBLIGATORIO VARCHAR(100),  
	@ORDEN INT ,
	@GRUPO VARCHAR(100),
	@EDITABLE VARCHAR(100)
AS  

	INSERT INTO .[CFControl]
           ([EtiquetaControl]
           ,[IdTipoControl]
           ,[MaxCaracteres]
           ,[IdGrupo]
           ,[FlgObligatorio]
           ,[Orden]
           ,[FlgHabilitado]
           ,[IdFormulario]
		   ,[FlgEditable])
     VALUES
           (@ETIQUETA
           ,@CONTROL
           ,@MAX
           ,@GRUPO
           ,@OBLIGATORIO
           ,@ORDEN
           ,'T'
           ,@ESTADO
		   ,@EDITABLE)


	DECLARE @UltimoOrdenPre INT
	DECLARE @UltimoOrdenPro INT
	declare @EtiquetabBusPre varchaR(200)
	declare @EtiquetabBusPro varchaR(200)
	declare @flagPre char(1)
	declare @flagPro char(1)

	set @flagPre=(select FlgHabilitado from TEMPColumnaArchivo where TipoArticulo='PRE' and NombreColumna=@ETIQUETA)
	set @flagPro=(select FlgHabilitado from TEMPColumnaArchivo where TipoArticulo='PRO' and NombreColumna=@ETIQUETA)

	SET @UltimoOrdenPre=(SELECT top  1   Orden FROM GRColumnaArchivo WHERE TipoArticulo='PRE' AND TipoColumna='C' AND IdArchivo='1' ORDER BY Orden desc)
	SET @UltimoOrdenPro=(SELECT top  1   Orden FROM GRColumnaArchivo WHERE TipoArticulo='PRO' AND TipoColumna='C' AND IdArchivo='1' ORDER BY Orden desc)

	SET @EtiquetabBusPre=(SELECT NombreColumna FROM GRColumnaArchivo WHERE TipoArticulo='PRE' AND TipoColumna='C' AND IdArchivo='1' AND NombreColumna=@ETIQUETA)
	SET @EtiquetabBusPro=(SELECT NombreColumna FROM GRColumnaArchivo WHERE TipoArticulo='PRO' AND TipoColumna='C' AND IdArchivo='1' AND NombreColumna=@ETIQUETA)

	if(@EtiquetabBusPre is null)
	begin
		if(@flagPre is null)
		begin
			INSERT INTO  GRColumnaArchivo(IdArchivo,NombreColumna,TipoArticulo,TipoColumna,Orden,FlgHabilitado)VALUES
								 ((SELECT IdArchivo FROM GRArchivo WHERE CodigoArchivo='PDO'),@ETIQUETA,'PRE','C',(@UltimoOrdenPre + 1),'T')
		end
		else
		begin
			INSERT INTO  GRColumnaArchivo(IdArchivo,NombreColumna,TipoArticulo,TipoColumna,Orden,FlgHabilitado)VALUES
								 ((SELECT IdArchivo FROM GRArchivo WHERE CodigoArchivo='PDO'),@ETIQUETA,'PRE','C',(@UltimoOrdenPre + 1),@flagPre)
		end
	end

	if(@EtiquetabBusPro is null)
	begin
		if(@flagPro is null)
		begin
			INSERT INTO  GRColumnaArchivo(IdArchivo,NombreColumna,TipoArticulo,TipoColumna,Orden,FlgHabilitado)VALUES
									 ((SELECT IdArchivo FROM GRArchivo WHERE CodigoArchivo='PDO'),@ETIQUETA,'PRO','C',(@UltimoOrdenPro + 1),'T')

		end
		else
		begin
		 INSERT INTO  GRColumnaArchivo(IdArchivo,NombreColumna,TipoArticulo,TipoColumna,Orden,FlgHabilitado)VALUES
									 ((SELECT IdArchivo FROM GRArchivo WHERE CodigoArchivo='PDO'),@ETIQUETA,'PRO','C',(@UltimoOrdenPro + 1),@flagPro)
	end
end

go

GO
/****** Object:  StoredProcedure [dbo].[USPS_JSON_SINCRONIZAR_TODO]    Script Date: 22/04/2019 11:35:15 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[USPS_JSON_SINCRONIZAR_TODO] 
 @PAR_CODUSUARIO as varchar(20)
--,
-- @PAR_FECHAMOVIL VARCHAR(20)
as
 BEGIN             
 SET DATEFIRST 1         
 
  
 DECLARE @DIA INT            
 SELECT @DIA = DATEPART(WEEKDAY,dbo.STRING_FECHA(getdate()))   
 
 
 --Validar Stock
 DECLARE @STOCK INT      
 SELECT 
	@STOCK = convert(INT, 
		CASE 
			WHEN Valor = 'T' 
				THEN 1 
				ELSE 0 
		END) 
 FROM 
	CFConfiguracion 
 WHERE 
	CodConfiguracion = 'STST'
	

 --Validar Precio mayor que 0
DECLARE @PRECIO INT      
 SELECT 
	@PRECIO = convert(INT, 
		CASE 
			WHEN Valor = 'T' 
				THEN 1 
				ELSE 0 
		END) 
 FROM 
	CFConfiguracion 
 WHERE 
	CodConfiguracion = 'PRVP'

 --Configuracion Precio - Tipo de Cliente
DECLARE @PRECIO_TIPOCLI INT      
SELECT 
	@PRECIO_TIPOCLI = convert(INT, 
		CASE 
			WHEN Valor = 'T' 
				THEN 1 
				ELSE 0 
		END) 
FROM 
	CFConfiguracion 
WHERE 
	CodConfiguracion = 'PRTC'

 --Configuracion Precio - Condicion de Venta
DECLARE @PRECIO_CONDVTA INT      
SELECT 
	@PRECIO_CONDVTA = convert(INT, 
		CASE 
			WHEN Valor = 'T' 
				THEN 1 
				ELSE 0 
		END) 
FROM 
	CFConfiguracion 
WHERE 
	CodConfiguracion = 'PRCV'
 
	IF OBJECT_ID('tempdb..#TmpTipoClienteRuta') IS NOT NULL BEGIN DROP TABLE #TmpTipoClienteRuta END; 
		CREATE TABLE #TmpTipoClienteRuta ( 
		ID BIGINT IDENTITY(1,1) NOT NULL, 
		TC_PK INT NULL)
	
	INSERT 
		#TmpTipoClienteRuta
	SELECT DISTINCT 
		tc.TC_PK 
	FROM 
		tbl_cliente tc
	JOIN tbl_ruta tr ON tr.cli_cod = tc.cli_codigo
    WHERE
	     tr.ven_cod = @PAR_CODUSUARIO

declare @tblcliente_temp as table (CLI_CODIGO VARCHAR(20));
 insert into @tblcliente_temp
 select distinct(R.CLI_COD) 
 FROM TBL_RUTA R              
  INNER JOIN TBL_CLIENTE C ON R.CLI_COD = C.CLI_CODIGO     
  LEFT JOIN TBL_COBRANZA CO ON C.CLI_PK = CO.CLI_PK AND C.CLI_CODIGO = CO.CLI_CODIGO
  LEFT JOIN TBL_TIPO_CLIENTE TC ON C.TC_PK = TC.TC_PK
  FULL OUTER JOIN TBL_PEDIDO P ON C.CLI_CODIGO = P.CLI_CODIGO AND P.PED_PK = (SELECT TOP 1 P.PED_PK FROM TBL_PEDIDO P WHERE P.CLI_CODIGO = C.CLI_CODIGO AND P.flgEnable = 'T' AND (P.PED_NOPEDIDO IS NULL OR P.PED_NOPEDIDO='') ORDER BY P.PED_PK desc) LEFT JOIN TBL_USUARIO U ON P.USR_CODIGO = U.USR_CODIGO               
  WHERE  R.VEN_COD = @PAR_CODUSUARIO
   AND SUBSTRING(R.RUTA_DIAVISITA,@DIA,1) = '1'
   AND C.FLGENABLE='T'
   AND R.FLGENABLE='T';
 
SELECT t.SCRIPT, t.orden FROM( 

 --RUTA
 SELECT 'DROP TABLE IF EXISTS TBL_CLIENTE;' as SCRIPT, 0 as orden    
 union    
 SELECT 'CREATE TABLE IF NOT EXISTS TBL_CLIENTE(RUTA_PK BIGINT, CLI_PK BIGINT, CLI_COD VARCHAR(20), CLI_NOMBRE VARCHAR(100), CLI_DIRECCION VARCHAR(100), TCLI_COD VARCHAR(20),TCLI_NOMBRE VARCHAR(20), CLI_GIRO VARCHAR(20), CLI_DEUDA VARCHAR(20), ESTADO VARCHAR(20), PED_FECREGISTRO VARCHAR(20), USR_CODIGO VARCHAR(20),CLI_CAMPO_ADICIONAL_1 VARCHAR(100),CLI_CAMPO_ADICIONAL_2 VARCHAR(100), CLI_CAMPO_ADICIONAL_3 VARCHAR(100),CLI_CAMPO_ADICIONAL_4 VARCHAR(100), CLI_CAMPO_ADICIONAL_5 VARCHAR(100),SALDO_CREDITO DOUBLE,LATITUD VARCHAR(50),LONGITUD VARCHAR(50), CONSTRAINT PkTblCliente PRIMARY KEY (RUTA_PK));' as SCRIPT, 1 as orden    


 union  
 SELECT 'INSERT INTO TBL_CLIENTE VALUES('+ CAST(R.RUTA_PK as VARCHAR(20)) + ',' +  CAST(C.CLI_PK as VARCHAR(20)) + ',''' + R.CLI_COD + ''',''' + UPPER(CLI_NOMBRE) + ''',''' + UPPER(CLI_DIRECCION) + ''',''' + UPPER(isnull(TC.TC_PK,'')) +''',''' +UPPER(isnull(TC.TC_NOMBRE,''))+ ''',''' + UPPER(isnull(C.CLI_GIRO,'')) + ''',''' + dbo.FORMATO_MONTO_VISTA(SUM(CONVERT(NUMERIC(18,6),COALESCE(COB_MONTOTOTAL,'0')) - CONVERT(NUMERIC(18,6),COALESCE(COB_MONTOPAGADO,'0')))) + ''',''POR VISITAR'''+','''+ isnull(dbo.FECHA_STRING(P.PED_FECREGISTRO),'') + ''',''' + isnull(U.USR_NOMBRE,'') + ''',''' + isnull(C.CampoAdicional1,'') + ''',''' + isnull(C.CampoAdicional2,'') + ''',''' +  isnull(C.CampoAdicional3,'') + ''',''' +  isnull(C.CampoAdicional4,'') + ''',''' +  isnull(C.CampoAdicional5,'')+''',''' +  cast((convert(float,isnull(C.LimiteCredito,''))- CONVERT(float, isnull(C.CreditoUtilizado,'')) - SUM(CONVERT(NUMERIC(18,6),COALESCE(COB_MONTOTOTAL,'0')) - CONVERT(NUMERIC(18,6),COALESCE(COB_MONTOPAGADO,'0')))) as varchar(20)) +''',''' +ISNULL(C.LATITUD,'')+''',''' +ISNULL(C.LONGITUD,'')+''');' AS SCRIPT, 2 as orden  
  FROM TBL_RUTA R              
  INNER JOIN TBL_CLIENTE C ON R.CLI_COD = C.CLI_CODIGO     
  LEFT JOIN TBL_COBRANZA CO ON C.CLI_CODIGO = CO.CLI_CODIGO AND CO.flgEnable = 'T'
  LEFT JOIN TBL_TIPO_CLIENTE TC ON C.TC_PK = TC.TC_PK
  FULL OUTER JOIN TBL_PEDIDO P ON C.CLI_CODIGO = P.CLI_CODIGO AND P.PED_PK = (SELECT TOP 1 P.PED_PK FROM TBL_PEDIDO P WHERE P.CLI_CODIGO = C.CLI_CODIGO AND P.flgEnable = 'T' AND (P.PED_NOPEDIDO IS NULL OR P.PED_NOPEDIDO='') ORDER BY P.PED_PK desc) LEFT JOIN TBL_USUARIO U ON P.USR_CODIGO = U.USR_CODIGO               
  WHERE  R.VEN_COD = @PAR_CODUSUARIO
   AND SUBSTRING(R.RUTA_DIAVISITA,@DIA,1) = '1'
   AND C.FLGENABLE='T'
   AND R.FLGENABLE='T'
 GROUP BY R.RUTA_PK, C.CLI_PK, R.CLI_COD, C.CLI_NOMBRE,           
        C.CLI_DIRECCION, TC.TC_NOMBRE, C.CLI_GIRO, P.PED_FECREGISTRO, U.USR_NOMBRE, C.CampoAdicional1, C.CampoAdicional2, 
        C.CampoAdicional3, C.CampoAdicional4, C.CampoAdicional5,C.LimiteCredito,C.CreditoUtilizado,
        C.LATITUD,C.LONGITUD, TC.TC_PK
 UNION
 
--Direcciones
 SELECT 'DROP TABLE IF EXISTS TBL_DIRECCION;' as SCRIPT, 0 as orden    
 union    
SELECT 'CREATE TABLE IF NOT EXISTS TBL_DIRECCION(DIR_INC INTEGER  PRIMARY KEY, DIR_PK BIGINT, CLI_CODIGO VARCHAR(20), DIR_NOMBRE VARCHAR(100), DIR_TIPO VARCHAR(1), FLGENVIADO VARCHAR(1), DIR_LATITUD VARCHAR(50), DIR_LONGITUD VARCHAR(50), DIR_COD VARCHAR(20), FECCREACION VARCHAR(20));' as SCRIPT, 1 as orden    
 union  
SELECT 'INSERT INTO TBL_DIRECCION (DIR_PK,DIR_COD,CLI_CODIGO, DIR_NOMBRE, DIR_TIPO, FLGENVIADO, DIR_LATITUD,DIR_LONGITUD) VALUES('+ CAST(D.DIR_PK as VARCHAR(20)) +',''' + D.DIR_CODIGO + ''',''' + D.CLI_CODIGO + ''',''' + UPPER(D.DIR_NOMBRE)+ ''',''' + D.DIR_TIPO + ''',''' + 'T' + ''',''' + ISNULL(D.LATITUD, '')+ ''',''' + ISNULL(D.LONGITUD, '') + ''');' AS SCRIPT, 2 as orden  
  FROM TBL_RUTA R              
  INNER JOIN TBL_CLIENTE C ON R.CLI_COD = C.CLI_CODIGO
  INNER JOIN TBL_DIRECCION D ON C.CLI_PK = D.CLI_PK
  WHERE  R.VEN_COD = @PAR_CODUSUARIO
  AND SUBSTRING(R.RUTA_DIAVISITA,@DIA,1) = '1'
   AND C.FLGENABLE='T'
   AND R.FLGENABLE='T'
   AND D.flgEnable = 'T'

 UNION
	
 --Productos
 SELECT 'DROP TABLE IF EXISTS TBL_PRODUCTO;' as SCRIPT, 0 as orden
 UNION    
 SELECT 'CREATE TABLE IF NOT EXISTS TBL_PRODUCTO(ID INTEGER not null, Codigo VARCHAR(20) not null,Nombre VARCHAR(150) not null,Stock VARCHAR(20) not null,UnidadDefecto VARCHAR(20) not null,PrecioBase VARCHAR(20) not null, DescuentoMinimo VARCHAR(50), DescuentoMaximo VARCHAR(50), CONSTRAINT PkTblProducto PRIMARY KEY (Codigo));' as SCRIPT, 1 as orden    
 UNION  
 SELECT 'INSERT INTO TBL_PRODUCTO VALUES('+CAST(PRO_PK as VARCHAR(20))+ ',''' + pro_codigo + ''',''' + UPPER(replace(pro_nombre,'-','')) + ''',''' + 
	
	dbo.FORMATO_MONTO_VISTA(
	
		CAST(COALESCE(PRO.PRO_STOCK, '0') AS NUMERIC(16,4)) - ISNULL(
		(
			SELECT 
				SUM(
					CAST(COALESCE(RES.DET_CANTIDAD, '0') AS NUMERIC(16,4))
					+
					CAST(COALESCE(RES.DET_BONIFICACION, '0') AS NUMERIC(16,4)) 
				)
			FROM
				TBL_RESERVA RES
			WHERE
				RES.PRO_CODIGO = PRO.PRO_CODIGO
			AND
				FLGENABLE = 'T'
		)
		, 0)
	) 
	+ ''',''' + pro_unidaddefecto + ''',''' + dbo.FORMATO_MONTO_VISTA(PRO_PRECIOBASE) + ''',''' + ISNULL(DESC_MIN, '0') + ''',''' + ISNULL(DESC_MAX, '0') + ''');' AS SCRIPT, 2 as orden  
 FROM TBL_PRODUCTO PRO  
 WHERE      
    PRO.FLGENABLE='T'   
 UNION
 --Presentacion
 SELECT 'DROP TABLE IF EXISTS TBL_PRESENTACION;' as SCRIPT, 0 as orden
 UNION    
 SELECT 'CREATE TABLE IF NOT EXISTS TBL_PRESENTACION(IdPresentacion INT NOT NULL PRIMARY KEY, Codigo VARCHAR(20) UNIQUE, IdProducto INTEGER,Nombre VARCHAR(50), Cantidad INTEGER,UnidadFraccionamiento INTEGER, Precio VARCHAR(20), Stock VARCHAR(20), DescuentoMinimo VARCHAR(50), DescuentoMaximo VARCHAR(50) );' as SCRIPT, 1 as orden    
 UNION  
 SELECT 'INSERT INTO TBL_PRESENTACION VALUES('+CAST(PRE_PK as VARCHAR(20))+ ', '''+CAST(CODIGO as VARCHAR(20))+ ''','+CAST(PRO_PK as VARCHAR(20))+ ',''' + ISNULL(UPPER(replace(NOMBRE,'-','')),'') + ''',' + CAST(CANTIDAD as VARCHAR(20)) + ',' + CAST(UNIDAD_FRACCIONAMIENTO as VARCHAR(20)) + ',''' + dbo.FORMATO_MONTO_VISTA(PRECIO) + ''',''' + 
	
	dbo.FORMATO_MONTO_VISTA(CAST(COALESCE(PRE.STOCK, '0') AS NUMERIC(16,4)) - ISNULL(
	(
			SELECT 
				SUM(
					(CAST(COALESCE(RES.DET_CANTIDAD, '0') AS NUMERIC(16,4)) * CAST(COALESCE(RES.PRE_CANTIDAD, '0') AS NUMERIC(16,4)))
					+
					(CAST(COALESCE(RES.DET_BONIFICACION, '0') AS NUMERIC(16,4)) * CAST(COALESCE(RES.PRE_CANTIDAD, '0') AS NUMERIC(16,4)))
					+
					CAST(COALESCE(RES.DET_CANTIDAD_FRAC, '0') AS NUMERIC(16,4)) 
				)
			FROM
				TBL_RESERVA_PRESENTACION RES
			WHERE
				RES.PRE_PK = PRE.PRE_PK
			AND
				FLGENABLE = 'T'
	)
	, 0)) 
	
	+ ''',''' + ISNULL(DESC_MIN, '0') + ''',''' + ISNULL(DESC_MAX, '0') + ''');' AS SCRIPT, 2 as orden  
 FROM TBL_PRESENTACION  PRE
 WHERE      
    FLG_ENABLE='T'        

  UNION
  
 --Almacenes
 SELECT 'DROP TABLE IF EXISTS TBL_ALMACEN;' as SCRIPT, 0 as orden
 union    
 SELECT 'CREATE TABLE IF NOT EXISTS TBL_ALMACEN(ID INTEGER PRIMARY KEY, ALM_CODIGO VARCHAR(20), ALM_NOMBRE VARCHAR(100), ALM_DIRECCION VARCHAR(100));' as SCRIPT, 1 as orden    
 union
 SELECT 'INSERT INTO TBL_ALMACEN VALUES('+CAST(ALM.ALM_PK as VARCHAR(20))+ ','''+ ALM.ALM_CODIGO + ''',''' + ALM.ALM_NOMBRE + ''',''' + ALM.ALM_DIRECCION + ''');' AS SCRIPT, 2 as orden    
FROM
	TBL_ALMACEN ALM
WHERE 
	ALM.ALM_HABILITADO = 'T'
	
 UNION
 
 --ALMACEN_PRODUCTO
 SELECT 'DROP TABLE IF EXISTS TBL_ALMACEN_PRODUCTO;' as SCRIPT, 0 as orden
 union    
 SELECT 'CREATE TABLE IF NOT EXISTS TBL_ALMACEN_PRODUCTO(ALM_PK INTEGER, PRO_PK INTEGER, ALM_PRO_STOCK VARCHAR(20));' as SCRIPT, 1 as orden    
 union
 SELECT 'INSERT INTO TBL_ALMACEN_PRODUCTO VALUES('+CAST(ALPR.ALM_PK as VARCHAR(20))+ ','+CAST(ALPR.PRO_PK as VARCHAR(20))+ ',''' +  
	dbo.FORMATO_MONTO_VISTA(CAST(COALESCE(ALPR.ALM_PRO_STOCK, '0') AS NUMERIC(16,4)) - ISNULL(
	(
			SELECT 
				SUM(
					CAST(COALESCE(RES.DET_CANTIDAD, '0') AS NUMERIC(16,4))
					+
					CAST(COALESCE(RES.DET_BONIFICACION, '0') AS NUMERIC(16,4)) 
				)
				
			FROM
				TBL_RESERVA RES
			WHERE
				RES.PRO_CODIGO = PRO.PRO_CODIGO
			AND
				RES.ALM_PK = ALPR.ALM_PK
			AND
				FLGENABLE = 'T'
	)
	, 0)) 
	
	+ ''');' AS SCRIPT, 2 as orden    
 FROM 
	TBL_ALMACEN_PRODUCTO ALPR
 INNER JOIN  
	TBL_ALMACEN ALM  
 ON 
	ALM.ALM_PK = ALPR.ALM_PK
 INNER JOIN
	TBL_PRODUCTO PRO
 ON
	PRO.PRO_PK = ALPR.PRO_PK
 WHERE 
	ALM.ALM_HABILITADO = 'T'
 AND 
	ALPR.ALM_PRO_STOCK <> ''      
 AND 
	isnumeric (ALPR.ALM_PRO_STOCK ) = 1 
	
UNION

--ALMACEN_PRESENTACION
 SELECT 'DROP TABLE IF EXISTS TBL_ALMACEN_PRESENTACION;' as SCRIPT, 0 as orden
 union    
 SELECT 'CREATE TABLE IF NOT EXISTS TBL_ALMACEN_PRESENTACION(IdAlmacen INTEGER, IdPresentacion INTEGER, stock VARCHAR(20));' as SCRIPT, 1 as orden    
 union
 SELECT 'INSERT INTO TBL_ALMACEN_PRESENTACION VALUES('+CAST(ALPRE.ALM_PK as VARCHAR(20))+ ','+CAST(ALPRE.PRE_PK as VARCHAR(20))+ ',''' +  
 
	dbo.FORMATO_MONTO_VISTA(CAST(COALESCE(ALPRE.STOCK, '0') AS NUMERIC(16,4)) - ISNULL(
		(
			SELECT 
				SUM(
					(CAST(COALESCE(RES.DET_CANTIDAD, '0') AS NUMERIC(16,4)) * CAST(COALESCE(RES.PRE_CANTIDAD, '0') AS NUMERIC(16,4)))
					+
					(CAST(COALESCE(RES.DET_BONIFICACION, '0') AS NUMERIC(16,4)) * CAST(COALESCE(RES.PRE_CANTIDAD, '0') AS NUMERIC(16,4)))
					+
					CAST(COALESCE(RES.DET_CANTIDAD_FRAC, '0') AS NUMERIC(16,4)) 
				)
			FROM
				TBL_RESERVA_PRESENTACION RES
			WHERE
				RES.PRE_PK = ALPRE.PRE_PK
			AND
				RES.ALM_PK = ALPRE.ALM_PK
			AND
				FLGENABLE = 'T'
		)
	, 0)) 
 
 + ''');' AS SCRIPT, 2 as orden    
 FROM 
	TBL_ALMACEN_PRESENTACION ALPRE
 INNER JOIN  
	TBL_ALMACEN ALM  
 ON 
	ALM.ALM_PK = ALPRE.ALM_PK
 WHERE 
	ALM.ALM_HABILITADO = 'T'
 AND 
	ALPRE.STOCK <> ''      
 AND 
	isnumeric (ALPRE.STOCK ) = 1 
 AND
	ALPRE.FLGENABLE = 'T'
	
UNION

 --Listaprecios PRESENTACION
	SELECT 'DROP TABLE IF EXISTS TBL_LISTAPRECIOS_PRESENTACION;' as SCRIPT, 0 as orden    
	UNION    
	SELECT 'CREATE TABLE IF NOT EXISTS TBL_LISTAPRECIOS_PRESENTACION(ID INTEGER not null PRIMARY KEY, IdPresentacion INTEGER, Canal VARCHAR(20) null,CondVta VARCHAR(20) null,Precio VARCHAR(20) not null, DescuentoMinimo VARCHAR(50), DescuentoMaximo VARCHAR(50));' as SCRIPT, 1 as orden    
	UNION  
	select 'INSERT INTO TBL_LISTAPRECIOS_PRESENTACION VALUES('+CAST(LPP.CODIGO as VARCHAR(20))+ ','+CAST(LPP.PRE_PK as VARCHAR(20))+ ',''' + UPPER(RTRIM(LTRIM(ISNULL(lpp.TC_PK,'')))) + ''','''+ ISNULL(LPP.CODIGO_CONDVTA,'') +''','''+ dbo.FORMATO_MONTO_VISTA(LPP.PRECIO) + ''','''+ ISNULL(LPP.DESC_MIN, '0') + ''','''+  ISNULL(LPP.DESC_MAX, '0') + ''');' AS SCRIPT, 2 as orden  
	FROM TBL_LISTAPRECIOS_PRESENTACION  LPP
	INNER JOIN TBL_PRESENTACION PRE ON LPP.PRE_PK = PRE.PRE_PK
	LEFT JOIN #TmpTipoClienteRuta tcr ON tcr.TC_PK = LPP.TC_PK
	WHERE PRE.FLG_ENABLE = 'T'
	AND LPP.FLAG = 'T'

UNION

 --Listaprecios
	SELECT 'DROP TABLE IF EXISTS TBL_LISTAPRECIOS;' as SCRIPT, 0 as orden    
	UNION    
	SELECT 'CREATE TABLE IF NOT EXISTS TBL_LISTAPRECIOS(ID INTEGER not null, Codigo VARCHAR(20) not null,Producto VARCHAR(20) not null,Canal VARCHAR(20) null,CondVta VARCHAR(20) null,Precio VARCHAR(20) not null, DescuentoMinimo VARCHAR(50), DescuentoMaximo VARCHAR(50),CONSTRAINT PkTblListaPrecios PRIMARY KEY (Codigo));' as SCRIPT, 1 as orden    
	UNION  
	SELECT 'INSERT INTO TBL_LISTAPRECIOS VALUES('+CAST(PR.PRO_PK as VARCHAR(20))+ ','''+ CONVERT(VARCHAR, codigo) + ''',''' + codigo_producto + ''',''' + UPPER(RTRIM(LTRIM(ISNULL(lp.TC_PK,'')))) + ''','''+ ISNULL(LP.CODIGO_CONDVTA,'') +''','''+ dbo.FORMATO_MONTO_VISTA(LP.PRECIO) + ''','''+ ISNULL(LP.DESC_MIN, '0') + ''','''+  ISNULL(LP.DESC_MAX, '0') + ''');' AS SCRIPT, 2 as orden  
	FROM TBL_LISTAPRECIOS  LP
	INNER JOIN TBL_PRODUCTO PR  ON LP.CODIGO_PRODUCTO = PRO_CODIGO
	LEFT JOIN #TmpTipoClienteRuta tcr ON tcr.TC_PK = LP.TC_PK
	WHERE PR.FLGENABLE = 'T'
	AND LP.FLAG = 'T'

UNION

--BONIFICACION
SELECT
		'DROP TABLE IF EXISTS TBL_BONIFICACION;' as SCRIPT, 0 as orden    
UNION    
	SELECT 
		'CREATE TABLE IF NOT EXISTS TBL_BONIFICACION(ID INTEGER NOT NULL PRIMARY KEY, IdProducto INTEGER, IdPresentacion INTEGER, CodigoArticulo VARCHAR(20), NombreArticulo VARCHAR(100), Cantidad VARCHAR(20), Maximo VARCHAR(20), Editable VARCHAR(20), TipoArticulo CHAR(3), Prioridad INTEGER);' as SCRIPT, 1 as orden    
UNION
	SELECT 
		'INSERT INTO TBL_BONIFICACION VALUES(' + CAST(BON.BON_PK as VARCHAR(20)) + ', ' + CAST(ISNULL(BON.PRO_PK, 0) as VARCHAR(20)) + ', ' + CAST(ISNULL(BON.PRE_PK, 0) as VARCHAR(20)) + ', ''' + BON.PROD_CODIGO +''', ''' + BON.PROD_NOMBRE +''', ''' + BON.BON_CANTIDAD + ''', ''' + BON.BON_MAXIMO +''', ''' + BON.BON_EDITABLE +''', ''' + BON.TIPO_ARTICULO +''', ' +  CAST(BON.BON_PRIORIDAD as VARCHAR(20)) + ');' AS SCRIPT, 2 as orden 
	FROM 
		TBL_BONIFICACION BON
	INNER JOIN TBL_BONIFICACION_DETALLE BONDET ON BON.BON_PK = BONDET.BON_PK
	WHERE
		BON.FLAGHABILITADO = 'T'
UNION			

--BONIFICACION DETALLE
	SELECT
		'DROP TABLE IF EXISTS TBL_BONIFICACION_DETALLE;' as SCRIPT, 0 as orden    
UNION    
	SELECT 
		'CREATE TABLE IF NOT EXISTS TBL_BONIFICACION_DETALLE(ID INTEGER NOT NULL PRIMARY KEY, IdBonificacion INTEGER, IdProducto INTEGER, IdPresentacion INTEGER, CodigoArticulo VARCHAR(20), NombreArticulo VARCHAR(100), Cantidad VARCHAR(20), Existe CHAR(1));' as SCRIPT, 1 as orden    
UNION
	SELECT 
		'INSERT INTO TBL_BONIFICACION_DETALLE VALUES(' + CAST(BONDET.BON_DET_PK as VARCHAR(20)) + ',' + CAST(BONDET.BON_PK as VARCHAR(20)) + ', ' + CAST(ISNULL(BONDET.PRO_PK, 0) as VARCHAR(20)) + ', ' + CAST(ISNULL(BONDET.PRE_PK, 0) as VARCHAR(20)) + ', ''' + BONDET.PROD_CODIGO +''', ''' + BONDET.PROD_NOMBRE +''', ''' + ISNULL(BONDET.PROD_CANTIDAD,0) + ''', ''' + BONDET.PROD_EXISTE +''');' AS SCRIPT, 2 as orden 
	FROM 
		TBL_BONIFICACION_DETALLE BONDET
	INNER JOIN TBL_BONIFICACION BON ON BONDET.BON_PK = BON.BON_PK
	WHERE
		BON.FLAGHABILITADO = 'T'
UNION
--Generales
SELECT 'DROP TABLE IF EXISTS TBL_GENERAL;' as SCRIPT, 0 as orden    
 union    
 SELECT 'CREATE TABLE IF NOT EXISTS TBL_GENERAL(pk INTEGER PRIMARY KEY,Grupo VARCHAR(20) not null,Codigo VARCHAR(20) not null,Descripcion VARCHAR(150) not null, FlagBanco CHAR(1) null, FlagDocumento CHAR(1) null,FlagFechaDiferida CHAR(1) null);' as SCRIPT, 1 as orden    
 union  
 select 'INSERT INTO TBL_GENERAL(Grupo,Codigo,Descripcion) VALUES(''1'',''' + UPPER(mot_codigo) + ''',''' + UPPER(mot_nombre) + ''');' AS SCRIPT, 2 as orden  
  from tbl_motivo  
  where FLGENABLE = 'T'
  AND upper(mot_tipo) = 'N'  
 union           
 select 'INSERT INTO TBL_GENERAL(Grupo,Codigo,Descripcion) VALUES(''2'',''' + UPPER(condvta_codigo) + ''',''' + UPPER(condvta_nombre) + ''');' AS SCRIPT, 2 as orden  
  from tbl_condvta
  WHERE  FLGENABLE = 'T'
 union           
 select 'INSERT INTO TBL_GENERAL(Grupo,Codigo,Descripcion) VALUES(''3'',''' + UPPER(BAN_CODIGO) + ''',''' + UPPER(BAN_NOMCORTO) + ''');' AS SCRIPT, 2 as orden  
  from TBL_BANCO
  WHERE  FLGENABLE = 'T'
 union           
 select 'INSERT INTO TBL_GENERAL(Grupo,Codigo,Descripcion,FlagBanco,FlagDocumento,FlagFechaDiferida ) VALUES(''4'',''' + UPPER(FPC_CODIGO) + ''',''' + UPPER(FPC_NOMBRE) + ''',''' + UPPER(FPC_FLGBANCO)+ ''',''' +UPPER(FPC_FLGDOCUMENTO)+''',''' +UPPER(FPC_FLGFECHADIFERIDA)+  ''');' AS SCRIPT, 2 as orden  
  from TBL_FPAGO_COBRANZA where FPC_FLGENABLE = 'T'
 union           
 select 'INSERT INTO TBL_GENERAL(Grupo,Codigo,Descripcion) VALUES(''5'',''' + UPPER(MOT_CODIGO) + ''',''' + UPPER(MOT_NOMBRE) + ''');' AS SCRIPT, 2 as orden  
  from TBL_MOTIVO
  WHERE  FLGENABLE = 'T'
  AND UPPER(MOT_TIPO) = 'C'
 union           
 select 'INSERT INTO TBL_GENERAL(Grupo,Codigo,Descripcion) VALUES(''6'',''' + UPPER(MOT_CODIGO) + ''',''' + UPPER(MOT_NOMBRE) + ''');' AS SCRIPT, 2 as orden  
  from TBL_MOTIVO
  WHERE  FLGENABLE = 'T'
  AND UPPER(MOT_TIPO) = 'D'
 union           
 select 'INSERT INTO TBL_GENERAL(Grupo,Codigo,Descripcion) VALUES(''7'',''' + UPPER(TC_CODIGO) + ''',''' + UPPER(TC_NOMBRE) + ''');' AS SCRIPT, 2 as orden  
  from TBL_TIPO_CLIENTE
  WHERE  FLGENABLE = 'T'
 UNION
 --Cobranza
 SELECT 'DROP TABLE IF EXISTS TBL_COBRANZA;' as SCRIPT, 0 as orden    
 union    
 SELECT 'CREATE TABLE IF NOT EXISTS TBL_COBRANZA(COB_PK INTEGER, COB_CODIGO VARCHAR(20), COB_NUMDOCUMENTO VARCHAR(20), COB_TIPODOCUMENTO VARCHAR(20), COB_MONTOPAGADO VARCHAR(20), COB_MONTOTOTAL VARCHAR(20), COB_FECVENCIMIENTO VARCHAR(20), COB_SERIE VARCHAR(20), CLI_CODIGO VARCHAR(20), COB_EMPRESA VARCHAR(20), COB_USUARIO VARCHAR(20), CLI_PK BIGINT, SALDO VARCHAR(20), CONSTRAINT PkTblCobra PRIMARY KEY (COB_PK));' as SCRIPT, 1 as orden    
 union  
 SELECT 'INSERT INTO TBL_COBRANZA VALUES('+ CAST(COB_PK as VARCHAR(20)) + 
		',''' + COB_CODIGO + 
		''',''' + UPPER(COB_NUMDOCUMENTO) + 
		''',''' + COB_TIPODOCUMENTO + 
		''',''' + COB_MONTOPAGADO +
		''',''' + COB_MONTOTOTAL + 
		''',''' + CONVERT(VARCHAR,COB_FECVENCIMIENTO,103) +
		''',''' + ISNULL(COB_SERIE,'')+ 
		''',''' + ISNULL(CLI_CODIGO,'')+ 
		''',''' + COB_EMPRESA+ 
		''',''' + COB_USUARIO+
		''','+ CAST(ISNULL(CLI_PK,'') as VARCHAR(20)) + 
		',''' + CAST((CONVERT(NUMERIC(18,2),COBRA.COB_MONTOTOTAL) - CONVERT(NUMERIC(18,2),COBRA.COB_MONTOPAGADO)) as VARCHAR(20)) + ''');' AS SCRIPT, 2 as orden
  FROM TBL_COBRANZA COBRA         
  WHERE (CONVERT(NUMERIC(18,2),COBRA.COB_MONTOTOTAL) - CONVERT(NUMERIC(18,2),COBRA.COB_MONTOPAGADO)) > 0
  --AND COBRA.COB_USUARIO = @PAR_CODUSUARIO
  AND COBRA.CLI_CODIGO in (select * from @tblcliente_temp)
  AND COBRA.FLGENABLE='T'
   -- Grupo
  UNION
  SELECT 'DROP TABLE IF EXISTS CFGrupo;' as SCRIPT, 0 as orden          
  UNION            
  SELECT 'CREATE TABLE IF NOT EXISTS CFGrupo(IdGrupo int not null, NombreGrupo VARCHAR(100) null,CodigoDetalle VARCHAR(50) null,NombreDetalle VARCHAR(100) null);' as SCRIPT, 1 as orden          
  UNION
  SELECT 'INSERT INTO CFGrupo VALUES('+ cast(G.IdGrupo as varchar(20))+','''+G.Nombre+''','''+GD.CodGrupoDetalle+''','''+GD.Nombre+''');' AS SCRIPT, 2 as orden
  from CFGrupo G
  inner join CFGrupoDetalle GD on G.IdGrupo = GD.IdGrupo
  where G.IdGrupo in (	select distinct(idGrupo)
 						from CFControl
 						where idGrupo is not null
 						and FlgHabilitado = 'T')
  and G.FlgHabilitado = 'T'
  and GD.FlgHabilitado = 'T'
  UNION
  
  --Prospectos
 SELECT 'DROP TABLE IF EXISTS TrProspecto;' as SCRIPT, 0 as orden
 UNION    
 SELECT 'CREATE TABLE IF NOT EXISTS TrProspecto(Id VARCHAR(50) not null, Codigo VARCHAR(20) not null,Nombre VARCHAR(100) not null,Direccion VARCHAR(100) not null,TcliCod VARCHAR(30) not null,Giro VARCHAR(100) not null, Latitud VARCHAR(50), Longitud VARCHAR(50),UserCod VARCHAR(20),CampoAdic1 VARCHAR(100),CampoAdic2 VARCHAR(100),CampoAdic3 VARCHAR(100),CampoAdic4 VARCHAR(100),CampoAdic5 VARCHAR(100),FechaMovil VARCHAR(20),Observacion VARCHAR(100),FlgEnviado CHAR(1), CONSTRAINT PkTblProducto PRIMARY KEY (Codigo));' as SCRIPT, 1 as orden     
  
  UNION
  
  
  -- Controles
  SELECT 'DROP TABLE IF EXISTS CFControl;' as SCRIPT, 0 as orden 
  UNION 
  SELECT 'CREATE TABLE IF NOT EXISTS CFControl(IdControl bigint not null, idFormulario int null, EtiquetaControl varchar(100) not null, IdTipoControl bigint null, MaxCaracteres int null, IdGrupo int null, FlgObligatorio char(1) null,FlgEditable char(1) null, Orden int null);' as SCRIPT, 1 as orden          
  UNION
  SELECT 'INSERT INTO CFControl VALUES('+cast(IdControl as varchar(20))+','+cast(idFormulario as varchar(20))+','''+EtiquetaControl+''','+cast(IdTipoControl as varchar(20))+','+cast(MaxCaracteres as varchar(20))+','+cast(isnull(IdGrupo,0) as varchar(20))+','''+FlgObligatorio+''','''+ISNULL(FlgEditable,'F')+''','+cast(Orden as varchar(20))+');' AS SCRIPT, 2 as orden
  from CFControl
  WHERE FlgHabilitado = 'T'
  and idTipoControl != 9

 --ORDER BY 2, 1
 ) t
 WHERE SCRIPT IS NOT NULL
 ORDER BY 2, 1




select 'Cliente : '+ cast(count(*) as varchar(20)) as Total , 0 as orden 
 FROM TBL_RUTA R              
  INNER JOIN TBL_CLIENTE C ON R.CLI_COD = C.CLI_CODIGO                
  WHERE  R.VEN_COD = @PAR_CODUSUARIO
   AND SUBSTRING(R.RUTA_DIAVISITA,@DIA,1) = '1'
   AND C.FLGENABLE='T'            
   AND R.FLGENABLE='T'
union

select ' Articulo : '+ cast(count(pro_codigo) as varchar(20)) as Total  , 1 as orden
 from tbl_producto  
 where flgenable='T'   
 UNION
 select ' Cobranza : ' +cast(count(COB_PK) as varchar(20)) as Total  , 2 as orden
 FROM TBL_COBRANZA COBRA         
  WHERE (CONVERT(NUMERIC(18,2),COBRA.COB_MONTOTOTAL) - CONVERT(NUMERIC(18,2),COBRA.COB_MONTOPAGADO)) > 0          
  --AND COBRA.COB_USUARIO = @PAR_CODUSUARIO  
  AND COBRA.CLI_CODIGO in (select * from @tblcliente_temp)
  AND COBRA.FLGENABLE='T' 
union  
select ' Precio Producto : '+ cast(count(codigo) as varchar(20)) as Total  , 3 as orden
	FROM TBL_LISTAPRECIOS  LP
	INNER JOIN TBL_PRODUCTO PR 
	ON LP.CODIGO_PRODUCTO = PRO_CODIGO
	WHERE PR.FLGENABLE = 'T'
	AND LP.FLAG = 'T'  
 union  
 select ' Precio Presentacion: '+ cast(count(LP.codigo) as varchar(20)) as Total  , 3 as orden
	FROM TBL_LISTAPRECIOS_PRESENTACION  LP
	INNER JOIN TBL_PRESENTACION PR 
	ON LP.PRE_PK = PR.PRE_PK
	WHERE PR.FLG_ENABLE = 'T'
	AND LP.FLAG = 'T' 	
	
 union
 select ' Motivo noVta : ' +cast(count(mot_codigo) as varchar(20)) as Total  , 4 as orden
 from tbl_motivo  
  where FLGENABLE = 'T'
  AND upper(mot_tipo) = 'N'
 union           
 select ' CondVta : ' +cast(count(condvta_codigo) as varchar(20)) as Total  , 5 as orden
  from tbl_condvta  
  where FLGENABLE = 'T'
 union           
 select ' Banco : ' +cast(count(BAN_CODIGO) as varchar(20)) as Total  ,6 as orden
  from TBL_BANCO
  WHERE  FLGENABLE = 'T'
 --union           
 --select ' Tipo pago : ' +cast(count(FPC_CODIGO) as varchar(20)) as Total  , 7 as orden
 -- from TBL_FPAGO_COBRANZA
 union
 select ' Motivo canje : ' +cast(count(mot_codigo) as varchar(20)) as Total  , 8 as orden
 from tbl_motivo  
  where FLGENABLE = 'T'
  AND upper(mot_tipo) = 'C'
 union
 select ' Motivo devolucion : ' +cast(count(mot_codigo) as varchar(20)) as Total , 9 as orden 
 from tbl_motivo  
  where FLGENABLE = 'T'
  AND upper(mot_tipo) = 'D'
  union 
  select 'Tipo Pago Doc : '++cast(count(FPC_CODIGO) as varchar(20)) as Total , 10 as orden 
  from TBL_FPAGO_COBRANZA
  where fpc_flgenable = 'T'
  
ORDER BY orden

END 




go