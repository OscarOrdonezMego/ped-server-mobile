--INSERT
SET IDENTITY_INSERT [dbo].[CFTipoControl] ON 
GO
INSERT [dbo].[CFTipoControl] ([IdTipoControl], [Nombre], [FlgHabilitado]) VALUES (9, N'POSICION', N'T')
GO
SET IDENTITY_INSERT [dbo].[CFTipoControl] OFF
GO

--CREATE
/****** Object:  StoredProcedure [dbo].[USPSO_PROSPECTO]    Script Date: 13/06/2018 08:14:50 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[USPSO_PROSPECTO]      
(      
 @CLI_PK BIGINT      
)      
AS      
BEGIN      
	 SELECT     
		P.ID AS PK,   
		P.CODIGO AS CODIGO,   
		P.CODIGO_EMPRESA AS EMPRESA,   
		P.NOMBRE AS NOMBRE,   
		P.DIRECCION  AS DIRECCION,   
		TC.TC_PK AS TIPO,   
		P.Giro AS GIRO,   
		P.LATITUD,  
		P.LONGITUD,  
		P.flgEnable AS ENABLE,    
		P.CampoAdicional1,
		P.CampoAdicional2,
		P.CampoAdicional3,
		P.CampoAdicional4,
		P.CampoAdicional5

	 FROM
		 TBL_PROSPECTO P
     inner join TBL_TIPO_CLIENTE TC on P.TCLI_COD = TC.TC_CODIGO     
	 WHERE 
	 	P.ID = @CLI_PK;     
END

/****** Object:  StoredProcedure [dbo].[USPW_PROSPECTO_CLIENTE_I]    Script Date: 13/06/2018 08:52:32 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USPW_PROSPECTO_CLIENTE_I]
(        
 @CLI_PK   BIGINT = NULL,         
 @CLI_CODIGO VARCHAR(20),          
 @CLI_NOMBRE VARCHAR(100),       
 @CLI_DIRECCION VARCHAR(100),
 @TCLI_COD VARCHAR(15),  
 @CLI_GIRO VARCHAR(30),
 @CLI_SECUENCIA INT,
 @CLI_LATITUD VARCHAR(20),
 @CLI_LONGITUD VARCHAR(20)
 ,@CLI_CAMPO_ADICIONAL1 VARCHAR(100)=NULL
 ,@CLI_CAMPO_ADICIONAL2 VARCHAR(100)=NULL
 ,@CLI_CAMPO_ADICIONAL3 VARCHAR(100)=NULL
 ,@CLI_CAMPO_ADICIONAL4 VARCHAR(100)=NULL
 ,@CLI_CAMPO_ADICIONAL5 VARCHAR(100)=NULL
)        

AS
BEGIN              
          
	DECLARE @EXISTE INT 
	DECLARE @LIMITE_CREDITO VARCHAR(50)
	DECLARE @CREDITO_UTILIZADO VARCHAR(50)
	         
	SET @EXISTE = 0          
              
	SET NOCOUNT ON
 
	 set @CLI_CAMPO_ADICIONAL1 = LTRIM(RTRIM(ISNULL(@CLI_CAMPO_ADICIONAL1,'')));
	 set @CLI_CAMPO_ADICIONAL2 = LTRIM(RTRIM(ISNULL(@CLI_CAMPO_ADICIONAL2,'')));
	 set @CLI_CAMPO_ADICIONAL3 = LTRIM(RTRIM(ISNULL(@CLI_CAMPO_ADICIONAL3,'')));
	 set @CLI_CAMPO_ADICIONAL4 = LTRIM(RTRIM(ISNULL(@CLI_CAMPO_ADICIONAL4,'')));
	 set @CLI_CAMPO_ADICIONAL5 = LTRIM(RTRIM(ISNULL(@CLI_CAMPO_ADICIONAL5,'')));
	 set @LIMITE_CREDITO = '0';
	 set @CREDITO_UTILIZADO ='0';
 
       
          
		--VERIFICAMOS QUE NO EXISTA EL CODIGO          
		SELECT @EXISTE = COUNT(*) FROM TBL_CLIENTE WHERE @CLI_CODIGO = CLI_CODIGO  --AND PRO_CODIGOEMPRESA = @CLI_COMPANIA      
	    
		--LANZAMOS EL ERROR          
		IF @EXISTE > 0 BEGIN RAISERROR('WEB.MANTENIMIENTO.CODIGOREPETIDO',18,1) END          
	                
		IF @EXISTE <= 0 BEGIN           
		  -- SI NO EXISTE SE INSERTA EL USUARIO           

			INSERT TBL_CLIENTE
			(
			CLI_CODIGO,CLI_NOMBRE,CLI_DIRECCION,TCLI_COD,CLI_SECUENCIA,LATITUD,LONGITUD,CLI_GIRO
			,CampoAdicional1,CampoAdicional2,CampoAdicional3,CampoAdicional4,CampoAdicional5,
			TC_PK,LimiteCredito,CreditoUtilizado
			)
			VALUES
			(
			@CLI_CODIGO,@CLI_NOMBRE,@CLI_DIRECCION,@TCLI_COD,@CLI_SECUENCIA,@CLI_LATITUD,@CLI_LONGITUD,@CLI_GIRO
			,@CLI_CAMPO_ADICIONAL1,@CLI_CAMPO_ADICIONAL2,@CLI_CAMPO_ADICIONAL3,@CLI_CAMPO_ADICIONAL4,@CLI_CAMPO_ADICIONAL5,@TCLI_COD,
			@LIMITE_CREDITO,@CREDITO_UTILIZADO
			);
            
		END  


		UPDATE TBL_PROSPECTO
		SET flgEnable = 'F'
		where ID = @CLI_PK

        
       
           
END

/****** Object:  StoredProcedure [dbo].[USPS_REPORTE_DINAMICO_ISGPS]    Script Date: 22/06/2018 04:48:00 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USPS_REPORTE_DINAMICO_ISGPS]
   
AS    
BEGIN    
    
SET NOCOUNT ON    
 
select count(idControl) as cantidad
from CFControl
where FlgHabilitado = 'T'
and IdTipoControl = 9

END


GO

/****** Object:  StoredProcedure [dbo].[USPS_OBTENER_GPS_PEDIDO]    Script Date: 22/06/2018 04:48:23 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USPS_OBTENER_GPS_PEDIDO]
(
@idPedido VARCHAR(50)
)
   
AS    
BEGIN    
    
SET NOCOUNT ON    
 
select isnull(latitud,0) as latitud, isnull(longitud,0) longitud
from tbl_pedido
where PED_PK = @idPedido

END


GO




--ALTER
/****** Object:  StoredProcedure [dbo].[USPW_ALMACENPRODUCTO_I]    Script Date: 12/06/2018 12:01:47 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[USPW_ALMACENPRODUCTO_I]
(
  @ALM_PRO_PK INT,
  @PRO_CODIGO VARCHAR(20),
  @XML_ALMACEN XML
)          
AS          
BEGIN
SET NOCOUNT ON;
            
	DECLARE @EXISTE INT;
	SET @EXISTE = 0;

	declare 
	@PRO_PK int,
	@vl_doc_almacen int;

	SELECT @PRO_PK = PRO_PK FROM TBL_PRODUCTO WHERE PRO_CODIGO = @PRO_CODIGO;

	CREATE TABLE #TmpAlmacenSel
	(
		NORDEN INT IDENTITY(1,1),
		ALM_CODIGO VARCHAR(20) NOT NULL,
		ALM_PRO_STOCK VARCHAR(20) NOT NULL
	)

	EXEC sp_xml_preparedocument @vl_doc_almacen OUTPUT, @XML_ALMACEN

	INSERT #TmpAlmacenSel
	(
		ALM_CODIGO,  
		ALM_PRO_STOCK
	)  
	SELECT * FROM OPENXML(@vl_doc_almacen, N'/ROOT/doc') with       
	(
		alm_codigo varchar(20),
		alm_pro_stock varchar(20)
	)
   
	EXEC sp_xml_removedocument @vl_doc_almacen
	
	UPDATE alp
	SET
	alp.ALM_PRO_STOCK = als.ALM_PRO_STOCK,
	alp.ALM_PRO_HABILITADO = 'T'
	FROM TBL_ALMACEN_PRODUCTO alp
	INNER JOIN TBL_ALMACEN a on a.ALM_PK = alp.ALM_PK
	INNER JOIN #TmpAlmacenSel als on als.ALM_CODIGO = a.ALM_CODIGO
	WHERE alp.PRO_PK = @PRO_PK
	
	
	INSERT TBL_ALMACEN_PRODUCTO
	(
		ALM_PK, PRO_PK, ALM_PRO_STOCK
	)
	SELECT
	a.ALM_PK, @PRO_PK, als.ALM_PRO_STOCK
	FROM #TmpAlmacenSel als
	INNER JOIN TBL_ALMACEN a on a.ALM_CODIGO = als.ALM_CODIGO
	LEFT JOIN TBL_ALMACEN_PRODUCTO ap on ap.ALM_PK = a.ALM_PK and ap.PRO_PK = @PRO_PK
	WHERE
	ap.ALM_PRO_PK IS NULL

	delete from TBL_ALMACEN_PRODUCTO
	where ALM_PRO_PK in (
	select AP.ALM_PRO_PK
	from TBL_ALMACEN_PRODUCTO AP 
	inner join TBL_PRODUCTO P on P.PRO_PK = AP.PRO_PK
	inner join TBL_ALMACEN A on A.ALM_PK = AP.ALM_PK
	where P.PRO_CODIGO = @PRO_CODIGO
	and a.ALM_CODIGO not in (select alm_codigo from #TmpAlmacenSel ))
	
	IF OBJECT_ID('tempdb..#TmpAlmacenSel') IS NOT NULL
	BEGIN
		drop table #TmpAlmacenSel
	END
          
END

/****** Object:  StoredProcedure [dbo].[spS_BorrarAlmacenPresentacion]    Script Date: 12/06/2018 01:37:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROC [dbo].[spS_BorrarAlmacenPresentacion](
@Pre_Pk INT
)
AS

BEGIN
DELETE TBL_ALMACEN_PRESENTACION WHERE PRE_PK=@Pre_Pk 

END

/****** Object:  StoredProcedure [dbo].[USPS_PROSPECTO]    Script Date: 13/06/2018 09:17:01 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[USPS_PROSPECTO]                            
(                            
 @IN_FECINICIO VARCHAR(50),
 @IN_FECFIN VARCHAR(50),
 @IN_USUARIO VARCHAR(20),
 @IN_PROSPECTO VARCHAR(20),
 @PIV_PAG_ACTUAL INT,
 @PIV_NUMERO_REGISTROS INT                           
)                            
AS

DECLARE @MAXIMO INT                                  
DECLARE @MINIMO INT                                  
DECLARE @TAMTOTAL INT                         

BEGIN                           
                            
SET NOCOUNT ON                                  
                    
SET @IN_FECINICIO = [dbo].FECHA_ADD(@IN_FECINICIO,'00','00','00')        
SET @IN_FECFIN = [dbo].FECHA_ADD(@IN_FECFIN,'23','59','59')        
        
SELECT @MAXIMO = (@PIV_PAG_ACTUAL * @PIV_NUMERO_REGISTROS)                                  
SELECT @MINIMO = @MAXIMO - (@PIV_NUMERO_REGISTROS - 1)                             
                            
DECLARE @TEMP TABLE
	(            
		NORDEN INT IDENTITY(1,1),
		ID VARCHAR(100),
		CODIGO VARCHAR(100),
		NOMBRE VARCHAR(100),
		DIRECCION VARCHAR(100),
		USER_COD VARCHAR(20),
		VENDEDOR VARCHAR(100),
		TCLI_COD VARCHAR(20),
		TIPOCLIENTE VARCHAR(100),
		GIRO VARCHAR(100),
		LATITUD VARCHAR(20),
		LONGITUD VARCHAR(20),
		FECHAREGISTRO VARCHAR(20),
		FECHAMOVIL VARCHAR(20),
		OBSERVACION VARCHAR(100),
		FLAG VARCHAR(20)
		,CampoAdicional1 varchar(100)
		,CampoAdicional2 varchar(100)
		,CampoAdicional3 varchar(100)
		,CampoAdicional4 varchar(100)
		,CampoAdicional5 varchar(100)	    
)

INSERT @TEMP             
	SELECT 
		P.ID,
		P.CODIGO,
		P.NOMBRE,         
		P.DIRECCION,
		U.USR_CODIGO,
		U.USR_NOMBRE,
		TC.TC_CODIGO,
		TC.TC_NOMBRE,
		P.GIRO,
		P.LATITUD,
		P.LONGITUD,
		dbo.FECHA_STRING(P.FechaRegistro),
		dbo.FECHA_STRING(P.FECHAMOVIL),		
		P.OBSERVACION,
		P.FLGENABLE,
		P.CampoAdicional1,
		P.CampoAdicional2,
		P.CampoAdicional3,
		P.CampoAdicional4,
		P.CampoAdicional5		
	FROM 
	TBL_PROSPECTO P	
		INNER JOIN TBL_USUARIO U ON P.USER_COD = U.USR_CODIGO
		LEFT JOIN TBL_TIPO_CLIENTE TC ON P.TCLI_COD = TC.TC_CODIGO
	WHERE 
		((@IN_USUARIO = '-1') OR (@IN_USUARIO <> '-1' AND U.USR_PK = @IN_USUARIO)) AND
		(P.FechaRegistro BETWEEN dbo.STRING_FECHA( @IN_FECINICIO ) AND dbo.STRING_FECHA(@IN_FECFIN)) AND
		( P.NOMBRE LIKE '%'+@IN_PROSPECTO + '%')
	
	ORDER BY 
		P.FechaRegistro DESC;
                            
SET   @TAMTOTAL = @@ROWCOUNT                            
                
-- PARA EL REPORTE EXCEL                
IF (@PIV_PAG_ACTUAL = -1 AND @PIV_NUMERO_REGISTROS  = -1)                
BEGIN                
 SET @MINIMO = 1                
 SET @MAXIMO = @TAMTOTAL                
END                
                            
SELECT 
		ID,
		CODIGO,
		NOMBRE,
		DIRECCION,
		TCLI_COD,
		TIPOCLIENTE,
		GIRO,
		USER_COD,
		VENDEDOR,
		LATITUD,
		LONGITUD,
		FECHAREGISTRO,
		FECHAMOVIL,
		OBSERVACION,
		FLAG,
		CampoAdicional1,
		CampoAdicional2,
		CampoAdicional3,
		CampoAdicional4,
		CampoAdicional5,
		@TAMTOTAL AS TAMANIOTOTAL	
	FROM 
		@TEMP            
	WHERE 
		(NORDEN BETWEEN @MINIMO AND @MAXIMO)                            
                
END

/****** Object:  StoredProcedure [dbo].[USPS_JSON_SINCRONIZAR_TODO]    Script Date: 22/06/2018 04:48:53 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[USPS_JSON_SINCRONIZAR_TODO] 
 @PAR_CODUSUARIO as varchar(20)
--,
-- @PAR_FECHAMOVIL VARCHAR(20)
as
 BEGIN             
 SET DATEFIRST 1         
 
  
 DECLARE @DIA INT            
 SELECT @DIA = DATEPART(WEEKDAY,dbo.STRING_FECHA(getdate()))   
 
 
 --Validar Stock
 DECLARE @STOCK INT      
 SELECT 
	@STOCK = convert(INT, 
		CASE 
			WHEN Valor = 'T' 
				THEN 1 
				ELSE 0 
		END) 
 FROM 
	CFConfiguracion 
 WHERE 
	CodConfiguracion = 'STST'
	

 --Validar Precio mayor que 0
DECLARE @PRECIO INT      
 SELECT 
	@PRECIO = convert(INT, 
		CASE 
			WHEN Valor = 'T' 
				THEN 1 
				ELSE 0 
		END) 
 FROM 
	CFConfiguracion 
 WHERE 
	CodConfiguracion = 'PRVP'

 --Configuracion Precio - Tipo de Cliente
DECLARE @PRECIO_TIPOCLI INT      
SELECT 
	@PRECIO_TIPOCLI = convert(INT, 
		CASE 
			WHEN Valor = 'T' 
				THEN 1 
				ELSE 0 
		END) 
FROM 
	CFConfiguracion 
WHERE 
	CodConfiguracion = 'PRTC'

 --Configuracion Precio - Condicion de Venta
DECLARE @PRECIO_CONDVTA INT      
SELECT 
	@PRECIO_CONDVTA = convert(INT, 
		CASE 
			WHEN Valor = 'T' 
				THEN 1 
				ELSE 0 
		END) 
FROM 
	CFConfiguracion 
WHERE 
	CodConfiguracion = 'PRCV'
 
	IF OBJECT_ID('tempdb..#TmpTipoClienteRuta') IS NOT NULL BEGIN DROP TABLE #TmpTipoClienteRuta END; 
		CREATE TABLE #TmpTipoClienteRuta ( 
		ID BIGINT IDENTITY(1,1) NOT NULL, 
		TC_PK INT NULL)
	
	INSERT 
		#TmpTipoClienteRuta
	SELECT DISTINCT 
		tc.TC_PK 
	FROM 
		tbl_cliente tc
	JOIN tbl_ruta tr ON tr.cli_cod = tc.cli_codigo
    WHERE
	     tr.ven_cod = @PAR_CODUSUARIO

declare @tblcliente_temp as table (CLI_CODIGO VARCHAR(20));
 insert into @tblcliente_temp
 select distinct(R.CLI_COD) 
 FROM TBL_RUTA R              
  INNER JOIN TBL_CLIENTE C ON R.CLI_COD = C.CLI_CODIGO     
  LEFT JOIN TBL_COBRANZA CO ON C.CLI_PK = CO.CLI_PK AND C.CLI_CODIGO = CO.CLI_CODIGO
  LEFT JOIN TBL_TIPO_CLIENTE TC ON C.TC_PK = TC.TC_PK
  FULL OUTER JOIN TBL_PEDIDO P ON C.CLI_CODIGO = P.CLI_CODIGO AND P.PED_PK = (SELECT TOP 1 P.PED_PK FROM TBL_PEDIDO P WHERE P.CLI_CODIGO = C.CLI_CODIGO AND P.flgEnable = 'T' AND (P.PED_NOPEDIDO IS NULL OR P.PED_NOPEDIDO='') ORDER BY P.PED_PK desc) LEFT JOIN TBL_USUARIO U ON P.USR_CODIGO = U.USR_CODIGO               
  WHERE  R.VEN_COD = @PAR_CODUSUARIO
   AND SUBSTRING(R.RUTA_DIAVISITA,@DIA,1) = '1'
   AND C.FLGENABLE='T'
   AND R.FLGENABLE='T';
 
SELECT t.SCRIPT, t.orden FROM( 

 --RUTA
 SELECT 'DROP TABLE IF EXISTS TBL_CLIENTE;' as SCRIPT, 0 as orden    
 union    
 SELECT 'CREATE TABLE IF NOT EXISTS TBL_CLIENTE(RUTA_PK BIGINT, CLI_PK BIGINT, CLI_COD VARCHAR(20), CLI_NOMBRE VARCHAR(100), CLI_DIRECCION VARCHAR(100), TCLI_COD VARCHAR(20),TCLI_NOMBRE VARCHAR(20), CLI_GIRO VARCHAR(20), CLI_DEUDA VARCHAR(20), ESTADO VARCHAR(20), PED_FECREGISTRO VARCHAR(20), USR_CODIGO VARCHAR(20),CLI_CAMPO_ADICIONAL_1 VARCHAR(100),CLI_CAMPO_ADICIONAL_2 VARCHAR(100), CLI_CAMPO_ADICIONAL_3 VARCHAR(100),CLI_CAMPO_ADICIONAL_4 VARCHAR(100), CLI_CAMPO_ADICIONAL_5 VARCHAR(100),SALDO_CREDITO DOUBLE,LATITUD VARCHAR(50),LONGITUD VARCHAR(50), CONSTRAINT PkTblCliente PRIMARY KEY (RUTA_PK));' as SCRIPT, 1 as orden    


 union  
 SELECT 'INSERT INTO TBL_CLIENTE VALUES('+ CAST(R.RUTA_PK as VARCHAR(20)) + ',' +  CAST(C.CLI_PK as VARCHAR(20)) + ',''' + R.CLI_COD + ''',''' + UPPER(CLI_NOMBRE) + ''',''' + UPPER(CLI_DIRECCION) + ''',''' + UPPER(isnull(TC.TC_PK,'')) +''',''' +UPPER(isnull(TC.TC_NOMBRE,''))+ ''',''' + UPPER(isnull(C.CLI_GIRO,'')) + ''',''' + dbo.FORMATO_MONTO_VISTA(SUM(CONVERT(NUMERIC(18,6),COALESCE(COB_MONTOTOTAL,'0')) - CONVERT(NUMERIC(18,6),COALESCE(COB_MONTOPAGADO,'0')))) + ''',''POR VISITAR'''+','''+ isnull(dbo.FECHA_STRING(P.PED_FECREGISTRO),'') + ''',''' + isnull(U.USR_NOMBRE,'') + ''',''' + isnull(C.CampoAdicional1,'') + ''',''' + isnull(C.CampoAdicional2,'') + ''',''' +  isnull(C.CampoAdicional3,'') + ''',''' +  isnull(C.CampoAdicional4,'') + ''',''' +  isnull(C.CampoAdicional5,'')+''',''' +  cast((convert(float,isnull(C.LimiteCredito,''))- CONVERT(float, isnull(C.CreditoUtilizado,'')) - SUM(CONVERT(NUMERIC(18,6),COALESCE(COB_MONTOTOTAL,'0')) - CONVERT(NUMERIC(18,6),COALESCE(COB_MONTOPAGADO,'0')))) as varchar(20)) +''',''' +ISNULL(C.LATITUD,'')+''',''' +ISNULL(C.LONGITUD,'')+''');' AS SCRIPT, 2 as orden  
  FROM TBL_RUTA R              
  INNER JOIN TBL_CLIENTE C ON R.CLI_COD = C.CLI_CODIGO     
  LEFT JOIN TBL_COBRANZA CO ON C.CLI_CODIGO = CO.CLI_CODIGO AND CO.flgEnable = 'T'
  LEFT JOIN TBL_TIPO_CLIENTE TC ON C.TC_PK = TC.TC_PK
  FULL OUTER JOIN TBL_PEDIDO P ON C.CLI_CODIGO = P.CLI_CODIGO AND P.PED_PK = (SELECT TOP 1 P.PED_PK FROM TBL_PEDIDO P WHERE P.CLI_CODIGO = C.CLI_CODIGO AND P.flgEnable = 'T' AND (P.PED_NOPEDIDO IS NULL OR P.PED_NOPEDIDO='') ORDER BY P.PED_PK desc) LEFT JOIN TBL_USUARIO U ON P.USR_CODIGO = U.USR_CODIGO               
  WHERE  R.VEN_COD = @PAR_CODUSUARIO
   AND SUBSTRING(R.RUTA_DIAVISITA,@DIA,1) = '1'
   AND C.FLGENABLE='T'
   AND R.FLGENABLE='T'
 GROUP BY R.RUTA_PK, C.CLI_PK, R.CLI_COD, C.CLI_NOMBRE,           
        C.CLI_DIRECCION, TC.TC_NOMBRE, C.CLI_GIRO, P.PED_FECREGISTRO, U.USR_NOMBRE, C.CampoAdicional1, C.CampoAdicional2, 
        C.CampoAdicional3, C.CampoAdicional4, C.CampoAdicional5,C.LimiteCredito,C.CreditoUtilizado,
        C.LATITUD,C.LONGITUD, TC.TC_PK
 UNION
 
--Direcciones
 SELECT 'DROP TABLE IF EXISTS TBL_DIRECCION;' as SCRIPT, 0 as orden    
 union    
SELECT 'CREATE TABLE IF NOT EXISTS TBL_DIRECCION(DIR_INC INTEGER  PRIMARY KEY, DIR_PK BIGINT, CLI_CODIGO VARCHAR(20), DIR_NOMBRE VARCHAR(100), DIR_TIPO VARCHAR(1), FLGENVIADO VARCHAR(1), DIR_LATITUD VARCHAR(50), DIR_LONGITUD VARCHAR(50), DIR_COD VARCHAR(20), FECCREACION VARCHAR(20));' as SCRIPT, 1 as orden    
 union  
SELECT 'INSERT INTO TBL_DIRECCION (DIR_PK,DIR_COD,CLI_CODIGO, DIR_NOMBRE, DIR_TIPO, FLGENVIADO, DIR_LATITUD,DIR_LONGITUD) VALUES('+ CAST(D.DIR_PK as VARCHAR(20)) +',''' + D.DIR_CODIGO + ''',''' + D.CLI_CODIGO + ''',''' + UPPER(D.DIR_NOMBRE)+ ''',''' + D.DIR_TIPO + ''',''' + 'T' + ''',''' + ISNULL(D.LATITUD, '')+ ''',''' + ISNULL(D.LONGITUD, '') + ''');' AS SCRIPT, 2 as orden  
  FROM TBL_RUTA R              
  INNER JOIN TBL_CLIENTE C ON R.CLI_COD = C.CLI_CODIGO
  INNER JOIN TBL_DIRECCION D ON C.CLI_PK = D.CLI_PK
  WHERE  R.VEN_COD = @PAR_CODUSUARIO
  AND SUBSTRING(R.RUTA_DIAVISITA,@DIA,1) = '1'
   AND C.FLGENABLE='T'
   AND R.FLGENABLE='T'
   AND D.flgEnable = 'T'

 UNION
	
 --Productos
 SELECT 'DROP TABLE IF EXISTS TBL_PRODUCTO;' as SCRIPT, 0 as orden
 UNION    
 SELECT 'CREATE TABLE IF NOT EXISTS TBL_PRODUCTO(ID INTEGER not null, Codigo VARCHAR(20) not null,Nombre VARCHAR(150) not null,Stock VARCHAR(20) not null,UnidadDefecto VARCHAR(20) not null,PrecioBase VARCHAR(20) not null, DescuentoMinimo VARCHAR(50), DescuentoMaximo VARCHAR(50), CONSTRAINT PkTblProducto PRIMARY KEY (Codigo));' as SCRIPT, 1 as orden    
 UNION  
 SELECT 'INSERT INTO TBL_PRODUCTO VALUES('+CAST(PRO_PK as VARCHAR(20))+ ',''' + pro_codigo + ''',''' + UPPER(replace(pro_nombre,'-','')) + ''',''' + 
	
	dbo.FORMATO_MONTO_VISTA(
	
		CAST(COALESCE(PRO.PRO_STOCK, '0') AS NUMERIC(16,4)) - ISNULL(
		(
			SELECT 
				SUM(
					CAST(COALESCE(RES.DET_CANTIDAD, '0') AS NUMERIC(16,4))
					+
					CAST(COALESCE(RES.DET_BONIFICACION, '0') AS NUMERIC(16,4)) 
				)
			FROM
				TBL_RESERVA RES
			WHERE
				RES.PRO_CODIGO = PRO.PRO_CODIGO
			AND
				FLGENABLE = 'T'
		)
		, 0)
	) 
	+ ''',''' + pro_unidaddefecto + ''',''' + dbo.FORMATO_MONTO_VISTA(PRO_PRECIOBASE) + ''',''' + ISNULL(DESC_MIN, '0') + ''',''' + ISNULL(DESC_MAX, '0') + ''');' AS SCRIPT, 2 as orden  
 FROM TBL_PRODUCTO PRO  
 WHERE      
    PRO.FLGENABLE='T'   
 UNION
 --Presentacion
 SELECT 'DROP TABLE IF EXISTS TBL_PRESENTACION;' as SCRIPT, 0 as orden
 UNION    
 SELECT 'CREATE TABLE IF NOT EXISTS TBL_PRESENTACION(IdPresentacion INT NOT NULL PRIMARY KEY, Codigo VARCHAR(20) UNIQUE, IdProducto INTEGER,Nombre VARCHAR(50), Cantidad INTEGER,UnidadFraccionamiento INTEGER, Precio VARCHAR(20), Stock VARCHAR(20), DescuentoMinimo VARCHAR(50), DescuentoMaximo VARCHAR(50) );' as SCRIPT, 1 as orden    
 UNION  
 SELECT 'INSERT INTO TBL_PRESENTACION VALUES('+CAST(PRE_PK as VARCHAR(20))+ ', '''+CAST(CODIGO as VARCHAR(20))+ ''','+CAST(PRO_PK as VARCHAR(20))+ ',''' + ISNULL(UPPER(replace(NOMBRE,'-','')),'') + ''',' + CAST(CANTIDAD as VARCHAR(20)) + ',' + CAST(UNIDAD_FRACCIONAMIENTO as VARCHAR(20)) + ',''' + dbo.FORMATO_MONTO_VISTA(PRECIO) + ''',''' + 
	
	dbo.FORMATO_MONTO_VISTA(CAST(COALESCE(PRE.STOCK, '0') AS NUMERIC(16,4)) - ISNULL(
	(
			SELECT 
				SUM(
					(CAST(COALESCE(RES.DET_CANTIDAD, '0') AS NUMERIC(16,4)) * CAST(COALESCE(RES.PRE_CANTIDAD, '0') AS NUMERIC(16,4)))
					+
					(CAST(COALESCE(RES.DET_BONIFICACION, '0') AS NUMERIC(16,4)) * CAST(COALESCE(RES.PRE_CANTIDAD, '0') AS NUMERIC(16,4)))
					+
					CAST(COALESCE(RES.DET_CANTIDAD_FRAC, '0') AS NUMERIC(16,4)) 
				)
			FROM
				TBL_RESERVA_PRESENTACION RES
			WHERE
				RES.PRE_PK = PRE.PRE_PK
			AND
				FLGENABLE = 'T'
	)
	, 0)) 
	
	+ ''',''' + ISNULL(DESC_MIN, '0') + ''',''' + ISNULL(DESC_MAX, '0') + ''');' AS SCRIPT, 2 as orden  
 FROM TBL_PRESENTACION  PRE
 WHERE      
    FLG_ENABLE='T'        

  UNION
  
 --Almacenes
 SELECT 'DROP TABLE IF EXISTS TBL_ALMACEN;' as SCRIPT, 0 as orden
 union    
 SELECT 'CREATE TABLE IF NOT EXISTS TBL_ALMACEN(ID INTEGER PRIMARY KEY, ALM_CODIGO VARCHAR(20), ALM_NOMBRE VARCHAR(100), ALM_DIRECCION VARCHAR(100));' as SCRIPT, 1 as orden    
 union
 SELECT 'INSERT INTO TBL_ALMACEN VALUES('+CAST(ALM.ALM_PK as VARCHAR(20))+ ','''+ ALM.ALM_CODIGO + ''',''' + ALM.ALM_NOMBRE + ''',''' + ALM.ALM_DIRECCION + ''');' AS SCRIPT, 2 as orden    
FROM
	TBL_ALMACEN ALM
WHERE 
	ALM.ALM_HABILITADO = 'T'
	
 UNION
 
 --ALMACEN_PRODUCTO
 SELECT 'DROP TABLE IF EXISTS TBL_ALMACEN_PRODUCTO;' as SCRIPT, 0 as orden
 union    
 SELECT 'CREATE TABLE IF NOT EXISTS TBL_ALMACEN_PRODUCTO(ALM_PK INTEGER, PRO_PK INTEGER, ALM_PRO_STOCK VARCHAR(20));' as SCRIPT, 1 as orden    
 union
 SELECT 'INSERT INTO TBL_ALMACEN_PRODUCTO VALUES('+CAST(ALPR.ALM_PK as VARCHAR(20))+ ','+CAST(ALPR.PRO_PK as VARCHAR(20))+ ',''' +  
	dbo.FORMATO_MONTO_VISTA(CAST(COALESCE(ALPR.ALM_PRO_STOCK, '0') AS NUMERIC(16,4)) - ISNULL(
	(
			SELECT 
				SUM(
					CAST(COALESCE(RES.DET_CANTIDAD, '0') AS NUMERIC(16,4))
					+
					CAST(COALESCE(RES.DET_BONIFICACION, '0') AS NUMERIC(16,4)) 
				)
				
			FROM
				TBL_RESERVA RES
			WHERE
				RES.PRO_CODIGO = PRO.PRO_CODIGO
			AND
				RES.ALM_PK = ALPR.ALM_PK
			AND
				FLGENABLE = 'T'
	)
	, 0)) 
	
	+ ''');' AS SCRIPT, 2 as orden    
 FROM 
	TBL_ALMACEN_PRODUCTO ALPR
 INNER JOIN  
	TBL_ALMACEN ALM  
 ON 
	ALM.ALM_PK = ALPR.ALM_PK
 INNER JOIN
	TBL_PRODUCTO PRO
 ON
	PRO.PRO_PK = ALPR.PRO_PK
 WHERE 
	ALM.ALM_HABILITADO = 'T'
 AND 
	ALPR.ALM_PRO_STOCK <> ''      
 AND 
	isnumeric (ALPR.ALM_PRO_STOCK ) = 1 
	
UNION

--ALMACEN_PRESENTACION
 SELECT 'DROP TABLE IF EXISTS TBL_ALMACEN_PRESENTACION;' as SCRIPT, 0 as orden
 union    
 SELECT 'CREATE TABLE IF NOT EXISTS TBL_ALMACEN_PRESENTACION(IdAlmacen INTEGER, IdPresentacion INTEGER, stock VARCHAR(20));' as SCRIPT, 1 as orden    
 union
 SELECT 'INSERT INTO TBL_ALMACEN_PRESENTACION VALUES('+CAST(ALPRE.ALM_PK as VARCHAR(20))+ ','+CAST(ALPRE.PRE_PK as VARCHAR(20))+ ',''' +  
 
	dbo.FORMATO_MONTO_VISTA(CAST(COALESCE(ALPRE.STOCK, '0') AS NUMERIC(16,4)) - ISNULL(
		(
			SELECT 
				SUM(
					(CAST(COALESCE(RES.DET_CANTIDAD, '0') AS NUMERIC(16,4)) * CAST(COALESCE(RES.PRE_CANTIDAD, '0') AS NUMERIC(16,4)))
					+
					(CAST(COALESCE(RES.DET_BONIFICACION, '0') AS NUMERIC(16,4)) * CAST(COALESCE(RES.PRE_CANTIDAD, '0') AS NUMERIC(16,4)))
					+
					CAST(COALESCE(RES.DET_CANTIDAD_FRAC, '0') AS NUMERIC(16,4)) 
				)
			FROM
				TBL_RESERVA_PRESENTACION RES
			WHERE
				RES.PRE_PK = ALPRE.PRE_PK
			AND
				RES.ALM_PK = ALPRE.ALM_PK
			AND
				FLGENABLE = 'T'
		)
	, 0)) 
 
 + ''');' AS SCRIPT, 2 as orden    
 FROM 
	TBL_ALMACEN_PRESENTACION ALPRE
 INNER JOIN  
	TBL_ALMACEN ALM  
 ON 
	ALM.ALM_PK = ALPRE.ALM_PK
 WHERE 
	ALM.ALM_HABILITADO = 'T'
 AND 
	ALPRE.STOCK <> ''      
 AND 
	isnumeric (ALPRE.STOCK ) = 1 
 AND
	ALPRE.FLGENABLE = 'T'
	
UNION

 --Listaprecios PRESENTACION
	SELECT 'DROP TABLE IF EXISTS TBL_LISTAPRECIOS_PRESENTACION;' as SCRIPT, 0 as orden    
	UNION    
	SELECT 'CREATE TABLE IF NOT EXISTS TBL_LISTAPRECIOS_PRESENTACION(ID INTEGER not null PRIMARY KEY, IdPresentacion INTEGER, Canal VARCHAR(20) null,CondVta VARCHAR(20) null,Precio VARCHAR(20) not null, DescuentoMinimo VARCHAR(50), DescuentoMaximo VARCHAR(50));' as SCRIPT, 1 as orden    
	UNION  
	select 'INSERT INTO TBL_LISTAPRECIOS_PRESENTACION VALUES('+CAST(LPP.CODIGO as VARCHAR(20))+ ','+CAST(LPP.PRE_PK as VARCHAR(20))+ ',''' + UPPER(RTRIM(LTRIM(ISNULL(lpp.TC_PK,'')))) + ''','''+ ISNULL(LPP.CODIGO_CONDVTA,'') +''','''+ dbo.FORMATO_MONTO_VISTA(LPP.PRECIO) + ''','''+ ISNULL(LPP.DESC_MIN, '0') + ''','''+  ISNULL(LPP.DESC_MAX, '0') + ''');' AS SCRIPT, 2 as orden  
	FROM TBL_LISTAPRECIOS_PRESENTACION  LPP
	INNER JOIN TBL_PRESENTACION PRE ON LPP.PRE_PK = PRE.PRE_PK
	LEFT JOIN #TmpTipoClienteRuta tcr ON tcr.TC_PK = LPP.TC_PK
	WHERE PRE.FLG_ENABLE = 'T'
	AND LPP.FLAG = 'T'

UNION

 --Listaprecios
	SELECT 'DROP TABLE IF EXISTS TBL_LISTAPRECIOS;' as SCRIPT, 0 as orden    
	UNION    
	SELECT 'CREATE TABLE IF NOT EXISTS TBL_LISTAPRECIOS(ID INTEGER not null, Codigo VARCHAR(20) not null,Producto VARCHAR(20) not null,Canal VARCHAR(20) null,CondVta VARCHAR(20) null,Precio VARCHAR(20) not null, DescuentoMinimo VARCHAR(50), DescuentoMaximo VARCHAR(50),CONSTRAINT PkTblListaPrecios PRIMARY KEY (Codigo));' as SCRIPT, 1 as orden    
	UNION  
	SELECT 'INSERT INTO TBL_LISTAPRECIOS VALUES('+CAST(PR.PRO_PK as VARCHAR(20))+ ','''+ CONVERT(VARCHAR, codigo) + ''',''' + codigo_producto + ''',''' + UPPER(RTRIM(LTRIM(ISNULL(lp.TC_PK,'')))) + ''','''+ ISNULL(LP.CODIGO_CONDVTA,'') +''','''+ dbo.FORMATO_MONTO_VISTA(LP.PRECIO) + ''','''+ ISNULL(LP.DESC_MIN, '0') + ''','''+  ISNULL(LP.DESC_MAX, '0') + ''');' AS SCRIPT, 2 as orden  
	FROM TBL_LISTAPRECIOS  LP
	INNER JOIN TBL_PRODUCTO PR  ON LP.CODIGO_PRODUCTO = PRO_CODIGO
	LEFT JOIN #TmpTipoClienteRuta tcr ON tcr.TC_PK = LP.TC_PK
	WHERE PR.FLGENABLE = 'T'
	AND LP.FLAG = 'T'

UNION

--BONIFICACION
SELECT
		'DROP TABLE IF EXISTS TBL_BONIFICACION;' as SCRIPT, 0 as orden    
UNION    
	SELECT 
		'CREATE TABLE IF NOT EXISTS TBL_BONIFICACION(ID INTEGER NOT NULL PRIMARY KEY, IdProducto INTEGER, IdPresentacion INTEGER, CodigoArticulo VARCHAR(20), NombreArticulo VARCHAR(100), Cantidad VARCHAR(20), Maximo VARCHAR(20), Editable VARCHAR(20), TipoArticulo CHAR(3), Prioridad INTEGER);' as SCRIPT, 1 as orden    
UNION
	SELECT 
		'INSERT INTO TBL_BONIFICACION VALUES(' + CAST(BON.BON_PK as VARCHAR(20)) + ', ' + CAST(ISNULL(BON.PRO_PK, 0) as VARCHAR(20)) + ', ' + CAST(ISNULL(BON.PRE_PK, 0) as VARCHAR(20)) + ', ''' + BON.PROD_CODIGO +''', ''' + BON.PROD_NOMBRE +''', ''' + BON.BON_CANTIDAD + ''', ''' + BON.BON_MAXIMO +''', ''' + BON.BON_EDITABLE +''', ''' + BON.TIPO_ARTICULO +''', ' +  CAST(BON.BON_PRIORIDAD as VARCHAR(20)) + ');' AS SCRIPT, 2 as orden 
	FROM 
		TBL_BONIFICACION BON
	INNER JOIN TBL_BONIFICACION_DETALLE BONDET ON BON.BON_PK = BONDET.BON_PK
	WHERE
		BON.FLAGHABILITADO = 'T'
UNION			

--BONIFICACION DETALLE
	SELECT
		'DROP TABLE IF EXISTS TBL_BONIFICACION_DETALLE;' as SCRIPT, 0 as orden    
UNION    
	SELECT 
		'CREATE TABLE IF NOT EXISTS TBL_BONIFICACION_DETALLE(ID INTEGER NOT NULL PRIMARY KEY, IdBonificacion INTEGER, IdProducto INTEGER, IdPresentacion INTEGER, CodigoArticulo VARCHAR(20), NombreArticulo VARCHAR(100), Cantidad VARCHAR(20), Existe CHAR(1));' as SCRIPT, 1 as orden    
UNION
	SELECT 
		'INSERT INTO TBL_BONIFICACION_DETALLE VALUES(' + CAST(BONDET.BON_DET_PK as VARCHAR(20)) + ',' + CAST(BONDET.BON_PK as VARCHAR(20)) + ', ' + CAST(ISNULL(BONDET.PRO_PK, 0) as VARCHAR(20)) + ', ' + CAST(ISNULL(BONDET.PRE_PK, 0) as VARCHAR(20)) + ', ''' + BONDET.PROD_CODIGO +''', ''' + BONDET.PROD_NOMBRE +''', ''' + ISNULL(BONDET.PROD_CANTIDAD,0) + ''', ''' + BONDET.PROD_EXISTE +''');' AS SCRIPT, 2 as orden 
	FROM 
		TBL_BONIFICACION_DETALLE BONDET
	INNER JOIN TBL_BONIFICACION BON ON BONDET.BON_PK = BON.BON_PK
	WHERE
		BON.FLAGHABILITADO = 'T'
UNION
--Generales
SELECT 'DROP TABLE IF EXISTS TBL_GENERAL;' as SCRIPT, 0 as orden    
 union    
 SELECT 'CREATE TABLE IF NOT EXISTS TBL_GENERAL(pk INTEGER PRIMARY KEY,Grupo VARCHAR(20) not null,Codigo VARCHAR(20) not null,Descripcion VARCHAR(150) not null, FlagBanco CHAR(1) null, FlagDocumento CHAR(1) null,FlagFechaDiferida CHAR(1) null);' as SCRIPT, 1 as orden    
 union  
 select 'INSERT INTO TBL_GENERAL(Grupo,Codigo,Descripcion) VALUES(''1'',''' + UPPER(mot_codigo) + ''',''' + UPPER(mot_nombre) + ''');' AS SCRIPT, 2 as orden  
  from tbl_motivo  
  where FLGENABLE = 'T'
  AND upper(mot_tipo) = 'N'  
 union           
 select 'INSERT INTO TBL_GENERAL(Grupo,Codigo,Descripcion) VALUES(''2'',''' + UPPER(condvta_codigo) + ''',''' + UPPER(condvta_nombre) + ''');' AS SCRIPT, 2 as orden  
  from tbl_condvta
  WHERE  FLGENABLE = 'T'
 union           
 select 'INSERT INTO TBL_GENERAL(Grupo,Codigo,Descripcion) VALUES(''3'',''' + UPPER(BAN_CODIGO) + ''',''' + UPPER(BAN_NOMCORTO) + ''');' AS SCRIPT, 2 as orden  
  from TBL_BANCO
  WHERE  FLGENABLE = 'T'
 union           
 select 'INSERT INTO TBL_GENERAL(Grupo,Codigo,Descripcion,FlagBanco,FlagDocumento,FlagFechaDiferida ) VALUES(''4'',''' + UPPER(FPC_CODIGO) + ''',''' + UPPER(FPC_NOMBRE) + ''',''' + UPPER(FPC_FLGBANCO)+ ''',''' +UPPER(FPC_FLGDOCUMENTO)+''',''' +UPPER(FPC_FLGFECHADIFERIDA)+  ''');' AS SCRIPT, 2 as orden  
  from TBL_FPAGO_COBRANZA where FPC_FLGENABLE = 'T'
 union           
 select 'INSERT INTO TBL_GENERAL(Grupo,Codigo,Descripcion) VALUES(''5'',''' + UPPER(MOT_CODIGO) + ''',''' + UPPER(MOT_NOMBRE) + ''');' AS SCRIPT, 2 as orden  
  from TBL_MOTIVO
  WHERE  FLGENABLE = 'T'
  AND UPPER(MOT_TIPO) = 'C'
 union           
 select 'INSERT INTO TBL_GENERAL(Grupo,Codigo,Descripcion) VALUES(''6'',''' + UPPER(MOT_CODIGO) + ''',''' + UPPER(MOT_NOMBRE) + ''');' AS SCRIPT, 2 as orden  
  from TBL_MOTIVO
  WHERE  FLGENABLE = 'T'
  AND UPPER(MOT_TIPO) = 'D'
 union           
 select 'INSERT INTO TBL_GENERAL(Grupo,Codigo,Descripcion) VALUES(''7'',''' + UPPER(TC_CODIGO) + ''',''' + UPPER(TC_NOMBRE) + ''');' AS SCRIPT, 2 as orden  
  from TBL_TIPO_CLIENTE
  WHERE  FLGENABLE = 'T'
 UNION
 --Cobranza
 SELECT 'DROP TABLE IF EXISTS TBL_COBRANZA;' as SCRIPT, 0 as orden    
 union    
 SELECT 'CREATE TABLE IF NOT EXISTS TBL_COBRANZA(COB_PK INTEGER, COB_CODIGO VARCHAR(20), COB_NUMDOCUMENTO VARCHAR(20), COB_TIPODOCUMENTO VARCHAR(20), COB_MONTOPAGADO VARCHAR(20), COB_MONTOTOTAL VARCHAR(20), COB_FECVENCIMIENTO VARCHAR(20), COB_SERIE VARCHAR(20), CLI_CODIGO VARCHAR(20), COB_EMPRESA VARCHAR(20), COB_USUARIO VARCHAR(20), CLI_PK BIGINT, SALDO VARCHAR(20), CONSTRAINT PkTblCobra PRIMARY KEY (COB_PK));' as SCRIPT, 1 as orden    
 union  
 SELECT 'INSERT INTO TBL_COBRANZA VALUES('+ CAST(COB_PK as VARCHAR(20)) + 
		',''' + COB_CODIGO + 
		''',''' + UPPER(COB_NUMDOCUMENTO) + 
		''',''' + COB_TIPODOCUMENTO + 
		''',''' + COB_MONTOPAGADO +
		''',''' + COB_MONTOTOTAL + 
		''',''' + CONVERT(VARCHAR,COB_FECVENCIMIENTO,103) +
		''',''' + ISNULL(COB_SERIE,'')+ 
		''',''' + ISNULL(CLI_CODIGO,'')+ 
		''',''' + COB_EMPRESA+ 
		''',''' + COB_USUARIO+
		''','+ CAST(ISNULL(CLI_PK,'') as VARCHAR(20)) + 
		',''' + CAST((CONVERT(NUMERIC(18,2),COBRA.COB_MONTOTOTAL) - CONVERT(NUMERIC(18,2),COBRA.COB_MONTOPAGADO)) as VARCHAR(20)) + ''');' AS SCRIPT, 2 as orden
  FROM TBL_COBRANZA COBRA         
  WHERE (CONVERT(NUMERIC(18,2),COBRA.COB_MONTOTOTAL) - CONVERT(NUMERIC(18,2),COBRA.COB_MONTOPAGADO)) > 0
  --AND COBRA.COB_USUARIO = @PAR_CODUSUARIO
  AND COBRA.CLI_CODIGO in (select * from @tblcliente_temp)
  AND COBRA.FLGENABLE='T'
   -- Grupo
  UNION
  SELECT 'DROP TABLE IF EXISTS CFGrupo;' as SCRIPT, 0 as orden          
  UNION            
  SELECT 'CREATE TABLE IF NOT EXISTS CFGrupo(IdGrupo int not null, NombreGrupo VARCHAR(100) null,CodigoDetalle VARCHAR(50) null,NombreDetalle VARCHAR(100) null);' as SCRIPT, 1 as orden          
  UNION
  SELECT 'INSERT INTO CFGrupo VALUES('+ cast(G.IdGrupo as varchar(20))+','''+G.Nombre+''','''+GD.CodGrupoDetalle+''','''+GD.Nombre+''');' AS SCRIPT, 2 as orden
  from CFGrupo G
  inner join CFGrupoDetalle GD on G.IdGrupo = GD.IdGrupo
  where G.IdGrupo in (	select distinct(idGrupo)
 						from CFControl
 						where idGrupo is not null
 						and FlgHabilitado = 'T')
  and G.FlgHabilitado = 'T'
  and GD.FlgHabilitado = 'T'
  UNION
  
  --Prospectos
 SELECT 'DROP TABLE IF EXISTS TrProspecto;' as SCRIPT, 0 as orden
 UNION    
 SELECT 'CREATE TABLE IF NOT EXISTS TrProspecto(Id VARCHAR(50) not null, Codigo VARCHAR(20) not null,Nombre VARCHAR(100) not null,Direccion VARCHAR(100) not null,TcliCod VARCHAR(30) not null,Giro VARCHAR(100) not null, Latitud VARCHAR(50), Longitud VARCHAR(50),UserCod VARCHAR(20),CampoAdic1 VARCHAR(100),CampoAdic2 VARCHAR(100),CampoAdic3 VARCHAR(100),CampoAdic4 VARCHAR(100),CampoAdic5 VARCHAR(100),FechaMovil VARCHAR(20),Observacion VARCHAR(100),FlgEnviado CHAR(1), CONSTRAINT PkTblProducto PRIMARY KEY (Codigo));' as SCRIPT, 1 as orden     
  
  UNION
  
  
  -- Controles
  SELECT 'DROP TABLE IF EXISTS CFControl;' as SCRIPT, 0 as orden 
  UNION 
  SELECT 'CREATE TABLE IF NOT EXISTS CFControl(IdControl bigint not null, idFormulario int null, EtiquetaControl varchar(100) not null, IdTipoControl bigint null, MaxCaracteres int null, IdGrupo int null, FlgObligatorio char(1) null, Orden int null);' as SCRIPT, 1 as orden          
  UNION
  SELECT 'INSERT INTO CFControl VALUES('+cast(IdControl as varchar(20))+','+cast(idFormulario as varchar(20))+','''+EtiquetaControl+''','+cast(IdTipoControl as varchar(20))+','+cast(MaxCaracteres as varchar(20))+','+cast(isnull(IdGrupo,0) as varchar(20))+','''+FlgObligatorio+''','+cast(Orden as varchar(20))+');' AS SCRIPT, 2 as orden
  from CFControl
  WHERE FlgHabilitado = 'T'
  and idTipoControl != 9

 --ORDER BY 2, 1
 ) t
 WHERE SCRIPT IS NOT NULL
 ORDER BY 2, 1




select 'Cliente : '+ cast(count(*) as varchar(20)) as Total , 0 as orden 
 FROM TBL_RUTA R              
  INNER JOIN TBL_CLIENTE C ON R.CLI_COD = C.CLI_CODIGO                
  WHERE  R.VEN_COD = @PAR_CODUSUARIO
   AND SUBSTRING(R.RUTA_DIAVISITA,@DIA,1) = '1'
   AND C.FLGENABLE='T'            
   AND R.FLGENABLE='T'
union

select ' Articulo : '+ cast(count(pro_codigo) as varchar(20)) as Total  , 1 as orden
 from tbl_producto  
 where flgenable='T'   
 UNION
 select ' Cobranza : ' +cast(count(COB_PK) as varchar(20)) as Total  , 2 as orden
 FROM TBL_COBRANZA COBRA         
  WHERE (CONVERT(NUMERIC(18,2),COBRA.COB_MONTOTOTAL) - CONVERT(NUMERIC(18,2),COBRA.COB_MONTOPAGADO)) > 0          
  --AND COBRA.COB_USUARIO = @PAR_CODUSUARIO  
  AND COBRA.CLI_CODIGO in (select * from @tblcliente_temp)
  AND COBRA.FLGENABLE='T' 
union  
select ' Precio Producto : '+ cast(count(codigo) as varchar(20)) as Total  , 3 as orden
	FROM TBL_LISTAPRECIOS  LP
	INNER JOIN TBL_PRODUCTO PR 
	ON LP.CODIGO_PRODUCTO = PRO_CODIGO
	WHERE PR.FLGENABLE = 'T'
	AND LP.FLAG = 'T'  
 union  
 select ' Precio Presentacion: '+ cast(count(LP.codigo) as varchar(20)) as Total  , 3 as orden
	FROM TBL_LISTAPRECIOS_PRESENTACION  LP
	INNER JOIN TBL_PRESENTACION PR 
	ON LP.PRE_PK = PR.PRE_PK
	WHERE PR.FLG_ENABLE = 'T'
	AND LP.FLAG = 'T' 	
	
 union
 select ' Motivo noVta : ' +cast(count(mot_codigo) as varchar(20)) as Total  , 4 as orden
 from tbl_motivo  
  where FLGENABLE = 'T'
  AND upper(mot_tipo) = 'N'
 union           
 select ' CondVta : ' +cast(count(condvta_codigo) as varchar(20)) as Total  , 5 as orden
  from tbl_condvta  
  where FLGENABLE = 'T'
 union           
 select ' Banco : ' +cast(count(BAN_CODIGO) as varchar(20)) as Total  ,6 as orden
  from TBL_BANCO
  WHERE  FLGENABLE = 'T'
 --union           
 --select ' Tipo pago : ' +cast(count(FPC_CODIGO) as varchar(20)) as Total  , 7 as orden
 -- from TBL_FPAGO_COBRANZA
 union
 select ' Motivo canje : ' +cast(count(mot_codigo) as varchar(20)) as Total  , 8 as orden
 from tbl_motivo  
  where FLGENABLE = 'T'
  AND upper(mot_tipo) = 'C'
 union
 select ' Motivo devolucion : ' +cast(count(mot_codigo) as varchar(20)) as Total , 9 as orden 
 from tbl_motivo  
  where FLGENABLE = 'T'
  AND upper(mot_tipo) = 'D'
  union 
  select 'Tipo Pago Doc : '++cast(count(FPC_CODIGO) as varchar(20)) as Total , 10 as orden 
  from TBL_FPAGO_COBRANZA
  where fpc_flgenable = 'T'
  
ORDER BY orden

END 



GO

/****** Object:  StoredProcedure [dbo].[USPS_REPORTE_DINAMICO_CONSOLIDADO]    Script Date: 22/06/2018 04:53:58 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[USPS_REPORTE_DINAMICO_CONSOLIDADO]
(      
	@par_codusuario VARCHAR(10) ,      
    @par_fecinicio VARCHAR(20) ,      
    @par_fecfin VARCHAR(20) ,           
    @pag_actual INT ,      
    @numero_registros INT, 
	@CODPERFIL VARCHAR(20) ,
    @CODGRUPO VARCHAR(10)   ,
	@Idusu varchar(10)   )                
AS  
BEGIN                    
    
 --   --
	--DECLARE @par_codusuario VARCHAR(10) = '-1';
	--DECLARE @par_fecinicio VARCHAR(20) = '20150528';
	--DECLARE @par_fecfin VARCHAR(20) = '20150528';
 --   DECLARE @pag_actual INT = 1;
 --   DECLARE @numero_registros INT = 8;
    
 --   --
        
    DECLARE @MAXIMO INT;                                      
    DECLARE @MINIMO INT;          
	DECLARE @columnas VARCHAR(MAX);
	DECLARE @nombre_columnas VARCHAR(MAX);
	DECLARE @tabla_temporal VARCHAR(MAX);
	DECLARE @SQLString NVARCHAR(MAX);
	DECLARE @CANTREG INT;

	SET @par_fecinicio = [dbo].FECHA_ADD(@par_fecinicio, '00', '00', '00')                          
	SET @par_fecfin = [dbo].FECHA_ADD(@par_fecfin, '23', '59', '59')                        

	SELECT  @MAXIMO = ( @pag_actual * @numero_registros )                                      
	SELECT  @MINIMO = @MAXIMO - ( @numero_registros - 1 )   
	
    IF OBJECT_ID('tempdb..#TmpControlColumnas') IS NOT NULL BEGIN DROP TABLE #TmpControlColumnas END;
	 
	CREATE TABLE #TABLE_TEMP (COLUMNA_AUX VARCHAR(100))  
	CREATE TABLE #TABLE_TEMPPERFIL (COLUMNA_AUX VARCHAR(100))   
    INSERT INTO #TABLE_TEMP(COLUMNA_AUX)
    select GD.Grupo_PK as COLUMNA_AUX from TBL_USUARIO U 
	                                       INNER JOIN GRGrupoDetalle GD ON U.USR_PK=GD.Usuario_pk 
										   WHERE U.USR_PK=@Idusu    
	 INSERT INTO #TABLE_TEMPPERFIL(COLUMNA_AUX)
     select GD.Grupo_PK as COLUMNA_AUX from TBL_USUARIO U 
	                                       INNER JOIN GRGrupoDetalle GD ON U.USR_PK=GD.Usuario_pk 
										   WHERE U.USR_PK=@CODPERFIL 
    CREATE TABLE #TmpControlColumnas(
     IdControl INT,
     EtiquetaControl VARCHAR(200));
     
     INSERT 
		#TmpControlColumnas
	  SELECT  
		cfc.IdControl,
		cfc.EtiquetaControl
	  FROM 
		CFControl cfc
		where IdTipoControl != 9
	  ORDER BY 
	    cfc.IdFormulario,cfc.Orden;
			
	SELECT @CANTREG = COUNT('X') FROM #TmpControlColumnas
			
	IF (@CANTREG > 0)
	BEGIN
					
		SET @SQLString = N'DECLARE @TAMTOTAL INT; ';
		SET @SQLString = @SQLString+ N' IF OBJECT_ID(''tempdb..#' + 'TmpControl' + ''') IS NOT NULL BEGIN DROP TABLE #' + 'TmpControl' + ' END;';
		SET @columnas = '';
		SET @nombre_columnas = '';
		SET @tabla_temporal ='';
		
		SELECT @columnas =  COALESCE(@columnas + '[' + CAST(EtiquetaControl AS VARCHAR(60)) + '],', ''), 
		@nombre_columnas =  COALESCE(@nombre_columnas + ',[' + CAST(EtiquetaControl AS VARCHAR(60)) + '] AS ' +''''+ EtiquetaControl+'''', ''),
		@tabla_temporal  =  COALESCE(@tabla_temporal + '['+ EtiquetaControl +'] VARCHAR(200) ,', '')
		FROM (
				SELECT  
					cfc.IdControl,
					cfc.EtiquetaControl
				FROM 
					#TmpControlColumnas cfc
			) AS Detalle_Columnas


		SET @nombre_columnas = 'Pedido,[Vendedor],[Grupo],[Código Cliente],[Cliente]' + @nombre_columnas		

		SET @columnas = left(@columnas,LEN(@columnas)-1)
			
		SET @tabla_temporal = N' CREATE TABLE #TmpControl( orden INT IDENTITY(1,1) ,Pedido VARCHAR(200),[Vendedor] VARCHAR(200),[Grupo] VARCHAR(200),[Código Cliente] VARCHAR(200),[Cliente] VARCHAR(200),' +left(@tabla_temporal,LEN(@tabla_temporal)-1)+');'

		SET @SQLString = @SQLString + @tabla_temporal+ N' INSERT #TmpControl SELECT ' + @nombre_columnas + '
		FROM
		(
			SELECT DISTINCT
				trd.IdPedido AS Pedido,
				tu.Usr_Nombre AS Vendedor,
				G.NombreGrupo AS Grupo , 
				tc.Cli_Codigo AS [Código Cliente],
				tc.Cli_Nombre AS [Cliente],
				trd.EtiquetaControl,
				CASE WHEN CFG.CODGRUPODETALLE IS NULL THEN trd.ValorControl ELSE CFG.NOMBRE END AS ValorControl
			FROM 
				TrDetalleControl trd
				INNER JOIN TBL_PEDIDO tp ON trd.IdPedido = tp.PED_PK
				INNER JOIN TBL_USUARIO tu ON tp.Usr_Codigo = tu.Usr_Codigo
				LEFT JOIN GRPerfil PER ON tu.IdPerfil=PER.IdPerfil
				LEFT JOIN CFTipoPerfil TPe ON PER.IdTipoPerfil=TPe.IdTipoPerfil 
				LEFT JOIN GRGrupoDetalle GD ON tu.USR_PK=GD.Usuario_pk  
				LEFT JOIN GRGrupo G ON GD.Grupo_PK=G.Grupo_PK
				INNER JOIN TBL_CLIENTE tc ON tp.Cli_Codigo = tc.Cli_Codigo
				LEFT JOIN CFControl CFC ON CFC.EtiquetaControl = trd.EtiquetaControl
				LEFT JOIN CFGrupoDetalle CFG ON CFC.idgrupo = CFG.IDGRUPO AND trd.VALORCONTROL = CFG.CODGRUPODETALLE
				WHERE
				'+'('''+@par_codusuario+'''= '''+'-1'+'''	OR tp.USR_PK = '''+@par_codusuario+''')'+      
				'AND tp.PED_FECINICIO BETWEEN dbo.STRING_FECHA('''+@par_fecinicio+''')'+
				'AND dbo.STRING_FECHA('''+@par_fecfin+''')'+ 
				'AND ('''+@CODPERFIL+''' = '''+' '+''' OR G.Grupo_PK in (select COLUMNA_AUX from #TABLE_TEMPPERFIL) and not tu.VEN_PERFIL=''SUP'')'+ 
				'AND ('''+@CODGRUPO+''' = '''+'0'+''' OR  G.Grupo_PK = '''+@CODGRUPO+''')'+ 
				'AND ('''+@Idusu+''' = '''+' '+''' OR  G.Grupo_PK in (select COLUMNA_AUX from #TABLE_TEMP) and not tu.VEN_PERFIL=''SUP'')
					
			) AS SourceTable PIVOT
		(
		MAX(ValorControl)
		FOR EtiquetaControl IN ( ' + @columnas + ')
		) AS TablaPivot; ';
		

		
		SET @SQLString = @SQLString + ' SET @TAMTOTAL = @@ROWCOUNT; ';
		SET @SQLString = @SQLString +' SELECT *, @TAMTOTAL AS TAMANIOTOTAL FROM #TmpControl WHERE orden BETWEEN '+ CAST(@MINIMO AS VARCHAR(60)) +' AND '+ CAST(@MAXIMO AS VARCHAR(60))+';'

		
		--print @SQLString
		EXECUTE sp_executesql @SQLString;
	END
	ELSE
	BEGIN
		select 0 as orden, '0' as TAMANIOTOTAL
	END
	        
END
SET ANSI_NULLS ON


GO

/****** Object:  StoredProcedure [dbo].[USPS_REPORTE_DINAMICO_CONSOLIDADO_EXCEL]    Script Date: 22/06/2018 04:54:28 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[USPS_REPORTE_DINAMICO_CONSOLIDADO_EXCEL]
(      
	@par_codusuario VARCHAR(10) ,      
    @par_fecinicio VARCHAR(20) ,      
    @par_fecfin VARCHAR(20),
	@CODPERFIL VARCHAR(20) ,
    @CODGRUPO VARCHAR(10),
	 @Idusu VARCHAR(10) )                     
AS  
BEGIN                    
	
	
	DECLARE @columnas VARCHAR(MAX);
	DECLARE @nombre_columnas VARCHAR(MAX);
	DECLARE @tabla_temporal VARCHAR(MAX);
	DECLARE @SQLString NVARCHAR(MAX);
	
	SET @par_fecinicio = [dbo].FECHA_ADD(@par_fecinicio, '00', '00', '00')                          
	SET @par_fecfin = [dbo].FECHA_ADD(@par_fecfin, '23', '59', '59')                        
    
    IF OBJECT_ID('tempdb..#TmpControlColumnas') IS NOT NULL BEGIN DROP TABLE #TmpControlColumnas END; 
	CREATE TABLE #TABLE_TEMP (COLUMNA_AUX VARCHAR(100))  
	CREATE TABLE #TABLE_TEMPPERFIL (COLUMNA_AUX VARCHAR(100))   
    INSERT INTO #TABLE_TEMP(COLUMNA_AUX)
    select GD.Grupo_PK as COLUMNA_AUX from TBL_USUARIO U 
	                                       INNER JOIN GRGrupoDetalle GD ON U.USR_PK=GD.Usuario_pk 
										   WHERE U.USR_PK=@Idusu    
	 INSERT INTO #TABLE_TEMPPERFIL(COLUMNA_AUX)
     select GD.Grupo_PK as COLUMNA_AUX from TBL_USUARIO U 
	                                       INNER JOIN GRGrupoDetalle GD ON U.USR_PK=GD.Usuario_pk 
										   WHERE U.USR_PK=@CODPERFIL 
    CREATE TABLE #TmpControlColumnas(
     IdControl INT,
     EtiquetaControl VARCHAR(100));
     
     INSERT 
		#TmpControlColumnas
	  SELECT  
		cfc.IdControl,
		cfc.EtiquetaControl
	  FROM 
		CFControl cfc
	  where IdTipoControl != 9
	  ORDER BY 
	    cfc.IdFormulario,cfc.Orden;
	    

	    
	SET @SQLString = N' IF OBJECT_ID(''tempdb..#' + 'TmpControl' + ''') IS NOT NULL BEGIN DROP TABLE #' + 'TmpControl' + ' END;';
	SET @columnas = '';
	SET @nombre_columnas = '';
	SET @tabla_temporal ='';
	SELECT @columnas =  COALESCE(@columnas + '[' + CAST(EtiquetaControl AS VARCHAR(60)) + '],', ''), 
	@nombre_columnas =  COALESCE(@nombre_columnas + ',[' + CAST(EtiquetaControl AS VARCHAR(60)) + '] AS ' +''''+ EtiquetaControl+'''', ''),
	@tabla_temporal  =  COALESCE(@tabla_temporal + '['+ EtiquetaControl +'] VARCHAR(200) ,', '')
	FROM (
			SELECT DISTINCT 
				cfc.IdControl,
				cfc.EtiquetaControl
			FROM 
				#TmpControlColumnas cfc
		) AS Detalle_Columnas

	SET @nombre_columnas = 'Pedido,[Código Vendedor],[Vendedor],[Grupo],[Código Cliente],[Cliente]' + @nombre_columnas
	SET @columnas = left(@columnas,LEN(@columnas)-1)
		
	SET @tabla_temporal = N' CREATE TABLE #TmpControl(Pedido VARCHAR(200),[Código Vendedor] VARCHAR(200),[Vendedor] VARCHAR(200),[Grupo] VARCHAR(200),[Código Cliente] VARCHAR(200),[Cliente] VARCHAR(200),' +left(@tabla_temporal,LEN(@tabla_temporal)-1)+');'
	
	SET @SQLString = @SQLString + @tabla_temporal+ N' INSERT #TmpControl SELECT ' + @nombre_columnas + ' 
	FROM
	(
		SELECT 
			trd.IdPedido AS Pedido,
			tu.Usr_Codigo AS [Código Vendedor],
			tu.Usr_Nombre AS [Vendedor],
			G.NombreGrupo AS Grupo ,
			tc.Cli_Codigo AS [Código Cliente],
			tc.Cli_Nombre AS [Cliente],
			trd.EtiquetaControl,
			CASE WHEN CFG.CODGRUPODETALLE IS NULL THEN trd.ValorControl ELSE CFG.NOMBRE END AS ValorControl
		FROM 
			TrDetalleControl trd
			INNER JOIN TBL_PEDIDO tp ON trd.IdPedido = tp.PED_PK
			INNER JOIN TBL_USUARIO tu ON tp.Usr_Codigo = tu.Usr_Codigo
			LEFT JOIN GRPerfil PER ON tu.IdPerfil=PER.IdPerfil
            LEFT JOIN CFTipoPerfil TPe ON PER.IdTipoPerfil=TPe.IdTipoPerfil 
            LEFT JOIN GRGrupoDetalle GD ON tu.USR_PK=GD.Usuario_pk  
            LEFT JOIN GRGrupo G ON GD.Grupo_PK=G.Grupo_PK
			INNER JOIN TBL_CLIENTE tc ON tp.Cli_Codigo = tc.Cli_Codigo
			LEFT JOIN CFControl CFC ON CFC.EtiquetaControl = trd.EtiquetaControl
			LEFT JOIN CFGrupoDetalle CFG ON CFC.idgrupo = CFG.IDGRUPO AND trd.VALORCONTROL = CFG.CODGRUPODETALLE
		WHERE'+             
			'('''+@par_codusuario+'''= '''+'-1'+'''    
				OR tp.USR_PK = '''+@par_codusuario+'''     
            )      
			AND tp.PED_FECINICIO BETWEEN dbo.STRING_FECHA('''+@par_fecinicio+''')'+      
			'AND dbo.STRING_FECHA('''+@par_fecfin+''')'+
			'AND ('''+@CODPERFIL+''' = '''+' '+'''OR G.Grupo_PK in (select COLUMNA_AUX from #TABLE_TEMPPERFIL) and not tu.VEN_PERFIL=''SUP'')'+ 
			'AND ('''+@CODGRUPO+''' = '''+'0'+''' OR  G.Grupo_PK = '''+@CODGRUPO+''')'+ 
			'AND ('''+@Idusu+''' = '''+' '+''' OR  G.Grupo_PK in (select COLUMNA_AUX from #TABLE_TEMP) and not tu.VEN_PERFIL=''SUP'')
				
		) AS SourceTable PIVOT
	(
	MAX(ValorControl)
	FOR EtiquetaControl IN ( ' + @columnas + ')
	) AS TablaPivot; ';
	
	SET @SQLString = @SQLString +' SELECT * FROM #TmpControl;'
	EXECUTE sp_executesql @SQLString;
	        
END
SET ANSI_NULLS ON


GO

/****** Object:  StoredProcedure [dbo].[USPS_REPCANJE]    Script Date: 26/06/2018 07:40:37 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[USPS_REPCANJE]                  
(     
 @IN_USUARIO INT,                 
 @IN_FECINICIO VARCHAR(20),                       
 @IN_FECFIN VARCHAR(20),  
 @IN_COBERTURA VARCHAR(10),               
 @PIV_PAG_ACTUAL INT,                       
 @PIV_NUMERO_REGISTROS INT ,
 @TIPO_ARTICULO CHAR(3)    ,
    @CODPERFIL VARCHAR(20) ,
  @CODGRUPO INT  ,
    @Idusu INT                    
)                  
AS                  
                  
DECLARE @MAXIMO INT                        
DECLARE @MINIMO INT                        
DECLARE @TAMTOTAL INT                     
 DECLARE @TABLE_TEMP TABLE (COLUMNA_AUX VARCHAR(100))  
	DECLARE @TABLE_TEMPPERFIL TABLE (COLUMNA_AUX VARCHAR(100))   
    INSERT INTO @TABLE_TEMP(COLUMNA_AUX)
    select GD.Grupo_PK as COLUMNA_AUX from TBL_USUARIO U 
	                                       INNER JOIN GRGrupoDetalle GD ON U.USR_PK=GD.Usuario_pk 
										   WHERE U.USR_PK=@Idusu    
	 INSERT INTO @TABLE_TEMPPERFIL(COLUMNA_AUX)
     select GD.Grupo_PK as COLUMNA_AUX from TBL_USUARIO U 
	                                       INNER JOIN GRGrupoDetalle GD ON U.USR_PK=GD.Usuario_pk 
										   WHERE U.USR_PK=@CODPERFIL                  
BEGIN                  
                  
SET NOCOUNT ON                        
SELECT @MAXIMO = (@PIV_PAG_ACTUAL * @PIV_NUMERO_REGISTROS)                        
SELECT @MINIMO = @MAXIMO - (@PIV_NUMERO_REGISTROS - 1)                   
                  
CREATE TABLE #TEMP_CANJE(                  
ID_CAB_CANJE BIGINT,                  
COD_CANJE VARCHAR(1000),                  
CABC_CODUSUARIO VARCHAR(100),                  
CABC_CODCLIENTE VARCHAR(100),                 
CABC_FECHA DATETIME,                 
CABC_NRODOC VARCHAR(300),                 
CABC_FECHA_CEL DATETIME,                 
ID_DET_CANJE BIGINT,                  
DETC_CODPRODUCTO NVARCHAR(1000),                 
DETC_CANTIDAD VARCHAR(100),                  
DETC_FECVCMTO DATETIME,                  
DETC_OBSERVACION VARCHAR(500),                 
PRO_NOMBRE VARCHAR(1000),                 
USR_NOMBRE VARCHAR(1000),                 
CLI_NOMBRE VARCHAR(1000),                 
MOT_NOMBRE VARCHAR(500),                 
LATITUD VARCHAR(20),                 
LONGITUD VARCHAR(20), 
NOMBRE_PRESENTACION VARCHAR(100),
DETC_CANTIDAD_PRE VARCHAR(100),
PRECIO_PRESENTACION VARCHAR(20), 
UNIDAD_FRACCIONAMIENTO VARCHAR(20),
DETC_CANTIDAD_FRAC VARCHAR(100),
ALM_NOMBRE   VARCHAR(200), 
NOMBREGRUPO VARCHAR(50),          
NORDEN INT IDENTITY(1,1)                  
)                  
 IF @TIPO_ARTICULO = 'PRO'
 BEGIN                 
INSERT #TEMP_CANJE                  
SELECT CC.ID_CAB_CANJE AS ID_CAB_CANJE,
       COD_CANJE AS COD_CANJE,
	   CABC_CODUSUARIO AS CABC_CODUSUARIO,
	   CABC_CODCLIENTE AS CABC_CODCLIENTE,                 
       CABC_FECHA AS CABC_FECHA,
	   CABC_NRODOC AS CABC_NRODOC,                  
       FecMovil AS CABC_FECHA_CEL,                 
       ID_DET_CANJE AS ID_DET_CANJE,
	   DETC_CODPRODUCTO AS DETC_CODPRODUCTO,
	   DETC_CANTIDAD,
	   DETC_FECVCMTO AS DETC_FECVCMTO
	   ,DETC_OBSERVACION AS DETC_OBSERVACION,                 
       P.PRO_NOMBRE AS PRO_NOMBRE,
	   U.USR_NOMBRE AS USR_NOMBRE,
	   CL.CLI_NOMBRE AS CLI_NOMBRE,
	   M.MOT_NOMBRE AS MOT_NOMBRE,
	   dbo.VALIDA_POSICION(CC.LATITUD) AS LATITUD,
	   dbo.VALIDA_POSICION(CC.LONGITUD) AS LONGITUD,
	   '' AS NOMBRE_PRESENTACION,
	   '' AS DETC_CANTIDAD_PRE,
	   '' AS NOMBRE_PRESENTACION,
	   '' AS PRECIO_PRESENTACION,
	   '' AS UNIDAD_FRACCIONAMIENTO,
	   A.ALM_NOMBRE AS ALM_NOMBRE , 
	   G.NombreGrupo AS NOMBREGRUPO              
FROM TBL_CAB_CANJE CC                  
INNER JOIN TBL_DET_CANJE DC ON CC.ID_CAB_CANJE = DC.ID_CAB_CANJE                  
INNER JOIN TBL_MOTIVO M ON DC.DETC_MOTIVO = M.MOT_CODIGO AND MOT_TIPO = 'C'                  
INNER JOIN TBL_USUARIO U ON CC.CABC_CODUSUARIO = U.USR_CODIGO 
LEFT JOIN GRPerfil PER ON U.IdPerfil=PER.IdPerfil
LEFT JOIN CFTipoPerfil TP ON PER.IdTipoPerfil=TP.IdTipoPerfil 
LEFT JOIN GRGrupoDetalle GD ON U.USR_PK=GD.Usuario_pk  
LEFT JOIN GRGrupo G ON GD.Grupo_PK=G.Grupo_PK   
INNER JOIN TBL_PRODUCTO P ON DC.DETC_CODPRODUCTO = P.PRO_CODIGO   
INNER JOIN TBL_CLIENTE CL ON CC.CABC_CODCLIENTE = CL.CLI_CODIGO 
left join TBL_ALMACEN A on DC.ALM_PK=A.ALM_PK  
WHERE
   CABC_TIPO_ARTICULO='PRO' 
  AND CONVERT(VARCHAR,CC.FecMovil,112) BETWEEN @IN_FECINICIO AND @IN_FECFIN                         
  AND ( (@IN_USUARIO = '-1') OR (@IN_USUARIO <> '-1' 
  AND U.USR_PK = @IN_USUARIO) )       
  AND CC.FLGENABLE='T'  
  AND (@IN_COBERTURA = '-1' OR (@IN_COBERTURA <> '-1' AND CC.FlgEnCobertura=@IN_COBERTURA))  
AND (@CODPERFIL = '' or  G.Grupo_PK in (select COLUMNA_AUX from @TABLE_TEMPPERFIL) and not U.VEN_PERFIL='SUP')    
AND (@CODGRUPO = '' or  G.Grupo_PK=@CODGRUPO) 
AND (@Idusu='' OR G.Grupo_PK in (select COLUMNA_AUX from @TABLE_TEMP) and not U.VEN_PERFIL='SUP') 
ORDER BY FecMovil DESC          
;                  
                  
SET   @TAMTOTAL = @@ROWCOUNT                  
                
-- PARA EL REPORTE EXCEL                
IF (@PIV_PAG_ACTUAL = -1 AND @PIV_NUMERO_REGISTROS  = -1)                
BEGIN                
 SET @MINIMO = 1                
 SET @MAXIMO = @TAMTOTAL                
END                
                
SELECT                  
ID_CAB_CANJE,                  
COD_CANJE,                  
CABC_CODUSUARIO,                  
CABC_CODCLIENTE,                 
CABC_FECHA,                 
CABC_NRODOC,                 
CABC_FECHA_CEL,                 
ID_DET_CANJE,                  
DETC_CODPRODUCTO,                 
[dbo].[FORMATO_MONTO_VISTA](DETC_CANTIDAD) AS DETC_CANTIDAD,                  
dbo.FECHA_STRING (DETC_FECVCMTO) AS DETC_FECVCMTO,                      
DETC_OBSERVACION,                 
PRO_NOMBRE,                 
USR_NOMBRE,                 
CLI_NOMBRE,                 
MOT_NOMBRE,                 
LATITUD,                 
LONGITUD,
NOMBRE_PRESENTACION ,
DETC_CANTIDAD_PRE,
PRECIO_PRESENTACION, 
UNIDAD_FRACCIONAMIENTO,
DETC_CANTIDAD_FRAC ,
ALM_NOMBRE  , 
NOMBREGRUPO,           
@TAMTOTAL AS TAMANIOTOTAL             
FROM #TEMP_CANJE                        
WHERE (NORDEN BETWEEN @MINIMO AND @MAXIMO)                    
                  
END
IF @TIPO_ARTICULO = 'PRE'
BEGIN
INSERT #TEMP_CANJE                  
SELECT cc.ID_CAB_CANJE as ID_CAB_CANJE, 
       cc.COD_CANJE as COD_CANJE,
	   cc.CABC_CODUSUARIO as CABC_CODUSUARIO,
	   cc.CABC_CODCLIENTE as CABC_CODCLIENTE,
	   cc.CABC_FECHA as CABC_FECHA,
	   cc.CABC_NRODOC as CABC_NRODOC,
	   cc.FecMovil as CABC_FECHA_CEL,
	   pre.DETC_CANTIDAD_PRE as ID_DET_CANJE,
	   PRO.PRO_CODIGO AS DETC_CODPRODUCTO,
	   PRE.DETC_CANTIDAD AS DETC_CANTIDAD,
	   CONVERT(VARCHAR(10), PRE.DETC_FECVCMTO,112) AS DETC_FECVCMTO,
	   PRE.DETC_OBSERVACION AS DETC_OBSERVACION,
	   PRO.PRO_NOMBRE AS PRO_NOMBRE,
	   U.USR_NOMBRE AS USR_NOMBRE,
	   CLI.CLI_NOMBRE AS CLI_NOMBRE,
	   MOT.MOT_NOMBRE AS MOT_NOMBRE,
	   CC.Latitud AS LATITUD,
	   CC.longitud AS LONGITUD,
	   p.NOMBRE as NOMBRE_PRESENTACION,
	   PRE.DETC_CANTIDAD_PRE AS DETC_CANTIDAD_PRE,
	   P.PRECIO AS PRECIO_PRESENTACION,  
	   P.UNIDAD_FRACCIONAMIENTO,
	   PRE.DETC_CANTIDAD_FRAC AS DETC_CANTIDAD_FRAC,
	   A.ALM_NOMBRE AS ALM_NOMBRE , 
	   G.NombreGrupo AS NOMBREGRUPO 	         
FROM TBL_CAB_CANJE CC 
     left join TBL_DET_CANJE_PRESENTACION PRE on CC.ID_CAB_CANJE=PRE.ID_CAB_CANJE
	 LEFT join  TBL_PRESENTACION p on pre.PRE_PK=p.PRE_PK
	 inner join  TBL_PRODUCTO PRO on PRO.PRO_PK=P.PRO_PK
	 inner join TBL_USUARIO U on CC.CABC_CODUSUARIO=U.USR_CODIGO
	 LEFT JOIN GRPerfil PER ON U.IdPerfil=PER.IdPerfil
     LEFT JOIN CFTipoPerfil TP ON PER.IdTipoPerfil=TP.IdTipoPerfil 
     LEFT JOIN GRGrupoDetalle GD ON U.USR_PK=GD.Usuario_pk  
     LEFT JOIN GRGrupo G ON GD.Grupo_PK=G.Grupo_PK 
	 inner join TBL_CLIENTE CLI on CC.CABC_CODCLIENTE=CLI.CLI_CODIGO
	 inner join TBL_MOTIVO MOT on PRE.DETC_MOTIVO=MOT.MOT_CODIGO AND MOT_TIPO = 'C'
	 left join TBL_ALMACEN A on PRE.ALM_PK=A.ALM_PK
	  WHERE   
	 CABC_TIPO_ARTICULO='PRE' 
     AND CONVERT(VARCHAR,CC.FecMovil,112) BETWEEN @IN_FECINICIO AND @IN_FECFIN                           
     AND ( (@IN_USUARIO = '-1') OR (@IN_USUARIO <> '-1' 
     AND U.USR_PK = @IN_USUARIO) )       
     AND CC.FLGENABLE='T'  
     AND (@IN_COBERTURA = '-1' OR (@IN_COBERTURA <> '-1' AND CC.FlgEnCobertura=@IN_COBERTURA)) 
	AND (@CODPERFIL = '' or  G.Grupo_PK in (select COLUMNA_AUX from @TABLE_TEMPPERFIL) and not U.VEN_PERFIL='SUP')    
AND (@CODGRUPO = '' or  G.Grupo_PK=@CODGRUPO) 
AND (@Idusu='' OR G.Grupo_PK in (select COLUMNA_AUX from @TABLE_TEMP) and not U.VEN_PERFIL='SUP')
ORDER BY FecMovil DESC          
;                               
SET   @TAMTOTAL = @@ROWCOUNT                  
                
-- PARA EL REPORTE EXCEL                
IF (@PIV_PAG_ACTUAL = -1 AND @PIV_NUMERO_REGISTROS  = -1)                
BEGIN                
 SET @MINIMO = 1                
 SET @MAXIMO = @TAMTOTAL                
END                
                
SELECT                  
ID_CAB_CANJE,                  
COD_CANJE,                  
CABC_CODUSUARIO,                  
CABC_CODCLIENTE,                 
CABC_FECHA,                 
CABC_NRODOC,                 
CABC_FECHA_CEL,                 
ID_DET_CANJE,                  
DETC_CODPRODUCTO,                 
[dbo].[FORMATO_MONTO_VISTA](DETC_CANTIDAD) AS DETC_CANTIDAD,                  
dbo.FECHA_STRING (DETC_FECVCMTO) AS DETC_FECVCMTO,                      
DETC_OBSERVACION,                 
PRO_NOMBRE,                 
USR_NOMBRE,                 
CLI_NOMBRE,                 
MOT_NOMBRE,                 
LATITUD,                 
LONGITUD, 
NOMBRE_PRESENTACION,
DETC_CANTIDAD_PRE,
PRECIO_PRESENTACION, 
UNIDAD_FRACCIONAMIENTO,
DETC_CANTIDAD_FRAC ,
ALM_NOMBRE  ,       
NOMBREGRUPO,      
@TAMTOTAL AS TAMANIOTOTAL             
FROM #TEMP_CANJE                        
WHERE (NORDEN BETWEEN @MINIMO AND @MAXIMO)      
END
END

