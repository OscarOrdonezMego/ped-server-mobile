/*
Script de implementación para DB_PED_271

Una herramienta generó este código.
Los cambios realizados en este archivo podrían generar un comportamiento incorrecto y se perderán si
se vuelve a generar el código.
*/

-- LA VERSION 2.7.0 nunca salio a produccion

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
PRINT N'Creando [dbo].[LstColumnasXls]...';


GO
CREATE TYPE [dbo].[LstColumnasXls] AS TABLE (
    [idColumnaxls] VARCHAR (100) NULL,
    [idColumna]    VARCHAR (100) NULL,
    [columnaxls]   VARCHAR (100) NULL);


GO
PRINT N'Modificando [dbo].[CFControl]...';


GO
ALTER TABLE [dbo].[CFControl]
    ADD [FlgEditable] CHAR (1) NULL;


GO
PRINT N'Creando [dbo].[GRMaestroTabla]...';


GO
CREATE TABLE [dbo].[GRMaestroTabla] (
    [id]            BIGINT        IDENTITY (1, 1) NOT NULL,
    [tabla]         VARCHAR (100) NULL,
    [descripcion]   VARCHAR (100) NULL,
    [tablaTemp]     VARCHAR (100) NULL,
    [spcargaTabla]  VARCHAR (100) NULL,
    [FlgHabilitado] CHAR (1)      NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
PRINT N'Creando [dbo].[GRMaestroTablaColumnas]...';


GO
CREATE TABLE [dbo].[GRMaestroTablaColumnas] (
    [id]            BIGINT        IDENTITY (1, 1) NOT NULL,
    [idMaestro]     BIGINT        NULL,
    [columna]       VARCHAR (100) NULL,
    [columnaTabla]  VARCHAR (100) NULL,
    [descripcion]   VARCHAR (100) NULL,
    [formato]       VARCHAR (100) NULL,
    [obligatorio]   CHAR (1)      NULL,
    [FlgHabilitado] CHAR (1)      NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
PRINT N'Creando [dbo].[GRMapeoMaestroTabla]...';


GO
CREATE TABLE [dbo].[GRMapeoMaestroTabla] (
    [id]          BIGINT        IDENTITY (1, 1) NOT NULL,
    [idMaestro]   BIGINT        NULL,
    [idColumna]   BIGINT        NULL,
    [columnaXls]  VARCHAR (100) NULL,
    [tipoFormato] CHAR (1)      NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
PRINT N'Creando [dbo].[FK_GRMaestroTablaColumnas_GRMaestroTabla]...';


GO
ALTER TABLE [dbo].[GRMaestroTablaColumnas] WITH NOCHECK
    ADD CONSTRAINT [FK_GRMaestroTablaColumnas_GRMaestroTabla] FOREIGN KEY ([idMaestro]) REFERENCES [dbo].[GRMaestroTabla] ([id]);


GO
PRINT N'Modificando [dbo].[USPS_JSON_SINCRONIZAR_TODO]...';


GO


ALTER PROCEDURE [dbo].[USPS_JSON_SINCRONIZAR_TODO] 
 @PAR_CODUSUARIO as varchar(20)
--,
-- @PAR_FECHAMOVIL VARCHAR(20)
as
 BEGIN             
 SET DATEFIRST 1         
 
  
 DECLARE @DIA INT            
 SELECT @DIA = DATEPART(WEEKDAY,dbo.STRING_FECHA(getdate()))   
 
 
 --Validar Stock
 DECLARE @STOCK INT      
 SELECT 
	@STOCK = convert(INT, 
		CASE 
			WHEN Valor = 'T' 
				THEN 1 
				ELSE 0 
		END) 
 FROM 
	CFConfiguracion 
 WHERE 
	CodConfiguracion = 'STST'
	

 --Validar Precio mayor que 0
DECLARE @PRECIO INT      
 SELECT 
	@PRECIO = convert(INT, 
		CASE 
			WHEN Valor = 'T' 
				THEN 1 
				ELSE 0 
		END) 
 FROM 
	CFConfiguracion 
 WHERE 
	CodConfiguracion = 'PRVP'

 --Configuracion Precio - Tipo de Cliente
DECLARE @PRECIO_TIPOCLI INT      
SELECT 
	@PRECIO_TIPOCLI = convert(INT, 
		CASE 
			WHEN Valor = 'T' 
				THEN 1 
				ELSE 0 
		END) 
FROM 
	CFConfiguracion 
WHERE 
	CodConfiguracion = 'PRTC'

 --Configuracion Precio - Condicion de Venta
DECLARE @PRECIO_CONDVTA INT      
SELECT 
	@PRECIO_CONDVTA = convert(INT, 
		CASE 
			WHEN Valor = 'T' 
				THEN 1 
				ELSE 0 
		END) 
FROM 
	CFConfiguracion 
WHERE 
	CodConfiguracion = 'PRCV'
 
	IF OBJECT_ID('tempdb..#TmpTipoClienteRuta') IS NOT NULL BEGIN DROP TABLE #TmpTipoClienteRuta END; 
		CREATE TABLE #TmpTipoClienteRuta ( 
		ID BIGINT IDENTITY(1,1) NOT NULL, 
		TC_PK INT NULL)
	
	INSERT 
		#TmpTipoClienteRuta
	SELECT DISTINCT 
		tc.TC_PK 
	FROM 
		tbl_cliente tc
	JOIN tbl_ruta tr ON tr.cli_cod = tc.cli_codigo
    WHERE
	     tr.ven_cod = @PAR_CODUSUARIO

declare @tblcliente_temp as table (CLI_CODIGO VARCHAR(20));
 insert into @tblcliente_temp
 select distinct(R.CLI_COD) 
 FROM TBL_RUTA R              
  INNER JOIN TBL_CLIENTE C ON R.CLI_COD = C.CLI_CODIGO     
  LEFT JOIN TBL_COBRANZA CO ON C.CLI_PK = CO.CLI_PK AND C.CLI_CODIGO = CO.CLI_CODIGO
  LEFT JOIN TBL_TIPO_CLIENTE TC ON C.TC_PK = TC.TC_PK
  FULL OUTER JOIN TBL_PEDIDO P ON C.CLI_CODIGO = P.CLI_CODIGO AND P.PED_PK = (SELECT TOP 1 P.PED_PK FROM TBL_PEDIDO P WHERE P.CLI_CODIGO = C.CLI_CODIGO AND P.flgEnable = 'T' AND (P.PED_NOPEDIDO IS NULL OR P.PED_NOPEDIDO='') ORDER BY P.PED_PK desc) LEFT JOIN TBL_USUARIO U ON P.USR_CODIGO = U.USR_CODIGO               
  WHERE  R.VEN_COD = @PAR_CODUSUARIO
   AND SUBSTRING(R.RUTA_DIAVISITA,@DIA,1) = '1'
   AND C.FLGENABLE='T'
   AND R.FLGENABLE='T';
 
SELECT t.SCRIPT, t.orden FROM( 

 --RUTA
 SELECT 'DROP TABLE IF EXISTS TBL_CLIENTE;' as SCRIPT, 0 as orden    
 union    
 SELECT 'CREATE TABLE IF NOT EXISTS TBL_CLIENTE(RUTA_PK BIGINT, CLI_PK BIGINT, CLI_COD VARCHAR(20), CLI_NOMBRE VARCHAR(100), CLI_DIRECCION VARCHAR(100), TCLI_COD VARCHAR(20),TCLI_NOMBRE VARCHAR(20), CLI_GIRO VARCHAR(20), CLI_DEUDA VARCHAR(20), ESTADO VARCHAR(20), PED_FECREGISTRO VARCHAR(20), USR_CODIGO VARCHAR(20),CLI_CAMPO_ADICIONAL_1 VARCHAR(100),CLI_CAMPO_ADICIONAL_2 VARCHAR(100), CLI_CAMPO_ADICIONAL_3 VARCHAR(100),CLI_CAMPO_ADICIONAL_4 VARCHAR(100), CLI_CAMPO_ADICIONAL_5 VARCHAR(100),SALDO_CREDITO DOUBLE,LATITUD VARCHAR(50),LONGITUD VARCHAR(50), CONSTRAINT PkTblCliente PRIMARY KEY (RUTA_PK));' as SCRIPT, 1 as orden    


 union  
 SELECT 'INSERT INTO TBL_CLIENTE VALUES('+ CAST(R.RUTA_PK as VARCHAR(20)) + ',' +  CAST(C.CLI_PK as VARCHAR(20)) + ',''' + R.CLI_COD + ''',''' + UPPER(CLI_NOMBRE) + ''',''' + UPPER(CLI_DIRECCION) + ''',''' + UPPER(isnull(TC.TC_PK,'')) +''',''' +UPPER(isnull(TC.TC_NOMBRE,''))+ ''',''' + UPPER(isnull(C.CLI_GIRO,'')) + ''',''' + dbo.FORMATO_MONTO_VISTA(SUM(CONVERT(NUMERIC(18,6),COALESCE(COB_MONTOTOTAL,'0')) - CONVERT(NUMERIC(18,6),COALESCE(COB_MONTOPAGADO,'0')))) + ''',''POR VISITAR'''+','''+ isnull(dbo.FECHA_STRING(P.PED_FECREGISTRO),'') + ''',''' + isnull(U.USR_NOMBRE,'') + ''',''' + isnull(C.CampoAdicional1,'') + ''',''' + isnull(C.CampoAdicional2,'') + ''',''' +  isnull(C.CampoAdicional3,'') + ''',''' +  isnull(C.CampoAdicional4,'') + ''',''' +  isnull(C.CampoAdicional5,'')+''',''' +  cast((convert(float,isnull(C.LimiteCredito,''))- CONVERT(float, isnull(C.CreditoUtilizado,'')) - SUM(CONVERT(NUMERIC(18,6),COALESCE(COB_MONTOTOTAL,'0')) - CONVERT(NUMERIC(18,6),COALESCE(COB_MONTOPAGADO,'0')))) as varchar(20)) +''',''' +ISNULL(C.LATITUD,'')+''',''' +ISNULL(C.LONGITUD,'')+''');' AS SCRIPT, 2 as orden  
  FROM TBL_RUTA R              
  INNER JOIN TBL_CLIENTE C ON R.CLI_COD = C.CLI_CODIGO     
  LEFT JOIN TBL_COBRANZA CO ON C.CLI_CODIGO = CO.CLI_CODIGO AND CO.flgEnable = 'T'
  LEFT JOIN TBL_TIPO_CLIENTE TC ON C.TC_PK = TC.TC_PK
  FULL OUTER JOIN TBL_PEDIDO P ON C.CLI_CODIGO = P.CLI_CODIGO AND P.PED_PK = (SELECT TOP 1 P.PED_PK FROM TBL_PEDIDO P WHERE P.CLI_CODIGO = C.CLI_CODIGO AND P.flgEnable = 'T' AND (P.PED_NOPEDIDO IS NULL OR P.PED_NOPEDIDO='') ORDER BY P.PED_PK desc) LEFT JOIN TBL_USUARIO U ON P.USR_CODIGO = U.USR_CODIGO               
  WHERE  R.VEN_COD = @PAR_CODUSUARIO
   AND SUBSTRING(R.RUTA_DIAVISITA,@DIA,1) = '1'
   AND C.FLGENABLE='T'
   AND R.FLGENABLE='T'
 GROUP BY R.RUTA_PK, C.CLI_PK, R.CLI_COD, C.CLI_NOMBRE,           
        C.CLI_DIRECCION, TC.TC_NOMBRE, C.CLI_GIRO, P.PED_FECREGISTRO, U.USR_NOMBRE, C.CampoAdicional1, C.CampoAdicional2, 
        C.CampoAdicional3, C.CampoAdicional4, C.CampoAdicional5,C.LimiteCredito,C.CreditoUtilizado,
        C.LATITUD,C.LONGITUD, TC.TC_PK
 UNION
 
--Direcciones
 SELECT 'DROP TABLE IF EXISTS TBL_DIRECCION;' as SCRIPT, 0 as orden    
 union    
SELECT 'CREATE TABLE IF NOT EXISTS TBL_DIRECCION(DIR_INC INTEGER  PRIMARY KEY, DIR_PK BIGINT, CLI_CODIGO VARCHAR(20), DIR_NOMBRE VARCHAR(100), DIR_TIPO VARCHAR(1), FLGENVIADO VARCHAR(1), DIR_LATITUD VARCHAR(50), DIR_LONGITUD VARCHAR(50), DIR_COD VARCHAR(20), FECCREACION VARCHAR(20));' as SCRIPT, 1 as orden    
 union  
SELECT 'INSERT INTO TBL_DIRECCION (DIR_PK,DIR_COD,CLI_CODIGO, DIR_NOMBRE, DIR_TIPO, FLGENVIADO, DIR_LATITUD,DIR_LONGITUD) VALUES('+ CAST(D.DIR_PK as VARCHAR(20)) +',''' + D.DIR_CODIGO + ''',''' + D.CLI_CODIGO + ''',''' + UPPER(D.DIR_NOMBRE)+ ''',''' + D.DIR_TIPO + ''',''' + 'T' + ''',''' + ISNULL(D.LATITUD, '')+ ''',''' + ISNULL(D.LONGITUD, '') + ''');' AS SCRIPT, 2 as orden  
  FROM TBL_RUTA R              
  INNER JOIN TBL_CLIENTE C ON R.CLI_COD = C.CLI_CODIGO
  INNER JOIN TBL_DIRECCION D ON C.CLI_PK = D.CLI_PK
  WHERE  R.VEN_COD = @PAR_CODUSUARIO
  AND SUBSTRING(R.RUTA_DIAVISITA,@DIA,1) = '1'
   AND C.FLGENABLE='T'
   AND R.FLGENABLE='T'
   AND D.flgEnable = 'T'

 UNION
	
 --Productos
 SELECT 'DROP TABLE IF EXISTS TBL_PRODUCTO;' as SCRIPT, 0 as orden
 UNION    
 SELECT 'CREATE TABLE IF NOT EXISTS TBL_PRODUCTO(ID INTEGER not null, Codigo VARCHAR(20) not null,Nombre VARCHAR(150) not null,Stock VARCHAR(20) not null,UnidadDefecto VARCHAR(20) not null,PrecioBase VARCHAR(20) not null, DescuentoMinimo VARCHAR(50), DescuentoMaximo VARCHAR(50), CONSTRAINT PkTblProducto PRIMARY KEY (Codigo));' as SCRIPT, 1 as orden    
 UNION  
 SELECT 'INSERT INTO TBL_PRODUCTO VALUES('+CAST(PRO_PK as VARCHAR(20))+ ',''' + pro_codigo + ''',''' + UPPER(replace(pro_nombre,'-','')) + ''',''' + 
	
	dbo.FORMATO_MONTO_VISTA(
	
		CAST(COALESCE(PRO.PRO_STOCK, '0') AS NUMERIC(16,4)) - ISNULL(
		(
			SELECT 
				SUM(
					CAST(COALESCE(RES.DET_CANTIDAD, '0') AS NUMERIC(16,4))
					+
					CAST(COALESCE(RES.DET_BONIFICACION, '0') AS NUMERIC(16,4)) 
				)
			FROM
				TBL_RESERVA RES
			WHERE
				RES.PRO_CODIGO = PRO.PRO_CODIGO
			AND
				FLGENABLE = 'T'
		)
		, 0)
	) 
	+ ''',''' + pro_unidaddefecto + ''',''' + dbo.FORMATO_MONTO_VISTA(PRO_PRECIOBASE) + ''',''' + ISNULL(DESC_MIN, '0') + ''',''' + ISNULL(DESC_MAX, '0') + ''');' AS SCRIPT, 2 as orden  
 FROM TBL_PRODUCTO PRO  
 WHERE      
    PRO.FLGENABLE='T'   
 UNION
 --Presentacion
 SELECT 'DROP TABLE IF EXISTS TBL_PRESENTACION;' as SCRIPT, 0 as orden
 UNION    
 SELECT 'CREATE TABLE IF NOT EXISTS TBL_PRESENTACION(IdPresentacion INT NOT NULL PRIMARY KEY, Codigo VARCHAR(20) UNIQUE, IdProducto INTEGER,Nombre VARCHAR(50), Cantidad INTEGER,UnidadFraccionamiento INTEGER, Precio VARCHAR(20), Stock VARCHAR(20), DescuentoMinimo VARCHAR(50), DescuentoMaximo VARCHAR(50) );' as SCRIPT, 1 as orden    
 UNION  
 SELECT 'INSERT INTO TBL_PRESENTACION VALUES('+CAST(PRE_PK as VARCHAR(20))+ ', '''+CAST(CODIGO as VARCHAR(20))+ ''','+CAST(PRO_PK as VARCHAR(20))+ ',''' + ISNULL(UPPER(replace(NOMBRE,'-','')),'') + ''',' + CAST(CANTIDAD as VARCHAR(20)) + ',' + CAST(UNIDAD_FRACCIONAMIENTO as VARCHAR(20)) + ',''' + dbo.FORMATO_MONTO_VISTA(PRECIO) + ''',''' + 
	
	dbo.FORMATO_MONTO_VISTA(CAST(COALESCE(PRE.STOCK, '0') AS NUMERIC(16,4)) - ISNULL(
	(
			SELECT 
				SUM(
					(CAST(COALESCE(RES.DET_CANTIDAD, '0') AS NUMERIC(16,4)) * CAST(COALESCE(RES.PRE_CANTIDAD, '0') AS NUMERIC(16,4)))
					+
					(CAST(COALESCE(RES.DET_BONIFICACION, '0') AS NUMERIC(16,4)) * CAST(COALESCE(RES.PRE_CANTIDAD, '0') AS NUMERIC(16,4)))
					+
					CAST(COALESCE(RES.DET_CANTIDAD_FRAC, '0') AS NUMERIC(16,4)) 
				)
			FROM
				TBL_RESERVA_PRESENTACION RES
			WHERE
				RES.PRE_PK = PRE.PRE_PK
			AND
				FLGENABLE = 'T'
	)
	, 0)) 
	
	+ ''',''' + ISNULL(DESC_MIN, '0') + ''',''' + ISNULL(DESC_MAX, '0') + ''');' AS SCRIPT, 2 as orden  
 FROM TBL_PRESENTACION  PRE
 WHERE      
    FLG_ENABLE='T'        

  UNION
  
 --Almacenes
 SELECT 'DROP TABLE IF EXISTS TBL_ALMACEN;' as SCRIPT, 0 as orden
 union    
 SELECT 'CREATE TABLE IF NOT EXISTS TBL_ALMACEN(ID INTEGER PRIMARY KEY, ALM_CODIGO VARCHAR(20), ALM_NOMBRE VARCHAR(100), ALM_DIRECCION VARCHAR(100));' as SCRIPT, 1 as orden    
 union
 SELECT 'INSERT INTO TBL_ALMACEN VALUES('+CAST(ALM.ALM_PK as VARCHAR(20))+ ','''+ ALM.ALM_CODIGO + ''',''' + ALM.ALM_NOMBRE + ''',''' + ALM.ALM_DIRECCION + ''');' AS SCRIPT, 2 as orden    
FROM
	TBL_ALMACEN ALM
WHERE 
	ALM.ALM_HABILITADO = 'T'
	
 UNION
 
 --ALMACEN_PRODUCTO
 SELECT 'DROP TABLE IF EXISTS TBL_ALMACEN_PRODUCTO;' as SCRIPT, 0 as orden
 union    
 SELECT 'CREATE TABLE IF NOT EXISTS TBL_ALMACEN_PRODUCTO(ALM_PK INTEGER, PRO_PK INTEGER, ALM_PRO_STOCK VARCHAR(20));' as SCRIPT, 1 as orden    
 union
 SELECT 'INSERT INTO TBL_ALMACEN_PRODUCTO VALUES('+CAST(ALPR.ALM_PK as VARCHAR(20))+ ','+CAST(ALPR.PRO_PK as VARCHAR(20))+ ',''' +  
	dbo.FORMATO_MONTO_VISTA(CAST(COALESCE(ALPR.ALM_PRO_STOCK, '0') AS NUMERIC(16,4)) - ISNULL(
	(
			SELECT 
				SUM(
					CAST(COALESCE(RES.DET_CANTIDAD, '0') AS NUMERIC(16,4))
					+
					CAST(COALESCE(RES.DET_BONIFICACION, '0') AS NUMERIC(16,4)) 
				)
				
			FROM
				TBL_RESERVA RES
			WHERE
				RES.PRO_CODIGO = PRO.PRO_CODIGO
			AND
				RES.ALM_PK = ALPR.ALM_PK
			AND
				FLGENABLE = 'T'
	)
	, 0)) 
	
	+ ''');' AS SCRIPT, 2 as orden    
 FROM 
	TBL_ALMACEN_PRODUCTO ALPR
 INNER JOIN  
	TBL_ALMACEN ALM  
 ON 
	ALM.ALM_PK = ALPR.ALM_PK
 INNER JOIN
	TBL_PRODUCTO PRO
 ON
	PRO.PRO_PK = ALPR.PRO_PK
 WHERE 
	ALM.ALM_HABILITADO = 'T'
 AND 
	ALPR.ALM_PRO_STOCK <> ''      
 AND 
	isnumeric (ALPR.ALM_PRO_STOCK ) = 1 
	
UNION

--ALMACEN_PRESENTACION
 SELECT 'DROP TABLE IF EXISTS TBL_ALMACEN_PRESENTACION;' as SCRIPT, 0 as orden
 union    
 SELECT 'CREATE TABLE IF NOT EXISTS TBL_ALMACEN_PRESENTACION(IdAlmacen INTEGER, IdPresentacion INTEGER, stock VARCHAR(20));' as SCRIPT, 1 as orden    
 union
 SELECT 'INSERT INTO TBL_ALMACEN_PRESENTACION VALUES('+CAST(ALPRE.ALM_PK as VARCHAR(20))+ ','+CAST(ALPRE.PRE_PK as VARCHAR(20))+ ',''' +  
 
	dbo.FORMATO_MONTO_VISTA(CAST(COALESCE(ALPRE.STOCK, '0') AS NUMERIC(16,4)) - ISNULL(
		(
			SELECT 
				SUM(
					(CAST(COALESCE(RES.DET_CANTIDAD, '0') AS NUMERIC(16,4)) * CAST(COALESCE(RES.PRE_CANTIDAD, '0') AS NUMERIC(16,4)))
					+
					(CAST(COALESCE(RES.DET_BONIFICACION, '0') AS NUMERIC(16,4)) * CAST(COALESCE(RES.PRE_CANTIDAD, '0') AS NUMERIC(16,4)))
					+
					CAST(COALESCE(RES.DET_CANTIDAD_FRAC, '0') AS NUMERIC(16,4)) 
				)
			FROM
				TBL_RESERVA_PRESENTACION RES
			WHERE
				RES.PRE_PK = ALPRE.PRE_PK
			AND
				RES.ALM_PK = ALPRE.ALM_PK
			AND
				FLGENABLE = 'T'
		)
	, 0)) 
 
 + ''');' AS SCRIPT, 2 as orden    
 FROM 
	TBL_ALMACEN_PRESENTACION ALPRE
 INNER JOIN  
	TBL_ALMACEN ALM  
 ON 
	ALM.ALM_PK = ALPRE.ALM_PK
 WHERE 
	ALM.ALM_HABILITADO = 'T'
 AND 
	ALPRE.STOCK <> ''      
 AND 
	isnumeric (ALPRE.STOCK ) = 1 
 AND
	ALPRE.FLGENABLE = 'T'
	
UNION

 --Listaprecios PRESENTACION
	SELECT 'DROP TABLE IF EXISTS TBL_LISTAPRECIOS_PRESENTACION;' as SCRIPT, 0 as orden    
	UNION    
	SELECT 'CREATE TABLE IF NOT EXISTS TBL_LISTAPRECIOS_PRESENTACION(ID INTEGER not null PRIMARY KEY, IdPresentacion INTEGER, Canal VARCHAR(20) null,CondVta VARCHAR(20) null,Precio VARCHAR(20) not null, DescuentoMinimo VARCHAR(50), DescuentoMaximo VARCHAR(50));' as SCRIPT, 1 as orden    
	UNION  
	select 'INSERT INTO TBL_LISTAPRECIOS_PRESENTACION VALUES('+CAST(LPP.CODIGO as VARCHAR(20))+ ','+CAST(LPP.PRE_PK as VARCHAR(20))+ ',''' + UPPER(RTRIM(LTRIM(ISNULL(lpp.TC_PK,'')))) + ''','''+ ISNULL(LPP.CODIGO_CONDVTA,'') +''','''+ dbo.FORMATO_MONTO_VISTA(LPP.PRECIO) + ''','''+ ISNULL(LPP.DESC_MIN, '0') + ''','''+  ISNULL(LPP.DESC_MAX, '0') + ''');' AS SCRIPT, 2 as orden  
	FROM TBL_LISTAPRECIOS_PRESENTACION  LPP
	INNER JOIN TBL_PRESENTACION PRE ON LPP.PRE_PK = PRE.PRE_PK
	LEFT JOIN #TmpTipoClienteRuta tcr ON tcr.TC_PK = LPP.TC_PK
	WHERE PRE.FLG_ENABLE = 'T'
	AND LPP.FLAG = 'T'

UNION

 --Listaprecios
	SELECT 'DROP TABLE IF EXISTS TBL_LISTAPRECIOS;' as SCRIPT, 0 as orden    
	UNION    
	SELECT 'CREATE TABLE IF NOT EXISTS TBL_LISTAPRECIOS(ID INTEGER not null, Codigo VARCHAR(20) not null,Producto VARCHAR(20) not null,Canal VARCHAR(20) null,CondVta VARCHAR(20) null,Precio VARCHAR(20) not null, DescuentoMinimo VARCHAR(50), DescuentoMaximo VARCHAR(50),CONSTRAINT PkTblListaPrecios PRIMARY KEY (Codigo));' as SCRIPT, 1 as orden    
	UNION  
	SELECT 'INSERT INTO TBL_LISTAPRECIOS VALUES('+CAST(PR.PRO_PK as VARCHAR(20))+ ','''+ CONVERT(VARCHAR, codigo) + ''',''' + codigo_producto + ''',''' + UPPER(RTRIM(LTRIM(ISNULL(lp.TC_PK,'')))) + ''','''+ ISNULL(LP.CODIGO_CONDVTA,'') +''','''+ dbo.FORMATO_MONTO_VISTA(LP.PRECIO) + ''','''+ ISNULL(LP.DESC_MIN, '0') + ''','''+  ISNULL(LP.DESC_MAX, '0') + ''');' AS SCRIPT, 2 as orden  
	FROM TBL_LISTAPRECIOS  LP
	INNER JOIN TBL_PRODUCTO PR  ON LP.CODIGO_PRODUCTO = PRO_CODIGO
	LEFT JOIN #TmpTipoClienteRuta tcr ON tcr.TC_PK = LP.TC_PK
	WHERE PR.FLGENABLE = 'T'
	AND LP.FLAG = 'T'

UNION

--BONIFICACION
SELECT
		'DROP TABLE IF EXISTS TBL_BONIFICACION;' as SCRIPT, 0 as orden    
UNION    
	SELECT 
		'CREATE TABLE IF NOT EXISTS TBL_BONIFICACION(ID INTEGER NOT NULL PRIMARY KEY, IdProducto INTEGER, IdPresentacion INTEGER, CodigoArticulo VARCHAR(20), NombreArticulo VARCHAR(100), Cantidad VARCHAR(20), Maximo VARCHAR(20), Editable VARCHAR(20), TipoArticulo CHAR(3), Prioridad INTEGER);' as SCRIPT, 1 as orden    
UNION
	SELECT 
		'INSERT INTO TBL_BONIFICACION VALUES(' + CAST(BON.BON_PK as VARCHAR(20)) + ', ' + CAST(ISNULL(BON.PRO_PK, 0) as VARCHAR(20)) + ', ' + CAST(ISNULL(BON.PRE_PK, 0) as VARCHAR(20)) + ', ''' + BON.PROD_CODIGO +''', ''' + BON.PROD_NOMBRE +''', ''' + BON.BON_CANTIDAD + ''', ''' + BON.BON_MAXIMO +''', ''' + BON.BON_EDITABLE +''', ''' + BON.TIPO_ARTICULO +''', ' +  CAST(BON.BON_PRIORIDAD as VARCHAR(20)) + ');' AS SCRIPT, 2 as orden 
	FROM 
		TBL_BONIFICACION BON
	INNER JOIN TBL_BONIFICACION_DETALLE BONDET ON BON.BON_PK = BONDET.BON_PK
	WHERE
		BON.FLAGHABILITADO = 'T'
UNION			

--BONIFICACION DETALLE
	SELECT
		'DROP TABLE IF EXISTS TBL_BONIFICACION_DETALLE;' as SCRIPT, 0 as orden    
UNION    
	SELECT 
		'CREATE TABLE IF NOT EXISTS TBL_BONIFICACION_DETALLE(ID INTEGER NOT NULL PRIMARY KEY, IdBonificacion INTEGER, IdProducto INTEGER, IdPresentacion INTEGER, CodigoArticulo VARCHAR(20), NombreArticulo VARCHAR(100), Cantidad VARCHAR(20), Existe CHAR(1));' as SCRIPT, 1 as orden    
UNION
	SELECT 
		'INSERT INTO TBL_BONIFICACION_DETALLE VALUES(' + CAST(BONDET.BON_DET_PK as VARCHAR(20)) + ',' + CAST(BONDET.BON_PK as VARCHAR(20)) + ', ' + CAST(ISNULL(BONDET.PRO_PK, 0) as VARCHAR(20)) + ', ' + CAST(ISNULL(BONDET.PRE_PK, 0) as VARCHAR(20)) + ', ''' + BONDET.PROD_CODIGO +''', ''' + BONDET.PROD_NOMBRE +''', ''' + ISNULL(BONDET.PROD_CANTIDAD,0) + ''', ''' + BONDET.PROD_EXISTE +''');' AS SCRIPT, 2 as orden 
	FROM 
		TBL_BONIFICACION_DETALLE BONDET
	INNER JOIN TBL_BONIFICACION BON ON BONDET.BON_PK = BON.BON_PK
	WHERE
		BON.FLAGHABILITADO = 'T'
UNION
--Generales
SELECT 'DROP TABLE IF EXISTS TBL_GENERAL;' as SCRIPT, 0 as orden    
 union    
 SELECT 'CREATE TABLE IF NOT EXISTS TBL_GENERAL(pk INTEGER PRIMARY KEY,Grupo VARCHAR(20) not null,Codigo VARCHAR(20) not null,Descripcion VARCHAR(150) not null, FlagBanco CHAR(1) null, FlagDocumento CHAR(1) null,FlagFechaDiferida CHAR(1) null);' as SCRIPT, 1 as orden    
 union  
 select 'INSERT INTO TBL_GENERAL(Grupo,Codigo,Descripcion) VALUES(''1'',''' + UPPER(mot_codigo) + ''',''' + UPPER(mot_nombre) + ''');' AS SCRIPT, 2 as orden  
  from tbl_motivo  
  where FLGENABLE = 'T'
  AND upper(mot_tipo) = 'N'  
 union           
 select 'INSERT INTO TBL_GENERAL(Grupo,Codigo,Descripcion) VALUES(''2'',''' + UPPER(condvta_codigo) + ''',''' + UPPER(condvta_nombre) + ''');' AS SCRIPT, 2 as orden  
  from tbl_condvta
  WHERE  FLGENABLE = 'T'
 union           
 select 'INSERT INTO TBL_GENERAL(Grupo,Codigo,Descripcion) VALUES(''3'',''' + UPPER(BAN_CODIGO) + ''',''' + UPPER(BAN_NOMCORTO) + ''');' AS SCRIPT, 2 as orden  
  from TBL_BANCO
  WHERE  FLGENABLE = 'T'
 union           
 select 'INSERT INTO TBL_GENERAL(Grupo,Codigo,Descripcion,FlagBanco,FlagDocumento,FlagFechaDiferida ) VALUES(''4'',''' + UPPER(FPC_CODIGO) + ''',''' + UPPER(FPC_NOMBRE) + ''',''' + UPPER(FPC_FLGBANCO)+ ''',''' +UPPER(FPC_FLGDOCUMENTO)+''',''' +UPPER(FPC_FLGFECHADIFERIDA)+  ''');' AS SCRIPT, 2 as orden  
  from TBL_FPAGO_COBRANZA where FPC_FLGENABLE = 'T'
 union           
 select 'INSERT INTO TBL_GENERAL(Grupo,Codigo,Descripcion) VALUES(''5'',''' + UPPER(MOT_CODIGO) + ''',''' + UPPER(MOT_NOMBRE) + ''');' AS SCRIPT, 2 as orden  
  from TBL_MOTIVO
  WHERE  FLGENABLE = 'T'
  AND UPPER(MOT_TIPO) = 'C'
 union           
 select 'INSERT INTO TBL_GENERAL(Grupo,Codigo,Descripcion) VALUES(''6'',''' + UPPER(MOT_CODIGO) + ''',''' + UPPER(MOT_NOMBRE) + ''');' AS SCRIPT, 2 as orden  
  from TBL_MOTIVO
  WHERE  FLGENABLE = 'T'
  AND UPPER(MOT_TIPO) = 'D'
 union           
 select 'INSERT INTO TBL_GENERAL(Grupo,Codigo,Descripcion) VALUES(''7'',''' + UPPER(TC_CODIGO) + ''',''' + UPPER(TC_NOMBRE) + ''');' AS SCRIPT, 2 as orden  
  from TBL_TIPO_CLIENTE
  WHERE  FLGENABLE = 'T'
 UNION
 --Cobranza
 SELECT 'DROP TABLE IF EXISTS TBL_COBRANZA;' as SCRIPT, 0 as orden    
 union    
 SELECT 'CREATE TABLE IF NOT EXISTS TBL_COBRANZA(COB_PK INTEGER, COB_CODIGO VARCHAR(20), COB_NUMDOCUMENTO VARCHAR(20), COB_TIPODOCUMENTO VARCHAR(20), COB_MONTOPAGADO VARCHAR(20), COB_MONTOTOTAL VARCHAR(20), COB_FECVENCIMIENTO VARCHAR(20), COB_SERIE VARCHAR(20), CLI_CODIGO VARCHAR(20), COB_EMPRESA VARCHAR(20), COB_USUARIO VARCHAR(20), CLI_PK BIGINT, SALDO VARCHAR(20), CONSTRAINT PkTblCobra PRIMARY KEY (COB_PK));' as SCRIPT, 1 as orden    
 union  
 SELECT 'INSERT INTO TBL_COBRANZA VALUES('+ CAST(COB_PK as VARCHAR(20)) + 
		',''' + COB_CODIGO + 
		''',''' + UPPER(COB_NUMDOCUMENTO) + 
		''',''' + COB_TIPODOCUMENTO + 
		''',''' + COB_MONTOPAGADO +
		''',''' + COB_MONTOTOTAL + 
		''',''' + CONVERT(VARCHAR,COB_FECVENCIMIENTO,103) +
		''',''' + ISNULL(COB_SERIE,'')+ 
		''',''' + ISNULL(CLI_CODIGO,'')+ 
		''',''' + COB_EMPRESA+ 
		''',''' + COB_USUARIO+
		''','+ CAST(ISNULL(CLI_PK,'') as VARCHAR(20)) + 
		',''' + CAST((CONVERT(NUMERIC(18,2),COBRA.COB_MONTOTOTAL) - CONVERT(NUMERIC(18,2),COBRA.COB_MONTOPAGADO)) as VARCHAR(20)) + ''');' AS SCRIPT, 2 as orden
  FROM TBL_COBRANZA COBRA         
  WHERE (CONVERT(NUMERIC(18,2),COBRA.COB_MONTOTOTAL) - CONVERT(NUMERIC(18,2),COBRA.COB_MONTOPAGADO)) > 0
  --AND COBRA.COB_USUARIO = @PAR_CODUSUARIO
  AND COBRA.CLI_CODIGO in (select * from @tblcliente_temp)
  AND COBRA.FLGENABLE='T'
   -- Grupo
  UNION
  SELECT 'DROP TABLE IF EXISTS CFGrupo;' as SCRIPT, 0 as orden          
  UNION            
  SELECT 'CREATE TABLE IF NOT EXISTS CFGrupo(IdGrupo int not null, NombreGrupo VARCHAR(100) null,CodigoDetalle VARCHAR(50) null,NombreDetalle VARCHAR(100) null);' as SCRIPT, 1 as orden          
  UNION
  SELECT 'INSERT INTO CFGrupo VALUES('+ cast(G.IdGrupo as varchar(20))+','''+G.Nombre+''','''+GD.CodGrupoDetalle+''','''+GD.Nombre+''');' AS SCRIPT, 2 as orden
  from CFGrupo G
  inner join CFGrupoDetalle GD on G.IdGrupo = GD.IdGrupo
  where G.IdGrupo in (	select distinct(idGrupo)
 						from CFControl
 						where idGrupo is not null
 						and FlgHabilitado = 'T')
  and G.FlgHabilitado = 'T'
  and GD.FlgHabilitado = 'T'
  UNION
  
  --Prospectos
 SELECT 'DROP TABLE IF EXISTS TrProspecto;' as SCRIPT, 0 as orden
 UNION    
 SELECT 'CREATE TABLE IF NOT EXISTS TrProspecto(Id VARCHAR(50) not null, Codigo VARCHAR(20) not null,Nombre VARCHAR(100) not null,Direccion VARCHAR(100) not null,TcliCod VARCHAR(30) not null,Giro VARCHAR(100) not null, Latitud VARCHAR(50), Longitud VARCHAR(50),UserCod VARCHAR(20),CampoAdic1 VARCHAR(100),CampoAdic2 VARCHAR(100),CampoAdic3 VARCHAR(100),CampoAdic4 VARCHAR(100),CampoAdic5 VARCHAR(100),FechaMovil VARCHAR(20),Observacion VARCHAR(100),FlgEnviado CHAR(1), CONSTRAINT PkTblProducto PRIMARY KEY (Codigo));' as SCRIPT, 1 as orden     
  
  UNION
  
  
  -- Controles
  SELECT 'DROP TABLE IF EXISTS CFControl;' as SCRIPT, 0 as orden 
  UNION 
  SELECT 'CREATE TABLE IF NOT EXISTS CFControl(IdControl bigint not null, idFormulario int null, EtiquetaControl varchar(100) not null, IdTipoControl bigint null, MaxCaracteres int null, IdGrupo int null, FlgObligatorio char(1) null,FlgEditable char(1) null, Orden int null);' as SCRIPT, 1 as orden          
  UNION
  SELECT 'INSERT INTO CFControl VALUES('+cast(IdControl as varchar(20))+','+cast(idFormulario as varchar(20))+','''+EtiquetaControl+''','+cast(IdTipoControl as varchar(20))+','+cast(MaxCaracteres as varchar(20))+','+cast(isnull(IdGrupo,0) as varchar(20))+','''+FlgObligatorio+''','''+ISNULL(FlgEditable,'F')+''','+cast(Orden as varchar(20))+');' AS SCRIPT, 2 as orden
  from CFControl
  WHERE FlgHabilitado = 'T'
  and idTipoControl != 9

 --ORDER BY 2, 1
 ) t
 WHERE SCRIPT IS NOT NULL
 ORDER BY 2, 1




select 'Cliente : '+ cast(count(*) as varchar(20)) as Total , 0 as orden 
 FROM TBL_RUTA R              
  INNER JOIN TBL_CLIENTE C ON R.CLI_COD = C.CLI_CODIGO                
  WHERE  R.VEN_COD = @PAR_CODUSUARIO
   AND SUBSTRING(R.RUTA_DIAVISITA,@DIA,1) = '1'
   AND C.FLGENABLE='T'            
   AND R.FLGENABLE='T'
union

select ' Articulo : '+ cast(count(pro_codigo) as varchar(20)) as Total  , 1 as orden
 from tbl_producto  
 where flgenable='T'   
 UNION
 select ' Cobranza : ' +cast(count(COB_PK) as varchar(20)) as Total  , 2 as orden
 FROM TBL_COBRANZA COBRA         
  WHERE (CONVERT(NUMERIC(18,2),COBRA.COB_MONTOTOTAL) - CONVERT(NUMERIC(18,2),COBRA.COB_MONTOPAGADO)) > 0          
  --AND COBRA.COB_USUARIO = @PAR_CODUSUARIO  
  AND COBRA.CLI_CODIGO in (select * from @tblcliente_temp)
  AND COBRA.FLGENABLE='T' 
union  
select ' Precio Producto : '+ cast(count(codigo) as varchar(20)) as Total  , 3 as orden
	FROM TBL_LISTAPRECIOS  LP
	INNER JOIN TBL_PRODUCTO PR 
	ON LP.CODIGO_PRODUCTO = PRO_CODIGO
	WHERE PR.FLGENABLE = 'T'
	AND LP.FLAG = 'T'  
 union  
 select ' Precio Presentacion: '+ cast(count(LP.codigo) as varchar(20)) as Total  , 3 as orden
	FROM TBL_LISTAPRECIOS_PRESENTACION  LP
	INNER JOIN TBL_PRESENTACION PR 
	ON LP.PRE_PK = PR.PRE_PK
	WHERE PR.FLG_ENABLE = 'T'
	AND LP.FLAG = 'T' 	
	
 union
 select ' Motivo noVta : ' +cast(count(mot_codigo) as varchar(20)) as Total  , 4 as orden
 from tbl_motivo  
  where FLGENABLE = 'T'
  AND upper(mot_tipo) = 'N'
 union           
 select ' CondVta : ' +cast(count(condvta_codigo) as varchar(20)) as Total  , 5 as orden
  from tbl_condvta  
  where FLGENABLE = 'T'
 union           
 select ' Banco : ' +cast(count(BAN_CODIGO) as varchar(20)) as Total  ,6 as orden
  from TBL_BANCO
  WHERE  FLGENABLE = 'T'
 --union           
 --select ' Tipo pago : ' +cast(count(FPC_CODIGO) as varchar(20)) as Total  , 7 as orden
 -- from TBL_FPAGO_COBRANZA
 union
 select ' Motivo canje : ' +cast(count(mot_codigo) as varchar(20)) as Total  , 8 as orden
 from tbl_motivo  
  where FLGENABLE = 'T'
  AND upper(mot_tipo) = 'C'
 union
 select ' Motivo devolucion : ' +cast(count(mot_codigo) as varchar(20)) as Total , 9 as orden 
 from tbl_motivo  
  where FLGENABLE = 'T'
  AND upper(mot_tipo) = 'D'
  union 
  select 'Tipo Pago Doc : '++cast(count(FPC_CODIGO) as varchar(20)) as Total , 10 as orden 
  from TBL_FPAGO_COBRANZA
  where fpc_flgenable = 'T'
  
ORDER BY orden

END
GO
PRINT N'Modificando [dbo].[sps_ManInsControl]...';


GO

ALTER PROCEDURE [dbo].[sps_ManInsControl]  
	@ESTADO VARCHAR(100),  
	@CONTROL VARCHAR(100),  
	@MAX VARCHAR(100),  
	@ETIQUETA VARCHAR(100),  
	@OBLIGATORIO VARCHAR(100),  
	@ORDEN INT ,
	@GRUPO VARCHAR(100),
	@EDITABLE VARCHAR(100)
AS  

	INSERT INTO .[CFControl]
           ([EtiquetaControl]
           ,[IdTipoControl]
           ,[MaxCaracteres]
           ,[IdGrupo]
           ,[FlgObligatorio]
           ,[Orden]
           ,[FlgHabilitado]
           ,[IdFormulario]
		   ,[FlgEditable])
     VALUES
           (@ETIQUETA
           ,@CONTROL
           ,@MAX
           ,@GRUPO
           ,@OBLIGATORIO
           ,@ORDEN
           ,'T'
           ,@ESTADO
		   ,@EDITABLE)


	DECLARE @UltimoOrdenPre INT
	DECLARE @UltimoOrdenPro INT
	declare @EtiquetabBusPre varchaR(200)
	declare @EtiquetabBusPro varchaR(200)
	declare @flagPre char(1)
	declare @flagPro char(1)

	set @flagPre=(select FlgHabilitado from TEMPColumnaArchivo where TipoArticulo='PRE' and NombreColumna=@ETIQUETA)
	set @flagPro=(select FlgHabilitado from TEMPColumnaArchivo where TipoArticulo='PRO' and NombreColumna=@ETIQUETA)

	SET @UltimoOrdenPre=(SELECT top  1   Orden FROM GRColumnaArchivo WHERE TipoArticulo='PRE' AND TipoColumna='C' AND IdArchivo='1' ORDER BY Orden desc)
	SET @UltimoOrdenPro=(SELECT top  1   Orden FROM GRColumnaArchivo WHERE TipoArticulo='PRO' AND TipoColumna='C' AND IdArchivo='1' ORDER BY Orden desc)

	SET @EtiquetabBusPre=(SELECT NombreColumna FROM GRColumnaArchivo WHERE TipoArticulo='PRE' AND TipoColumna='C' AND IdArchivo='1' AND NombreColumna=@ETIQUETA)
	SET @EtiquetabBusPro=(SELECT NombreColumna FROM GRColumnaArchivo WHERE TipoArticulo='PRO' AND TipoColumna='C' AND IdArchivo='1' AND NombreColumna=@ETIQUETA)

	if(@EtiquetabBusPre is null)
	begin
		if(@flagPre is null)
		begin
			INSERT INTO  GRColumnaArchivo(IdArchivo,NombreColumna,TipoArticulo,TipoColumna,Orden,FlgHabilitado)VALUES
								 ((SELECT IdArchivo FROM GRArchivo WHERE CodigoArchivo='PDO'),@ETIQUETA,'PRE','C',(@UltimoOrdenPre + 1),'T')
		end
		else
		begin
			INSERT INTO  GRColumnaArchivo(IdArchivo,NombreColumna,TipoArticulo,TipoColumna,Orden,FlgHabilitado)VALUES
								 ((SELECT IdArchivo FROM GRArchivo WHERE CodigoArchivo='PDO'),@ETIQUETA,'PRE','C',(@UltimoOrdenPre + 1),@flagPre)
		end
	end

	if(@EtiquetabBusPro is null)
	begin
		if(@flagPro is null)
		begin
			INSERT INTO  GRColumnaArchivo(IdArchivo,NombreColumna,TipoArticulo,TipoColumna,Orden,FlgHabilitado)VALUES
									 ((SELECT IdArchivo FROM GRArchivo WHERE CodigoArchivo='PDO'),@ETIQUETA,'PRO','C',(@UltimoOrdenPro + 1),'T')

		end
		else
		begin
		 INSERT INTO  GRColumnaArchivo(IdArchivo,NombreColumna,TipoArticulo,TipoColumna,Orden,FlgHabilitado)VALUES
									 ((SELECT IdArchivo FROM GRArchivo WHERE CodigoArchivo='PDO'),@ETIQUETA,'PRO','C',(@UltimoOrdenPro + 1),@flagPro)
	end
end
GO
PRINT N'Modificando [dbo].[spS_ManSelFormulario]...';


GO
ALTER PROCEDURE [dbo].[spS_ManSelFormulario]   
   
AS    
BEGIN    
    
SET NOCOUNT ON    
 
 
 SELECT distinct IdFormulario FROM CFControl
where FlgHabilitado='T'
order by IDFORMULARIO

SELECT EtiquetaControl,IdTipoControl,MaxCaracteres,CFControl.IdGrupo,FlgObligatorio,Orden,IdFormulario,IdControl, CFGrupo.Nombre,ISNULL(FlgEditable,'') AS FlgEditable
FROM CFControl left join CFGrupo on CFControl.IdGrupo=CFGrupo.IdGrupo
where CFControl.FlgHabilitado='T'
order by IDFORMULARIO,orden

END
GO
PRINT N'Creando [dbo].[spS_AuxSelLinkAutoAyuda]...';


GO
create PROCEDURE [dbo].spS_AuxSelLinkAutoAyuda
(
	@CUENTAS VARCHAR(200) OUTPUT
)
AS
BEGIN
	SET NOCOUNT ON;

	SET @CUENTAS = ISNULL((SELECT dbo.FX_GetIdiomaWeb('JAVA_LINKAUTOAYUDA')), '')

END
GO
PRINT N'Creando [dbo].[spS_AuxUpdEtiquetasLink]...';


GO
create PROCEDURE [dbo].spS_AuxUpdEtiquetasLink
(
	@ETIQUETA VARCHAR(200)
)
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE CFValorEtiqueta
	SET Descripcion = @ETIQUETA
	WHERE IdPais IN (SELECT P.IdPais FROM CFPais P INNER JOIN VW_CFCONFIGURACION C ON P.CodPais = C.Culture)
		AND IdEtiqueta IN (SELECT E.IdEtiqueta FROM CFEtiqueta E WHERE E.CodEtiqueta = 'JAVA_LINKAUTOAYUDA')
END
GO
PRINT N'Creando [dbo].[SPS_GuardarMaestroTabla]...';


GO
create procedure SPS_GuardarMaestroTabla
	@idTabla bigint,
	@lstCol AS dbo.LstColumnasXls READONLY
as
begin
	--select idMaestro,idColumna,columnaXls from GRMapeoMaestroTabla
	delete from GRMapeoMaestroTabla where idMaestro=@idTabla and tipoFormato='C'
	insert into GRMapeoMaestroTabla(idMaestro,idColumna,columnaXls,tipoFormato)
	select @idTabla,idColumna,columnaxls,'C' from @lstCol
end
GO
PRINT N'Creando [dbo].[SPS_LISTAR_MaestroTabla]...';


GO
create procedure SPS_LISTAR_MaestroTabla
as
begin
	select id,tabla,descripcion
	from GRMaestroTabla
	where FlgHabilitado='T'
end
GO
PRINT N'Creando [dbo].[SPS_LISTAR_MaestroTablaColumnas]...';


GO
create procedure SPS_LISTAR_MaestroTablaColumnas
 @idMaestro bigint
as
begin
	select id,idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio
	from GRMaestroTablaColumnas
	where FlgHabilitado='T' and
	idMaestro=@idMaestro
end
GO
PRINT N'Creando [dbo].[SPS_LISTAR_MaestroTablaColumnasID]...';


GO
create procedure SPS_LISTAR_MaestroTablaColumnasID
 @idMaestroColumna bigint
as
begin
	select id,idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio
	from GRMaestroTablaColumnas
	where FlgHabilitado='T' and
	id=@idMaestroColumna
end
GO
PRINT N'Creando [dbo].[SPS_MapeoMaestroTabla]...';


GO
create procedure SPS_MapeoMaestroTabla
as
begin
	select distinct b.tabla, a.tipoFormato from GRMapeoMaestroTabla a
	inner join GRMaestroTabla b on a.idMaestro=b.id
end
GO
PRINT N'Creando [dbo].[SPS_MapeoMaestroTablaTablaTipo]...';


GO
create procedure SPS_MapeoMaestroTablaTablaTipo
(
	@tabla varchar(100),
	@tipo varchar(1)
)
as
begin
	select b.tabla,c.columna,c.descripcion,a.columnaXls,c.columnaTabla, a.tipoFormato,b.tablaTemp,b.spcargaTabla from GRMapeoMaestroTabla a
	inner join GRMaestroTabla b on a.idMaestro=b.id
	inner join GRMaestroTablaColumnas c on c.idMaestro=b.id and c.id=a.idColumna
	where tabla=@tabla and a.tipoFormato=@tipo
end
GO
PRINT N'Actualizando [dbo].[sps_ManDelControl]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[sps_ManDelControl]';


GO
PRINT N'Actualizando [dbo].[USPS_DESCARGA_CABECERA]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[USPS_DESCARGA_CABECERA]';


GO
PRINT N'Actualizando [dbo].[USPS_DESCARGA_CABECERA_PEDIDO]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[USPS_DESCARGA_CABECERA_PEDIDO]';


GO
PRINT N'Actualizando [dbo].[USPS_REPORTE_DINAMICO_CONSOLIDADO]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[USPS_REPORTE_DINAMICO_CONSOLIDADO]';


GO
PRINT N'Actualizando [dbo].[USPS_REPORTE_DINAMICO_CONSOLIDADO_EXCEL]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[USPS_REPORTE_DINAMICO_CONSOLIDADO_EXCEL]';


GO
PRINT N'Actualizando [dbo].[USPS_REPORTE_DINAMICO_ISGPS]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[USPS_REPORTE_DINAMICO_ISGPS]';


GO
PRINT N'Actualizando [dbo].[USPS_DESCARGA]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[USPS_DESCARGA]';


GO
PRINT N'Comprobando los datos existentes con las restricciones recién creadas';


GO
ALTER TABLE [dbo].[GRMaestroTablaColumnas] WITH CHECK CHECK CONSTRAINT [FK_GRMaestroTablaColumnas_GRMaestroTabla];


GO
PRINT N'Actualización completada.';


GO






/*
Este script se creó con Visual Studio el 10/06/2019 a las 05:44 p.m..
Ejecute el script para que sea igual que DB_PED_271.
Este script realiza las acciones en el siguiente orden:
1. Deshabilita las restricciones de clave externa.
2. Ejecuta los comandos DELETE. 
3. Ejecuta los comandos UPDATE.
4. Ejecuta los comandos INSERT.
5. Habilita de nuevo las restricciones de clave externa.
Realice una copia de seguridad de la base de datos de destino antes de ejecutar este script.
*/
SET NUMERIC_ROUNDABORT OFF
GO
SET XACT_ABORT, ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
/*Puntero usado para actualizaciones de texto o imágenes. Puede que no sea necesario, pero se declara aquí por si lo fuera*/
DECLARE @pv binary(16)
BEGIN TRANSACTION
ALTER TABLE [dbo].[GRColumnaArchivo] DROP CONSTRAINT [FK__GRColumna__IdArc__01F34141]
ALTER TABLE [dbo].[GRMaestroTablaColumnas] DROP CONSTRAINT [FK_GRMaestroTablaColumnas_GRMaestroTabla]
ALTER TABLE [dbo].[TBL_CLIENTE_TEMP] DROP CONSTRAINT [FK_TBL_CLIENTE_TEMP_TBL_TIPO_CLIENTE]
ALTER TABLE [dbo].[TBL_USUARIO] DROP CONSTRAINT [FK__TBL_USUAR__IdPer__216BEC9A]
ALTER TABLE [dbo].[TBL_USUARIO] DROP CONSTRAINT [FK__TBL_USUAR__Sup_A__226010D3]
ALTER TABLE [dbo].[GRPerfil] DROP CONSTRAINT [FK_GRPerfil_CFRol]
ALTER TABLE [dbo].[TBL_DET_DEVOLUCION] DROP CONSTRAINT [FK__TBL_DET_D__ALM_P__0F4D3C5F]
ALTER TABLE [dbo].[TBL_DET_CANJE] DROP CONSTRAINT [FK__TBL_DET_C__ALM_P__0B7CAB7B]
ALTER TABLE [dbo].[TBL_ALMACEN_PRESENTACION] DROP CONSTRAINT [FK_ALM_PK]
ALTER TABLE [dbo].[TBL_ALMACEN_PRESENTACION] DROP CONSTRAINT [FK_PRE_PK]
ALTER TABLE [dbo].[GRPerfilMenu] DROP CONSTRAINT [FK_GRPerfilMenu_CFMenu]
ALTER TABLE [dbo].[GRPerfilMenu] DROP CONSTRAINT [FK_GRPerfilMenu_GRPerfil]
ALTER TABLE [dbo].[TBL_PEDIDO_DETALLE_PRESENTACION] DROP CONSTRAINT [FK__TBL_PEDID__ALM_P__18D6A699]
ALTER TABLE [dbo].[TBL_PEDIDO_DETALLE_PRESENTACION] DROP CONSTRAINT [FK__TBL_PEDID__PED_P__19CACAD2]
ALTER TABLE [dbo].[TBL_PEDIDO_DETALLE_PRESENTACION] DROP CONSTRAINT [FK__TBL_PEDID__PRE_P__31D75E8D]
ALTER TABLE [dbo].[CFFuncion] DROP CONSTRAINT [FK_CFFuncion_CFModulo]
ALTER TABLE [dbo].[TBL_DET_CANJE_PRESENTACION] DROP CONSTRAINT [FK__TBL_DET_C__ALM_P__0C70CFB4]
ALTER TABLE [dbo].[TBL_DET_CANJE_PRESENTACION] DROP CONSTRAINT [FK__TBL_DET_C__ID_CA__0D64F3ED]
ALTER TABLE [dbo].[TBL_DET_CANJE_PRESENTACION] DROP CONSTRAINT [FK__TBL_DET_C__PRE_P__0A888742]
ALTER TABLE [dbo].[CFValorEtiqueta] DROP CONSTRAINT [FK_CFIdioma_CFEtiqueta]
ALTER TABLE [dbo].[CFValorEtiqueta] DROP CONSTRAINT [FK_CFIdioma_CFPais]
ALTER TABLE [dbo].[CFManual] DROP CONSTRAINT [FK_CFManual_CFModulo]
ALTER TABLE [dbo].[CFManual] DROP CONSTRAINT [FK_CFManual_CFRol]
ALTER TABLE [dbo].[CFConfiguracion] DROP CONSTRAINT [FK_CFConfiguracion_CFFuncion]
ALTER TABLE [dbo].[CFServicioPermiso] DROP CONSTRAINT [FK_CFServicio_CFConfiguracion]
ALTER TABLE [dbo].[CFServicioPermiso] DROP CONSTRAINT [FK_CFServicio_CFFuncion]
ALTER TABLE [dbo].[CFServicioPermiso] DROP CONSTRAINT [FK_CFServicio_CFModulo]
ALTER TABLE [dbo].[CFServicioPermiso] DROP CONSTRAINT [FK_CFServicioPermiso_CFServicio]
ALTER TABLE [dbo].[CFMenu] DROP CONSTRAINT [FK_CFMenu_CFEtiqueta]
ALTER TABLE [dbo].[CFMenu] DROP CONSTRAINT [FK_CFMenu_CFFuncion]
ALTER TABLE [dbo].[CFMenu] DROP CONSTRAINT [FK_CFMenu_CFMenu]
ALTER TABLE [dbo].[TBL_DET_DEVOLUCION_PRESENTACION] DROP CONSTRAINT [FK__TBL_DET_D__ALM_P__10416098]
ALTER TABLE [dbo].[TBL_DET_DEVOLUCION_PRESENTACION] DROP CONSTRAINT [FK__TBL_DET_D__ID_CA__113584D1]
ALTER TABLE [dbo].[TBL_DET_DEVOLUCION_PRESENTACION] DROP CONSTRAINT [FK__TBL_DET_D__PRE_P__1ABEEF0B]
ALTER TABLE [dbo].[TBL_RESERVA_PRESENTACION] DROP CONSTRAINT [FK__TBL_RESER__ALM_P__1CA7377D]
ALTER TABLE [dbo].[TBL_RESERVA_PRESENTACION] DROP CONSTRAINT [FK__TBL_RESER__PED_P__1D9B5BB6]
ALTER TABLE [dbo].[TBL_RESERVA_PRESENTACION] DROP CONSTRAINT [FK__TBL_RESER__PRE_P__2A363CC5]
ALTER TABLE [dbo].[TBL_LISTAPRECIOS_PRESENTACION] DROP CONSTRAINT [FK__TBL_LISTA__PRE_P__1DD065E0]
SET IDENTITY_INSERT [dbo].[CFEtiqueta] ON
INSERT INTO [dbo].[CFEtiqueta] ([IdEtiqueta], [CodEtiqueta], [Descripcion], [FlgSincronizar], [FchRegistro]) VALUES (68, 'JAVA_LINKAUTOAYUDA', 'Link Autoayuda', 'T', '20190517 16:30:55.087')
SET IDENTITY_INSERT [dbo].[CFEtiqueta] OFF
INSERT INTO [dbo].[CFValorEtiqueta] ([IdPais], [IdEtiqueta], [Descripcion], [FchRegistro]) VALUES (1, 68, '', '20190517 16:30:55.090')
SET IDENTITY_INSERT [dbo].[GRMaestroTabla] ON
INSERT INTO [dbo].[GRMaestroTabla] ([id], [tabla], [descripcion], [tablaTemp], [spcargaTabla], [FlgHabilitado]) VALUES (1, 'Cliente', 'Tabla cliente', 'TEMP_CLIENTE', 'USPM_CARGA_CLIENTE_NEW', 'T')
INSERT INTO [dbo].[GRMaestroTabla] ([id], [tabla], [descripcion], [tablaTemp], [spcargaTabla], [FlgHabilitado]) VALUES (2, 'Producto', 'Tabla Producto', 'TEMP_PRODUCTO', 'USPM_CARGA_ARTICULO_NEW', 'T')
INSERT INTO [dbo].[GRMaestroTabla] ([id], [tabla], [descripcion], [tablaTemp], [spcargaTabla], [FlgHabilitado]) VALUES (3, 'GENERAL', 'Tabla GENERAL', 'TEMP_GENERAL', 'USPM_CARGA_GENERAL_NEW', 'T')
INSERT INTO [dbo].[GRMaestroTabla] ([id], [tabla], [descripcion], [tablaTemp], [spcargaTabla], [FlgHabilitado]) VALUES (4, 'VENDEDOR', 'Tabla VENDEDOR', 'TEMP_USUARIO', 'USPM_CARGA_USUARIO_NEW', 'T')
INSERT INTO [dbo].[GRMaestroTabla] ([id], [tabla], [descripcion], [tablaTemp], [spcargaTabla], [FlgHabilitado]) VALUES (5, 'RUTA', 'Tabla RUTA', 'TEMP_RUTA', 'USPM_CARGA_RUTA_NEW', 'T')
INSERT INTO [dbo].[GRMaestroTabla] ([id], [tabla], [descripcion], [tablaTemp], [spcargaTabla], [FlgHabilitado]) VALUES (6, 'FAMILIA', 'Tabla FAMILIA', 'TEMP_FAMILIA', 'USPM_CARGA_FAMILIA_NEW', 'T')
INSERT INTO [dbo].[GRMaestroTabla] ([id], [tabla], [descripcion], [tablaTemp], [spcargaTabla], [FlgHabilitado]) VALUES (7, 'FAMILIA_PRODUCTO', 'Tabla FAMILIA_PRODUCTO', 'TEMP_FAMILIA_PRODUCTO', 'USPM_CARGA_FAMILIA_PRODUCTO_NEW', 'T')
INSERT INTO [dbo].[GRMaestroTabla] ([id], [tabla], [descripcion], [tablaTemp], [spcargaTabla], [FlgHabilitado]) VALUES (8, 'MARCA', 'Tabla MARCA', 'TEMP_MARCA', 'USPM_CARGA_MARCA_NEW', 'T')
INSERT INTO [dbo].[GRMaestroTabla] ([id], [tabla], [descripcion], [tablaTemp], [spcargaTabla], [FlgHabilitado]) VALUES (9, 'MARCA_PRODUCTO', 'Tabla MARCA_PRODUCTO', 'TEMP_MARCA_PRODUCTO', 'USPM_CARGA_MARCA_PRODUCTO_NEW', 'T')
SET IDENTITY_INSERT [dbo].[GRMaestroTabla] OFF
SET IDENTITY_INSERT [dbo].[GRMaestroTablaColumnas] ON
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (1, 1, 'CODIGO', 'CLI_CODIGO', 'Codigo de cliente', 'TEXTO (500)', 'T', 'T')
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (2, 1, 'NOMBRE', 'CLI_NOMBRE', 'Nombre de cliente', 'TEXTO (500)', 'T', 'T')
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (3, 1, 'DIRECCION', 'CLI_DIRECCION', 'Direccion de cliente', 'TEXTO (500)', 'T', 'T')
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (4, 1, 'TIPO DE CLIENTE', 'TCLI_COD', 'TipoCliente de cliente', 'TEXTO (500)', 'T', 'T')
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (5, 1, 'GIRO', 'CLI_GIRO', 'GIRO de cliente', 'TEXTO (500)', 'F', 'T')
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (6, 1, 'LATITUD', 'CLI_LATITUD', 'LATITUD de cliente', 'TEXTO (500)', 'F', 'T')
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (7, 1, 'LONGITUD', 'CLI_LONGITUD', 'LONGITUD de cliente', 'TEXTO (500)', 'F', 'T')
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (8, 1, 'ADICIONAL 1', 'CLI_ADICIONAL_1', 'ADICIONAL 1 de cliente', 'TEXTO (500)', 'F', 'T')
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (9, 1, 'ADICIONAL 2', 'CLI_ADICIONAL_2', 'ADICIONAL 2 de cliente', 'TEXTO (500)', 'F', 'T')
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (10, 1, 'ADICIONAL 3', 'CLI_ADICIONAL_3', 'ADICIONAL 3 de cliente', 'TEXTO (500)', 'F', 'T')
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (11, 1, 'ADICIONAL 4', 'CLI_ADICIONAL_4', 'ADICIONAL 4 de cliente', 'TEXTO (500)', 'F', 'T')
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (12, 1, 'ADICIONAL 5', 'CLI_ADICIONAL_5', 'ADICIONAL 5 de cliente', 'TEXTO (500)', 'F', 'T')
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (13, 1, 'LIMITE CREDITO', 'LIMITE_CREDITO', 'LIMITE CREDITO de cliente', 'TEXTO (500)', 'F', 'T')
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (14, 1, 'CREDITO UTILIZADO', 'CREDITO_UTILIZADO', 'CREDITO UTILIZADO de cliente', 'TEXTO (500)', 'F', 'T')
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (15, 2, 'CODIGO PRO/PRE', 'PRO_CODIGO', 'CODIGO PRO/PRE de Producto', 'TEXTO (500)', 'T', 'T')
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (16, 2, 'NOMBRE', 'PRO_NOMBRE', 'NOMBRE de Producto', 'TEXTO (500)', 'T', 'T')
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (17, 2, 'PRECIO BASE', 'PRO_PRECIOBASE', 'PRECIO BASE de Producto', 'TEXTO (500)', 'T', 'T')
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (18, 2, 'STOCK', 'PRO_STOCK', 'STOCK de Producto', 'TEXTO (500)', 'T', 'T')
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (19, 2, 'DESCUENTO MINIMO', 'DESC_MIN', 'DESCUENTO MINIMO de Producto', 'TEXTO (500)', 'F', 'T')
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (20, 2, 'DESCUENTO MAXIMO', 'DESC_MAX', 'DESCUENTO MAXIMO de Producto', 'TEXTO (500)', 'F', 'T')
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (21, 2, 'UNIDAD FRACCIONAMIENTO', 'UNIDAD_FRACCIONAMIENTO', 'UNIDAD FRACCIONAMIENTO de Producto', 'TEXTO (500)', 'F', 'T')
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (22, 2, 'CANTIDAD', 'CANTIDAD', 'CANTIDAD de Producto', 'TEXTO (500)', 'F', 'T')
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (23, 3, 'TIPO CARGA', 'Tipo', 'TIPO CARGA de GENERAL', 'TEXTO (500)', 'T', 'T')
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (24, 3, 'CODIGO CONCEPTO', 'Codigo', 'CODIGO CONCEPTO de GENERAL', 'TEXTO (500)', 'T', 'T')
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (25, 3, 'DESCRIPCION CONCEPTO', 'Nombre', 'DESCRIPCION CONCEPTO de GENERAL', 'TEXTO (500)', 'T', 'T')
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (26, 3, 'ABREVIATURA', 'Abreviatura', 'ABREVIATURA de GENERAL', 'TEXTO (500)', 'T', 'T')
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (27, 4, 'CODIGO VENDEDOR', 'USR_CODIGO', 'CODIGO VENDEDOR de VENDEDOR', 'TEXTO (500)', 'T', 'T')
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (28, 4, 'LOGIN', 'USR_LOGIN', 'LOGIN de VENDEDOR', 'TEXTO (500)', 'T', 'T')
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (29, 4, 'NOMBRE VENDEDOR', 'USR_NOMBRE', 'NOMBRE VENDEDOR de VENDEDOR', 'TEXTO (500)', 'T', 'T')
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (30, 4, 'CLAVE VENDEDOR', 'USR_CLAVE', 'CLAVE VENDEDOR de VENDEDOR', 'TEXTO (500)', 'T', 'T')
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (31, 4, 'CODIGO GRUPO', 'USR_GRUPO', 'CODIGO GRUPO de VENDEDOR', 'TEXTO (500)', 'T', 'T')
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (32, 4, 'CODIGO PERFIL', 'USR_PERFIL', 'CODIGO PERFIL de VENDEDOR', 'TEXTO (500)', 'T', 'T')
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (33, 5, 'CODIGO CLIENTE', 'CLI_COD', 'CODIGO CLIENTE de RUTA', 'TEXTO (500)', 'T', 'T')
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (34, 5, 'CODIGO VENDEDOR', 'VEN_COD', 'CODIGO VENDEDOR de RUTA', 'TEXTO (500)', 'T', 'T')
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (35, 5, 'RUTA', 'VCLID_FECHA', 'RUTA de RUTA', 'TEXTO (500)', 'T', 'T')
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (36, 6, 'CODIGO FAMILIA', 'FAM_CODIGO', 'CODIGO FAMILIA de FAMILIA', 'TEXTO (500)', 'T', 'T')
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (37, 6, 'NOMBRE FAMILIA', 'FAM_NOMBRE', 'NOMBRE FAMILIA de FAMILIA', 'TEXTO (500)', 'T', 'T')
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (38, 7, 'CODIGO FAMILIA', 'FAM_CODIGO', 'CODIGO FAMILIA de FAMILIA', 'TEXTO (500)', 'T', 'T')
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (39, 7, 'CODIGO PRODUCTO', 'PRO_CODIGO', 'CODIGO PRODUCTO de FAMILIA', 'TEXTO (500)', 'T', 'T')
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (40, 8, 'CODIGO MARCA', 'MAR_CODIGO', 'CODIGO MARCA de MARCA', 'TEXTO (500)', 'T', 'T')
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (41, 8, 'NOMBRE MARCA', 'MAR_NOMBRE', 'NOMBRE MARCA de MARCA', 'TEXTO (500)', 'T', 'T')
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (42, 9, 'CODIGO MARCA', 'MAR_CODIGO', 'CODIGO MARCA de MARCA_PRODUCTO', 'TEXTO (500)', 'T', 'T')
INSERT INTO [dbo].[GRMaestroTablaColumnas] ([id], [idMaestro], [columna], [columnaTabla], [descripcion], [formato], [obligatorio], [FlgHabilitado]) VALUES (43, 9, 'CODIGO PRODUCTO', 'PRO_CODIGO', 'CODIGO PRODUCTO de MARCA_PRODUCTO', 'TEXTO (500)', 'T', 'T')
SET IDENTITY_INSERT [dbo].[GRMaestroTablaColumnas] OFF
ALTER TABLE [dbo].[GRColumnaArchivo]
    ADD FOREIGN KEY ([IdArchivo]) REFERENCES [dbo].[GRArchivo] ([IdArchivo])
ALTER TABLE [dbo].[GRMaestroTablaColumnas]
    ADD CONSTRAINT [FK_GRMaestroTablaColumnas_GRMaestroTabla] FOREIGN KEY ([idMaestro]) REFERENCES [dbo].[GRMaestroTabla] ([id])
ALTER TABLE [dbo].[TBL_CLIENTE_TEMP]
    ADD CONSTRAINT [FK_TBL_CLIENTE_TEMP_TBL_TIPO_CLIENTE] FOREIGN KEY ([TC_PK]) REFERENCES [dbo].[TBL_TIPO_CLIENTE] ([TC_PK])
ALTER TABLE [dbo].[TBL_USUARIO]
    ADD FOREIGN KEY ([IdPerfil]) REFERENCES [dbo].[GRPerfil] ([IdPerfil])
ALTER TABLE [dbo].[TBL_USUARIO]
    ADD FOREIGN KEY ([Sup_Asignado]) REFERENCES [dbo].[TBL_USUARIO] ([USR_PK])
ALTER TABLE [dbo].[GRPerfil]
    ADD CONSTRAINT [FK_GRPerfil_CFRol] FOREIGN KEY ([IdTipoPerfil]) REFERENCES [dbo].[CFTipoPerfil] ([IdTipoPerfil])
ALTER TABLE [dbo].[TBL_DET_DEVOLUCION]
    ADD FOREIGN KEY ([ALM_PK]) REFERENCES [dbo].[TBL_ALMACEN] ([ALM_PK])
ALTER TABLE [dbo].[TBL_DET_CANJE]
    ADD FOREIGN KEY ([ALM_PK]) REFERENCES [dbo].[TBL_ALMACEN] ([ALM_PK])
ALTER TABLE [dbo].[TBL_ALMACEN_PRESENTACION]
    ADD CONSTRAINT [FK_ALM_PK] FOREIGN KEY ([ALM_PK]) REFERENCES [dbo].[TBL_ALMACEN] ([ALM_PK])
ALTER TABLE [dbo].[TBL_ALMACEN_PRESENTACION]
    ADD CONSTRAINT [FK_PRE_PK] FOREIGN KEY ([PRE_PK]) REFERENCES [dbo].[TBL_PRESENTACION] ([PRE_PK]) ON DELETE CASCADE
ALTER TABLE [dbo].[GRPerfilMenu]
    ADD CONSTRAINT [FK_GRPerfilMenu_CFMenu] FOREIGN KEY ([IdMenu]) REFERENCES [dbo].[CFMenu] ([IdMenu])
ALTER TABLE [dbo].[GRPerfilMenu]
    ADD CONSTRAINT [FK_GRPerfilMenu_GRPerfil] FOREIGN KEY ([IdPerfil]) REFERENCES [dbo].[GRPerfil] ([IdPerfil])
ALTER TABLE [dbo].[TBL_PEDIDO_DETALLE_PRESENTACION]
    ADD FOREIGN KEY ([ALM_PK]) REFERENCES [dbo].[TBL_ALMACEN] ([ALM_PK])
ALTER TABLE [dbo].[TBL_PEDIDO_DETALLE_PRESENTACION]
    ADD FOREIGN KEY ([PED_PK]) REFERENCES [dbo].[TBL_PEDIDO] ([PED_PK])
ALTER TABLE [dbo].[TBL_PEDIDO_DETALLE_PRESENTACION]
    ADD CONSTRAINT [FK__TBL_PEDID__PRE_P__31D75E8D] FOREIGN KEY ([PRE_PK]) REFERENCES [dbo].[TBL_PRESENTACION] ([PRE_PK]) ON DELETE SET NULL
ALTER TABLE [dbo].[CFFuncion]
    ADD CONSTRAINT [FK_CFFuncion_CFModulo] FOREIGN KEY ([IdModulo]) REFERENCES [dbo].[CFModulo] ([IdModulo])
ALTER TABLE [dbo].[TBL_DET_CANJE_PRESENTACION]
    ADD FOREIGN KEY ([ALM_PK]) REFERENCES [dbo].[TBL_ALMACEN] ([ALM_PK])
ALTER TABLE [dbo].[TBL_DET_CANJE_PRESENTACION]
    ADD FOREIGN KEY ([ID_CAB_CANJE]) REFERENCES [dbo].[TBL_CAB_CANJE] ([ID_CAB_CANJE])
ALTER TABLE [dbo].[TBL_DET_CANJE_PRESENTACION]
    ADD CONSTRAINT [FK__TBL_DET_C__PRE_P__0A888742] FOREIGN KEY ([PRE_PK]) REFERENCES [dbo].[TBL_PRESENTACION] ([PRE_PK]) ON DELETE SET NULL
ALTER TABLE [dbo].[CFValorEtiqueta]
    ADD CONSTRAINT [FK_CFIdioma_CFEtiqueta] FOREIGN KEY ([IdEtiqueta]) REFERENCES [dbo].[CFEtiqueta] ([IdEtiqueta])
ALTER TABLE [dbo].[CFValorEtiqueta]
    ADD CONSTRAINT [FK_CFIdioma_CFPais] FOREIGN KEY ([IdPais]) REFERENCES [dbo].[CFPais] ([IdPais])
ALTER TABLE [dbo].[CFManual]
    ADD CONSTRAINT [FK_CFManual_CFModulo] FOREIGN KEY ([IdModulo]) REFERENCES [dbo].[CFModulo] ([IdModulo])
ALTER TABLE [dbo].[CFManual]
    ADD CONSTRAINT [FK_CFManual_CFRol] FOREIGN KEY ([IdTipoPerfil]) REFERENCES [dbo].[CFTipoPerfil] ([IdTipoPerfil])
ALTER TABLE [dbo].[CFConfiguracion]
    ADD CONSTRAINT [FK_CFConfiguracion_CFFuncion] FOREIGN KEY ([IdFuncion]) REFERENCES [dbo].[CFFuncion] ([IdFuncion])
ALTER TABLE [dbo].[CFServicioPermiso]
    ADD CONSTRAINT [FK_CFServicio_CFConfiguracion] FOREIGN KEY ([IdConfiguracion]) REFERENCES [dbo].[CFConfiguracion] ([IdConfiguracion])
ALTER TABLE [dbo].[CFServicioPermiso]
    ADD CONSTRAINT [FK_CFServicio_CFFuncion] FOREIGN KEY ([IdFuncion]) REFERENCES [dbo].[CFFuncion] ([IdFuncion])
ALTER TABLE [dbo].[CFServicioPermiso]
    ADD CONSTRAINT [FK_CFServicio_CFModulo] FOREIGN KEY ([IdModulo]) REFERENCES [dbo].[CFModulo] ([IdModulo])
ALTER TABLE [dbo].[CFServicioPermiso]
    ADD CONSTRAINT [FK_CFServicioPermiso_CFServicio] FOREIGN KEY ([IdServicio]) REFERENCES [dbo].[CFServicio] ([IdServicio])
ALTER TABLE [dbo].[CFMenu]
    WITH NOCHECK ADD CONSTRAINT [FK_CFMenu_CFEtiqueta] FOREIGN KEY ([IdEtiqueta]) REFERENCES [dbo].[CFEtiqueta] ([IdEtiqueta])
ALTER TABLE [dbo].[CFMenu]
    WITH NOCHECK ADD CONSTRAINT [FK_CFMenu_CFFuncion] FOREIGN KEY ([IdFuncion]) REFERENCES [dbo].[CFFuncion] ([IdFuncion])
ALTER TABLE [dbo].[CFMenu]
    WITH NOCHECK ADD CONSTRAINT [FK_CFMenu_CFMenu] FOREIGN KEY ([IdMenuPadre]) REFERENCES [dbo].[CFMenu] ([IdMenu])
ALTER TABLE [dbo].[TBL_DET_DEVOLUCION_PRESENTACION]
    ADD FOREIGN KEY ([ALM_PK]) REFERENCES [dbo].[TBL_ALMACEN] ([ALM_PK])
ALTER TABLE [dbo].[TBL_DET_DEVOLUCION_PRESENTACION]
    ADD FOREIGN KEY ([ID_CAB_DEVOLUCION]) REFERENCES [dbo].[TBL_CAB_DEVOLUCION] ([ID_CAB_DEVOLUCION])
ALTER TABLE [dbo].[TBL_DET_DEVOLUCION_PRESENTACION]
    ADD CONSTRAINT [FK__TBL_DET_D__PRE_P__1ABEEF0B] FOREIGN KEY ([PRE_PK]) REFERENCES [dbo].[TBL_PRESENTACION] ([PRE_PK]) ON DELETE SET NULL
ALTER TABLE [dbo].[TBL_RESERVA_PRESENTACION]
    ADD FOREIGN KEY ([ALM_PK]) REFERENCES [dbo].[TBL_ALMACEN] ([ALM_PK])
ALTER TABLE [dbo].[TBL_RESERVA_PRESENTACION]
    ADD FOREIGN KEY ([PED_PK]) REFERENCES [dbo].[TBL_PEDIDO] ([PED_PK])
ALTER TABLE [dbo].[TBL_RESERVA_PRESENTACION]
    ADD CONSTRAINT [FK__TBL_RESER__PRE_P__2A363CC5] FOREIGN KEY ([PRE_PK]) REFERENCES [dbo].[TBL_PRESENTACION] ([PRE_PK]) ON DELETE CASCADE
ALTER TABLE [dbo].[TBL_LISTAPRECIOS_PRESENTACION]
    ADD CONSTRAINT [FK__TBL_LISTA__PRE_P__1DD065E0] FOREIGN KEY ([TC_PK]) REFERENCES [dbo].[TBL_TIPO_CLIENTE] ([TC_PK]) ON DELETE CASCADE
COMMIT TRANSACTION


UPDATE [dbo].[TBL_CONFIG_CARGA] SET [TIPO] = 'X' WHERE [TIPO] = 'E'


