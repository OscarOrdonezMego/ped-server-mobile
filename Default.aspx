<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Mobile._Default" Codebehind="Default.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Inicio | ENTEL PER&Uacute;</title>
<script type="text/javascript" src="<%=Request.ApplicationPath + "/about/js/jquery.min1.10.2.js" %>"></script>
<link href="<%=Request.ApplicationPath + "/about/css/css_version.css" %>"  type="text/css" rel="stylesheet" />  
<script type="text/javascript">
    $(document).ready(function () {
            $.ajax({ url: '<%=Request.ApplicationPath + "/about/recuperarVersion.ashx" %>', success: function (result) {
                $("#wrap").html(result);
            }
            });
    });
</script>
</head>
<body class="fondoversion">
  <div id="wrap">

  </div>  
</body>
</html>