<%@ WebHandler Language="C#" Class="GrabarPedido" %>
using System;
using System.IO;
using System.Web;
using Nextel.BE;
using System.Web.Script.Serialization;
using Nextel.Utils;
using Java.Control;

public class GrabarPedido : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        //-----------------------------------------------------------------------------------
        // Recepcion de Datos en Xml
        //-----------------------------------------------------------------------------------
        String lsJson;
        //byte[] loinputBinary;
        //int liBinaryBytes;
        //liBinaryBytes = context.Request.TotalBytes;
        //loinputBinary = context.Request.BinaryRead(liBinaryBytes);
        //lsJson = BLBase.fnConvertirBinaryToString(loinputBinary);
        //lsJson = BLBase.SimpleBinaryToString(loinputBinary);
        lsJson = new StreamReader(context.Request.InputStream).ReadToEnd();

        //Graba No Pedido
        //lsJson = "{'codAgencia':'','codCanalVenta':'01','codCliente':'500','codFormaPago':'','codMotNoPedido':'41','codTipoDoc':'','codVendedor':'10','descAgencia':'','descCliente':'A.F.DISTRIBUIDORA E.I.R.L.','dirEntrega':'','dirFacturacion':'','fecFin':'20120227 20:27:13','fecInicio':'20120227 20:26:43','fechaEntrega':'','fechaId':1330374403672,'idxAgencia':0,'idxDirEntrega':0,'idxDirFacturacion':0,'idxFormaPago':0,'idxTipoDoc':0,'letras':'','listaDetPedido':[],'mntIGV':'0.00','mntSubTotal':'0.00','mntTotal':'0.00','obsLetras':'','observaciones':''}";

        //lsJson = "{'codAgencia':'568','codCanalVenta':'01','codCliente':'002211','codFormaPago':'13','codMotNoPedido':'','codTipoDoc':'FA','codVendedor':'0033','descAgencia':'LUCHINE CONDOR LUZ MARLENI','descCliente':'GILBERTO CARBONEL EFFIO','dirEntrega':'a���������. huaylas 901','dirFacturacion':'av. huaylas 901','dsctoFormaPago':'0.0','fecFin':'20120315 15:59:08','fecInicio':'20120315 15:47:16','fechaEntrega':'','fechaId':1331844436778,'idxDirEntrega':0,'idxDirFacturacion':0,'idxFormaPago':9,'idxTipoDoc':1,'letras':'12 36','limiteCredito':'0.0000','limiteCreditoDisp':'0.0000','listaDetPedido':[{'cantidad':'30','codCategoria':'0140','codListaPrecio':'01','codigo':'110293','descripcion':'CAMARA PARA LLANTA DE CARRETILLA 11866-TRUPER','dscto1':'0.0','dscto2':'10.0','dscto3':'0.0','dsctoSucesivo':'0.09999999999999998','flgBonificacion':'F','idProdPadre':0,'idProducto':9741,'precio1':'7.15','precio2':'6.435','precio3':'0.0','precioFinal':'6.435','precioLPBR':'0.0','precioLPG':'7.15','precioLPM':'0.0','presentacion':'UND ','stock':'4841.0000','subTotal':'193.05','valorBruto':'214.5','valorDscto':'21.45','valorIGV':'34.749','valorTotal':'227.799'},{'cantidad':'69','codCategoria':'0118','codListaPrecio':'01','codigo':'010317','descripcion':'ADAPTADOR ENC 1/2 A 3/8 13422-A-5254 TRUPER','dscto1':'0.0','dscto2':'15.0','dscto3':'3.0','dsctoSucesivo':'0.1755','flgBonificacion':'F','idProdPadre':0,'idProducto':7809,'precio1':'4.45','precio2':'3.7825','precio3':'3.669','precioFinal':'3.669','precioLPBR':'0.0','precioLPG':'4.45','precioLPM':'0.0','presentacion':'UND ','stock':'123.0000','subTotal':'253.161','valorBruto':'307.05','valorDscto':'53.889','valorIGV':'45.569','valorTotal':'298.73'},{'cantidad':'96','codCategoria':'0034','codListaPrecio':'01','codigo':'010580','descripcion':'PORTA MACHOS 3/32\' A 3/8\' (11435) TRUPER','dscto1':'0.0','dscto2':'0.0','dscto3':'0.0','dsctoSucesivo':'0.0','flgBonificacion':'F','idProdPadre':0,'idProducto':8028,'precio1':'9.65','precio2':'9.65','precio3':'0.0','precioFinal':'9.65','precioLPBR':'0.0','precioLPG':'9.65','precioLPM':'0.0','presentacion':'UND ','stock':'6.0000','subTotal':'926.4','valorBruto':'926.4','valorDscto':'0.0','valorIGV':'166.752','valorTotal':'1093.152'},{'cantidad':'21','codCategoria':'0067','codListaPrecio':'01','codigo':'067529','descripcion':'BROCA P/MADERA 10MM HOLZ','dscto1':'0.0','dscto2':'15.0','dscto3':'0.0','dsctoSucesivo':'0.15000000000000002','flgBonificacion':'F','idProdPadre':0,'idProducto':9101,'precio1':'11.35','precio2':'9.6475','precio3':'0.0','precioFinal':'9.6475','precioLPBR':'0.0','precioLPG':'11.35','precioLPM':'0.0','presentacion':'UND ','stock':'1443.0000','subTotal':'202.5975','valorBruto':'238.35','valorDscto':'35.7525','valorIGV':'36.4676','valorTotal':'239.0651'},{'cantidad':'17','codCategoria':'0007','codListaPrecio':'01','codigo':'067718','descripcion':'MODULO # 29 SDS PLUS F8 EXTREME','dscto1':'0.0','dscto2':'10.0','dscto3':'2.0','dsctoSucesivo':'0.118','flgBonificacion':'F','idProdPadre':0,'idProducto':10821,'precio1':'2777.8','precio2':'2500.02','precio3':'2450.0196','precioFinal':'2450.0196','precioLPBR':'0.0','precioLPG':'2777.8','precioLPM':'0.0','presentacion':'UND ','stock':'1.0000','subTotal':'41650.3332','valorBruto':'47222.6','valorDscto':'5572.2668','valorIGV':'7497.06','valorTotal':'49147.3932'},{'cantidad':'70','codCategoria':'0143','codListaPrecio':'01','codigo':'010472','descripcion':'CEPILLO P/CARPINTERO ESTRIADO # 5 (12015)TRUPER','dscto1':'0.0','dscto2':'0.0','dscto3':'0.0','dsctoSucesivo':'0.0','flgBonificacion':'F','idProdPadre':0,'idProducto':7917,'precio1':'122.514','precio2':'122.514','precio3':'0.0','precioFinal':'122.514','precioLPBR':'0.0','precioLPG':'122.514','precioLPM':'0.0','presentacion':'UND ','stock':'43.0000','subTotal':'8575.98','valorBruto':'8575.98','valorDscto':'0.0','valorIGV':'1543.6764','valorTotal':'10119.6564'},{'cantidad':'15','codCategoria':'0051','codListaPrecio':'01','codigo':'051477','descripcion':'AHORRADOR 3 BIAX 20W L/B CAJA (77194) G.E.','dscto1':'0.0','dscto2':'15.0','dscto3':'2.0','dsctoSucesivo':'0.16700000000000004','flgBonificacion':'F','idProdPadre':0,'idProducto':8548,'precio1':'10.25','precio2':'8.7125','precio3':'8.5382','precioFinal':'8.5382','precioLPBR':'0.0','precioLPG':'10.25','precioLPM':'0.0','presentacion':'UND ','stock':'3022.0000','subTotal':'128.073','valorBruto':'153.75','valorDscto':'25.677','valorIGV':'23.0531','valorTotal':'151.1261'},{'cantidad':'1','codCategoria':'0099','codListaPrecio':'','codigo':'099327','descripcion':'AHORRADOR 3 BIAX 20W L/B CAJA (77194)G.E.','dscto1':'0.0','dscto2':'0.0','dscto3':'0.0','dsctoSucesivo':'0.00','flgBonificacion':'T','idProdPadre':8548,'idProducto':8921,'precio1':'0.0','precio2':'0.0','precio3':'0.0','precioFinal':'0.0','precioLPBR':'0.0','precioLPG':'0.0','precioLPM':'0.0','presentacion':'UND ','stock':'496.0000','subTotal':'0.0','valorBruto':'0.0','valorDscto':'0.0','valorIGV':'0.0','valorTotal':'0.0'}],'mntBruto':'57638.63','mntDscto':'5709.0353','mntIGV':'9347.3271','mntSubTotal':'51929.5947','mntTotal':'61276.9218','obsLetras':'todos van al banco bcp','observaciones':'pruebas desde ideos'}";

        BaseLN.registrarLog("GrabarPedido - IN: " + lsJson);

        JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
        BEEnvCabPedido loBEEnvCabPedido = jsonSerializer.Deserialize<BEEnvCabPedido>(lsJson);

        BEValidaPedidoAndroid loBEValidaPedidoAndroid = ControlJava.fnRegistrarPedidoAndroid_Validar(loBEEnvCabPedido);
        BaseLN.registrarLog("GrabaPedido - OUT: " + loBEValidaPedidoAndroid.resultado);          
        context.Response.Write(jsonSerializer.Serialize(loBEValidaPedidoAndroid));
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}