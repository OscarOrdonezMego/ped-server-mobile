 <%@ WebHandler Language="C#" Class="ConsultaPedidoEstado" %>

using System;
using System.IO;
using System.Web;
using Nextel.BL;

public class ConsultaPedidoEstado : IHttpHandler
{
    public void ProcessRequest(HttpContext context)
    {
        String lsJson;
        //byte[] loinputBinary;
        //int liBinaryBytes;
        //liBinaryBytes = context.Request.TotalBytes;
        //loinputBinary = context.Request.BinaryRead(liBinaryBytes);
        //lsJson = BLBase.fnConvertirBinaryToString(loinputBinary);
        //lsJson = BLBase.SimpleBinaryToString(loinputBinary);
        lsJson = new StreamReader(context.Request.InputStream).ReadToEnd();

        System.Net.WebClient loWebClient = new System.Net.WebClient();
        loWebClient.Headers.Add("content-type", "application/json");
        String lsResponse = System.Text.Encoding.ASCII.GetString(loWebClient
            .UploadData(System.Configuration.ConfigurationManager.AppSettings["WS_CONSULTA_URL"].ToString() + "pedidos.svc/consultas", "POST",
            System.Text.Encoding.Default.GetBytes(lsJson)));

        context.Response.Write(lsResponse);
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }


}