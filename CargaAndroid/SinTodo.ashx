<%@ WebHandler Language="C#" Class="SinTodo" %>

using System;
using System.Web;
using Nextel.BE;
using Java.Control;

public class SinTodo : IHttpHandler
{
    public void ProcessRequest(HttpContext context)
    {
        String lsTelefono;
        String lscodUsuario = context.Request.QueryString["codUsuario"];
        try
        {
            lsTelefono = context.Request.QueryString["telefono"];
        }
        catch (Exception e)
        {
            lsTelefono = "";
        }

        BESincronizar loBESincronizar = ControlJava.fnSinScriptString(lscodUsuario, lsTelefono);
        HttpResponse Response = HttpContext.Current.Response;
        Response.AddHeader("Resultado", loBESincronizar.Respuesta);
        Response.AddHeader("IdResultado", loBESincronizar.idRespuesta.ToString());
        Response.AddHeader("tamano", loBESincronizar.StringScript.Length.ToString());
        Response.AddHeader("Content-Encoding", "gzip");
        context.Response.Filter = new System.IO.Compression.GZipStream(Response.Filter, System.IO.Compression.CompressionMode.Compress);
        context.Response.Write(loBESincronizar.StringScript);
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
}