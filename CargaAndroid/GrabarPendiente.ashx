<%@ WebHandler Language="C#" Class="GrabarPendiente" %>

using System;
using System.IO;
using System.Web;
using System.Web.Script.Serialization;
using Nextel.Utils;
using Java.Control;
using Java.Bean;

public class GrabarPendiente : IHttpHandler
{
    public void ProcessRequest(HttpContext context)
    {
        //-----------------------------------------------------------------------------------
        // Recepcion de Datos en Xml
        //-----------------------------------------------------------------------------------
        String lsJson;
        //byte[] loinputBinary;
        //int liBinaryBytes;
        //liBinaryBytes = context.Request.TotalBytes;
        //loinputBinary = context.Request.BinaryRead(liBinaryBytes);
        //lsJson = BLBase.fnConvertirBinaryToString(loinputBinary);
        lsJson = new StreamReader(context.Request.InputStream).ReadToEnd();
        //lsJson = BLBase.SimpleBinaryToString(loinputBinary);

        //lsJson = "{'errorConexion':'0','errorPosicion':'0','listCanjes':[],'listDevoluciones':[],'listPagos':[],'listPedidos':[{'cantidadTotal':'2','celda':'-','clipk':'','codDireccion':'','codigoCliente':'CLIENTE006','codigoMotivo':'','codigoPedido':'','codigoUsuario':'UP','condicionVenta':'0001','empresa':'','errorConexion':'0','errorPosicion':'0','fechaFin':'30/01/2013 16:24:58','fechaInicio':'30/01/2013 16:23:38','latitud':'0','listaDetPedido':[{'CodPedido':'','cantidad':'2','codPedido':'','codigoArticulo':'41718','descuento':'0','empresa':'','idArticulo':null,'monto':'254.0','nombreArticulo':'BALSAME COLORES PROTECTEUR 300 ML','precioBase':'127.00','stock':'1.00'}],'longitud':'0','montoTotal':'254.0','observacion':''}]}";
        BaseLN.registrarLog("GrabarPendiente - IN: " + lsJson);

        JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();

        //List<BEEnvCabPedido> lolistaBEEnvCabPedido = jsonSerializer.Deserialize<List<BEEnvCabPedido>>(lsJson);
        //String msnOut = ControlJava.fnRegistrarPendientesAndroid(lolistaBEEnvCabPedido);

        BEPendientes lopendientes = jsonSerializer.Deserialize<BEPendientes>(lsJson);
        BEResponsePendientes loBEResponsePendientes = ControlJava.fnRegistrarPendientesAndroid(lopendientes);

        context.Response.Write(jsonSerializer.Serialize(loBEResponsePendientes));
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}