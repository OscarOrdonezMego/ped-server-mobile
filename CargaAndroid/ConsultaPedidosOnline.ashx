﻿<%@ WebHandler Language="C#" Class="ConsultaPedidosOnline" %>

using System;
using System.Web;
using Java.Control;
using Nextel.BE;

public class ConsultaPedidosOnline : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        
        String lscodUsuario = context.Request.QueryString["codUsuario"];
        String lsFecha = context.Request.QueryString["fecha"];
        String lsTipoArticulo = context.Request.QueryString["tipoArticulo"];
        String lscodCliente = context.Request.QueryString["codCliente"];

        BESincronizar loBESincronizar = ControlJava.fnConsultaPedidosScript(lscodUsuario, lsFecha, lsTipoArticulo, lscodCliente);
        HttpResponse Response = HttpContext.Current.Response;
        Response.AddHeader("Resultado", loBESincronizar.Respuesta);
        Response.AddHeader("IdResultado", loBESincronizar.idRespuesta.ToString());
        Response.AddHeader("tamano", loBESincronizar.StringScript.Length.ToString());
        Response.AddHeader("Content-Encoding", "gzip");
        context.Response.Filter = new System.IO.Compression.GZipStream(Response.Filter, System.IO.Compression.CompressionMode.Compress);
        context.Response.Write(loBESincronizar.StringScript);        
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}