 <%@ WebHandler Language="C#" Class="ProductoOnline" %>

using System;
using System.IO;
using System.Web;
using Nextel.BE;
using System.Web.Script.Serialization;
using Java.Control;
using Java.Bean;

public class ProductoOnline : IHttpHandler
{
    
    public void ProcessRequest(HttpContext context)
    {
        //-----------------------------------------------------------------------------------
        // Recepcion de Datos en Xml
        //-----------------------------------------------------------------------------------
        String lsJson = new StreamReader(context.Request.InputStream).ReadToEnd();
        JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
        BEArticuloOnline loBeanArticulo = jsonSerializer.Deserialize<BEArticuloOnline>(lsJson);
        BEListaArticuloOnline loResultado = ControlJava.fnObtenerStockAndroid(loBeanArticulo);
        context.Response.Write(jsonSerializer.Serialize(loResultado));
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }


}