<%@ WebHandler Language="C#" Class="GrabarCobranza" %>
using System;
using System.IO;
using System.Web;
using System.Web.Script.Serialization;
using Nextel.Utils;
using Java.Control;
using Java.Bean;

public class GrabarCobranza : IHttpHandler
{
    public void ProcessRequest(HttpContext context)
    {
        //-----------------------------------------------------------------------------------
        // Recepcion de Datos en Xml
        //-----------------------------------------------------------------------------------
        String lsJson;
        //byte[] loinputBinary;
        //int liBinaryBytes;
        //liBinaryBytes = context.Request.TotalBytes;
        //loinputBinary = context.Request.BinaryRead(liBinaryBytes);
        //lsJson = BLBase.fnConvertirBinaryToString(loinputBinary);
        //lsJson = BLBase.SimpleBinaryToString(loinputBinary);
        lsJson = new StreamReader(context.Request.InputStream).ReadToEnd();

        //Graba Cobranza
        //lsJson = "{'celda':'-','cob_montopagado':'12','cob_montototal':'224','cob_pk':'5','codPago':'1356629076423','cuenta':'','errorConexion':'0','errorPosicion':'0','fechaDocumento':'02/11/2013','fechaMovil':'27/12/2012 12:24:36','idBanco':'','idCliente':'1','idDocCobranza':'C001','idFormaPago':'1','idUsuario':'1','latitud':'0','longitud':'0','nroDocumento':'','pago':'2.00','saldo':'212.00'}";//,'recordIdCobranza':'0','posicionIdCobranza':'0'}";
        BaseLN.registrarLog("GrabarCobranza - IN: " + lsJson);

        JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
        BeanPago loBEPago = jsonSerializer.Deserialize<BeanPago>(lsJson);
        //BLTRCabPedido loBLTRCabPedido = new BLTRCabPedido();

        //String msnOut = loBLTRCabPedido.fnPerInsertCabDetalle(loBEEnvCabPedido);
        //String msnOut = loBLTRCabPedido.fnPerGrabarPedido(loBEEnvCabPedido);
        String msnOut = ControlJava.fnRegistrarCobranzaAndroid(loBEPago);

        BaseLN.registrarLog("GrabarCobranza - OUT: " + msnOut);

        context.Response.Write(msnOut);
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}