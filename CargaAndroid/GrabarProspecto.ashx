﻿<%@ WebHandler Language="C#" Class="GrabarProspecto" %>

using System;
using System.IO;
using System.Web;
using Nextel.BE;
using System.Web.Script.Serialization;
using Nextel.Utils;
using Java.Control;
using Java.Bean;

public class GrabarProspecto : IHttpHandler
{
    public void ProcessRequest(HttpContext context)
    {
        String lsJson;
        lsJson = new StreamReader(context.Request.InputStream).ReadToEnd();

        BaseLN.registrarLog("GrabarProspecto - IN: " + lsJson);

        JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
        BEProspecto loBEProspecto = jsonSerializer.Deserialize<BEProspecto>(lsJson);

        String msnOut = ControlJava.fnRegistrarProspectoAndroid(loBEProspecto);

        BaseLN.registrarLog("GrabarProspecto - OUT: " + msnOut);

        context.Response.Write(msnOut);
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}