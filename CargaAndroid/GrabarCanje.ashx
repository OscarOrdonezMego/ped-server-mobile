<%@ WebHandler Language="C#" Class="GrabarCanje" %>

using System;
using System.IO;
using System.Web;
using System.Web.Script.Serialization;
using Nextel.Utils;
using Java.Control;
using Java.Bean;

public class GrabarCanje : IHttpHandler
{
    public void ProcessRequest(HttpContext context)
    {
        //-----------------------------------------------------------------------------------
        // Recepcion de Datos en Xml
        //-----------------------------------------------------------------------------------
        String lsJson;
        //byte[] loinputBinary;
        //int liBinaryBytes;
        //liBinaryBytes = context.Request.TotalBytes;
        //loinputBinary = context.Request.BinaryRead(liBinaryBytes);
        //lsJson = BLBase.fnConvertirBinaryToString(loinputBinary);
        //lsJson = BLBase.SimpleBinaryToString(loinputBinary);
        lsJson = new StreamReader(context.Request.InputStream).ReadToEnd();

        //Graba No Pedido
        //lsJson = "{'codAgencia':'','codCanalVenta':'01','codCliente':'500','codFormaPago':'','codMotNoPedido':'41','codTipoDoc':'','codVendedor':'10','descAgencia':'','descCliente':'A.F.DISTRIBUIDORA E.I.R.L.','dirEntrega':'','dirFacturacion':'','fecFin':'20120227 20:27:13','fecInicio':'20120227 20:26:43','fechaEntrega':'','fechaId':1330374403672,'idxAgencia':0,'idxDirEntrega':0,'idxDirFacturacion':0,'idxFormaPago':0,'idxTipoDoc':0,'letras':'','listaDetPedido':[],'mntIGV':'0.00','mntSubTotal':'0.00','mntTotal':'0.00','obsLetras':'','observaciones':''}";

        BaseLN.registrarLog("GrabarCanje - IN: " + lsJson);

        JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
        BeanCanjeCab loBEEnvCabCanje = jsonSerializer.Deserialize<BeanCanjeCab>(lsJson);
        //BLTRCabPedido loBLTRCabPedido = new BLTRCabPedido();

        //String msnOut = loBLTRCabPedido.fnPerInsertCabDetalle(loBEEnvCabPedido);
        //String msnOut = loBLTRCabPedido.fnPerGrabarPedido(loBEEnvCabPedido);
        String msnOut = ControlJava.fnRegistrarCanjeAndroid(loBEEnvCabCanje);

        BaseLN.registrarLog("GrabarCanje - OUT: " + msnOut);

        context.Response.Write(msnOut);
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}