<%@ WebHandler Language="C#" Class="ConsultaOnline" %>

using System;
using System.Web;
using System.IO;
using System.Web.Script.Serialization;
using Java.Control;
using Java.Bean;

public class ConsultaOnline : IHttpHandler
{
    public void ProcessRequest(HttpContext context)
    {
        //-----------------------------------------------------------------------------------
        // Recepcion de Datos en Xml
        //-----------------------------------------------------------------------------------
        String lsJson;
        //byte[] loinputBinary;
        //int liBinaryBytes;
        //liBinaryBytes = context.Request.TotalBytes;
        //loinputBinary = context.Request.BinaryRead(liBinaryBytes);
        //lsJson = BLBase.fnConvertirBinaryToString(loinputBinary);
        //lsJson = BLBase.SimpleBinaryToString(loinputBinary);
        lsJson = new StreamReader(context.Request.InputStream).ReadToEnd();

        //  lsJson = "{'clave':'1','codigo':'','flgPermisoFoto':'','flgPermisoGps':'','flgPermisoMotivo':'','id':0,'idResultado':0,'login':'1','nombre':'','resultado':''}";
        JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
        BEClienteOnline loBeanUsuario = jsonSerializer.Deserialize<BEClienteOnline>(lsJson);

        BEObtenerClienteOnline loObtener = ControlJava.fnObtenerClienteCobranzasAndroid(loBeanUsuario);

        context.Response.Write(jsonSerializer.Serialize(loObtener));
        //context.Response.Write("{\"clave\":\"1\",\"codigo\":\"001\",\"descripcion\":\"usuario 1\",\"idResultado\":1,\"idUsuario\":1,\"resultado\":\"Bienvenido usuario 1\"}");
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }


}