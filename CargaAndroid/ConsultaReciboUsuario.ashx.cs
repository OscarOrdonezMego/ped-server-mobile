﻿using Java.Bean;
using Java.Control;
using Pedidos_Mobile.AppCode.Java.BeanAndroid;
using System;
using System.IO;
using System.Web;
using System.Web.Script.Serialization;

namespace Pedidos_Mobile.CargaAndroid
{
    public class ConsultaReciboUsuario : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            String lsJson = new StreamReader(context.Request.InputStream).ReadToEnd();
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            BEUsuario loBeanUsuario = jsonSerializer.Deserialize<BEUsuario>(lsJson);
            BEUsuarioReciboOnline loBeanRecibo = ControlJava.fnObtenerReciboAndroid(loBeanUsuario);
            context.Response.Write(jsonSerializer.Serialize(loBeanRecibo));
        }

        public bool IsReusable
        { get { return false; } }
    }
}