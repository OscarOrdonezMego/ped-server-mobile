﻿<%@ WebHandler Language="C#" Class="SincronizarConfiguracion" %>


using System.Web;
using Java.Control;
using Nextel.Utils;

public class SincronizarConfiguracion : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        var loBESincronizar = ControlJava.fnSincronizarConfiguracion();
        var Response = HttpContext.Current.Response;

        var version = BaseLN.recuperarVersioSuite();

        Response.AddHeader("Resultado", loBESincronizar.Respuesta + "|" + version.ToString());
        Response.AddHeader("IdResultado", loBESincronizar.idRespuesta.ToString());
        Response.AddHeader("tamano", loBESincronizar.StringScript.Length.ToString());
        Response.AddHeader("Content-Encoding", "gzip");
        context.Response.Filter = new System.IO.Compression.GZipStream(Response.Filter, System.IO.Compression.CompressionMode.Compress);
        context.Response.Write(loBESincronizar.StringScript);
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}