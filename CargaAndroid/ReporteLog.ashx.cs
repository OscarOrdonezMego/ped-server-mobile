﻿using System.Configuration;
using System.IO;
using System.Web;

namespace Pedidos_Mobile.CargaAndroid
{
    public class ReporteLog : IHttpHandler
    {
        HttpContext _context;

        public void ProcessRequest(HttpContext context)
        {
            _context = context;
            GenerateDownloadLinks();
        }

        public bool IsReusable
        { get { return false; } }

        private void GenerateDownloadLinks()
        {
            _context.Response.ContentType = "text/html";
            _context.Response.Write("<html><body><table>");

            string ruta_log = ConfigurationManager.AppSettings["rutaLog"].Trim();
            string[] a_ruta_log = ruta_log.Split('/');
            string path = HttpContext.Current.Server.MapPath("~/") + "\\" + a_ruta_log[1];

            //string[] filePaths = Directory.GetFiles(Server.MapPath("~/Uploads/"));
            string[] filePaths = Directory.GetFiles(path);

            foreach (string filePath in filePaths)
            {
                _context.Response.Write("<tr><td>");
                _context.Response.Write("<a href='" + VirtualPathUtility.ToAbsolute("~" + ruta_log + Path.GetFileName(filePath)) + "' >" + Path.GetFileName(filePath) + "</a>");
                _context.Response.Write("</td></tr>");
            }
            _context.Response.Write("</table></body></html>");
            _context.Response.End();
        }

        //protected void DownloadFile()
        //{
        //    string filePath = (sender as LinkButton).CommandArgument;
        //    _context.Response.ContentType = ContentType;
        //    _context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(filePath));
        //    _context.Response.WriteFile(filePath);
        //    _context.Response.End();
        //}
    }
}