﻿using Java.Bean;
using Java.Control;
using Nextel.Utils;
using System;
using System.IO;
using System.Web;
using System.Web.Script.Serialization;

namespace Pedidos_Mobile.CargaAndroid
{
    public class GrabarDocumentoHistorial : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            //-----------------------------------------------------------------------------------
            // Recepcion de Datos en Xml
            //-----------------------------------------------------------------------------------
            String lsJson;
            lsJson = new StreamReader(context.Request.InputStream).ReadToEnd();
            BaseLN.registrarLog("GrabarDocumento - IN: " + lsJson);

            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            BeanDocumento loBEDocumento = jsonSerializer.Deserialize<BeanDocumento>(lsJson);
            String msnOut = ControlJava.fnRegistrarDocumentoAndroid(loBEDocumento);
            BaseLN.registrarLog("GrabarDocumento - OUT: " + msnOut);
            context.Response.Write(msnOut);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}