﻿<%@ WebHandler Language="C#" Class="ConsultaEficienciaOnline" %>

using System;
using System.Web;
using Nextel.BL;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Xml;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Utilities;
using Java.Control;
using Java.Bean;

public class ConsultaEficienciaOnline : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        String lsJson;
        byte[] loinputBinary;
        int liBinaryBytes;
        liBinaryBytes = context.Request.TotalBytes;
        loinputBinary = context.Request.BinaryRead(liBinaryBytes);
        lsJson = BLBase.fnConvertirBinaryToString(loinputBinary);
        //lsJson = BLBase.SimpleBinaryToString(loinputBinary);

        JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
        BEListaEficienciaOnline loListaBeanEficiencia = jsonSerializer.Deserialize<BEListaEficienciaOnline>(lsJson);
        String codigoUsuario = loListaBeanEficiencia.usuario;
        loListaBeanEficiencia = ControlJava.fnObtenerEficienciaAndroid(codigoUsuario, loListaBeanEficiencia.perfilUsuario);
        context.Response.Write(jsonSerializer.Serialize(loListaBeanEficiencia));
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}