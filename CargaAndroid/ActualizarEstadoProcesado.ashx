<%@ WebHandler Language="C#" Class="ActualizarEstadoProcesado" %>

using System;
using System.IO;
using System.Web;
using System.Web.Script.Serialization;
using Nextel.Utils;
using Java.Control;
using Java.Bean;
using Nextel.BE;

public class ActualizarEstadoProcesado : IHttpHandler
{
    public void ProcessRequest(HttpContext context)
    {
        //-----------------------------------------------------------------------------------
        // Recepcion de Datos en Xml
        //-----------------------------------------------------------------------------------
        String lsJson;        
        lsJson = new StreamReader(context.Request.InputStream).ReadToEnd();
       
        BaseLN.registrarLog("ActualizarEstadoProcesado - IN: " + lsJson);

        JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
        BEEnvCabPedido pedido = jsonSerializer.Deserialize<BEEnvCabPedido>(lsJson);

        String msnOut = ControlJava.fnActualizarEstadoProcesado(pedido);

        BaseLN.registrarLog("ActualizarEstadoProcesado - OUT: " + msnOut);

        context.Response.Write(msnOut);
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}