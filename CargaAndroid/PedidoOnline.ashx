﻿<%@ WebHandler Language="C#" Class="PedidoOnline" %>

using System;
using System.IO;
using System.Web;
using Nextel.BE;
using System.Web.Script.Serialization;
using Java.Control;

public class PedidoOnline : IHttpHandler
{
    public void ProcessRequest(HttpContext context)
    {
        //-----------------------------------------------------------------------------------
        // Recepcion de Datos en Xml
        //-----------------------------------------------------------------------------------
        String lsJson;
        //byte[] loinputBinary;
        //int liBinaryBytes;
        //liBinaryBytes = context.Request.TotalBytes;
        //loinputBinary = context.Request.BinaryRead(liBinaryBytes);
        //lsJson = BLBase.fnConvertirBinaryToString(loinputBinary);
        //lsJson = BLBase.SimpleBinaryToString(loinputBinary);
        lsJson = new StreamReader(context.Request.InputStream).ReadToEnd();

        //  lsJson = "{'clave':'1','codigo':'','flgPermisoFoto':'','flgPermisoGps':'','flgPermisoMotivo':'','id':0,'idResultado':0,'login':'1','nombre':'','resultado':''}";
        JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();

        BEEnvCabPedido loBEEnvCabPedido = jsonSerializer.Deserialize<BEEnvCabPedido>(lsJson);

        BEListaPedidoOnline loResultado = ControlJava.fnObtenerUltimoPedidoAndroid(loBEEnvCabPedido);
        context.Response.Write(jsonSerializer.Serialize(loResultado));
        //context.Response.Write("{\"clave\":\"1\",\"codigo\":\"001\",\"descripcion\":\"usuario 1\",\"idResultado\":1,\"idUsuario\":1,\"resultado\":\"Bienvenido usuario 1\"}");
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}