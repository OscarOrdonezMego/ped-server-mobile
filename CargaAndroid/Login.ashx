<%@ WebHandler Language="C#" Class="Login" %>

using System;
using System.Web;
using System.Web.Script.Serialization;
using Java.Control;
using Java.Bean;
using System.IO;

public class Login : IHttpHandler
{
    public void ProcessRequest(HttpContext context)
    {
        //-----------------------------------------------------------------------------------
        // Recepcion de Datos en Xml
        //-----------------------------------------------------------------------------------
        String lsJson;
        //byte[] loinputBinary;
        //int liBinaryBytes;
        //liBinaryBytes = context.Request.TotalBytes;
        //loinputBinary = context.Request.BinaryRead(liBinaryBytes);

        //lsJson = BLBase.fnConvertirBinaryToString(loinputBinary);
        //lsJson = BLBase.SimpleBinaryToString(loinputBinary);
        lsJson = new StreamReader(context.Request.InputStream).ReadToEnd();

        //StreamReader loReader = new StreamReader(context.Request.InputStream);
        //String lsEncodedString = loReader.ReadToEnd();
        //lsJson = HttpUtility.UrlDecode(lsEncodedString);     

        JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
        BEUsuario loBeanUsuario = jsonSerializer.Deserialize<BEUsuario>(lsJson);

        loBeanUsuario = ControlJava.fnValidarUsuarioAndroid(loBeanUsuario);
        context.Response.Write(jsonSerializer.Serialize(loBeanUsuario));
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }


}