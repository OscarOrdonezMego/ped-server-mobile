<%@ WebHandler Language="C#" Class="GrabarDireccion" %>

using System;
using System.IO;
using System.Web;
using System.Web.Script.Serialization;
using Java.Control;
using Java.Bean;

public class GrabarDireccion : IHttpHandler
{
    public void ProcessRequest(HttpContext context)
    {
        //-----------------------------------------------------------------------------------
        // Recepcion de Datos en Xml
        //-----------------------------------------------------------------------------------
        String lsJson;
        //byte[] loinputBinary;
        //int liBinaryBytes;
        //liBinaryBytes = context.Request.TotalBytes;
        //loinputBinary = context.Request.BinaryRead(liBinaryBytes);
        //lsJson = BLBase.fnConvertirBinaryToString(loinputBinary);
        //lsJson = BLBase.SimpleBinaryToString(loinputBinary);
        lsJson = new StreamReader(context.Request.InputStream).ReadToEnd();

        //  lsJson = "{"idCliente" : "CLIENTE001","nombre" : "prueba","latitud" : "-12.084634830400923", "longitud" : "-76.98511058685301", "tipo" : "D"}";
        JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();

        BeanClienteDireccion loBeanClienteDireccion = jsonSerializer.Deserialize<BeanClienteDireccion>(lsJson);

        BeanClienteDireccion loResponseBeanClienteDireccion = ControlJava.fnRegistrarDireccionAndroid(loBeanClienteDireccion);

        context.Response.Write(jsonSerializer.Serialize(loResponseBeanClienteDireccion));
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }


}