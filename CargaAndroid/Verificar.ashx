﻿<%@ WebHandler Language="C#" Class="Verificar" %>

using System;
using System.IO;
using System.Web;
using System.Web.Script.Serialization;
using Nextel.BE;
using Java.Control;
using Nextel.Utils;
using System.Text;

public class Verificar : IHttpHandler
{
    public void ProcessRequest(HttpContext context)
    {
        String lsJson;

        lsJson = new StreamReader(context.Request.InputStream).ReadToEnd();
        JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
        BERequestService loBEVerification = jsonSerializer.Deserialize<BERequestService>(lsJson);

        string tel = context.Request.Headers["MSISDN"] ?? "";

        if (loBEVerification.telefono == null || loBEVerification.telefono.Equals(String.Empty))
        {
            loBEVerification.telefono = tel;
        }

        loBEVerification.instalacion = System.Configuration.ConfigurationManager.AppSettings["vs_instalacion"].ToString();
        loBEVerification.servicio = System.Configuration.ConfigurationManager.AppSettings["vs_servicio"].ToString();
        loBEVerification.url = HttpContext.Current.Request.Url.Authority + "/" + HttpContext.Current.Request.ApplicationPath;

        StringBuilder stb = new StringBuilder();
        stb.AppendLine("TELEFONO: " + loBEVerification.telefono);
        stb.AppendLine("INSTALACION: " + loBEVerification.instalacion);
        stb.AppendLine("SERVICIO: " + loBEVerification.servicio);
        stb.AppendLine("URL INSTANCIA: " + loBEVerification.url);

        BaseLN.registrarLog("----- VALIDACIÓN DE SUSCRIPTOR ----" + stb.ToString());

        String resultado = ControlJava.fnVerificar(jsonSerializer.Serialize(loBEVerification));
        resultado += "@@" + tel;
                
        BaseLN.registrarLog("----- FIN VALIDACIÓN DE SUSCRIPTOR - RESPONSE: " + resultado);
        context.Response.Write(resultado);
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}