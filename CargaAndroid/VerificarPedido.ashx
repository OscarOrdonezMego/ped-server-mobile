﻿<%@ WebHandler Language="C#" Class="VerificarPedido" %>

using System;
using System.Web;
using Nextel.BL;
using Nextel.Utils;

public class VerificarPedido : IHttpHandler {

    public void ProcessRequest(HttpContext context)
    {

        try {
            String lsJson;
            System.IO.StreamReader loReader = new System.IO.StreamReader(context.Request.InputStream);
            String lsEncodedString = loReader.ReadToEnd();
            lsJson = HttpUtility.UrlDecode(lsEncodedString);
            BaseLN.registrarLog("VerificarPedido - IN: " + lsJson);
            System.Net.WebClient loWebClient = new System.Net.WebClient();
            loWebClient.Headers.Add("content-type", "application/json; charset=UTF-8");
            String url = System.Configuration.ConfigurationManager.AppSettings["WS_CONSULTA_URL"].ToString() + "pedidos.svc/verificaciones";
            //byte[] bytes =  System.Text.Encoding.Default.GetBytes(lsJson);
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(lsJson);

            String lsResponse = System.Text.Encoding.ASCII.GetString(loWebClient.UploadData(url, "POST", bytes));            
            context.Response.Write(lsResponse);
        }
        catch (Exception e) {
            BaseLN.registrarLog("VerificarPedido - ERROR: " + e.Message);
            
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}