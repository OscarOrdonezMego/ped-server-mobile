﻿<%@ WebHandler Language="C#" Class="ConsultaPagosOnline" %>

using System;
using System.Web;
using Java.Control;
using Nextel.BE;

public class ConsultaPagosOnline : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        String lscodUsuario = context.Request.QueryString["codUsuario"];
        String lsFecha = context.Request.QueryString["fecha"];
        String lscodCliente = context.Request.QueryString["codCliente"];

        BESincronizar loBESincronizar = ControlJava.fnConsultaPagosScript(lscodUsuario, lsFecha, lscodCliente);
        HttpResponse Response = HttpContext.Current.Response;
        Response.AddHeader("Resultado", loBESincronizar.Respuesta);
        Response.AddHeader("IdResultado", loBESincronizar.idRespuesta.ToString());
        Response.AddHeader("tamano", loBESincronizar.StringScript.Length.ToString());
        Response.AddHeader("Content-Encoding", "gzip");
        context.Response.Filter = new System.IO.Compression.GZipStream(Response.Filter, System.IO.Compression.CompressionMode.Compress);
        context.Response.Write(loBESincronizar.StringScript);   
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }
}